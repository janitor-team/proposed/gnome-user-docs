<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-calibrate-printer" xml:lang="id">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrate-scanner"/>
    <link type="seealso" xref="color-calibrate-screen"/>
    <link type="seealso" xref="color-calibrate-camera"/>
    <desc>Mengkalibrasi pencetak Anda penting untuk mencetak warna-warna akurat.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Bagaimana saya mengkalibrasi pencetak saya?</title>

  <p>Ada dua cara untuk membuat profil perangkat pencetak:</p>

  <list>
    <item><p>Memakai perangkat foto spektrometer seperti Pantone ColorMunki</p></item>
    <item><p>Mengunduh berkas acuan pencetakan dari suatu perusahaan warna</p></item>
  </list>

  <p>Menggunakan perusahaan warna untuk menghasilkan profil pencetak biasanya merupakan pilihan termurah jika Anda hanya memiliki satu atau dua jenis kertas berbeda. Dengan mengunduh grafik referensi dari situs web perusahaan, Anda dapat mengirim mereka kembali cetak dalam amplop berlapis dimana mereka akan memindai kertas, menghasilkan profil, dan mengiriim kembali ke Anda profil ICC yang akurat.</p>
  <p>Memakai perangkat mahal seperti ColorMunki akan menjadi lebih murah hanya jika Anda memprofil sejumlah besar set tinta atau tipe kertas.</p>

  <note style="tip">
    <p>Bila Anda mengubah pemasok tinta Anda, pastikan Anda mengkalibrasi ulang pencetak!</p>
  </note>

</page>
