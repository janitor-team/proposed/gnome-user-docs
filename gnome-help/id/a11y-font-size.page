<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-font-size" xml:lang="id">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.26" date="2017-08-04" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Memakai fonta lebih besar untuk membuat teks lebih mudah dibaca.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Mengubah ukuran teks pada layar</title>

  <p>Bila Anda mengalami kesulitan membaca teks pada layar Anda, Anda dapat mengubah ukuran fonta.</p>

  <steps>
    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulai mengetik <gui>Aksesibilitas</gui>.</p>
    </item>
    <item>
      <p>Klik pada <gui>Aksesibilitas</gui> untuk membuka panel.</p>
    </item>
    <item>
      <p>Dalam bagian <gui>Penglihatan</gui>, nyalakan <gui>Teks Besar</gui>.</p>
    </item>
  </steps>

  <p>Sebagai alternatif, Anda dapat mengubah dengan cepat ukuran teks dengan mengklik <link xref="a11y-icon">ikon aksesibilitas</link> pada bilah puncak dan memilih <gui>Teks Besar</gui>.</p>

  <note style="tip">
    <p>Dalam banyak aplikasi, Anda dapat memperbesar ukuran teks kapanpun dengan menekan <keyseq><key>Ctrl</key><key>+</key></keyseq>. Untuk memperkecil ukuran teks, tekan <keyseq><key>Ctrl</key><key>-</key></keyseq>.</p>
  </note>

  <p><gui>Teks Besar</gui> akan memperbesar skala teks 1,2 kali. Anda dapat memakai <app>Alat Oprek</app> untuk membuat ukuran teks lebih besar atau kecil.</p>

</page>
