<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-inklevel" xml:lang="id">

  <info>
    <link type="guide" xref="printing"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Anita Reitere</name>
      <email>nitalynx@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Periksalah banyaknya tinta atau toner yang tersisa dalam cartridge pencetak.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Bagaimana saya bisa memeriksa tingkat toner/tinta pencetak saya?</title>

  <p>Bagaimana Anda memeriksa berapa banyak tinta atau toner yang tersisa di pencetak Anda tergantung pada model dan produsen pencetak Anda, dan driver dan aplikasi yang diinstal pada komputer Anda.</p>

  <p>Beberapa pencetak memiliki layar bawaan untuk menampilkan level tinta dan informasi lainnya.</p>

  <p>Beberapa pencetak melaporkan tingkat toner atau tinta ke komputer, yang dapat ditemukan di panel <gui>Pencetak</gui> di <app>Pengaturan</app>. Level tinta akan ditampilkan dengan rincian pencetak jika tersedia.</p>

  <p>Driver dan alat status untuk sebagian besar pencetak HP disediakan oleh proyek HP Linux Imaging and Printing (HPLIP). Produsen lain mungkin menyediakan driver proprietary dengan fitur serupa.</p>

  <p>Atau, Anda dapat menginstal aplikasi untuk memeriksa atau memantau tingkat tinta. <app>Inkblot</app> menunjukkan status tinta untuk banyak pencetak HP, EPSON, dan Canon. Lihat apakah pencetak Anda berada dalam <link href="http://libinklevel.sourceforge.net/#supported">daftar model yang didukung</link>. Aplikasi tingkat tinta lain untuk Epson dan beberapa pencetak lainnya adalah <app>mtink</app>.</p>

  <p>Beberapa pencetak belum didukung dengan baik di Linux, dan yang lainnya tidak dirancang untuk melaporkan tingkat tinta mereka.</p>

</page>
