<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="bluetooth-remove-connection" xml:lang="id">

  <info>
    <link type="guide" xref="bluetooth"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.12" date="2014-03-04" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Menghapus suatu perangkat dari daftar perangkat Bluetooth.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Putuskan sebuah perangkat Bluetooth</title>

  <p>Bila Anda tak ingin tersambung ke suatu perangkat Bluetooth lagi, Anda dapat menghapus koneksi. Ini berguna bila Anda tak ingin memakai lagi suatu perangkat seperti tetikus atau headset, atau Anda tak ingin lagi mentransfer berkas ke atau dari suatu perangkat.</p>

  <steps>
    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulai mengetik <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Klik pada <gui>Bluetooth</gui> untuk membuka panel.</p>
    </item>
    <item>
      <p>Pilih perangkat yang ingin Anda putus dari daftar.</p>
    </item>
    <item>
      <p>Dalam kotak dialog perangkat, matikan <gui>Koneksi</gui>, atau untuk menghapus perangkat dari daftar <gui>Perangkat</gui>, klik <gui>Hapus Perangkat</gui>.</p>
    </item>
  </steps>

  <p>Anda dapat <link xref="bluetooth-connect-device">menyambung kembali perangkat Bluetooth</link> nanti bila diinginkan.</p>

</page>
