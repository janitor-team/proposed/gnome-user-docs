<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-select" xml:lang="id">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mencetak hanya halaman tertentu, atau hanya suatu jangkauan halaman.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Hanya mencetak halaman tertentu</title>

  <p>Untuk mencetak hanya halaman tertentu dari dokumen:</p>

  <steps>
    <item>
      <p>Buka dialog cetak dengan menekan <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Dalam tab <gui>Umum</gui>, pilih <gui>Halaman</gui> dari seksi <gui>Rentang</gui>.</p>
    </item>
    <item><p>Ketik nomor halaman yang ingin Anda cetak di kotak teks, dipisahkan dengan koma. Gunakan tanda hubung untuk menunjukkan rentang halaman.</p></item>
  </steps>

  <note>
    <p>Misalnya, jika Anda memasukkan "1,3,5-7" di kotak teks <gui>Halaman</gui>, halaman 1, 3, 5, 6, dan 7 akan dicetak.</p>
    <media type="image" src="figures/printing-select.png"/>
  </note>

</page>
