<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="user-goodpassword" xml:lang="id">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Proyek Dokumentasi GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Memakai sandi yang lebih panjang, yang lebih rumit.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Pilih sandi yang aman</title>

  <note style="important">
    <p>Membuat kata sandi Anda cukup mudah bagi Anda untuk mengingat, tapi sangat sulit bagi orang lain (termasuk program komputer) untuk menebak.</p>
  </note>

  <p>Memilih kata sandi yang baik akan membantu untuk menjaga komputer Anda aman. Jika kata sandi Anda mudah ditebak, seseorang mungkin mengetahuinya dan mendapatkan akses ke informasi pribadi Anda.</p>

  <p>Orang bahkan dapat menggunakan komputer untuk secara sistematis mencoba menebak kata sandi Anda, sehingga bahkan salah satu yang akan sulit bagi manusia untuk menebak mungkin sangat mudah untuk program komputer pecahkan. Berikut adalah beberapa tips untuk memilih kata sandi yang baik:</p>
  
  <list>
    <item>
      <p>Gunakan campuran huruf besar dan kecil, angka, simbol, dan spasi di kata sandi. Hal ini membuat lebih sulit ditebak; ada lebih banyak simbol yang dapat dipilih, yang berarti lebih banyak kata sandi yang harus diperiksa oleh seseorng ketika mencoba menebak milik Anda.</p>
      <note>
        <p>Sebuah metode yang baik untuk memilih sandi adalah untuk mengambil huruf pertama dari setiap kata dalam frase yang Anda ingat. Ungkapan bisa menjadi nama sebuah film, buku, lagu atau album. Sebagai contoh, "Flatland: A Romance of Many Dimensions" akan menjadi F:ARoMD atau faromd atau f: aromd.</p>
      </note>
    </item>
    <item>
      <p>Buat kata sandi Anda sepanjang mungkin. Semakin banyak karakter yang dikandungnya, semakin lama waktu yang diperlukan bagi seseorang atau komputer untuk menerka itu.</p>
    </item>
    <item>
      <p>Jangan gunakan kata yang muncul dalam kamus standar dalam bahasa apa pun. Password cracker akan mencoba ini pertama. Password yang paling umum adalah "password" - orang bisa menebak kata sandi seperti ini sangat cepat!</p>
    </item>
    <item>
      <p>Jangan gunakan informasi pribadi seperti tanggal, nomor plat kendaraan, atau nama anggota keluarga.</p>
    </item>
    <item>
      <p>Jangan memakai kata benda apapun.</p>
    </item>
    <item>
      <p>Pilih suatu sandi yang dapat diketikkan dengan cepat, untuk mengurangi kesempatan bagi seseorang menebak apa yang telah Anda ketikkan apabila mereka mengamati Anda.</p>
      <note style="tip">
        <p>Jangan pernah menuliskan sandi Anda dimanapun. Mereka dapat dengan mudah ditemukan!</p>
      </note>
    </item>
    <item>
      <p>Memakai sandi lain untuk akun hal-hal yang berbeda.</p>
    </item>
    <item>
      <p>Memakai sandi lain untuk akun yang berbeda.</p>
      <p>Bila Anda memakai sandi yang sama bagi semua akun Anda, siapapun yang menebaknya akan dapat mengakses semua akun Anda seketika.</p>
      <p>Namun bisa menjadi sulit untuk mengingat banyak kata sandi. Walaupun tak seaman seperti memakai kata sandi yang berbeda untuk setiap hal, mungkin lebih mudah untuk memakai yang sama bagi hal-hal yang tak penting (seperti situs web), dan yang berbeda untuk hal-hal penting (seperti akun bank daring Anda dan surel Anda).</p>
   </item>
   <item>
     <p>Mengubah sandi Anda secara reguler.</p>
   </item>
  </list>

</page>
