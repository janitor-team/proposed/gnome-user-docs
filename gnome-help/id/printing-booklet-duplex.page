<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-booklet-duplex" xml:lang="id">

  <info>
    <link type="guide" xref="printing-booklet"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Cetak booklet yang dilipat (seperti buku atau pamflet) dari PDF menggunakan kertas ukuran A4/Letter yang normal.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Mencetak buklet di pencetak dua sisi</title>

  <p>Anda dapat membuat buklet yang dilipat (seperti buku atau pamflet kecil) dengan mencetak halaman dokumen dengan urutan khusus dan mengubah beberapa opsi pencetakan.</p>

  <p>Petunjuk ini adalah untuk mencetak buklet dari dokumen PDF.</p>

  <p>Jika Anda ingin mencetak buklet dari dokumen <app>LibreOffice</app>, ekspor terlebih dahulu ke PDF dengan memilih <guiseq><gui>Berkas</gui><gui>Ekspor sebagai PDF…</gui></guiseq>. Dokumen Anda harus memiliki jumlah halaman kelipatan 4 (4, 8, 12, 16,…). Anda mungkin perlu menambahkan hingga 3 halaman kosong.</p>

  <p>Bagaimana mencetak booklet:</p>

  <steps>
    <item>
      <p>Buka dialog cetak. Hal ini biasanya dapat dilakukan melalui <gui style="menuitem">Cetak</gui> di menu atau menggunakan pintasan papan tik <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Klik tombol <gui>Properti…</gui></p>
      <p>Dalam daftar tarik-turun <gui>Orientasi</gui>, pastikan bahwa <gui>Mendatar</gui> dipilih.</p>
      <p>Dalam daftar tarik-turun <gui>Dupleks</gui>, pilih <gui>Tepi Pendek</gui>.</p>
      <p>Klik <gui>OK</gui> untuk kembali ke dialog cetak.</p>
    </item>
    <item>
      <p>Di bawah <gui>Rentang dan Salinan</gui>, pilih <gui>Halaman</gui>.</p>
    </item>
    <item>
      <p>Ketikkan jumlah halaman dalam urutan ini (n adalah jumlah halaman, dan kelipatan 4):</p>
      <p>n, 1, 2, n-1, n-2, 3, 4, n-3, n-4, 5, 6, n-5, n-6, 7, 8, n-7, n-8, 9, 10, n-9, n-10, 11, 12, n-11…</p>
      <p>Contoh:</p>
      <list>
        <item><p>booklet 4 halaman: Ketik <input>4,1,2,3</input></p></item>
        <item><p>booklet 8 halaman: Ketik <input>8,1,2,7,6,3,4,5</input></p></item>
        <item><p>booklet 20 halaman: ketik <input>20,1,2,19,18,3,4,17,16,5,6,15,14,7,8,13,12,9,10,11</input></p></item>
      </list>
    </item>
    <item>
      <p>Pilih tab <gui>Tata Letak Halaman</gui>.</p>
      <p>Di bawah <gui>Tata Letak</gui>, pilih <gui>Brosur</gui>.</p>
      <p>Di bagian <gui>Sisi Halaman</gui>, di daftar tarik-turun <gui>Sertakan</gui>, pilih <gui>Semua halaman</gui>.</p>
    </item>
    <item>
      <p>Klik <gui>Cetak</gui>.</p>
    </item>
  </steps>

</page>
