<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-changepassword" xml:lang="id">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>
    <link type="seealso" xref="user-goodpassword"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Proyek Dokumentasi GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jaga keamanan akun dengan sering mengubah sandi Anda di pengaturan akun.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Mengubah kata sandi Anda</title>

  <p>Ini adalah ide yang baik untuk mengubah kata sandi Anda dari waktu ke waktu, terutama jika Anda berpikir orang lain tahu kata sandi Anda.</p>

  <p>Anda memerlukan <link xref="user-admin-explain">hak istimewa administrator</link> untuk menyunting akun pengguna selain milik Anda.</p>

  <steps>
    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulai mengetik <gui>Pengguna</gui>.</p>
    </item>
    <item>
      <p>Klik <gui>Pengguna</gui> untuk membuka panel.</p>
    </item>
    <item>
      <p>Klik label <gui>·····</gui> di samping <gui>Kata Sandi</gui>. Jika Anda mengubah kata sandi untuk pengguna yang berbeda, Anda harus terlebih dahulu <gui>Membuka Kunci</gui> panel.</p>
    </item>
    <item>
      <p>Masukkan kata sandi saat ini Anda, lalu suatu kata sandi baru. Masukkan kata sandi baru Anda lagi dalam ruas <gui>Konfirmasikan Kata Sandi</gui>.</p>
      <p>Anda dapat menekan ikon <gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">buat kata sandi</span></media></gui> untuk membuat secara otomatis suatu kata sandi acak.</p>
    </item>
    <item>
      <p>Klik <gui>Ubah</gui>.</p>
    </item>
  </steps>

  <p>Pastikan bahwa Anda <link xref="user-goodpassword">memilih suatu sandi yang baik</link>. Ini akan membantu menjaga akun pengguna Anda aman.</p>

  <note>
    <p>Ketika Anda memutakhirkan sandi log masuk ANda, sandi ring kunci log masuk Anda akan secara otomatis dimutakhirkan agar sama dengan sandi log masuk baru Anda.</p>
  </note>

  <p>Jika Anda lupa kata sandi Anda, setiap pengguna dengan hak istimewa administrator dapat mengubahnya untuk Anda.</p>

</page>
