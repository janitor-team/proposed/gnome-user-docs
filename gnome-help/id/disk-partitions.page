<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="disk-partitions" xml:lang="id">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>Proyek Dokumentasi GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>

    <desc>Memahami apa itu volume dan partisi dan memakai utilitas disk untuk mengelola mereka.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

 <title>Mengelola volume dan partisi</title>

  <p>Kata <em>volume</em> dipakai untuk menjelaskan suatu perangkat penyimpanan, seperti sebuah hard disk. Itu juga bisa mengacu kepada sebuah <em>bagian</em> dari penyimpanan pada perangkat, karena Anda dapat memecah penyimpanan ke dalam bongkahan-bongkahan. Komputer membuat penyimpanan ini dapat diakses melalui sistem berkas Anda dalam suatu proses yang dikenal sebagai <em>mengait</em>. Volume yang dikat mungkin berubah hard disk, USB, DVD-RW, kartu SD, dan media lain. Bila suatu volume saat ini sedang dikait, Anda dapat membaca (dan mungkin menulis) berkas-berkas padanya.</p>

  <p>Seringkali, suatu volume yang dikait dinamai sebagai <em>partisi</em>, walaupun belum tentu mereka merupakan hal yang sama. Suatu "partisi" mengacu ke suatu wilayah penyimpanan <em>fisik</em> pada suatu disk tunggal. Sekali sebuah partisi telah dikait, itu dapat diacu sebagai suatu volume karena Anda dapat mengakses berkas-berkas padanya. Anda dapat menganggap volume sebagai "etalase" berlabel yang dapat diakses, untuk "ruang belakang" partisi dan drive yang fungsional.</p>

<section id="manage">
 <title>Lihat dan kelola volume dan partisi memakai utilitas disk</title>

  <p>Anda dapat memeriksa dan mengubah volume penyimpanan komputer Anda dengan utilitas disk.</p>

<steps>
  <item>
    <p>Buka ringkasan <gui>Aktivitas</gui> dan mulai <app>Disk</app>.</p>
  </item>
  <item>
    <p>Pada daftar perangkat penyimpanan di sebelah kiri, Anda akan menemukan hardisk, drive CD/DVD, dan perangkat fisik lainnya. Klik pada perangkat yang akan Anda periksa.</p>
  </item>
  <item>
    <p>Panel kanan menyediakan rincian visual dari volume dan partisi yang ada pada perangkat yang dipilih. Ini juga berisi berbagai alat yang digunakan untuk mengelola volume ini.</p>
    <p>Berhati-hatilah: mungkin saja menghapus sepenuhnya data pada disk Anda dengan utilitas ini.</p>
  </item>
</steps>

  <p>Komputer Anda kemungkinan besar memiliki setidaknya satu partisi <em>primer</em> dan satu partisi <em>swap</em>. Partisi swap digunakan oleh sistem operasi untuk manajemen memori, dan jarang dipasang. Partisi utama berisi sistem operasi, aplikasi, pengaturan, dan berkas pribadi. Berkas-berkas ini juga dapat didistribusikan di antara beberapa partisi untuk keamanan atau kenyamanan.</p>

  <p>Salah satu partisi utama harus berisi informasi yang menggunakan komputer Anda untuk memulai, atau <em>boot</em>. Untuk alasan ini kadang-kadang disebut partisi boot, atau volume boot. Untuk menentukan apakah volume bisa dipakai boot, pilih partisi dan klik tombol menu pada bilah alat di bawah daftar partisi. Lalu klik <gui>Sunting Partisi...</gui> dan lihat pada <gui>Bendera</gui> . Media eksternal seperti USB drive dan CD juga mengandung volume yang bisa dipakai boot.</p>

</section>

</page>
