<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-differentsize" xml:lang="id">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <its:rules xmlns:its="http://www.w3.org/2005/11/its" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <desc>Mencetak dokumen pada kertas dengan ukuran atau orientasi yang berbeda.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Mengubah ukuran kertas ketika mencetak</title>

  <p>Bila Anda ingin mengubah ukuran kertas dari dokumen Anda (sebagai contoh, mencetak PDF berukuran US-Letter pada kertas A4), Anda dapat mengubah format pencetakan bagi dokumen.</p>

  <steps>
    <item>
      <p>Buka dialog cetak dengan menekan <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Pilih tab <gui>Penyiapan Halaman</gui>.</p>
    </item>
    <item>
      <p>Di bawah kolom <gui>Kertas</gui>, pilih <gui>Ukuran kertas</gui> Anda dari daftar drop down.</p>
    </item>
    <item>
      <p>Klik <gui>Cetak</gui> untuk mencetak dokumen.</p>
    </item>
  </steps>

  <p>Anda juga dapat memakai menu <gui>Orientasi</gui> untuk memilih orientasi lain:</p>

  <list>
    <item><p><gui>Potret</gui></p></item>
    <item><p><gui>Landskap</gui></p></item>
    <item><p><gui>Potret terbalik</gui></p></item>
    <item><p><gui>Lansekap terbalik</gui></p></item>
  </list>

</page>
