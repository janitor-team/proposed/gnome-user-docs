<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-assignprofiles" xml:lang="id">

  <info>
    <link type="guide" xref="color"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-why-calibrate"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-04" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Cari di <guiseq><gui>Pengaturan</gui><gui>Warna</gui></guiseq> untuk menambah suatu profil warna bagi layar Anda.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Bagaimana saya mengarahkan profil ke perangkat?</title>

  <p>Anda mungkin ingin menentukan suatu profil warna bagi layar atau pencetak Anda sehingga warna yang ditampilkannya lebih akurat.</p>

  <steps>
    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulai mengetik <gui>Pengaturan</gui>.</p>
    </item>
    <item>
      <p>Klik <gui>Pengaturan</gui>.</p>
    </item>
    <item>
      <p>Klik <gui>Warna</gui> dalam bilah sisi untuk membuka panel.</p>
    </item>
    <item>
      <p>Pilih perangkat yang ingin Anda tambahkan profil baginya.</p>
    </item>
    <item>
      <p>Klik <gui>Tambah profil</gui> untuk memilih profil yang ada atau mengimpor suatu profil baru.</p>
    </item>
    <item>
      <p>Tekan <gui>Tambah</gui> untuk mengkonfirmasi pilihan Anda.</p>
    </item>
    <item>
      <p>Untuk mengubah profil yang digunakan, pilih profil yang ingin Anda gunakan dan tekan <gui>Fungsikan</gui> untuk mengonfirmasi pilihan Anda.</p>
    </item>
  </steps>

  <p>Setiap perangkat dapat memiliki beberapa profil yang ditugaskan untuk itu, tetapi hanya satu profil dapat menjadi profil <em>baku</em>. Profil baku digunakan ketika tidak ada informasi tambahan untuk memungkinkan pemilihan profil secara otomatis. Contoh dari seleksi otomatis ini adalah jika satu profil diciptakan untuk kertas mengkilat dan satu lagi untuk kertas polos.</p>

  <p>Bila perangkat keras kalibrasi tersambung, tombol <gui>Kalibrasikan…</gui> akan membuat profil baru.</p>

</page>
