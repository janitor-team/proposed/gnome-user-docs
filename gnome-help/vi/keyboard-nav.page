<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="keyboard-nav" xml:lang="vi">
  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision pkgversion="3.7.5" version="0.2" date="2013-02-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.20" date="2016-08-13" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
       <name>Julita Inca</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
       <name>Ekaterina Gerasimova</name>
       <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Dùng máy tính không cần chuột.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nguyễn Thái Ngọc Duy</mal:name>
      <mal:email>pclouds@gmail.com</mal:email>
      <mal:years>2011-2012.</mal:years>
    </mal:credit>
  </info>

  <title>Di chuyển dùng bàn phím</title>

  <p>Trang này mô tả chi tiết cách di chuyển bằng bàn phím cho người dùng không thể dùng chuột hoặc thiết bị tương tự, hoặc thích dùng bàn phím. Xem <link xref="shell-keyboard-shortcuts"/> để biết các phím tắt hữu dụng cho mọi người dùng.</p>

  <note style="tip">
    <p>Nếu bạn không thể dùng chuột hoặc thiết bị tương tự, bạn có thể điều khiển chuột dùng bàn phím số. Xem <link xref="mouse-mousekeys"/> để biết thêm.</p>
  </note>

<table frame="top bottom" rules="rows">
  <title>Di chuyển giữa giao tiếp người dùng</title>
  <tr>
    <td><p><key>Tab</key> and</p>
    <p><keyseq><key>Ctrl</key><key>Tab</key></keyseq></p>
    </td>
    <td>
      <p>Di chuyển tiêu điểm bàn phím giữa các ô điều khiển. <keyseq><key>Ctrl</key><key>Tab</key></keyseq> di chuyển giữa các nhóm ô điều khiển, như thanh bên và phần nội dung chính. <keyseq><key>Ctrl</key><key>Tab</key></keyseq> cũng có thể thoát khỏi ô điều khiển sử dụng <key>Tab</key> một cách đặc biệt, ví dụ ô nhập văn bản.</p>
      <p>Nhấn giữ <key>Shift</key> và di chuyển tiêu điểm theo thứ tự ngược.</p>
    </td>
  </tr>
  <tr>
    <td><p>Phím mũi tên</p></td>
    <td>
      <p>Di chuyển vùng chọn giữa các mục trong một ô điều khiện, hoặc giữa nhóm điều khiển có liên hệ. Dùng phím mũi tên để đặt tiêu điểm vào nút trên thanh công cụ, chọn mục trong kiểu trình bày danh sách hoặc biểu tượng, hoặc chọn nút chọn trong một nhóm.</p>
    </td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key>Arrow keys</keyseq></p></td>
    <td><p>In a list or icon view, move the keyboard focus to another item
    without changing which item is selected.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key>Phím mũi tên</keyseq></p></td>
    <td><p>In a list or icon view, select all items from the currently selected
    item to the newly focused item.</p>
    <p>In a tree view, items that have children can be expanded or collapsed,
    to show or hide their children: expand by pressing
    <keyseq><key>Shift</key><key>→</key></keyseq>, and collapse by
    pressing <keyseq><key>Shift</key><key>←</key></keyseq>.</p></td>
  </tr>
  <tr>
    <td><p><key>Khoảng trắng</key></p></td>
    <td><p>Activate a focused item such as a button, check box, or list
    item.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Khoảng trắng</key></keyseq></p></td>
    <td><p>In a list or icon view, select or deselect the focused item without
    deselecting other items.</p></td>
  </tr>
  <tr>
    <td><p><key>Alt</key></p></td>
    <td><p>Hold down the <key>Alt</key> key to reveal <em>accelerators</em>:
    underlined letters on menu items, buttons, and other controls. Press
    <key>Alt</key> plus the underlined letter to activate a control, just
    as if you had clicked on it.</p></td>
  </tr>
  <tr>
    <td><p><key>Esc</key></p></td>
    <td><p>Thoát khỏi trình đơn, cửa sổ hỏi đáp, bộ chuyển, hoặc hộp thoại.</p></td>
  </tr>
  <tr>
    <td><p><key>F10</key></p></td>
    <td><p>Open the first menu on the menubar of a window. Use the arrow keys
    to navigate the menus.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key xref="keyboard-key-super">Super</key>
    <key>F10</key></keyseq></p></td>
    <td><p>Open the application menu on the top bar.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key><key>F10</key></keyseq> or</p>
    <p><key xref="keyboard-key-menu">Menu</key></p></td>
    <td>
      <p>Pop up the context menu for the current selection, as if you had
      right-clicked.</p>
    </td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>F10</key></keyseq></p></td>
    <td><p>In the file manager, pop up the context menu for the current folder,
    as if you had right-clicked on the background and not on any item.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>PageUp</key></keyseq></p>
    <p>and</p>
    <p><keyseq><key>Ctrl</key><key>PageDown</key></keyseq></p></td>
    <td><p>In a tabbed interface, switch to the tab to the left or
    right.</p></td>
  </tr>
</table>

<table frame="top bottom" rules="rows">
  <title>Di chuyển</title>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="alt-f1"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-tab"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-tick"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="ctrl-alt-tab"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-updown"/>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F6</key></keyseq></p></td>
    <td><p>Cycle through windows in the same application. Hold down the
    <key>Alt</key> key and press <key>F6</key> until the window you want is
    highlighted, then release <key>Alt</key>. This is similar to the
    <keyseq><key>Alt</key><key>`</key></keyseq> feature.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
    <td><p>Đi vòng qua mọi cửa sổ trong vùng làm việc.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
    <td><p><link xref="shell-notifications#notificationlist">Open the
    notification list.</link> Press <key>Esc</key> to close.</p></td>
  </tr>
</table>

<table frame="top bottom" rules="rows">
  <title>Chọn cửa sổ</title>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
    <td><p>Đóng cửa sổ hiện tại.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F5</key></keyseq> or
    <keyseq><key>Super</key><key>↓</key></keyseq></p></td>
    <td><p>Restore a maximized window to its original size. Use
    <keyseq><key>Alt</key><key>F10</key></keyseq> to maximize.
    <keyseq><key>Alt</key><key>F10</key></keyseq> both maximizes and
    restores.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
    <td><p>Move the current window. Press
    <keyseq><key>Alt</key><key>F7</key></keyseq>, then use the arrow keys to
    move the window. Press <key>Enter</key> to finish moving the window, or
    <key>Esc</key> to return it to its original place.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
    <td><p>Resize the current window. Press
    <keyseq><key>Alt</key><key>F8</key></keyseq>, then use the arrow keys to
    resize the window. Press <key>Enter</key> to finish resizing the window, or
    <key>Esc</key> to return it to its original size.</p></td>
  </tr>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-updown"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-left"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-right"/>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F10</key></keyseq> or
    <keyseq><key>Super</key><key>↑</key></keyseq></p>
    </td>
    <td><p><link xref="shell-windows-maximize">Maximize</link> a window. Press
    <keyseq><key>Alt</key><key>F10</key></keyseq> or
    <keyseq><key>Super</key><key>↓</key></keyseq> to
    restore a maximized window to its original size.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>H</key></keyseq></p></td>
    <td><p>Minimize a window.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>←</key></keyseq></p></td>
    <td><p>Maximize a window vertically along the left side of the screen.
    Press again to restore the window to its previous size. Press
    <keyseq><key>Super</key><key>→</key></keyseq> to switch
    sides.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>→</key></keyseq></p></td>
    <td><p>Maximize a window vertically along the right side of the screen.
    Press again to restore the window to its previous size. Press
    <keyseq><key>Super</key><key>←</key></keyseq> to
    switch sides.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Khoảng trắng</key></keyseq></p></td>
    <td><p>Pop up the window menu, as if you had right-clicked on the
    titlebar.</p></td>
  </tr>
</table>

</page>
