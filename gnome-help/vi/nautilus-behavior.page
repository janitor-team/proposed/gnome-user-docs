<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-behavior" xml:lang="vi">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-behavior"/>

    <desc>Nhấn đơn để mở cửa sổ, chạy hoặc xem tập tin có thể thực thi, và xác định hành vi liên quan đến sọt rác.</desc>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Sindhu S</name>
      <email>sindhus@live.in</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nguyễn Thái Ngọc Duy</mal:name>
      <mal:email>pclouds@gmail.com</mal:email>
      <mal:years>2011-2012.</mal:years>
    </mal:credit>
  </info>

<title>Tuỳ thích hành vi trình quản lý tập tin</title>
<p>You can control whether you single-click or double-click files, how
executable text files are handled, and the trash behavior. Click the menu
button in the top-right corner of the window, select <gui>Preferences</gui>,
then go to the <gui>General</gui> section.</p>

<section id="behavior">
<title>Hành vi</title>
<terms>
 <item>
  <title><gui>Action to Open Items</gui></title>
  <p>Mặc định nhấn tập tin để chọn và nhấp đúp để mở. Bạn có thể chọn nhấp đơn để mở tức thì. Khi bạn dùng chế độ nhấp đơn, bạn có thể nhấn giữ phím <key>Ctrl</key> khi nhấn để chọn một hoặc nhiều tập tin.</p>
 </item>
</terms>

</section>
<section id="executable">
<title>Tập tin văn bản có quyền thực thi</title>
 <p>An executable text file is a file that contains a program that you can run
 (execute). The <link xref="nautilus-file-properties-permissions">file
 permissions</link> must also allow for the file to run as a program. The most
 common are <sys>Shell</sys>, <sys>Python</sys> and <sys>Perl</sys> scripts.
 These have extensions <file>.sh</file>, <file>.py</file> and <file>.pl</file>,
 respectively.</p>

 <p>Executable text files are also called <em>scripts</em>. All scripts in the
 <file>~/.local/share/nautilus/scripts</file> folder will appear in the context
 menu for a file under the <gui style="menuitem">Scripts</gui> submenu. When a
 script is executed from a local folder, all selected files will be pasted to
 the script as parameters. To execute a script on a file:</p>

<steps>
  <item>
    <p>Navigate to the desired folder.</p>
  </item>
  <item>
    <p>Select the desired file.</p>
  </item>
  <item>
    <p>Right click on the file to open the context menu and select the desired
    script to execute from the <gui style="menuitem">Scripts</gui> menu.</p>
  </item>
</steps>

 <note style="important">
  <p>A script will not be passed any parameters when executed from a remote
  folder such as a folder showing web or <sys>ftp</sys> content.</p>
 </note>

</section>

</page>
