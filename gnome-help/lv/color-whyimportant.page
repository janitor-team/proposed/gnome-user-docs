<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="color-whyimportant" xml:lang="lv">

  <info>
    <link type="guide" xref="color"/>
    <desc>Krāsu pārvaldība ir svarīga dizaineriem, fotogrāfiem un māksliniekiem.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Kāpēc krāsu pārvaldība ir svarīga?</title>
  <p>Krāsu pārvaldība ir process — krāsu tveršana, izmantojot ievades ierīci, tās rādīšana uz ekrāna un tā visa izdrukāšana, pārvaldot precīzas krāsas un to apgabalus katrai videi.</p>

  <p>Vajadzību pēc krāsu pārvaldības var visuzskatāmāk parādīt ar putna fotogrāfiju aukstā ziemas dienā.</p>

  <figure>
    <desc>Putns uz apsarmojušas sienas, kā to redz fotoaparāta skatu meklētājā</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/color-camera.png"/>
  </figure>

  <p>Rāda tipiski pārsātinātu zilo kanālu, kas bildes padara aukstas.</p>

  <figure>
    <desc>Tas ir tas, ko lietotājs redz uz parasta darba klēpjdatora ekrāna</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/color-display.png"/>
  </figure>

  <p>Ievērojiet, ka baltais nav “papīra balts” un melnā acs zīlīte ir duļķaini brūna.</p>

  <figure>
    <desc>Šo lietotājs redz, drukājot ar pastu tintes printeri</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/color-printer.png"/>
  </figure>

  <p>Šeit problēmas pamatā ir tas, ka katra ierīce var apstrādāt dažādus krāsu apgabalus. Tāpēc, lai arī jūs varat uzņemt fotogrāfiju ar elektriski zilu krāsu, vairums printeru to nespēs reproducēt.</p>
  <p>Vairums attēlu tiek uzņemti RGB (Red, Green, Blue) telpā un tos ir jāpārveido uz CMYK (Cyan, Magenta, Yellow, un Black), lai izdrukātu. Vēl viena problēma ir tā, ka nav <em>baltas</em> tintes, tāpēc baltais būs tikai tik balts, cik balts ir papīrs.</p>

  <p>Vēl viena problēma ir mērvienības. Nenorādot, kādā mērogā tiek mērītas krāsas, mēs nezinām, vai 100% sarkans ir tuvu infrasarkanajam vai sarkanākais, ko var radīt printeris. Kas ir 50% sarkans uz viena ekrāna būs 62% sarkans uz cita. Tas ir līdzīgi, kā pateikt, ka attālums ir 7 vienības — bez mērvienībām jūs nezināsiet, vai tie ir 7 kilometri vai 7 metri.</p>

  <p>Krāsu pasaulē, mēs norādām mērvienības kā gamutu. Gamuts ir krāsu apgabals, ko var reproducēt. Ierīcei, piemēram, digitālai spoguļkamerai,  var būt ļoti liels gamuts, kas var tvert visas saulrieta krāsas, bet projektoram ir ļoti mazs gamuts, un visas krāsas izskatīsies "izbalējušas".</p>

  <p>Dažos gadījumos mēs varam <em>izlabot</em> ierīces izvadi, mainot datus, ko mēs uz to sūtam, bet citos gadījumos, kad tas nav iespējams (nevar izdrukāt elektriski zilo), lietotājam jāparāda, kādas izskatīsies rezultāta krāsas.</p>

  <p>Fotogrāfiem ir jēga izmantot pilnu tonālo apgabalu krāsu ierīcei, lai varētu veikt gludas krāsu pārejas. Cita veida grafikai jūs varētu vēlēties, lai krāsas sakristu precīzi, kas ir svarīgi, ja mēģināt apdrukāt krūzīti ar Red Hat logo, kurai <em>jābūt </em> tieši Red Hat sarkanā krāsā.</p>

</page>
