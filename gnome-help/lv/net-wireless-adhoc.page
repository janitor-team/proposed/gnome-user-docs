<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-adhoc" xml:lang="lv">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.32" date="2019-09-01" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <desc>Lietojiet ekspromta tīklu, lai ļautu citām ierīcēm savienoties ar jūsu datoru un tā tīkla savienojumiem.</desc>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Izveidot bezvadu tīklāju</title>

  <p>Jūs varat izmantot savu datoru kā bezvadu tīklāju. Tas ļauj citām ierīcēm ar jums savienoties bez atsevišķa tīkla, un ļauj koplietot interneta savienojumu, kuru jūs izveidojāt ar citu saskarni, kā piemēram, vadu tīklu vai mobilo tīklu.</p>

<steps>
  <item>
    <p>Open the <gui xref="shell-introduction#systemmenu">system menu</gui> from the right
    side of the top bar.</p>
  </item>
  <item>
    <p>Select
    <gui>Wi-Fi Not Connected</gui> or the name of the wireless network to which
    you are already connected. The Wi-Fi section of the menu will expand.</p>
  </item>
  <item>
    <p>Spiediet <gui>Wi-Fi iestatījumi</gui>.</p></item>
  <item><p>Press the menu button in the top-right corner of the window and
  select <gui>Turn On Wi-Fi Hotspot…</gui>.</p></item>
  <item><p>Ja dators jau ir savienots ar bezvadu tīklu, jums jautās vai vēlaties atvienoties no pašreizējā tīkla. Viens bezvadu tīkla adapters var tikai savienoties vai veidot vienu tīklu vienlaicīgi. Spiediet <gui>Ieslēgt</gui>, lai apstiprinātu.</p></item>
</steps>

<p>Tīkla nosaukums (SSID) un drošības atslēga tiks automātiski radīta. Tīkla nosaukums tiks balstīts uz jūsu datora nosaukuma. Citām ierīcēm būs vajadzīga šī informācija, lai pieslēgtos izveidotajam tīklājam.</p>

<note style="tip">
  <p>You can also connect to the wireless network by scanning the QR Code on
  your phone or tablet using the built-in camera app or a QR code scanner.</p>
</note>

</page>
