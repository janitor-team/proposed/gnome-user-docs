<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="mouse-problem-notmoving" xml:lang="lv">

  <info>
    <link type="guide" xref="mouse#problems"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <!-- TODO: reorganise page and tidy because it's one ugly wall of text -->
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
        <name>Phil Bull</name>
        <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Kā pārbaudīt to, kāpēc jūsu pele nestrādā.</desc>
  </info>

<title>Peles rādītājs nekustās</title>

<links type="section"/>

<section id="plugged-in">
 <title>Pārbaudiet vai pele ir pievienota</title>
 <p>Ja ir pele ar vadu, pārbaudiet vai tas ir kārtīgi pievienots.</p>
 <p>Ja tā ir USB pele (ar taisnstūrveida savienojumu), pamēģiniet to iespraust citā USB portā. Ja tā ir PS/2 pele (ar mazu, apaļu savienojumu ar 6 adatiņām), pārliecinieties, ka tas ir iesprausts zaļajā peles portā, nevis violetajā tastatūras portā. Ja vads nebija iesprausts, iespējams, ka būs jāpārstartē dators.</p>
</section>

<section id="broken">
 <title>Pārbaudiet vai pele tiešām strādā</title>
 <p>Pievieno peli citam datoram un pārbaudi vai tā strādā.</p>

 <p>Ja jums ir optiskā vai lāzerpele, peles apakšā būtu jāspīd gaismai, ja tā ir ieslēgta. Ja gaisma nespīd, pārbaudi vai pele ir ieslēgta. Ja vēl jo projām nav gaismas, pele var būt salauzta.</p>
</section>

<section id="wireless-mice">
 <title>Bezvadu peles pārbaudīšana</title>

  <list>
    <item>
      <p>Pārliecinies, ka pele ir ieslēgta. Peles apakšā parasti ir slēdzis, lai pilnībā izslēgtu peli, lai to varētu ņem sev līdzi, nepamodinot datoru no snaudiena.</p>
    </item>
   <item><p>Ja izmanto Bluetooth, pārliecinieties, ka pele tiešām ir sapārota ar datoru. Lasi <link xref="bluetooth-connect-device"/>.</p></item>
  <item>
   <p>Nospied pogu un skaties vai peles rādītājs kustās tagad. Dažas bezvadu peles iet gulēšanas režīmā, lai taupītu enerģiju, tāpēc tās var nereaģēt kamēr nenospiedīsiet tās pogu. Lasi <link xref="mouse-wakeup"/>.</p>
  </item>
  <item>
   <p>Pārbaudi vai peles baterija ir uzlādēta.</p>
  </item>
  <item>
   <p>Pārliecinies, ka uztvērējs ir kārtīgi pievienots datoram.</p>
  </item>
  <item>
   <p>Ja pele un uztvērējs var darboties dažādos kanālos, pārliecinieties, ka tie abi atrodas vienā kanālā.</p>
  </item>
  <item>
   <p>Iespējams, ka vajadzēs nospiest pogu uz peles, uztvērēja vai uz abiem, lai izveidotu savienojumu. Peles instrukciju rokasgrāmatā vajadzētu būt vairāk informācijai par šo situāciju.</p>
  </item>
 </list>

 <p>Vairumam RF (radio) bezvadu peļu vajadzētu automātiski strādāt, kolīdz to pievieno datoram. Ja jums ir Bluetooth vai IR (infrasarkanā) bezvadu pele, vajadzētu veikt papildus soļus, lai tā sāktu strādāt. Šie soļi var būt atkarīgi no peles ražotāja vai modeļa. </p>
</section>

</page>
