<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" version="1.0 if/1.0" id="gnome-classic" xml:lang="lv">

  <info>
    <link type="guide" xref="shell-overview"/>

    <revision pkgversion="3.10.1" date="2013-10-28" status="review"/>
    <revision pkgversion="3.29" date="2018-08-28" status="review"/>
    <revision version="gnome:40" date="2021-06-16" status="review"/>

    <credit type="author">
      <name>GNOME dokumentācijas projekts</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ja vēlaties tradicionālāku darbvirsmas vidi, apsveriet pārslēgšanos uz klasisko GNOME.</desc>
  </info>

<title>Kas ir klasiskais GNOME?</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
  <p><em>GNOME Classic</em> is a feature for users who prefer a more traditional
  desktop experience. While <em>GNOME Classic</em> is based on modern GNOME
  technologies, it provides a number of changes to the user interface, such as
  the <gui>Applications</gui> and
  <gui>Places</gui> menus on the top bar, and a window
  list at the bottom of the screen.</p>
  </if:when>
  <if:when test="platform:gnome-classic">
<p><em>GNOME Classic</em> is a feature for users who prefer a more traditional
  desktop experience. While <em>GNOME Classic</em> is based on modern GNOME
  technologies, it provides a number of changes to the user interface, such as
  the <gui xref="shell-introduction#activities">Applications</gui> and
  <gui>Places</gui> menus on the top bar, and a window
  list at the bottom of the screen.</p>
  </if:when>
</if:choose>

<p>You can use the <gui>Applications</gui> menu on the top bar to launch
 applications.</p>

<section id="gnome-classic-window-list">
<title>Logu saraksts</title>

<p>Ekrāna apakšā esošais logu saraksts dod pieeju visiem atvērtajiem logiem un lietotnēm, kā arī ļauj tos minimizēt un atjaunot.</p>

<p>The <gui xref="shell-introduction#activities">Activities</gui> overview is available
 by clicking the button at the left-hand side of the window list at the bottom.</p>

<p>Lai piekļūtu <em><gui>Aktivitāšu</gui> pārskatam</em>, varat arī spiest taustiņu <key xref="keyboard-key-super">Super</key>.</p>
 
 <p>Logu saraksta labajā pusē GNOME parāda pašreizējās darbvietas identifikatoru, piemēram, <gui>1</gui> pirmajai (augšējai) darbvietai. Identifikators arī attēlo kopējo pieejamo darbvietu skaitu. Lai pārslēgtos uz citu darbvietu, varat spiest uz identifikatora un izvēlēties vēlamo darbvietu no izvēlnes.</p>

</section>

<section id="gnome-classic-switch">
<title>Pārslēgties uz un no klasiskā GNOME</title>

<note if:test="!platform:gnome-classic" style="important">
<p>Klasiskais GNOME ir pieejams tikai uz sistēmām, kurās ir uzinstalēti noteikti GNOME čaulas paplašinājumi. Dažās Linux distribūcijās šie paplašinājumi varētu nebūt pieejami vai pēc noklusējuma uzinstalēti.</p>
</note>

  <steps>
    <title>Lai pārslēgtos no <em>GNOME</em> uz <em>klasisko GNOME</em>:</title>
    <item>
      <p>Save any open work, and then log out. Click the system menu on the right
      side of the top bar and then choose the right option.</p>
    </item>
    <item>
      <p>Sistēma prasīs jums apstiprinājumu. Spiediet <gui>Izrakstīties</gui>, lai apstiprinātu savu izvēli.</p>
    </item>
    <item>
      <p>Ierakstīšanās ekrānā izvēlieties no saraksta savu lietotāja vārdu.</p>
    </item>
    <item>
      <p>Click the <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">settings</span></media>
      button in the bottom right corner.</p>
    </item>
    <item>
      <p>Select <gui>GNOME Classic</gui> from the list.</p>
    </item>
    <item>
      <p>Ievadiet savu paroli paroles ievades kastē.</p>
    </item>
    <item>
      <p>Press <key>Enter</key>.</p>
    </item>
  </steps>

  <steps>
    <title>Pārslēgties no <em>klasiskā GNOME</em> uz <em>GNOME</em>:</title>
    <item>
      <p>Saglabājiet atvērtos darbus un izrakstieties. Spiediet sistēmas lietotāju augšējās joslas labajā pusē, spiediet uz sava vārda un izvēlieties atbilstošo opciju.</p>
    </item>
    <item>
      <p>Sistēma prasīs jums apstiprinājumu. Spiediet <gui>Izrakstīties</gui>, lai apstiprinātu savu izvēli.</p>
    </item>
    <item>
      <p>Ierakstīšanās ekrānā izvēlieties no saraksta savu lietotāja vārdu.</p>
    </item>
    <item>
      <p>Click the options icon in the bottom right corner.</p>
    </item>
    <item>
      <p>Select <gui>GNOME</gui> from the list.</p>
    </item>
    <item>
      <p>Ievadiet savu paroli paroles ievades kastē.</p>
    </item>
    <item>
      <p>Press <key>Enter</key>.</p>
    </item>
  </steps>
  
</section>

</page>
