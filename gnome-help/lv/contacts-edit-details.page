<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="contacts-edit-details" xml:lang="lv">

  <info>
    <link type="guide" xref="contacts"/>

    <revision pkgversion="3.5.5" date="2012-08-13" status="review"/>
    <revision pkgversion="3.8" date="2013-04-27" status="review"/>
    <revision pkgversion="3.12" date="2014-02-26" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.36.2" date="2020-08-11" status="review"/>
    <revision pkgversion="3.38.0" date="2020-11-02" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <credit type="editor">
      <name>Pranali Deshmukh</name>
      <email>pranali21293@gmail.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Rediģēt informāciju katram kontaktam.</desc>
  </info>

<title>Rediģēt kontaktinformāciju</title>

  <p>Kontaktu datu rediģēšana palīdz uzturēt pilnvērtīgu informāciju jūsu adrešu grāmatā.</p>

  <steps>
    <item>
      <p>No kontaktu saraksta izvēlieties kontaktu.</p>
    </item>
    <item>
      <p>Press the <media its:translate="no" type="image" src="figures/view-more-symbolic.svg">
      <span its:translate="yes">view more</span></media> 
      button in the top-right corner of the window and
      select <gui style="menuitem">Edit</gui>.</p>
    </item>
    <item>
      <p>Rediģēt kontaktinformāciju.</p>
      <p>To add a <em>detail</em> such as a new phone number or email
      address, just fill in the details on the next empty field of the
      type (phone number, email, etc.) you want to add.</p>
      <note style="tip">
        <p>Press the <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">view more</span></media>
        option at the bottom to expand available options,
        revealing fields like <gui>Website</gui> and <gui>Birthday</gui>.</p>
      </note>
    </item>
    <item>
      <p>Spiediet <gui style="button">Pabeigt</gui>, lai beigturediģēt kontaktu.</p>
    </item>
  </steps>

</page>
