<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-usespeakers" xml:lang="lv">

  <info>
    <link type="guide" xref="media#sound"/>
    <link type="seealso" xref="sound-usemic"/>

    <revision version="gnome:40" date="2021-02-26" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Pievienojiet skaļruņus vai austiņas un izvēlieties noklusējuma audio izvades ierīci.</desc>
  </info>

  <title>Izvēlieties atšķirīgus skaļruņus vai austiņas</title>

  <p>Varat izmantot ārējos atskaņotājus vai austiņas savam datoram. Atskaņotājus parasti savieno, izmantojot apļveida TRS (no angļu valodas "tip, ring, sleeve" —<em>uzgali, gredzenu, uzmavi</em>) kontaktu vai arī USB.</p>

  <p>Ja skaļruņiem vai austiņām ir TRS kontakts, pievienojiet to piemērotajam kontaktam datorā. Lielākajai daļai datoru ir divi kontakti — viens mikrofoniem un viens skaļruņiem. Šī ligzda parasti ir zaļa vai ir ar austiņu attēlu. Skaļruņi vai austiņas, iesprausti TRS kontaktā, parasti tiek izmantoti pēc noklusējuma. Ja tā nav, zemāk lasi instrukcijas, kā iestatīt noklusējuma ierīci.</p>

  <p>Daži datori atbalsta vairāku kanālu izvadi telpiskai skaņai. Tam parasti vajag vairākas TRS kontaktdakšas, kurām parasti ir krāsu kodi. Ja neesat drošs, kuras kontaktdakšas jāpievieno kurai ligzdai, varat pārbaudīt skaņas izvadi skaņas iestatījumos.</p>

  <p>Ja jums ir USB skaļruņi vai austiņas, vai USB skaņu kartē iespraustas analogās austiņas, iespraudiet tās jebkurā USB portā. USB skaļruņi darbojas kā atsevišķas audio ierīces un, iespējams, vajadzēs norādīt, kurus skaļruņus izmantot pēc noklusējuma.</p>

  <steps>
    <title>Select a default audio output device</title>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskatu un sāciet rakstīt <gui>Skaņa</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Skaņa</gui>, lai atvērtu paneli.</p>
    </item>
    <item>
      <p>In the <gui>Output</gui> section, select the device that you want to
      use.</p>
    </item>
  </steps>

  <p>Use the <gui style="button">Test</gui> button to check that all
  speakers are working and are connected to the correct socket.</p>

</page>
