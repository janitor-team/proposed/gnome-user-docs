<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-dwellclick" xml:lang="nl">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-21" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Met de functie <gui>ZweefKlik</gui> (Dwell Click) kunt u klikken door de muis stil te houden.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Klikken simuleren door erboven te zweven</title>

  <p>U kunt klikken of slepen door simpelweg met de muisaanwijzer boven een knop of object op het scherm te zweven. Dit is nuttig wanneer het voor u moeilijk is de muis te bewegen en gelijktijdig te klikken. Deze functie wordt <gui>ZweefKlik</gui> of ‘Hangklik’ genoemd.</p>

  <p>Wanneer <gui>ZweefKlik</gui> is ingeschakeld kunt u de muisaanwijzer boven een knop plaatsen, de muis loslaten en even wachten totdat er voor u op de knop geklikt wordt.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Accessibility</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Accessibility</gui> to open the panel.</p>
    </item>
    <item>
      <p>Druk op <gui>Typ-assistent</gui> onder het kopje <gui>Aanwijzen &amp; klikken</gui>.</p>
    </item>
    <item>
      <p>Switch <gui>Hover Click</gui> to on.</p>
    </item>
  </steps>

  <p>Het venster <gui>Zweefklik</gui> zal openen en zal zich altijd op de voorgrond bevinden, boven al uw andere vensters. U kunt dit venster gebruiken om te kiezen wat voor soort klik gemaakt moet worden wanneer de muis boven een knop zweeft. Bijvoorbeeld, als u <gui>Secondaire klik</gui> inschakelt, zal de muis automatisch rechtsklikken wanneer u boven een knop zweeft. Na het dubbelklikken, rechtsklikken of slepen, schakelt de muis automatisch terug naar normaal klikken.</p>

  <p>Wanneer u de muisaanwijzer boven een knop laat zweven, en niet beweegt, zal de knop geleidelijk aan van kleur veranderen. Wanneer de knop volledig van kleur veranderd is, zal deze aangeklikt worden.</p>

  <p>Gebruik de <gui>Vertraging</gui>-schuifbalk om in te stellen hoe lang u de muis stil moet houden voordat er een muisklik wordt uitgevoerd.</p>

  <p>U hoeft de muis niet helemaal stil te houden wanneer u de muisaanwijzer boven een knop laat zweven. De muis mag een klein beetje bewegen, en zal dan nog steeds klikken. Indien de aanwijzer te hevig beweegt, zal de klik niet plaatsvinden.</p>

  <p>Pas de instelling <gui>Bewegingsdrempel</gui> aan om te wijzigen hoeveel de muisaanwijzer mag bewegen om nog steeds als zweven beschouwd te worden.</p>

</page>
