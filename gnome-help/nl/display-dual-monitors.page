<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="display-dual-monitors" xml:lang="nl">

  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-23" status="review"/>
    <revision pkgversion="3.28" date="2018-07-28" status="review"/>
    <revision pkgversion="3.34" date="2019-11-12" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Een externe monitor instellen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

<title>Een andere monitor op uw computer aansluiten</title>

<!-- TODO: update video
<section id="video-demo">
  <title>Video Demo</title>
  <media its:translate="no" type="video" width="500" mime="video/webm" src="figures/display-dual-monitors.webm">
    <p its:translate="yes">Demo</p>
    <tt:tt its:translate="yes" xmlns:tt="http://www.w3.org/ns/ttml">
      <tt:body>
        <tt:div begin="1s" end="3s">
          <tt:p>Type <input>displays</input> in the <gui>Activities</gui>
          overview to open the <gui>Displays</gui> settings.</tt:p>
        </tt:div>
        <tt:div begin="3s" end="9s">
          <tt:p>Click on the image of the monitor you would like to activate or
          deactivate, then switch it <gui>ON/OFF</gui>.</tt:p>
        </tt:div>
        <tt:div begin="9s" end="16s">
          <tt:p>The monitor with the top bar is the main monitor. To change
          which monitor is “main”, click on the top bar and drag it over to
          the monitor you want to set as the “main” monitor.</tt:p>
        </tt:div>
        <tt:div begin="16s" end="25s">
          <tt:p>To change the “position” of a monitor, click on it and drag it
          to the desired position.</tt:p>
        </tt:div>
        <tt:div begin="25s" end="29s">
          <tt:p>If you would like both monitors to display the same content,
          check the <gui>Mirror displays</gui> box.</tt:p>
        </tt:div>
        <tt:div begin="29s" end="33s">
          <tt:p>When you are happy with your settings, click <gui>Apply</gui>
          and then click <gui>Keep This Configuration</gui>.</tt:p>
        </tt:div>
        <tt:div begin="33s" end="37s">
          <tt:p>To close the <gui>Displays Settings</gui> click on the
          <gui>×</gui> in the top corner.</tt:p>
        </tt:div>
      </tt:body>
    </tt:tt>
  </media>
</section>
-->

<section id="steps">
  <title>Een externe monitor instellen</title>
  <p>Sluit de monitor aan om een tweede monitor op uw computer te installeren. Als uw systeem het niet direct herkent, of als u de instellingen wil aanpassen:</p>

  <steps>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Schermen</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Displays</gui> to open the panel.</p>
    </item>
    <item>
      <p>In the display arrangement diagram, drag your displays to the relative
      positions you want.</p>
      <note style="tip">
        <p>The numbers on the diagram are shown at the top-left of each
        display when the <gui>Displays</gui> panel is active.</p>
      </note>
    </item>
    <item>
      <p>Click <gui>Primary Display</gui> to choose your primary display.</p>

      <note>
        <p>The primary display is the one with the
        <link xref="shell-introduction">top bar</link>, and where the
        <gui>Activities</gui> overview is shown.</p>
      </note>
    </item>
    <item>
      <p>Select the orientation, resolution or scale, and refresh rate.</p>
    </item>
    <item>
      <p>Klik op <gui>Toepassen</gui>. De nieuwe instellingen zullen 20 seconden worden toegepast alvorens terug te gaan naar de vorige. Aldus zullen, als u met de nieuwe instellingen niets ziet, uw oude instellingen automatisch worden hersteld. Als u tevreden bent met de nieuwe instellingen klikt u op <gui>Deze instelling behouden</gui>.</p>
    </item>
  </steps>

</section>

<section id="modes">

  <title>Display modes</title>
    <p>With two screens, these display modes are available:</p>
    <list>
      <item><p><gui>Join Displays:</gui> screen edges are joined so things can
      pass from one display to another.</p></item>
      <item><p><gui>Mirror:</gui> the same content is shown on two displays,
      with the same resolution and orientation for both.</p></item>
      <item><p><gui>Single Display:</gui> only one display is configured,
      effectively turning off the other one. For instance, an external monitor
      connected to a docked laptop with the lid closed would be the single
      configured display.</p></item>
    </list>

</section>

<section id="multiple">

  <title>Adding more than one monitor</title>
    <p>With more than two screens, <gui>Join Displays</gui> is the only mode
    available.</p>
    <list>
      <item>
        <p>Press the button for the display that you would like to
        configure.</p>
      </item>
      <item>
        <p>Drag the screens to the desired relative positions.</p>
      </item>
    </list>

</section>
</page>
