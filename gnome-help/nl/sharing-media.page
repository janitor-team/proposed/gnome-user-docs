<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="sharing-media" xml:lang="nl">

  <info>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.10" version="0.2" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Media delen op uw lokale netwerk via UPnP.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Uw muziek, foto's en video's delen</title>

  <p>U kunt de media op uw computer doorbladeren, doorzoeken en afspelen via een apparaat dat <sys>UPnP</sys> of <sys>DLNA</sys> ondersteunt, zoals een telefoon, TV of spelcomputer. Pas <gui>Mediadelen</gui> aan zodat deze apparaten toegang hebben tot de mappen met daarin uw muziek, foto's en video's.</p>

  <note style="info package">
    <p>Om <gui>Mediadelen</gui> zichtbaar te krijgen dient het pakket <app>Rygel</app> geïnstalleerd te zijn.</p>

    <if:choose xmlns:if="http://projectmallard.org/if/1.0/">
      <if:when test="action:install">
        <p><link action="install:rygel" style="button">Rygel installeren</link></p>
      </if:when>
    </if:choose>
  </note>

  <steps>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Delen</gui>.</p>
    </item>
    <item>
      <p>Klik op <gui>Delen</gui> om het paneel te openen.</p>
    </item>
    <item>
      <p>If the <gui>Sharing</gui> switch in the top-right of the window is
      set to off, switch it to on.</p>

      <note style="info"><p>Als u de tekst onder <gui>Computernaam</gui>kunt bewerken, dan kunt u de naam van uw computer zoals die getoond wordt in het netwerk <link xref="sharing-displayname">wijzigen</link></p></note>
    </item>
    <item>
      <p>Selecteer <gui>Media delen</gui>.</p>
    </item>
    <item>
      <p>Switch the <gui>Media Sharing</gui> switch to on.</p>
    </item>
    <item>
      <p>Standaard worden <file>Muziek</file>, <file>Foto's</file> en <file>Video's</file> gedeeld. Als u één hiervan wilt verwijderen klikt u op de <gui>×</gui> naast de mapnaam.</p>
    </item>
    <item>
      <p>Om nog een map toe te voegen: klik op <gui style="button">+</gui> om het venster <gui>Een map kiezen</gui> te openen. Ga <em>naar</em> de gewenste map en klik op <gui style="button">Openen</gui>.</p>
    </item>
    <item>
      <p>Klik op <gui style="button">×</gui>. Nu kunt u media doorbladeren of afspelen in de mappen die u gekozen heeft met het externe apparaat.</p>
    </item>
  </steps>

  <section id="networks">
  <title>Netwerken</title>

  <p>The <gui>Networks</gui> section lists the networks to which you are
  currently connected. Use the switch next to each to choose where your media
  can be shared.</p>

  </section>

</page>
