<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="tip" version="1.0 if/1.0 ui/1.0" id="shell-keyboard-shortcuts" xml:lang="nl">

  <info>
    <link type="guide" xref="tips"/>
    <link type="guide" xref="keyboard"/>
    <link type="guide" xref="shell-overview#apps"/>
    <link type="seealso" xref="keyboard-key-super"/>

    <revision pkgversion="3.8.0" version="0.4" date="2013-04-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.20" date="2016-08-13" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Door het bureaublad lopen via het toetsenbord.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

<title>Handige sneltoetsen</title>

<p>Deze pagina biedt een overzicht van sneltoetsen die u kunnen helpen bij het efficiënter gebruiken van uw bureaublad en toepassingen. Als u helemaal geen muis of aanwijsapparaat kunt gebruiken, zie <link xref="keyboard-nav"/> voor meer informatie over het navigeren door de gebruikersinterface met alleen het toetsenbord.</p>

<table rules="rows" frame="top bottom" ui:expanded="true">
<title>Omgaan met het bureaublad</title>
  <tr xml:id="alt-f1">
    <td><p>
      <keyseq><key>Alt</key><key>F1</key></keyseq> or the</p>
      <p><key xref="keyboard-key-super">Super</key> key
    </p></td>
    <td><p>Ga van het <gui>Activiteiten</gui>-overzicht naar het bureaublad. Begin te typen in het overzicht om meteen uw toepassingen, contacten en documenten te doorzoeken.</p></td>
  </tr>
  <tr xml:id="alt-f2">
    <td><p><keyseq><key>Alt</key><key>F2</key></keyseq></p></td>
    <td><p>Pop up command window (for quickly running commands).</p>
    <p>Use the arrow keys to quickly access previously run commands.</p></td>
  </tr>
  <tr xml:id="super-tab">
    <td><p><keyseq><key>Super</key><key>Tab</key></keyseq></p></td>
    <td><p><link xref="shell-windows-switching">Snel van venster wisselen</link>. Houd de <key>Shift</key>-toets ingedrukt voor de omgekeerde volgorde.</p></td>
  </tr>
  <tr xml:id="super-tick">
    <td><p><keyseq><key>Super</key><key>`</key></keyseq></p></td>
    <td>
      <p>Schakelen tussen vensters van dezelfde toepassing, of van de geselecteerde toepassing na <keyseq><key>Super</key><key>Tab</key></keyseq>.</p>
      <p>Deze sneltoets gebruikt <key>`</key> op US toetsenborden, waarbij de <key>`</key>-toets zich boven de <key>Tab</key>-toets bevindt. Op alle andere toetsenborden is de sneltoets <key>Super</key> plus wat er zich boven de <key>Tab</key>-toets bevindt.</p>
    </td>
  </tr>
  <tr xml:id="alt-escape">
    <td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
    <td>
      <p>Switch between windows in the current workspace. Hold down
      <key>Shift</key> for reverse order.</p>
    </td>
  </tr>
  <tr xml:id="ctrl-alt-tab">
    <!-- To be updated to <key>Tab</key> in the future. -->
    <td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq></p></td>
    <td>
      <p>Give keyboard focus to the top bar. In the <gui>Activities</gui>
      overview, switch keyboard focus between the top bar, dash, windows
      overview, applications list, and search field. Use the arrow keys to
      navigate.</p>
    </td>
  </tr>
  <tr xml:id="super-a">
    <td><p><keyseq><key>Super</key><key>A</key></keyseq></p></td>
    <td><p>De lijst met toepassingen tonen.</p></td>
  </tr>
  <tr xml:id="super-updown">
    <td>
      <p if:test="!platform:gnome-classic"><keyseq><key>Super</key><key>Page Up</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Ctrl</key><key>Alt</key><key>→</key></keyseq></p>
      <p>and</p>
      <p if:test="!platform:gnome-classic"><keyseq><key>Super</key><key>Page Down</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Ctrl</key><key>Alt</key><key>←</key></keyseq></p>
    </td>
    <td><p><link xref="shell-workspaces-switch">Wisselen van werkblad</link>.</p></td>
  </tr>
  <tr xml:id="shift-super-updown">
    <td>
      <p if:test="!platform:gnome-classic"><keyseq><key>Shift</key><key>Super</key><key>Page Up</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Shift</key><key>Ctrl</key><key>Alt</key><key>→</key></keyseq></p>
      <p>and</p>
      <p if:test="!platform:gnome-classic"><keyseq><key>Shift</key><key>Super</key><key>Page Down</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Shift</key><key>Ctrl</key><key>Alt</key><key>←</key></keyseq></p>
    </td>
    <td><p><link xref="shell-workspaces-movewindow">Het huidige venster naar een ander werkblad verplaatsen</link>.</p></td>
  </tr>
  <tr xml:id="shift-super-left">
    <td><p><keyseq><key>Shift</key><key>Super</key><key>←</key></keyseq></p></td>
    <td><p>Huidige venster één scherm naar links verplaatsen.</p></td>
  </tr>
  <tr xml:id="shift-super-right">
    <td><p><keyseq><key>Shift</key><key>Super</key><key>→</key></keyseq></p></td>
    <td><p>Huidige venster één scherm naar rechts verplaatsen.</p></td>
  </tr>
  <tr xml:id="ctrl-alt-Del">
    <td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Delete</key></keyseq></p></td>
    <td><p><link xref="shell-exit">Show the Power Off dialog</link>.</p></td>
  </tr>
  <tr xml:id="super-l">
    <td><p><keyseq><key>Super</key><key>L</key></keyseq></p></td>
    <td><p><link xref="shell-exit#lock-screen">Het scherm vergrendelen.</link></p></td>
  </tr>
  <tr xml:id="super-v">
    <td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
    <td><p>Show <link xref="shell-notifications#notificationlist">the notification
    list</link>. Press <keyseq><key>Super</key><key>V</key></keyseq> again or
    <key>Esc</key> to close.</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Sneltoetsen voor algemene bewerking</title>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>A</key></keyseq></p></td>
    <td><p>Alle tekst of lijstonderdelen selecteren.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>X</key></keyseq></p></td>
    <td><p>Geselecteerde tekst of items knippen (weghalen) en die op het klembord plaatsen.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>C</key></keyseq></p></td>
    <td><p>Geselecteerde tekst of items naar het klembord kopiëren.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>V</key></keyseq></p></td>
    <td><p>De inhoud van het klembord plakken.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>Z</key></keyseq></p></td>
    <td><p>De laatste handeling ongedaan maken.</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Schermafdrukken maken</title>
  <tr>
    <td><p><key>Print Screen</key></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Schermafdruk maken.</link></p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Print Screen</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Schermafdruk van een venster maken.</link></p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key><key>Print Screen</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Schermafdruk maken van een gedeelte van het scherm.</link> De cursor zal in een kruis veranderen. Sleep een rand om het gebied waarvan u een afdruk wilt maken.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Shift</key><key>R</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screencast">Start and stop screencast
     recording.</link></p></td>
  </tr>
</table>

</page>
