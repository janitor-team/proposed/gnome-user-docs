<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-capacity" xml:lang="nl">
  <info>
    <link type="guide" xref="disk"/>

    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Rafael Fontenelle</name>
      <email>rafaelff@gnome.org</email>
    </credit>

    <revision pkgversion="3.4.3" date="2012-06-15" status="review"/>
    <revision pkgversion="3.13.91" date="2014-09-05" status="review"/>

    <desc>Use <gui>Disk Usage Analyzer</gui>, <gui>System Monitor</gui>, or
    <gui>Usage</gui> to check space and capacity.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

<title>Controleer hoeveel schijfruimte er nog besschikbaar is</title>

  <p>You can check how much disk space is left with <app>Disk Usage Analyzer</app>,
  <app>System Monitor</app>, or <app>Usage</app>.</p>

<section id="disk-usage-analyzer">
<title>Controleren met Schijfgebruik</title>

  <p>Om de vrije ruimte van de schijf en de schijfcapaciteit te controleren via <app>Schijfgebruik</app>:</p>

  <list>
    <item>
      <p>Open <app>Schijfgebruik</app> vanuit het <gui>Activiteiten</gui>-overzicht. Het venster geeft een lijst met bestandslocaties weer tezamen met het gebruik en de capaciteit van elk.</p>
    </item>
    <item>
      <p>Click one of the items in the list to view a detailed summary of the
      usage for that item. Click the menu button, and then <gui>Scan
      Folder…</gui> to scan a different location.</p>
    </item>
  </list>
  <p>De informatie wordt weergegeven naar <gui>Map</gui>, <gui>Grootte</gui>, <gui>Inhoud</gui> en wanneer de gegevens voor het laatst zijn <gui>Gewijzigd</gui>.  Ga voor meer informatie naar <link href="help:baobab"><app>Schijfgebruik</app></link>.</p>

</section>

<section id="system-monitor">

<title>Met Systeemmonitor controleren</title>

  <p>Vrije schijfruimte en schijfcapaciteit controleren met <app>Systeemmonitor</app>:</p>

<steps>
 <item>
  <p>Open vanuit het <gui>Activiteiten</gui>overzicht de <app>Systeemmonitor</app>-toepassing.</p>
 </item>
 <item>
  <p>Selecteer het tabblad <gui>Bestandssystemen</gui> om de partities van het systeem en het schijfgebruik te bekijken. De informatie wordt weergegeven in de kolommen <gui>Totaal</gui>, <gui>Vrij</gui>, <gui>Beschikbaar</gui> en <gui>Gebruikt</gui>.</p>
 </item>
</steps>
</section>

<section id="usage">
<title>Check with Usage</title>

  <p>To check the free disk space and disk capacity with <app>Usage</app>:</p>

<steps>
  <item>
    <p>Open the <app>Usage</app> application from the <gui>Activities</gui>
    overview.</p>
  </item>
  <item>
    <p>Select <gui>Storage</gui> tab to view the system’s total <gui>Used</gui>
    and <gui>Available</gui> disk space, as well as the used by the
    <gui>Operating System</gui> and common user’s directories.</p>
  </item>
</steps>

<note style="tip">
  <p>Disk space can be freed from user’s directories and its subdirectories
  by checking the box next to the directory name.</p>
</note>
</section>

<section id="disk-full">

<title>Wat als de schijf te vol is?</title>

  <p>Als de schijf te vol is moet u:</p>

 <list>
  <item>
   <p>Bestanden die niet belangrijk zijn of bestanden die u niet meer gebruikt verwijderen.</p>
  </item>
  <item>
   <p>Maak <link xref="backup-why">reservekopieën</link> van de belangrijke bestanden die u voorlopig niet nodig heeft en wis ze van de harde schijf.</p>
  </item>
 </list>
</section>

</page>
