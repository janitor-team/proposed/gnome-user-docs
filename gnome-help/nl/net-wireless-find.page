<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-wireless-find" xml:lang="nl">

  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="net-wireless-hidden"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.12" date="2014-03-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Het draadloos-apparaat kan uitgeschakeld of stuk zijn, of misschien probeert u te verbinden met een verborgen netwerk.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

<title>Ik zie mijn draadloos netwerk niet in de lijst</title>

  <p>Er kunnen een aantal redenen zijn waarom u uw draadloos netwerk niet ziet in de lijst met beschikbare netwerken van het systeemmenu.</p>

<list>
 <item>
  <p>Als er geen netwerken in de lijst getoond worden, dan kan het zijn dat uw draadloos-hardware uitgeschakeld is, of dat die <link xref="net-wireless-troubleshooting">mogelijk niet goed werkt</link>. Zorg ervoor dat het apparaat ingeschakeld is.</p>
 </item>
<!-- Does anyone have lots of wireless networks to test this? Pretty sure it's
     no longer correct.
  <item>
    <p>If there are lots of wireless networks nearby, the network you are
    looking for might not be on the first page of the list. If this is the
    case, look at the bottom of the list for an arrow pointing towards the
    right and hover your mouse over it to display the rest of the wireless
    networks.</p>
  </item>-->
 <item>
  <p>U kunt buiten bereik van het netwerk zijn. Probeer dichter bij het daadloos basisstation/de router te komen en kijk of het netwerk na een poosje in de lijst verschijnt.</p>
 </item>
 <item>
  <p>Het duurt even voordat de lijst met draadloze netwerken bijgewerkt is. als u net uw computer heeft aangezet of naar een andere locatie bent gegaan, wacht een paar minuten en controleer dan of het netwerk in de lijst is verschenen.</p>
 </item>
 <item>
  <p>Het netwerk kan verborgen zijn. U dient te <link xref="net-wireless-hidden">verbinden op een andere manier</link> als het een verborgen netwerk is.</p>
 </item>
</list>

</page>
