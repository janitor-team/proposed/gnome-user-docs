<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-list" xml:lang="nl">

  <info>
    <its:rules xmlns:its="http://www.w3.org/2005/11/its" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="nautilus-prefs" group="nautilus-list"/>

    <revision pkgversion="3.5.92" date="2012-09-19" status="review"/>
    <revision pkgversion="3.14.0" date="2014-09-23" status="review"/>
    <revision pkgversion="3.18" date="2014-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>
    <revision pkgversion="41.0" date="2021-10-15" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Beheren welke informatie weergegeven wordt in kolommen in de lijstweergave.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Bestandsbeheer-voorkeuren: lijstkolommen</title>

  <p>There are several columns of information that you can display in the
  <gui>Files</gui> list view. Right-click a column header and select or
  deselect which columns should be visible.</p>

  <terms>
    <item>
      <title><gui>Naam</gui></title>
      <p>De naam van mappen en bestanden.</p>
      <note style="tip">
        <p>De kolom <gui>Naam</gui> kan niet worden verborgen.</p>
      </note>
    </item>
    <item>
      <title><gui>Grootte</gui></title>
      <p>De grootte van een map wordt aangegeven als het aantal items in die map. De grootte van een bestand wordt gegeven in bytes, KB of MB.</p>
    </item>
    <item>
      <title><gui>Type</gui></title>
      <p>Weergegeven als map, of bestandstype zoals PDF-document, JPEG-afbeelding, MP3 audio.</p>
    </item>
    <item>
      <title><gui>Gewijzigd</gui></title>
      <p>Geeft de datum van de laatste keer dat het bestand werd gewijzigd.</p>
    </item>
    <item>
      <title><gui>Eigenaar</gui></title>
      <p>De naam van de gebruiker die eigenaar is van de map of het bestand.</p>
    </item>
    <item>
      <title><gui>Groep</gui></title>
      <p>De groep waartoe het bestand behoort. Elke gebruiker zit normaalgesproken in een eigen groep, maar het is mogelijk dat meerdere gebruikers in één groep zitten. Zo kan een afdeling een eigen groep hebben in een werkomgeving.</p>
    </item>
    <item>
      <title><gui>Rechten</gui></title>
      <p>Geeft de toegangsrechten tot het bestand weer, bijv. <gui>drwxrw-r--</gui></p>
      <list>
        <item>
          <p>Het eerste teken <gui>-</gui> is het bestandstype. <gui>-</gui> betekent normaal bestand en <gui>d</gui> betekent directory (map). In zeldzame gevallen kunnen er ook andere tekens weergegeven worden.</p>
        </item>
        <item>
          <p>De volgende drie tekens <gui>rwx</gui> geven de rechten aan voor de gebruiker die eigenaar is van het bestand.</p>
        </item>
        <item>
          <p>De volgende drie tekens <gui>rw-</gui> geven de rechten aan voor alle leden van de groep die eigenaar is van het bestand.</p>
        </item>
        <item>
          <p>De laatste drie tekens in de kolom <gui>r--</gui> geven de rechten aan voor alle andere gebruikers van het systeem.</p>
        </item>
      </list>
      <p>De betekenis van de rechten:</p>
      <list>
        <item>
          <p><gui>r</gui>: leesbaar, hetgeen inhoudt dat u het bestand of de map kunt openen</p>
        </item>
        <item>
          <p><gui>w</gui>: schrijfbaar, hetgeen betekent dat u wijzigingen kunt opslaan</p>
        </item>
        <item>
          <p><gui>x</gui>: uitvoerbaar, hetgeen betekent dat u het kan uitvoeren als het een programma of scriptbestand is, of dat u toegang heeft tot submappen en bestanden als het een map is</p>
        </item>
        <item>
          <p><gui>-</gui>: permissie niet ingesteld</p>
        </item>
      </list>
    </item>
    <item>
      <title><gui>MIME-type</gui></title>
      <p>Geeft het MIME-type van het item weer.</p>
    </item>
    <item>
      <title><gui>Locatie</gui></title>
      <p>Het pad naar de locatie van het bestand.</p>
    </item>
    <item>
      <title><gui>Gewijzigd — Tijd</gui></title>
      <p>Geeft de datum en tijd van de laatste keer dat het bestand werd gewijzigd.</p>
    </item>
    <item>
      <title><gui>Benaderd</gui></title>
      <p>Geeft de datum of tijd van de laatste keer dat het bestand werd gewijzigd.</p>
    </item>
  </terms>

</page>
