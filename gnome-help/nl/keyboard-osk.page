<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="keyboard-osk" xml:lang="nl">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>

    <credit type="author">
      <name>Jeremy Bicha</name>
      <email>jbicha@ubuntu.com</email>
    </credit>
    <credit type="author">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Een schermtoetsenbord gebruiken om tekst in te voeren door met de muis of aanraken op knoppen te klikken.</desc>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Een schermtoetsenbord gebruiken</title>

  <p>Als er geen toetsenbord aangesloten is op uw computer of als u die liever niet wilt gebruiken, dan kunt u het <em>schermtoetsenbord</em> inschakelen om tekst in te voeren.</p>

  <note>
    <p>Het schermtoetsenbord wordt automatisch ingeschakeld als u een aanraakscherm gebruikt</p>
  </note>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Accessibility</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Schakel het <gui>Schermtoetsenbord</gui> in onder het kopje <gui>Typen</gui></p>
    </item>
  </steps>

  <p>Wanneer u vervolgens de mogelijkheid om te typen heeft, zal het schermtoetsenbord onderaan het scherm geopend worden.</p>

  <p>Press the <gui style="button">?123</gui> button to enter numbers and
  symbols. More symbols are available if you then press the
  <gui style="button">=/&lt;</gui> button. To return to the alphabet keyboard,
  press the <gui style="button">ABC</gui> button.</p>

  <p>You can press the
  <gui style="button"><media its:translate="no" type="image" src="figures/go-down-symbolic.svg" width="16" height="16"><span its:translate="yes">down</span></media></gui>
  button to hide the keyboard temporarily. The keyboard will show again
  automatically when you next press on something where you can use it.
  On a touchscreen, you can also pull up the keyboard by
  <link xref="touchscreen-gestures">dragging up from the bottom edge</link>.</p>
  <p>Press the
  <gui style="button"><media its:translate="no" type="image" src="figures/emoji-flags-symbolic.svg" width="16" height="16"><span its:translate="yes">flag</span></media></gui>
  button to change your settings for
  <link xref="session-language">Language</link> or
  <link xref="keyboard-layouts">Input Sources</link>.</p>

</page>
