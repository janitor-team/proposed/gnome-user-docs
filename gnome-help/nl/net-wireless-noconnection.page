<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-wireless-noconnection" xml:lang="nl">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" version="0.2" date="2013-11-11" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Controleer of u het juiste wachtwoord heeft, en andere dingen om te proberen.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

<title>Ik heb het juiste wachtwoord ingevoerd, maar ik kan nog geen verbinding maken</title>

<p>Als u er zeker van bent dat u het juiste <link xref="net-wireless-wepwpa">wachtwoord voor draadloos netwerk</link> heeft ingevoerd, maar u kunt nog steeds geen verbinding maken met een draadloos netwerk, probeer dan het volgende:</p>

<list>
 <item>
  <p>Controleer of u het juiste wachtwoord heeft</p>
  <p> Wachtwoorden zijn hoofdlettergevoelig (het maakt uit of ze hoofdletters of kleine letters bevatten); controleer dus op de juiste invoer van hoofdletters en kleine letters.</p>
 </item>

 <item>
  <p>Probeer het hex- of ASCII-wachtwoord</p>
  <p>Het in te voeren wachtwoord kan ook op een andere manier weergegeven worden — als een reeks van tekens in hexadecimaal (cijfers 0-9 en letters a-f), aanmeldcode genaamd. Elk wachtwoord heeft een overeenkomende aanmeldcode. Als u toegang heeft tot zowel de aanmeldcode als het wachtwoord/de wachtwoordzin, probeer dan de aanmeldcode in te voeren. Zorg ervoor dat u de juiste optie voor <gui>beveiliging draadloos netwerk</gui> selecteert wanneer naar uw wachtwoord wordt gevraagd (kies bijvoorbeeld <gui>WEP 40/128-bit Key</gui> als u de uit 40 tekens bestaande aanmeldcode voor een WEP-versleutelde verbinding intypt).</p>
 </item>

 <item>
  <p>Probeer uw draadloze netwerkkaart uit en dan weer in te schakelen</p>
  <p>Soms lopen draadloze netwerkkaarten vast of hebben een klein probleem waardoor er geen verbinding kan worden gemaakt. Probeer de kaart uit en dan weer in te schakelen om hem te resetten — zie <link xref="net-wireless-troubleshooting"/> voor meer informatie.</p>
 </item>

 <item>
  <p>Controleer of u het juiste type beveiliging voor het draadloos netwerk gebruikt</p>
  <p>Wanneer u gevraagd wordt naar uw beveiligingswachtwoord, dan kunt u kiezen welk soort beveiliging u gebruikt. Zorg ervoor dat u die kiest welke gebruikt wordt door de router of het basisstation. Dit zou standaard geselecteerd moeten zijn, maar soms is dat om een of andere reden niet het geval. Als u niet weet welke u moet hebben, probeer dan de verschillende opties uit.</p>
 </item>

 <item>
  <p>Controleer of uw netwerkkaart op de juiste manier ondersteund wordt</p>
  <p>Sommige netwerkkaarten worden niet goed ondersteund. Ze worden getoond als een draadloze verbinding, maar ze kunnen geen verbinding maken met een netwerk omdat hun stuurprogramma's dit niet kunnen. Probeer een ander stuurprogramma te vinden of kijk of u extra handelingen moet verrichten (zoals het installeren van andere <em>firmware</em>). Zie <link xref="net-wireless-troubleshooting"/> voor meer informatie.</p>
 </item>

</list>

</page>
