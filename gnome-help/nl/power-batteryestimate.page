<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="power-batteryestimate" xml:lang="nl">

  <info>

    <link type="guide" xref="power#faq"/>
    <link type="guide" xref="status-icons#batteryicons"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>
    <revision version="gnome:40" date="2021-03-22" status="candidate"/>

    <desc>De levensduur van de accu welke wordt getoond wanneer u op het <gui>accupictogram</gui> klikt is een schatting.</desc>

    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

<title>De geschatte levensduur van de accu is fout</title>

<p>Wanneer u de levensduur van de accu controleert, kan het zijn dat de gemelde resterende tijd verschilt van de tijd die er werkelijk nog resteert. Dit komt doordat de resterende tijd van de accu alleen maar geschat kan worden. De schattingen zullen echter wel steeds nauwkeuriger worden.</p>

<p>Om de resterende tijd van de accu te schatten, moet rekening gehouden worden met een aantal factoren. Eén daarvan is de hoeveelheid energie die de computer momenteel gebruikt: het energieverbruik varieert, afhankelijk van hoeveel programma's u geopend heeft, welke apparaten er zijn aangesloten en of er intensieve taken worden uitgevoerd (zoals het bekijken van een video of het converteren van muziekbestanden). Dit verandert van moment op moment en is moeilijk te voorspellen.</p>

<p>Een andere factor is hoe de accu ontlaadt. Sommige accu's ontladen sneller naarmate ze leger raken. Zonder precies te weten hoe de accu ontlaadt, kan er slechts een ruwe schatting worden gemaakt van de resterende accuduur.</p>

<p>Terwijl de accu ontlaadt zal Energiebeheer achter de ontlaadeigenschappen komen en leren hoe het de levensduur van de accu beter kan inschatten. De schattingen zullen echter nooit helemaal nauwkeurig zijn.</p>

<note>
  <p>Als u een totaal onwaarschijnlijke schatting van de acculevensduur krijgt (laten we zeggen honderden dagen!), dan mist Energiebeheer waarschijnlijk enkele gegevens die nodig zijn om een goede schatting te maken.</p>
  <p>Als u een poosje op accustroom draait, en daarna weer op netstroom overgaat en weer gaat opladen, dan zou energiebeheer de benodigde gegevens moeten verkrijgen.</p>
</note>

</page>
