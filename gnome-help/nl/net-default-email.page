<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-default-email" xml:lang="nl">

  <info>
    <link type="guide" xref="net-email"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.38.2" date="2021-03-07" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Change the default email client by going to <gui>Default Applications</gui> in <gui>Settings</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Wijzigen welk e-mailprogramma gebruikt wordt om e-mails te schrijven</title>

  <p>Wanneer u op een knop of link klikt om een nieuwe e-mail te verzenden (bijvoorbeeld in uw tekstverwerker), dan wordt uw standaard e-mailprogramma geopend met een leeg bericht, klaar voor u om een e-mailbericht te schrijven. Heeft u echter meer dan één e-mailprogramma geïnstalleerd, dan kan het zijn dat het verkeerde e-mailprogramma geopend wordt. Dit kunt u oplossen door uw standaard e-mailprogramma te wijzigen:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Default Applications</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Default Applications</gui>.</p>
    </item>
    <item>
      <p>Kies welk e-mailprogramma u standaard wilt gebruiken door middel van de <gui>Mail</gui>-optie.</p>
    </item>
  </steps>

</page>
