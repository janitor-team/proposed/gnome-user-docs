<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="clock-world" xml:lang="nl">

  <info>
    <link type="guide" xref="clock" group="#last"/>
    <link type="seealso" href="help:gnome-clocks/index"/>

    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.28" date="2018-07-30" status="review"/>
    <revision pkgversion="3.37" date="2020-08-06" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhill@gnome.org</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Tijden in andere steden onder de agenda weergeven.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Een wereldklok toevoegen</title>

  <p>Gebruik <app>Klok</app> om tijden in andere steden toe te voegen.</p>

  <note>
    <p>Hiervoor moet de <app>Klok</app>-toepassing geïnstalleerd zijn.</p>
    <p>Bij de meeste distributies is <app>Klok</app> standaard geïnstalleerd. Als dat bij u niet het geval is, moet u het installeren met het pakketbeheerprogramma van uw distributie.</p>
  </note>

  <p>Om een wereldklok toe te voegen:</p>

  <steps>
    <item>
      <p>Click the clock on the top bar.</p>
    </item>
    <item>
      <p>Click the <gui>Add world clocks…</gui> button under the calendar to
      launch <app>Clocks</app>.</p>

    <note>
       <p>Als u al één of meer wereldklokken heeft, klik dan op één van deze en <app>Klok</app> zal starten.</p>
    </note>

    </item>
    <item>
      <p>In the <app>Clocks</app> window, click
      <gui style="button">+</gui> button or press
      <keyseq><key>Ctrl</key><key>N</key></keyseq> to add a new city.</p>
    </item>
    <item>
      <p>Begin met het intypen van de naam van de stad in de zoekbalk.</p>
    </item>
    <item>
      <p>Selecteer de juiste stad of de locatie die het dichtst bij is in de lijst.</p>
    </item>
    <item>
      <p>Druk op <gui style="button">Toevoegen</gui> om het toevoegen van de stad af te ronden.</p>
    </item>
  </steps>

  <p>Raadpleeg <link href="help:gnome-clocks">Klok Hulp</link> voor meer mogelijkheden van  <app>Klokken</app>.</p>

</page>
