<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-howtoimport" xml:lang="nl">

  <info>
    <link type="guide" xref="color#profiles"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-assignprofiles"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="review"/>
    <revision pkgversion="3.28" date="2018-04-05" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Kleurprofielen kunnen geïmporteerd worden door ze te openen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Hoe importeer ik  kleurprofielen?</title>

  <p>U kunt een kleurprofiel importeren door te dubbelklikken op een <file>.ICC-</file> of <file>.ICM-</file>-bestand in de bestandsverkenner.</p>

  <p>U kunt ook uw kleurprofielen beheren via het paneel <gui>Kleur</gui></p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Color</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Select your device.</p>
    </item>
    <item>
      <p>Klik op <gui>Profiel toevoegen</gui> om een bestaand profiel te selecteren of een nieuw profiel te importeren.</p>
    </item>
    <item>
      <p>Druk op <gui>Toevoegen</gui> om uw selectie te bevestigen.</p>
    </item>
  </steps>

  <p>De fabrikant van uw scherm kan een profiel hebben meegeleverd dat u kunt gebruiken. Deze profielen zijn meestal gemaakt voor het gemiddelde scherm en dus niet ideaal voor dat van u. Voor de beste kalibratie kunt u <link xref="color-calibrate-screen">uw eigen profiel aanmaken</link> met behulp van een colorimeter of een spectrofotometer.</p>

</page>
