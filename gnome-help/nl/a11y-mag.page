<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-mag" xml:lang="nl">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-10" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Zoom in op uw scherm zodat u makkelijker dingen kunt zien.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Een schermgebied vergroten</title>

  <p>Het scherm vergroten is iets anders dan alleen de <link xref="a11y-font-size">tekstgrootte</link> vergroten. Deze functie lijkt op een vergrootglas, waarmee u kunt bewegen door op gedeeltes van het scherm in te zoomen.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Accessibility</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Accessibility</gui> to open the panel.</p>
    </item>
    <item>
      <p>Klik op <gui>Zoomen</gui> vanuit de <gui>Zicht</gui>-sectie.</p>
    </item>
    <item>
      <p>Switch the <gui>Zoom</gui> switch in the top-right corner of the
      <gui>Zoom Options</gui> window to on.</p>
      <!--<note>
        <p>The <gui>Zoom</gui> section lists the current settings for the
        shortcut keys, which can be set in the <gui>Accessibility</gui>
        section of the <link xref="keyboard-shortcuts-set">Shortcuts</link> tab
        on the <gui>Keyboard</gui> panel.</p>
      </note>-->
    </item>
  </steps>

  <p>U kunt nu in het schermgebied bewegen. Door uw muis naar de rand van het scherm te bewegen, beweegt u het vergrote gebied in verschillende richtingen, waarmee u het gebied van uw keuze kunt zien.</p>

  <note style="tip">
    <p>U kunt zoomen ook snel in- en uitschakelen door te klikken op het <link xref="a11y-icon">toegankelijkheidspictogram</link> in de bovenbalk en <gui>Zoomen</gui> te selecteren.</p>
  </note>

  <p>U kunt de vergrotingsfactor, het volgen van de muis, en de positie van de vergrote weergave op het scherm wijzigen. Pas deze aan in het tabblad van het <gui>Vergrootglas</gui> van het <gui>Zoomopties</gui>-venster.</p>

  <p>U kunt een draadkruis aanzetten om u te helpen bij het vinden van de muis of touchpad-cursor. U kunt het aanzetten en de lengte, kleur en dikte aanpassen in het <gui>Draadkruis</gui>-tabblad van het <gui>Zoom</gui>instellingen-venster.</p>

  <p>U kunt overschakelen naar inverse video of <gui>Wit op zwart</gui>, en de helderheid, contrast en kleur van het vergrootglas aanpassen. De combinatie van deze opties is nuttig voor mensen met beperkt zicht, lichtschuwheid, of gewoon om de computer te gebruiken met tegenlicht. Selecteer het <gui>Kleureffecten</gui>-tabblad in het <gui>Zoom</gui>-instellingenvenster om deze opties te kunnen inschakelen en wijzigen.</p>

</page>
