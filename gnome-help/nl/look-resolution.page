<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="look-resolution" xml:lang="nl">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="seealso" xref="look-display-fuzzy"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.28" date="2018-07-19" status="review"/>
    <revision pkgversion="3.33" date="2019-07-19" status="candidate"/>
    <revision pkgversion="3.34" date="2019-11-12" status="review"/>

    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>De resolutie en de oriëntatie (rotatie) van het scherm wijzigen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Change the resolution or orientation of the screen</title>

  <p>U kunt wijzigen hoe groot (of hoe gedetailleerd) dingen op uw scherm weergegeven worden door de <em>schermresolutie</em> te wijzigen. U kunt wijzigen welke kant boven is (bijvoorbeeld als u een roterend beeldscherm heeft) door de <em>rotatie</em> te wijzigen.</p>

  <steps>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Schermen</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Displays</gui> to open the panel.</p>
    </item>
    <item>
      <p>Als u meerdere beeldschermen heeft, en ze niet gespiegeld zijn, dan kunt u elk beeldscherm anders instellen. Kies een beeldscherm in het voorbeeldgebied.</p>
    </item>
    <item>
      <p>Select the orientation, resolution or scale, and refresh rate.</p>
    </item>
    <item>
      <p>Klik op <gui>Toepassen</gui>. De nieuwe instellingen zullen 20 seconden worden toegepast alvorens terug te gaan naar de vorige. Aldus zullen, als u met de nieuwe instellingen niets ziet, uw oude instellingen automatisch worden hersteld. Als u tevreden bent met de nieuwe instellingen klikt u op <gui>Deze instelling behouden</gui>.</p>
    </item>
  </steps>

<section id="orientation">
  <title>Orientation</title>

  <p>On some devices, you can physically rotate the screen in many directions.
  Click <gui>Orientation</gui> in the panel and choose from
  <gui>Landscape</gui>, <gui>Portrait Right</gui>, <gui>Portrait Left</gui>, or
  <gui>Landscape (flipped)</gui>.</p>

  <note style="tip">
    <p>If your device rotates the screen automatically, you can lock the current
    rotation using the
<media its:translate="no" type="image" src="figures/rotation-locked-symbolic.svg"><span its:translate="yes">rotation lock</span></media> button at the bottom of the <gui xref="shell-introduction#systemmenu">system menu</gui>. To unlock, press the 
<media its:translate="no" type="image" src="figures/rotation-allowed-symbolic.svg"><span its:translate="yes">rotation unlock</span></media> button</p>
  </note>

</section>

<section id="resolution">
  <title>Resolutie</title>

  <p>De resolutie is het aantal pixels (puntjes op het scherm) dat in elke richting kan worden weergegeven. Elke resolutie heeft een <em>beeldverhouding</em>, de verhouding tussen de lengte en de breedte. Breedbeeldschermen gebruiken een beeldverhouding van 16:9, terwijl traditionele schermen 4:3 gebruiken. Als u een resolutie kiest die niet overeenkomt met de beeldverhouding van uw scherm, dan zal het scherm aangepast worden om vervorming te voorkomen door aan de boven- en onderkant van het scherm zwarte balken toe te voegen.</p>

  <p>U kunt de resolutie die u het liefst heeft kiezen uit de <gui>Resolutie</gui>-keuzelijst. Bedenk wel dat, als u een resolutie kiest die niet past bij uw scherm, het er <link xref="look-display-fuzzy">wazig of korrelig uit kan zien</link>.</p>

</section>

<section id="native">
  <title>Native Resolution</title>

  <p>The <em>native resolution</em> of a laptop screen or LCD monitor is the
  one that works best: the pixels in the video signal will line up precisely
  with the pixels on the screen. When the screen is required to show other
  resolutions, interpolation is necessary to represent the pixels, causing a
  loss of image quality.</p>

</section>

<section id="refresh">
  <title>Refresh Rate</title>

  <p>The refresh rate is the number of times per second the screen image is
  drawn, or refreshed.</p>
</section>

<section id="scale">
  <title>Scale</title>

  <p>The scale setting increases the size of objects shown on the screen to
  match the density of your display, making them easier to read. Choose
  <gui>100%</gui> or <gui>200%</gui>.</p>

</section>

</page>
