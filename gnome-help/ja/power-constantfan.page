<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-constantfan" xml:lang="ja">

  <info>
    <link type="guide" xref="power#problems"/>
    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>ファン制御ソフトウェアがインストールされていないか、ラップトップの作動熱で高温になっている可能性があります。</desc>

    <credit type="author">
      <name>GNOME ドキュメンテーションプロジェクト</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>松澤 二郎</mal:name>
      <mal:email>jmatsuzawa@gnome.org</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>赤星 柔充</mal:name>
      <mal:email>yasumichi@vinelinux.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kentaro KAZUHAMA</mal:name>
      <mal:email>kazken3@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shushi Kurose</mal:name>
      <mal:email>md81bird@hitaki.net</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Noriko Mizumoto</mal:name>
      <mal:email>noriko@fedoraproject.org</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>坂本 貴史</mal:name>
      <mal:email>o-takashi@sakamocchi.jp</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>日本GNOMEユーザー会</mal:name>
      <mal:email>http://www.gnome.gr.jp/</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>ラップトップのファンが常に作動しています</title>

<p>ラップトップの冷却ファンが常に作動している状態の場合、コンピューター内の冷却システムを制御するハードウェアが Linux では十分にサポートされていない可能性があります。冷却ファンを効率よく制御するには追加ソフトウェアが必要になるコンピューターがあります。しかしそのソフトウェアがインストールされていない (または Linux では使用できない) ために、ファンが常にフルスピードで作動してしまうことがあります。</p>

<p>このような場合には、設定を変更するかファン制御ができるよう追加ソフトウェアをインストールします。たとえば、Sony VAIO シリーズのラップトップなら <link href="http://vaio-utils.org/fan/">vaiofand</link> をインストールしてファンを制御できるようになります。この種のソフトウェアのインストール作業は、コンピューターの製造元およびモデルにより大きく異なり、非常に技術的なプロセスを必要とします。そのため、お使いのコンピューターに適した設定方法の情報を集めるとよいでしょう。</p>

<p>ラップトップ自体が多量の熱を発している場合も考えられます。これは、必ずしもオーバーヒートしているわけではなく、コンピューターを冷却するためファンが単にフルスピードで動作しているだけの可能性があります。この場合、冷却ファンを動作させる以外の対策はあまりありません。補助として冷却用アクセサリーを追加で買い足すと効果があることもあります。</p>

</page>
