<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-changepicture" xml:lang="ja">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.14.0" date="2014-10-08" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.32.0" date="2019-03-16" status="final"/>

    <credit type="author">
      <name>GNOME ドキュメンテーションプロジェクト</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>ログイン画面およびユーザー画面に写真を追加します。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>松澤 二郎</mal:name>
      <mal:email>jmatsuzawa@gnome.org</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>赤星 柔充</mal:name>
      <mal:email>yasumichi@vinelinux.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kentaro KAZUHAMA</mal:name>
      <mal:email>kazken3@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shushi Kurose</mal:name>
      <mal:email>md81bird@hitaki.net</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Noriko Mizumoto</mal:name>
      <mal:email>noriko@fedoraproject.org</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>坂本 貴史</mal:name>
      <mal:email>o-takashi@sakamocchi.jp</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>日本GNOMEユーザー会</mal:name>
      <mal:email>http://www.gnome.gr.jp/</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>ログイン画面の写真を変更する</title>

  <p>ログインやユーザー切り替え時に、ログイン写真付きのユーザーリストが表示されます。あなたの写真をストック画像やあなた自身の画像に変更できます。ウェブカメラで新しいログイン写真を撮ることもできます。</p>

  <p>You need <link xref="user-admin-explain">administrator privileges</link>
  to edit user accounts other than your own.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Users</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Users</gui> to open the panel.</p>
    </item>
    <item>
      <p>If you want to edit a user other than yourself, press
      <gui style="button">Unlock</gui> in the top right corner and type in your
      password when prompted.</p>
    </item>
    <item>
      <p>あなたの名前の横の画像をクリックします。ログイン写真用のストック画像の一覧が表示されます。お好みのものがあれば、それをクリックしてあなた用の画像として使用してください。</p>
      <list>
        <item>
          <p>If you would rather use a picture you already have on your
          computer, click <gui>Select a file…</gui>.</p>
        </item>
        <item>
          <p>If you have a webcam, you can take a new login photo right now by
          clicking <gui>Take a picture…</gui>. Take your
          picture, then move and resize the square outline to crop out the
          parts you do not want. If you do not like the picture you took, click
          <gui style="button">Take Another Picture</gui> to try again, or
          <gui>Cancel</gui> to give up.</p>
        </item>
      </list>
    </item>
  </steps>

</page>
