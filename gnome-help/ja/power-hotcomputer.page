<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-hotcomputer" xml:lang="ja">

  <info>
    <link type="guide" xref="power#problems"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>コンピューターが熱を持つのは普通ですが、熱くなり過ぎるとオーバーヒートを起して損傷する恐れがあります。</desc>

    <credit type="author">
      <name>GNOME ドキュメンテーションプロジェクト</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>松澤 二郎</mal:name>
      <mal:email>jmatsuzawa@gnome.org</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>赤星 柔充</mal:name>
      <mal:email>yasumichi@vinelinux.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kentaro KAZUHAMA</mal:name>
      <mal:email>kazken3@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shushi Kurose</mal:name>
      <mal:email>md81bird@hitaki.net</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Noriko Mizumoto</mal:name>
      <mal:email>noriko@fedoraproject.org</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>坂本 貴史</mal:name>
      <mal:email>o-takashi@sakamocchi.jp</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>日本GNOMEユーザー会</mal:name>
      <mal:email>http://www.gnome.gr.jp/</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>コンピューターがかなり熱くなります</title>

<p>しばらくするとコンピューターが熱くなり、かなり高熱になるものもあります。これ自体は通常です。コンピューターを冷却させるプロセスの一部です。ただし、コンピューターが非常に熱くなっている場合、オーバーヒートを起こしている兆候の可能性があります。これはコンピューターの損傷の原因となります。</p>

<p>Most laptops get reasonably warm once you have been using them for a while.
It is generally nothing to worry about — computers produce a lot of heat and
laptops are very compact, so they need to remove their heat rapidly and their
outer casing warms up as a result. Some laptops do get too hot, however, and
may be uncomfortable to use. This is normally the result of a poorly-designed
cooling system. You can sometimes get additional cooling accessories which fit
to the bottom of the laptop and provide more efficient cooling.</p>

<p>If you have a desktop computer which feels hot to the touch, it may have
insufficient cooling. If this concerns you, you can buy extra cooling fans or
check that the cooling fans and vents are free from dust and other blockages.
You might want to consider putting the computer in a better-ventilated area too
— if kept in confined spaces (for example, in a cupboard), the cooling system in the
computer may not be able to remove heat and circulate cool air fast enough.</p>

<p>Some people are concerned about the health risks of using hot laptops. There
are suggestions that prolonged use of a hot laptop on your lap might possibly
reduce fertility, and there are reports of minor burns being suffered
too (in extreme cases). If you are concerned about these potential problems,
you may wish to consult a medical practitioner for advice. Of course, you can
simply choose not to rest the laptop on your lap.</p>

<p>最近のコンピューターは熱くなりすぎると損傷を防ぐために自動的にシャットダウンするようになっていることがほとんどです。シャットダウンを繰り返す場合はこれが原因である可能性があります。オーバーヒートを起している場合には、おそらく修理が必要になります。</p>

</page>
