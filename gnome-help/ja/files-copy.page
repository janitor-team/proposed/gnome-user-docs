<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="files-copy" xml:lang="ja">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-15" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>アイテムを新しいフォルダーにコピーまたは移動します。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>松澤 二郎</mal:name>
      <mal:email>jmatsuzawa@gnome.org</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>赤星 柔充</mal:name>
      <mal:email>yasumichi@vinelinux.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kentaro KAZUHAMA</mal:name>
      <mal:email>kazken3@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shushi Kurose</mal:name>
      <mal:email>md81bird@hitaki.net</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Noriko Mizumoto</mal:name>
      <mal:email>noriko@fedoraproject.org</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>坂本 貴史</mal:name>
      <mal:email>o-takashi@sakamocchi.jp</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>日本GNOMEユーザー会</mal:name>
      <mal:email>http://www.gnome.gr.jp/</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>ファイル、フォルダーをコピーまたは移動する</title>

 <p>ファイルやフォルダーは別の場所へコピーしたり移動したりできます。その方法としては、マウスによるドラッグ・アンド・ドロップ、コピーおよび貼り付けコマンド、あるいはキーボードショートカットがあります。</p>

 <p>For example, you might want to copy a presentation onto a memory stick so
 you can take it to work with you. Or, you could make a back-up copy of a
 document before you make changes to it (and then use the old copy if you don’t
 like your changes).</p>

 <p>ここで述べる説明は、ファイルとフォルダー両方に当てはまります。ファイルとフォルダーとまったく同じ方法でコピーや移動ができます。</p>

<steps ui:expanded="false">
<title>ファイルのコピーと貼り付け</title>
<item><p>コピーするアイテムをシングルクリックで選択します。</p></item>
<item><p>右クリックして<gui>コピー</gui>を選択するか、<keyseq><key>Ctrl</key><key>C</key></keyseq> を押します。</p></item>
<item><p>ファイルのコピーを配置する別のフォルダーを表示します。</p></item>
<item><p>Click the menu button and pick <gui>Paste</gui> to finish copying the
 file, or press <keyseq><key>Ctrl</key><key>V</key></keyseq>. There
 will now be a copy of the file in the original folder and the other
 folder.</p></item>
</steps>

<steps ui:expanded="false">
<title>ファイルの切り取りと貼り付けでファイルを移動する</title>
<item><p>移動するアイテムをシングルクリックで選択します。</p></item>
<item><p>右クリックして<gui>切り取り</gui>を選択するか、<keyseq><key>Ctrl</key><key>X</key></keyseq> を押します。</p></item>
<item><p>ファイルを移動する別のフォルダーを表示します。</p></item>
<item><p>Click the menu button in the toolbar and pick <gui>Paste</gui> to
 finish moving the file, or press <keyseq><key>Ctrl</key><key>V</key></keyseq>.
 The file will be taken out of its original folder and moved to the other
 folder.</p></item>
</steps>

<steps ui:expanded="false">
<title>ファイルのドラッグでコピーまたは移動する</title>
<item><p>ファイルマネージャーを開き、コピーしたいファイルのあるフォルダーを表示します。</p></item>
<item><p>トップバーの<gui>ファイル</gui>をクリックして<gui>新しいウィンドウ</gui>を選択する (あるいは<keyseq><key>Ctrl</key><key>N</key></keyseq> を押す) と、ふたつ目のウィンドウが開きます。新しいウィンドウでは、ファイルを移動またはコピーするフォルダーを表示します。</p></item>
<item>
 <p>一方のウィンドウから他方のウィンドウへファイルをドラッグします。移動先のフォルダーが<em>同じ</em>デバイス上にある場合、ファイルを<em>移動</em>します。移動先のフォルダーが<em>異なる</em>デバイス上にある場合、ファイルを<em>コピー</em>します</p>
 <p>For example, if you drag a file from a USB memory stick to your Home folder,
 it will be copied, because you’re dragging from one device to another.</p>
 <p><key>Ctrl</key> キーを押したままでドラッグすると、コピーとなり、<key>Shift</key> キーを押したままでドラッグすると、移動となります。</p>
 </item>
</steps>

<note>
  <p>You cannot copy or move a file into a folder that is <em>read-only</em>.
  Some folders are read-only to prevent you from making changes to their
  contents. You can change things from being read-only by
  <link xref="nautilus-file-properties-permissions">changing file permissions
  </link>.</p>
</note>

</page>
