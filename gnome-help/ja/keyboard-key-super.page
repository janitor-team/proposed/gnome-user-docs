<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="keyboard-key-super" xml:lang="ja">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.7.91" version="0.2" date="2013-03-16" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author">
      <name>GNOME ドキュメンテーションプロジェクト</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc><key>Super</key> キーは<gui>アクティビティ</gui>画面を開くのに使用します。通常、このキーはキーボードの <key>Alt</key> キーの隣にあります。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>松澤 二郎</mal:name>
      <mal:email>jmatsuzawa@gnome.org</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>赤星 柔充</mal:name>
      <mal:email>yasumichi@vinelinux.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kentaro KAZUHAMA</mal:name>
      <mal:email>kazken3@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shushi Kurose</mal:name>
      <mal:email>md81bird@hitaki.net</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Noriko Mizumoto</mal:name>
      <mal:email>noriko@fedoraproject.org</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>坂本 貴史</mal:name>
      <mal:email>o-takashi@sakamocchi.jp</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>日本GNOMEユーザー会</mal:name>
      <mal:email>http://www.gnome.gr.jp/</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title><key>Super</key> キーとは何ですか?</title>

  <p><key>Super</key> キーを押すと、<gui>アクティビティ</gui>画面が表示されます。通常、このキーは、キーボードの最下段左側、<key>Alt</key> キーの隣に位置しています。たいていの場合、キートップに Windows ロゴが刻印されています。<em>Windows キー</em>や、システムキーなどと呼ばれることもあります。</p>

  <note>
    <p>If you have an Apple keyboard, you will have a <key>⌘</key> (Command)
    key instead of the Windows key, while Chromebooks have a magnifying glass
    instead.</p>
  </note>

  <p><gui>アクティビティ</gui>画面を表示するキーを変更する手順は次のとおりです。</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Keyboard</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Keyboard Shortcuts</gui> section, select <gui>Customize Shortcuts</gui>.</p>
    </item>
    <item>
      <p>Select the <gui>System</gui> category.</p>
    </item>
    <item>
      <p>Click the row with <gui>Show the activities overview</gui>.</p>
    </item>
    <item>
      <p>割り当てるキーの組み合わせを押します。</p>
    </item>
  </steps>

</page>
