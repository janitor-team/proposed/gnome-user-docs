<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="color-whatisspace" xml:lang="sr">

  <info>
    <link type="guide" xref="color#profiles"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <desc>Простор боје је одређени опсег боја.</desc>

    <credit type="author">
      <name>Ричардс Хјуџиз</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Шта је то простор боје?</title>

  <p>Простор боје је одређени опсег боја. Добро познати простори боје укључују сРГБ, АдобеРГБ и ПроФотоРГБ.</p>

  <p>Људски видни систем није једноставан РГБ сензор, али можемо приближно да одредимо како се око одазива на ЦИЕ 1931 дијаграм хроматичности који приказује људски визуелни одзив у облику коњске потковице. Можете видети да у људском виду постоји много више откривених нијанси зелене него плаве и црвене. Са трикроматским простором боје као што је РГБ представљамо боје на рачунару користећи три вредности, што ограничава на кодирање <em>троугла</em> боја.</p>

  <note>
    <p>Коришћење модела као што је ЦИЕ 1931 дијаграм хроматичности је велико поједностављивање људског видног система, и стварни опсези се изражавају као 3Д трупови, уместо 2Д пројекције. 2Д пројекција 3Д облика може понекад бити варљива, зато ако желите да видите 3Д труп, користите програм <code>gcm-viewer</code>.</p>
  </note>

  <figure>
    <desc>сРГБ, АдобеРГБ и ПроФотоРГБ представљени помоћу троуглова</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/color-space.png"/>
  </figure>

  <p>Прво, гледајући сРГБ, који је мањи простор и може да кодира најмањи рој боја. То је приближност 10 година старог ЦРТ екрана, и зато већина новијих монитора може с лакоћом да приказује више боја од овога. сРГБ је стандард <em>најмањег заједничког именитеља</em> и користи се у великом броју програма (укључујући и Интернет).</p>
  <p>АдобеРГБ се често користи као <em>простор уређивања</em>. Може да кодира више боја него сРГБ, што значи да можете да измените боје у фотографији а да не бринете превише да ли су најживље боје скраћене или скрхкане црнилом.</p>
  <p>Про Фото је највећи доступан простор и често се користи за архивирање докумената. Може да кодира скоро читав опсег боја откривених људским оком, и чак кодира боје које око не може да открије!</p>

  <p>Сада, ако је Про Фото најбољи, зашто га не користимо за све? Одговор се налази у <em>квантизацији</em>. Ако имате само 8 бита (256 нивоа) за кодирање сваког канала, онда ће већи опсег имати веће кораке између сваке вредности.</p>
  <p>Већи кораци значе веће грешке између снимљене боје и причуване боје, и за неке боје ово је велики проблем. Испоставља се да су кључне боје, као боје коже, веома важне и да ће мале грешке чак и неувежбаног посматрача обавестити да на слици нешто не изгледа како треба.</p>
  <p>Наравно, коришћење 16-битне слике ће проузроковати много више корака и много мању грешку квантизације, али ово удвостручава величину сваке датотеке слике. Већина садржаја који постоји данас је 8б/т, нпр. 8 бита по тачки.</p>
  <p>Управљање бојом је процес за претварање из једног простора боје у други, где простор боје може бити врло знан одређени простор као сРГБ, или произвољан простор као што је профил вашег монитора или штампача.</p>

</page>
