<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-font-size" xml:lang="sr">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.26" date="2017-08-04" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Шон Мек Кенс</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Користите веће словне ликове да учините текст лакшим за читање.</desc>
  </info>

  <title>Измените величину текста на екрану</title>

  <p>Ако имате потешкоћа при читању текста на вашем екрану, можете да измените величину словног лика.</p>

  <steps>
    <item>
      <p>Отворите преглед <gui xref="shell-introduction#activities">Активности</gui> и почните да куцате <gui>Приступачност</gui>.</p>
    </item>
    <item>
      <p>Кликните на <gui>Приступачност</gui> да отворите панел.</p>
    </item>
    <item>
      <p>У одељку <gui>Видети</gui>, пребаците прекидач <gui>Велики текст</gui> на укључено.</p>
    </item>
  </steps>

  <p>Другачије, можете да измените величину текста тако што ћете кликнути на <link xref="a11y-icon">иконицу приступачности</link> на горњој траци и изабрати <gui>Велики текст</gui>.</p>

  <note style="tip">
    <p>У многим програмима, можете да увећате величину текста у било ком тренутку тако што ћете притиснути <keyseq><key>Ктрл</key><key>+</key></keyseq>. Да умањите величину текста, притисните <keyseq><key>Ктрл</key><key>-</key></keyseq>.</p>
  </note>

  <p><gui>Велики текст</gui> ће променити величину текста 1,2 пута. Можете да користите <app>Алат за лицкање</app> да увећате или умањите текст.</p>

</page>
