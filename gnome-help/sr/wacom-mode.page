<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-mode" xml:lang="sr">

  <info>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>
    <revision pkgversion="3.33" date="2019-07-21" status="candidate"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Пребацујте таблицу између режима таблице и режима миша.</desc>
  </info>

  <title>Подесите режим праћења Ваком таблице</title>

<p><gui>Режим праћења</gui> одређује начин мапирања оловке на екрану.</p>

<steps>
  <item>
    <p>Отворите преглед <gui xref="shell-introduction#activities">Активности</gui> и почните да куцате <gui>Ваком таблица</gui>.</p>
  </item>
  <item>
    <p>Кликните на <gui>Ваком таблица</gui> да отворите панел.</p>
  </item>
  <item>
    <p>Кликните на дугме <gui>Таблица</gui> у траци заглавља.</p>
    <note style="tip"><p>Ако таблица није откривена, од вас ће бити затражено да <gui>прикључите или упалите вашу Ваком таблицу</gui>. Кликните на везу <gui>Подешавања блутута</gui> да повежете бежичну таблицу.</p></note>
  </item>
  <item><p>Поред <gui>Режима праћења</gui>, изаберите <gui>Таблица (апсолутно)</gui> или <gui>Додирна табла (релативно)</gui>.</p></item>
</steps>

<note style="info"><p>У <em>апсолутном</em> режиму, свака тачка на таблици се мапира на тачку на екрану. Горњи леви угао екрана, на пример, увек одговара истој тачки на таблици.</p>
 <p>У <em>релативном</em> режиму, ако подигнете оловку са таблице и спустите је на друго место, показивач на екрану се не помера. Ово је начин на који ради миш.</p>
  </note>

</page>
