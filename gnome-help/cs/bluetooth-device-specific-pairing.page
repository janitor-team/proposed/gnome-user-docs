<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="bluetooth-device-specific-pairing" xml:lang="cs">

  <info>
    <link type="guide" xref="bluetooth" group="#last"/>

    <credit type="author">
      <name>Bastien Nocera</name>
      <email>hadess@hadess.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak spárovat některá speciální zařízení s počítačem.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Návody k párování speciálních zařízení</title>

  <p>I když se řídíte originálními příručkami k zařízení, nemusí obsahovat dostatek informací pro úspěšné spárování. Proto zde jsou podrobnosti pro několik nejběžnějších zařízení.</p>

  <terms>
    <item>
      <title>Ovladač PlayStation 3</title>
      <p>Tato zařízení používají „párování přes kabel“. Zasuňte ovladač do USB a přitom mějte otevřená <gui>Nastavení Bluetooth</gui> a Bluetooth zapnuté. Po zmáčknutí tlačítka „PS“ se objeví dotaz, zda ovladač nastavit. Odpojte jej a zmáčkněte tlačítko „PS“, aby se začal používat přes Bluetooth.</p>
    </item>
    <item>
      <title>Ovladač PlayStation 4</title>
      <p>I tato zařízení používají „párování přes kabel“. Zasuňte ovladač do USB a přitom mějte otevřená <gui>Nastavení Bluetooth</gui> a Bluetooth zapnuté. Dotaz, zda ovladač nastavit, se objeví sám bez potřeby mačkat tlačítko „PS“. Odpojte jej a nyní tlačítko „PS“ zmáčkněte, aby se začal používat přes Bluetooth.</p>
      <p>Pokud nemáte po ruce kabel USB, dá se u těchto zařízení použít ke zviditelnění a spárování přes Bluetooth také kombinace tlačítek „PS“ a „Share“.</p>
    </item>
    <item>
      <title>Ovladač PlayStation 3 BD</title>
      <p>Držte naráz tlačítka „Start“ a „Enter“ po dobu 5 sekund. Ovladač můžete vybrat v seznamu zařízení běžným způsobem.</p>
    </item>
    <item>
      <title>Ovladače Nintendo Wii and Wii U</title>
      <p>Ke spuštění párování použijte červené tlačítko „Sync“ ukryté v prostoru pro baterie. Jiné kombinace tlačítek informaci o spárování neuchovávají, takže byste párování museli stále znovu opakovat. Také pozor na to, že některý software chce k ovladači přímý přístup, a v takovém případě byste jej neměli nastavovat v systémovém panelu Bluetooth. Na instrukce se podívejte do příručky k dané aplikaci.</p>
    </item>
    <item>
      <title>ION iCade</title>
      <p>Proces párování spustíte přidržením zmáčknutých čtyř spodních tlačítek a horního bílého tlačítka. Až se objeví instrukce k párování, zadejte vstupní kód výhradně pomocí ovládání směru a následně jej potvrďte jedním ze dvou bílých tlačítek úplně vpravo od ovládání směru.</p>
    </item>
  </terms>

</page>
