<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-usemic" xml:lang="cs">

  <info>
    <link type="guide" xref="media#sound"/>

    <revision version="gnome:40" date="2021-02-26" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak používat analogový nebo USB mikrofon a jak vybrat výchozí vstupní zařízení.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Používání rozličných mikrofónů</title>

  <p>Můžete používat externí mikrofon pro diskutování s přáteli, pro rozhovory s kolegy v práci, pro hlasové nahrávky nebo pro nějaké jiné multimediální aplikace. Přestože váš počítač může mít zabudovaný mikrofon nebo webovou kameru s mikrofonem, externí mikrofon obvykle poskytuje lepší kvalitu zvuku.</p>

  <p>Pokud má váš mikrofon konektor s kulatým průřezem (jack), zastrčte jej do příslušného zvukového vstupu počítače. Většina počítačů má dvě zvukové zdířky: jednu pro mikrofon a druhou pro sluchátka/reproduktory. Zdířka pro mikrofon má obvykle světle červenou barvu nebo je označena symbolem mikrofonu. Po zastrčení by měl být mikrofon obvykle použit jako výchozí. Když se tak nestane, podívejte se níže jak vybrat výchozí vstupní zvukové zařízení.</p>

  <p>Pokud máte mikrofon do USB, zastrčte jej do libovolného portu USB ve vašem počítači. Takovýto mikrofon funguje jako samostatné zvukové zařízení a možná budete muset určit, že se má používat jako výchozí.</p>

  <steps>
    <title>Výběr výchozího vstupního zvukového zařízení</title>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Zvuk</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Zvuk</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>V části <gui>Vstup</gui> vyberte zařízení, které chcete používat. Indikátor úrovně by měl reagovat na váš hlas.</p>
    </item>
  </steps>

  <p>V tomto panelu můžete upravit hlasitost a vypnout mikrofon.</p>

</page>
