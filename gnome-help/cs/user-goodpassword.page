<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="user-goodpassword" xml:lang="cs">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Používejte dlouhá a složitá hesla.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Volba bezpečného hesla</title>

  <note style="important">
    <p>Vytvořte si takové heslo, které si snadno zapamatujete, ale pro ostatní (včetně počítačových programů) bude velmi obtížné jej uhodnout.</p>
  </note>

  <p>Volba dobrého hesla pomáhá udržet váš počítač v bezpečí. Když jde heslo snadno uhodnout, může na něj někdo přijít a získat přístup k vašim osobním datům.</p>

  <p>Někdo by také mohl použít počítače k systematickému hádání vašeho hesla (tzv. útok hrubou silou), takže i když by bylo těžce uhodnutelné pro člověka, může být snadno rozlousknutelné počítačem. Zde je pár rad, jak si zvolit dobré heslo:</p>
  
  <list>
    <item>
      <p>Použijte v hesle směs malých a velkých písmen, číslic, symbolů a mezer. Tím se stane těžko uhodnutelným, protože více různých znaků znamená více možných kombinací, které je potřeba při hádání hesla vyzkoušet.</p>
      <note>
        <p>Dobrou metodou pro výběr kvalitního hesla je vzít první znaky slov z nějaké fráze, kterou si dobře pamatujete. Třeba úryvek z nějaké písně, básně nebo hlášky z filmu, knihy. Například „Kams to dostal Václave? Ale do křídla, člověče.“  by mohlo dát „KtdV? Adk, c.“ nebo „KtdV?Adkc.“ nebo „KtdVAdkc“.</p>
      </note>
    </item>
    <item>
      <p>Vytvořte heslo co nejdelší. Čím více znaků obsahuje, tím déle by trvalo člověku nebo počítači jej uhodnout.</p>
    </item>
    <item>
      <p>Nepoužívejte běžná slova, která se objevují v slovnících libovolného jazyka. Takováto slova zkouší útočníci jako první. Nejběžnějším heslem je „heslo“ (respektive v angličtině „password“) – heslo tohoto typu lze uhodnout velmi rychle!</p>
    </item>
    <item>
      <p>Nepoužívejte osobní údaje, jako datum narození, registrační značka, jméno člena rodiny.</p>
    </item>
    <item>
      <p>Nepoužívejte běžná slova.</p>
    </item>
    <item>
      <p>Volte hesla, která dokážete napsat rychle, abyste snížili šance vysledovat co píšete osobou, které se poštěstilo vás při psaní hesla pozorovat.</p>
      <note style="tip">
        <p>Hesla si nikdy nikam nezapisujte. Někdo by je mohl snadno najít.</p>
      </note>
    </item>
    <item>
      <p>Pro různé věci používejte různá hesla.</p>
    </item>
    <item>
      <p>Pro různé účty používejte různá hesla.</p>
      <p>Pokud použijete stejné heslo ke všem svým účtům, může se kdokoliv, kdo heslo zjistí, dostat ihned do všech vašich účtů.</p>
      <p>Může být obtížné zapamatovat si spoustu hesel. I když nejbezpečnější je používat pro každou věc jiné heslo, můžete si to usnadnit tím, že použijete to stejné heslo pro nekritické věci (jako třeba pro některé weby) a jiná hesla pro věci důležité (jako internetové bankovnictví a váš e-mail).</p>
   </item>
   <item>
     <p>Svá hesla pravidelně měňte.</p>
   </item>
  </list>

</page>
