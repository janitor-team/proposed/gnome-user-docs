<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sharing-bluetooth" xml:lang="cs">

  <info>
    <link type="guide" xref="bluetooth"/>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.8.2" date="2013-05-13" status="draft"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-08" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="author">
      <name>David King</name>
      <email>davidk@gnome.org</email>
      <years>2014 – 2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak umožnit nahrávání souborů na váš počítač přes Bluetooth.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Ovládání sdílení po Bluetooth</title>

  <p>Můžete povolit sdílení přes <gui>Bluetooth</gui>, abyste mohli přes Bluetooth přijímat soubory do složky <file>Stažené</file>.</p>

  <steps>
    <title>Povolení sdílení souborů do složky <file>Stažené</file></title>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Bluetooth</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>Ujistěte se, že je <link xref="bluetooth-turn-on-off"><gui>Bluetooth</gui> zapnuté</link>.</p>
    </item>
    <item>
      <p>Zařízení se zapnutým Bluetooth mohou posílat soubory do vaší složky <file>Stažené</file>, jen když je panel <gui>Bluetooth</gui> otevřený.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Název svého počítače, který se ukazuje jiným zařízením, můžete <link xref="sharing-displayname">změnit</link>.</p>
  </note>

</page>
