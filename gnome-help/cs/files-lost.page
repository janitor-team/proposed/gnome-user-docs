<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-lost" xml:lang="cs">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Řiďte se těmito radami, když potřebujete najít soubory, které jste vytvořili nebo stáhli.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Vyhledání ztracených souborů</title>

<p>Když vytvoříte nebo stáhnete soubor, ale nemůžete jej pak najít, řiďte se následujícími radami.</p>

<list>
  <item><p>Pokud si nepamatujete, kam jste soubor uložili, ale máte aspoň představu, jak se nazývá, můžete dotyčný <link xref="files-search">soubor vyhledat podle názvu</link>.</p></item>

  <item><p>Pokud jste soubor stáhli, mohl jej webový prohlížeč automaticky uložit do složky k tomu určené. Podívejte se do složky <file>Stažené</file> nebo <file>Plocha</file> ve své domovské složce.</p></item>

  <item><p>Soubor jste si mohli nechtěně smazat. Když soubor smažete, přesune se do koše, kde zůstává, dokud koš ručně nevysypete. Viz <link xref="files-recover"/> o tom, jak obnovit smazaný soubor.</p></item>

  <item><p>Mohli jste soubor přejmenovat tak, že je teď skrytý. Soubory začínající <file>.</file> nebo končící <file>~</file> jsou ve správci souborů skryté. Klikněte v <app>Souborech</app> na nástrojové liště na tlačítko voleb zobrazení a zvolte <gui>Zobrazovat skryté soubory</gui>, aby se zobrazily. Více viz <link xref="files-hidden"/>.</p></item>
</list>

</page>
