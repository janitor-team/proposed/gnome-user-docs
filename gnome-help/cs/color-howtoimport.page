<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-howtoimport" xml:lang="cs">

  <info>
    <link type="guide" xref="color#profiles"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-assignprofiles"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="review"/>
    <revision pkgversion="3.28" date="2018-04-05" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Profily barev lze naimportovat jejich otevřením.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Jak mohu naimportovat profily barev?</title>

  <p>Profil barev můžete naimportovat tím, že dvojitě kliknete na soubor <file>.ICC</file> nebo <file>.ICM</file> ve správci souborů.</p>

  <p>Případně můžete své profily barev spravovat v panelu <gui>Barvy</gui>.</p>

  <steps>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Nastavení</gui>.</p>
    </item>
    <item>
      <p>Klikněte na <gui>Nastavení</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Barvy</gui> v postranním panelu otevřete příslušný panel.</p>
    </item>
    <item>
      <p>Vyberte své zařízení.</p>
    </item>
    <item>
      <p>Klikněte na <gui>Přidat profil</gui> pro výběr stávajícího profilu nebo import nového profilu.</p>
    </item>
    <item>
      <p>Zmáčknutím <gui>Přidat</gui> svoji volbu potvrdíte.</p>
    </item>
  </steps>

  <p>Výrobce může mít podporu v podobě profilu, který můžete použít. Tyto profily jsou ale obvykle vytvořeny pro průměrný displej, takže nejsou přesné pro ten váš konkrétní kus. Z důvodu lepší kalibrace byste si měli <link xref="color-calibrate-screen">vytvořit svůj vlastní profil</link> pomocí kolorimetru nebo spektrofotometru.</p>

</page>
