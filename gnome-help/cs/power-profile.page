<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-profile" xml:lang="cs">

  <info>
    <link type="guide" xref="power#saving"/>
    <link type="seealso" xref="display-brightness"/>

    <revision version="gnome:40" date="2021-03-21" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2021</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak vyvážit výkon vs. využití baterie.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Volba profilu napájení</title>

  <p>Pomocí výběru <gui>Režimu napájení</gui> máte možnost spravovat využití energie. Připravené jsou tři různé profily:</p>
  
  <list>
    <item>
      <p><gui>Rovnováha</gui>: Standardní výkon i využití energie. Jedná se o výchozí nastavení.</p>
    </item>
    <item>
      <p><gui>Úspora energie</gui>: Snižuje výkon, ale sníží se tím spotřeba energie. Cílem nastavení je co nejdelší výdrž baterie. Může zapnout některé volby šetřící energii a znemožnit vám je vypnout.</p>
    </item>
    <item>
      <p><gui>Výkon</gui>: Vysoký výkon za cenu vysoké spotřeby energie. Tento režim je viditelný pouze, pokud jej počítač podporuje. Vybrat jej lze jen v situaci, kdy je počítač napájen z elektrické sítě. Pokud váše zařízení umí rozpoznat, že jej máte na klíně, místo na stole, nedovolí vám tento režim zvolit ve chvíli, kdy se nachází na klíně.</p>
    </item>
  </list>

  <steps>
    <title>Když si chcete zvolit profil napájení:</title>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Napájení</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Napájení</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>V části <gui>Režim napájení</gui> vyberte jeden z profilů.</p>
    </item>
    
  </steps>

</page>
