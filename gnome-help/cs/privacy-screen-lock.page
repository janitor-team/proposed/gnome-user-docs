<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-screen-lock" xml:lang="cs">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="seealso" xref="session-screenlocks"/>
    <link type="seealso" xref="shell-exit#lock-screen"/>

    <revision pkgversion="3.8" date="2013-05-21" status="candidate"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>
    <revision pkgversion="3.38.1" date="2020-11-22" status="final"/>
    <revision pkgversion="3.38.4" date="2020-03-07" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit>
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak zabránit ostatním lidem v používání vašeho systému, když nejste u počítače.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Automatické zamykání obrazovky</title>
  
  <p>Když odcházíte od svého počítače, měli byste <link xref="shell-exit#lock-screen">zamknout obrazovku</link>, abyste zabránili ostatním lidem v používání vašeho uživatelského prostředí a v přístupu k vašim souborům. Pokud na to zapomínáte, můžete si přát, aby se zamykání obrazovky dělo automaticky po nějaké době. Pomůže vám to udržet svůj počítač zabezpečený, když jej nepoužíváte.</p>

  <note><p>Po dobu, co je obrazovka zamknutá, vaše aplikace a systémové procesy pokračují v běhu, ale abyste je mohli znovu používat, musíte zadat heslo.</p></note>
  
  <steps>
    <title>Jak nastavit dobu, po jejímž uplynutí se obrazovka automaticky uzamkne:</title>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Zamykání obrazovky</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Zamykání obrazovky</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>Ujistěte se, že <gui>Automatické zamknutí obrazovky</gui> je přepnuté do polohy zapnuto a pak vyberte v rozbalovacím seznamu <gui>Prodleva automatického zamknutí obrazovky</gui> časovou prodlevu.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Aplikace vám i na dále mohou zasílat upozornění, která se zobrazují na uzamknuté obrazovce. To je pro vaše pohodlí, abyste například viděli i bez odemknutí obrazovky, že vám přišla pošta. Jestli máte obavu, že ostatní lidé tato upozornění uvidí, přepněte vypínač <gui>Zobrazovat upozornění na uzamknuté obrazovce</gui> do polohy vypnuto. Další nastavení týkající se upozornění najdete v <link xref="shell-notifications"/>.</p>
  </note>

  <p>Když je obrazovka zamknutá a chcete ji odemknout, zmáčkněte <key>Esc</key> nebo ji vytáhněte myší zdola nahoru. Pak zadejte heslo a zmáčkněte <key>Enter</key> nebo klikněte na <gui>Odemknout</gui>. Případně stačí začít psát heslo a opona se zvedne automaticky.</p>

</page>
