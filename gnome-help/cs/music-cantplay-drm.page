<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="music-cantplay-drm" xml:lang="cs">
  <info>
    <link type="guide" xref="media#music"/>


    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <desc>Možná není nainstalovaná podpora pro daný formát souboru nebo jsou skladby „chráněny proti kopírování“.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Nemohu přehrát písničky, které jsem koupil on-line v obchodě s hudbou</title>

<p>Když si stáhnete nějakou hudbu z obchodu on-line, můžete narazit na to, že nepůjde na počítači přehrát, zvláště pokud jste ji koupili ve Windows nebo Mac OS a pak ji překopírovali.</p>

<p>To může být tím, že je hudba ve formátu, který váš počítač nerozpozná. Abyste mohli písničku přehrát, potřebujete mít nainstalovanou podporu příslušného zvukového formátu – například, když chcete přehrát soubor MP3, musíte mít nainstalovanou podporu pro MP3. Pokud podporu pro daný formát zvuku nainstalovanou nemáte, měli byste uvidět zprávu, která vás na to upozorní. Tato zpráva by vám měla poskytnout i instrukce, jak podporu pro tento formát nainstalovat, aby přehrávání fungovalo.</p>

<p>Pokud máte podporu zvukového formátu, ve kterém je skladba uložená, nainstalovanou a stále skladbu nemůžete přehrát, může mít skladba <em>ochranu proti kopírování</em> (známou také jako <em>omezení pomocí DRM</em>). DRM je způsob, jak omezit, kdo může skladbu přehrát a na jakém zařízení ji může přehrát. A pravidla určuje vydavatelská společnost, ne vy. V případě, že má hudební soubor omezení pomocí DRM, nebudete jej nejspíše moci přehrát – obecně potřebujete specializovaný software od konkrétního výrobce, abyste mohli přehrávat soubory s omezením pomocí DRM, ale tento software často není na Linuxu podporován.</p>

<p>O DRM se můžete více dozvědět od <link href="https://www.eff.org/issues/drm">Electronic Frontier Foundation</link> (odkaz je v angličtině).</p>

</page>
