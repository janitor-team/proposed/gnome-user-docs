<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-formats" xml:lang="cs">

  <info>
    <link type="guide" xref="prefs-language"/>
    <link type="seealso" xref="session-language"/>

    <revision pkgversion="3.10" version="0.4" date="2013-11-03" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.38.4" date="2021-03-11" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak vybrat region určující formát data, času a čísel, měnu a jednotky.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Změna formátů data a měrných jednotek</title>

  <p>Můžete určit formáty, které se používají pro datum, čas a čísla, měnu a měrné jednotky, aby odpovídaly místním zvyklostem ve vašem regionu.</p>

  <steps>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Region a jazyk</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Region a jazyk</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>Klikněte na <gui>Formáty</gui>.</p>
    </item>
    <item>
      <p>V <gui>Běžných formátech</gui> nebo <gui>Všech formátech</gui> vyberte region a jazyk, který nejlépe odpovídá formátům, které byste chtěli používat.</p>
    </item>
    <item>
      <p>Kliknutím na <gui style="button">Hotovo</gui> nastavení uložíte.</p>
    </item>
    <item>
      <p>Aby se změny projevily, je nutné restartovat sezení. Buď klikněte na <gui style="button">Restartovat…</gui> nebo se později ručně odhlašte a znovu přihlašte.</p>
    </item>
  </steps>

  <p>Po té, co vyberete region, uvidíte v pravé části zobrazený seznam různých příkladů, jak se budou data a jiné údaje zobrazovat. Ačkoliv to není uvedeno v příkladech, výběr regionu ovlivňuje také první den týdne v kalendáři.</p>

  <note style="tip">
    <p>Pokud v systému existuje více uživatelských účtů, je v panelu <gui>Region a jazyk</gui> zvláštní část pro přihlašovací obrazovku. Kliknutím na tlačítko <gui>Přihlašovací obrazovka</gui> v pravém horním rohu se přepínáte mezi oběma nastaveními.</p>
  </note>

</page>
