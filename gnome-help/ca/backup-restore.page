<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="backup-restore" xml:lang="ca">

  <info>
    <link type="guide" xref="files#backup"/>
    <desc>Com recuperar els fitxers d'una còpia de seguretat.</desc>
    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit>
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>Recuperar una còpia de seguretat</title>

  <p>Si heu perdut o esborrat alguns dels vostres fitxers, però en teniu una còpia de seguretat, podeu restaurar-los des de la còpia:</p>

<list>
 <item><p>Si vàreu copiar els fitxers a una ubicació segura, com un disc dur extern, un altre ordinador de la xarxa o un llapis USB, podeu <link xref="files-copy">copiar-los</link> una altra vegada cap al vostre ordinador.</p></item>

 <item><p>Si heu creat la vostra còpia de seguretat usant una aplicació de còpia de seguretat com ara <app>Déjà Dup</app>, es recomana que utilitzeu la mateixa aplicació per a restaurar-la. Reviseu l'ajuda de l'aplicació del vostre programa de còpia de seguretat: us proporcionarà instruccions específiques sobre com restaurar els fitxers.</p></item>
</list>

</page>
