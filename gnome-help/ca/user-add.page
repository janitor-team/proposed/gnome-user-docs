<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-add" xml:lang="ca">

  <info>
    <link type="guide" xref="user-accounts#manage" group="#first"/>

    <revision pkgversion="3.8" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2013-11-01" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Afegiu usuaris nous perquè altres persones puguin iniciar sessió a l'ordinador.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Afegir un nou compte d'usuari</title>

  <p>Podeu afegir diversos comptes d'usuari a l'ordinador. Doneu un compte a cada persona de la vostra llar o empresa. Cada usuari té la seva pròpia carpeta d'inici, documents i configuració.</p>

  <p>Heu de tenir <link xref="user-admin-explain">privilegis d'administrador</link> per a afegir comptes d'usuari.</p>

  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Usuaris</gui>.</p>
    </item>
    <item>
      <p>Feu clic a <gui>Usuaris</gui> per a obrir el quadre.</p>
    </item>
    <item>
      <p>Premeu <gui style="button">Desbloqueja</gui> a l'extrem superior dret i escriviu la vostra contrasenya quan se us demani.</p>
    </item>
    <item>
      <p>Premeu el botó <gui style="button">+</gui>, a sota de la llista de comptes de l'esquerra, per a afegir un nou compte d'usuari.</p>
    </item>
    <item>
      <p>Si voleu que el nou usuari tingui <link xref="user-admin-explain">accés d'administrador</link> a l'ordinador, seleccioneu <gui>Administrador</gui> al tipus de compte.</p>
      <p>Els administradors poden fer coses com afegir o eliminar usuaris, instal·lar programari i controladors, i canviar la data i l'hora.</p>
    </item>
    <item>
      <p>Introduïu el nom complet del nou usuari. El nom d'usuari s'omplirà automàticament en funció del nom complet. Si no us agrada el nom d'usuari proposat, podeu canviar-lo.</p>
    </item>
    <item>
      <p>Podeu establir una contrasenya per al nou usuari, o bé deixar-los configurar ells mateixos en el primer inici de sessió.</p>
      <p>Si decidiu establir la contrasenya ara, podeu prémer la icona <gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">generar contrasenya</span></media></gui> per a generar automàticament una contrasenya aleatòria.</p>
    </item>
    <item>
      <p>Feu clic a <gui>Afegeix</gui>.</p>
    </item>
  </steps>

  <p>Si voleu canviar la contrasenya després de crear el compte, seleccioneu el compte, <gui style="button">Desbloquegeu</gui> el quadre i premeu l'estat actual de la contrasenya.</p>

  <note>
    <p>Al quadre <gui>Usuaris</gui>, podeu fer clic a la imatge a l'esquerra del nom de l'usuari per a configurar una imatge per al compte. Aquesta imatge es mostrarà a la finestra d'inici de sessió. Podeu utilitzar alguna de les imatges d'arxiu proporcionades pel sistema, seleccionar-ne una vostra o fer una fotografia amb la càmera web.</p>
  </note>

</page>
