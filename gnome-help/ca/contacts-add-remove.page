<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="contacts-add-remove" xml:lang="ca">

  <info>
    <link type="guide" xref="contacts"/>
    <revision pkgversion="3.5.5" date="2012-08-13" status="review"/>
    <revision pkgversion="3.8" date="2013-04-27" status="review"/>
    <revision pkgversion="3.12" date="2014-02-26" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.38.0" date="2020-10-31" status="review"/>

    <credit type="author">
      <name>Lucie Hankey</name>
      <email>ldhankey@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Pranali Deshmukh</name>
      <email>pranali21293@gmail.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Afegiu o elimineu un contacte a la llibreta d'adreces local.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>Afegir o eliminar un contacte</title>

  <p>Per a afegir un contacte:</p>

  <steps>
    <item>
      <p>Premeu el botó <gui style="button">+</gui>.</p>
    </item>
    <item>
      <p>En el diàleg <gui>Contacte nou</gui>, introduïu el nom del contacte i la seva informació. Premeu la llista desplegable al costat de cada camp per a triar el tipus de detall.</p>
    </item>
    <item>
      <p>Per a afegir més detalls premeu l'opció <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">Veure'n més</span></media>.</p>
    </item>
    <item>
      <p>Premeu <gui style="button">Afegiu</gui> per a desar el contacte.</p>
    </item>
  </steps>

  <p>Per a eliminar un contacte:</p>

  <steps>
    <item>
      <p>Trieu el contacte de la llista de contactes.</p>
    </item>
    <item>
      <p>Premeu el botó <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">Veure'n més</span></media> a la barra de capçalera a la cantonada superior dreta.</p>
    </item>
    <item>
      <p>Premeu l'opció <gui style="menu item">Suprimeix</gui> per a eliminar el contacte.</p>
    </item>
   </steps>
    <p>Per a eliminar un o més contactes, marqueu les caselles al costat dels contactes que voleu suprimir i premeu <gui style="button">Suprimeix</gui>.</p>
</page>
