<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="get-involved" xml:lang="ca">

  <info>
    <link type="guide" xref="more-help"/>
    <desc>Com i on informar dels problemes amb aquests temes d'ajuda.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>
  <title>Participar en la millora d'aquesta guia</title>

  <section id="submit-issue">

   <title>Enviar un problema</title>

   <p>Aquesta documentació d'ajuda està creada per una comunitat de voluntaris. Us convidem a participar-hi. Si observeu un problema amb aquestes pàgines d'ajuda (com errors tipogràfics, instruccions incorrectes o temes que s'han de cobrir però que no ho estan), podeu enviar un <em>informe d'error</em>. Per a enviar un informe d'error, aneu al <link href="https://gitlab.gnome.org/GNOME/gnome-user-docs/issues">gestor d'errors</link>.</p>

   <p>Cal registrar-se per a poder presentar un informe d'error i rebre actualitzacions per correu electrònic sobre el seu estat. Si encara no teniu un compte, feu clic al botó <gui><link href="https://gitlab.gnome.org/users/sign_in">Sign in / Register</link></gui> per a crear-ne un.</p>

   <p>Un cop tingueu un compte, inicieu la sessió, feu clic a <guiseq><gui>Notificar un error</gui><gui>Cor</gui><gui>gnome-user-docs</gui></guiseq>. Abans de notificar un error, llegeix el fitxer <link href="https://bugzilla.gnome.org/page.cgi?id=bug-writing.html">pautes d'escriptura d'errors</link>, i, si us plau <link href="https://bugzilla.gnome.org/browse.cgi?product=gnome-user-docs">cerqueu</link> l'error per a veure si ja hi ha alguna cosa semblant.</p>

   <p>Per a notificar el vostre error, trieu l'etiqueta al menú <gui>Labels</gui>. Si esteu emplenant un error d'aquesta documentació, heu de triar l'etiqueta <gui>gnome-help</gui>. Si no esteu segur de l'etiqueta que cal assignar a l'error, trieu <gui>general</gui>.</p>

   <p>Si sol·liciteu ajuda sobre un tema que considereu que no està cobert, trieu <gui>Feature</gui> com a etiqueta. Empleneu les seccions resum i descripció, i feu clic a <gui>Submit issue</gui>.</p>

   <p>La vostra qüestió tindrà un número d'identificació i el seu estat s'actualitzarà a mesura que es vagi gestionant. Gràcies per a ajudar a millorar l'Ajuda del GNOME!</p>

   </section>

   <section id="contact-us">
   <title>Contacta'ns</title>

   <p>Podeu enviar un <link href="mailto:gnome-doc-list@gnome.org">correu electrònic</link> a la llista de correu «docs» de GNOME per a obtenir més informació sobre com involucrar-se amb l'equip de documentació.</p>

   </section>
</page>
