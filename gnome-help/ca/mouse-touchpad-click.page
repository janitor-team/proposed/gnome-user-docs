<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-touchpad-click" xml:lang="ca">

  <info>
    <link type="guide" xref="mouse"/>

    <revision pkgversion="3.7" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-29" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <!--
    For 41: https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/121
    -->
    <revision version="gnome:40" date="2021-03-18" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Fer clic, arrossegar o desplaçar-vos fent servir tocs i gestos al tauler tàctil.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Fer clic, arrossegar o desplaçar-vos amb el ratolí tàctil</title>

  <p>Podeu fer clic, doble clic, arrossegar i desplaçar només fent servir el ratolí tàctil, sense separar necessitar botons del maquinari.</p>

  <note>
    <p>Els <link xref="touchscreen-gestures">gestos del ratolí tàctil</link> es recullen per separat.</p>
  </note>

<section id="tap">
  <title>Fer un toc per a fer un clic</title>

  <media src="figures/touch-tap.svg" its:translate="no" style="floatend"/>

  <p>Podeu tocar el ratolí tàctil per a fer clic en comptes d'utilitzar un botó.</p>

  <list>
    <item>
      <p>Per a fer clic, toqueu sobre el ratolí tàctil.</p>
    </item>
    <item>
      <p>Per a fer doble clic, toqueu dues vegades.</p>
    </item>
    <item>
      <p>Per a arrossegar un element, toqueu dos cops però no aixequeu el dit després del segon toc. Arrossegueu l'element on vulgueu i, a continuació, aixequeu el dit per a deixar-lo anar.</p>
    </item>
    <item>
      <p>Si el vostre tauler tàctil és compatible amb tocs simultanis, feu clic amb el botó dret fent clic amb dos dits alhora. En cas contrari, encara haureu d'utilitzar botons del maquinari per a fer clic amb el botó dret. Veure <link xref="a11y-right-click"/> per a un mètode per a fer el clic dret sense el segon botó del ratolí.</p>
    </item>
    <item>
      <p>Si el vostre tauler tàctil dona suport a tocs simultanis, <link xref="mouse-middleclick">Feu clic amb el botó del mig</link> tocant-lo amb tres dits a la vegada.</p>
    </item>
  </list>

  <note>
    <p>Quan toqueu o arrossegueu amb diversos dits, assegureu-vos que els vostres dits estiguin ben separats. Si els vostres dits estan massa a prop, l'equip pot pensar que és un sol dit.</p>
  </note>

  <steps>
    <title>Habilitar fes un toc per a fer un clic</title>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Ratolí i ratolí tàctil</gui>.</p>
    </item>
    <item>
      <p>Feu clic sobre <gui>Ratolí i ratolí tàctil</gui> per a obrir el quadre.</p>
    </item>
    <item>
      <p>A la secció <gui>Tauler tàctil</gui>, assegureu-vos que l'interruptor <gui>Tauler tàctil</gui> està activat.</p>
      <note>
        <p>La secció <gui>Panell tàctil</gui> només apareix si el seu sistema té un tauler tàctil.</p>
      </note>
    </item>
   <item>
      <p>Commuteu <gui>Fes un toc per a fer un clic</gui> a <gui>ON</gui>.</p>
    </item>
  </steps>
</section>

<section id="twofingerscroll">
  <title>Desplaçar amb dos dits</title>

  <media src="figures/touch-scroll.svg" its:translate="no" style="floatend"/>

  <p>Podeu desplaçar-vos usant el vostre tauler tàctil amb dos dits.</p>

  <p>Quan estigui seleccionat, tocar i arrossegar amb un dit funcionarà de manera normal, però si arrossegueu dos dits a través de qualsevol part del tauler tàctil, es desplaçarà. Moveu els dits entre la part superior i inferior del tauler tàctil per a desplaçar-vos cap amunt i cap avall, o moveu els dits per sobre del tauler tàctil per a desplaçar-vos de costat. Aneu amb compte de deixar els dits una mica separats. Si els vostres dits estan molt a prop, pot semblar un sol dit.</p>

  <note>
    <p>Desplaçar amb dos dits pot no funcionar a tots els taulers tàctils.</p>
  </note>

  <steps>
    <title>Habilitar el desplaçament amb dos dits</title>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Ratolí i ratolí tàctil</gui>.</p>
    </item>
    <item>
      <p>Feu clic sobre <gui>Ratolí i ratolí tàctil</gui> per a obrir el quadre.</p>
    </item>
    <item>
      <p>A la secció <gui>Tauler tàctil</gui>, assegureu-vos que l'interruptor <gui>Tauler tàctil</gui> està activat.</p>
    </item>
    <item>
      <p>Canvieu <gui>Desplaçament amb dos dits</gui> a <gui>ON</gui>.</p>
    </item>
  </steps>
</section>

<section id="contentsticks">
  <title>Desplaçament natural</title>

  <p>Podeu arrossegar el contingut com si féssiu lliscar un tros de paper utilitzant el tauler tàctil.</p>

  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Ratolí i ratolí tàctil</gui>.</p>
    </item>
    <item>
      <p>Feu clic sobre <gui>Ratolí i ratolí tàctil</gui> per a obrir el quadre.</p>
    </item>
    <item>
      <p>A la secció <gui>Tauler tàctil</gui>, assegureu-vos que el commutador <gui>Tauler tàctil</gui> estigui a activat.</p>
    </item>
    <item>
      <p>Canvieu el <gui>Desplaçament natural</gui> a activat.</p>
    </item>
  </steps>

  <note>
    <p>Aquesta funcionalitat també es coneix com a <em>Desplaçament invers</em>.</p>
  </note>

</section>

</page>
