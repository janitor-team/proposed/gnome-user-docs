<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-delete" xml:lang="ca">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="user-add"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Elimineu els usuaris que ja no usin l'ordinador.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Esborrar un compte d'usuari</title>

  <p>Podeu <link xref="user-add">afegir múltiples comptes d'usuari a l'ordinador</link>. Si algú ja no fa servir l'ordinador, podeu eliminar el compte d'aquest usuari.</p>

  <p>Necessiteu <link xref="user-admin-explain">privilegis d'administrador</link> per a esborrar comptes d'usuari.</p>

  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Usuaris</gui>.</p>
    </item>
    <item>
      <p>Feu clic <gui>Usuaris</gui> per a obrir el quadre.</p>
    </item>
    <item>
      <p>Premeu <gui style="button">Desbloqueja</gui> a l'extrem superior dret i escriviu la vostra contrasenya quan se us demani.</p>
    </item>
    <item>
      <p>Seleccioneu l'usuari que voleu suprimir i premeu el botó <gui style="button">-</gui> a sota de la llista de comptes de l'esquerra, per a eliminar aquest compte d'usuari.</p>
    </item>
    <item>
      <p>Cada usuari té la seva pròpia carpeta d'inici per als seus fitxers i configuracions. Podeu triar mantenir o eliminar la carpeta d'inici de l'usuari. Feu clic a <gui>Suprimeix fitxers</gui> si esteu segur que ja no s'utilitzaran i necessiteu alliberar espai al disc. Aquests fitxers se suprimeixen permanentment. No es poden recuperar. És possible que vulgueu fer una còpia de seguretat dels fitxers en un dispositiu d'emmagatzematge extern abans de suprimir-los.</p>
    </item>
  </steps>

</page>
