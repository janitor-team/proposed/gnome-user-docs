<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="net-what-is-ip-address" xml:lang="ca">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Una adreça IP és com un número de telèfon per a l'ordinador.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Què és una adreça IP?</title>

  <p>“Adreça IP” fa referència a <em>Adreça Internet</em>, i cada dispositiu connectat a una xarxa (com Internet) té una.</p>

  <p>Una adreça IP és similar al vostre número de telèfon. El vostre número de telèfon és un conjunt únic de números que identifica el telèfon perquè altres persones us puguin trucar. De la mateixa manera, una adreça IP és un conjunt únic de números que identifica l'ordinador perquè pugui enviar i rebre dades amb altres equips.</p>

  <p>Actualment, la majoria d'adreces IP consisteixen en quatre conjunts de números, cadascun separats per un punt. <code>192.168.1.42</code> és un exemple d'adreça IP.</p>

  <note style="tip">
    <p>Una adreça IP pot ser <em>dinàmica</em> o <em>estàtica</em>. Les adreces IP dinàmiques s'assignen temporalment cada vegada que l'ordinador es connecta a una xarxa. Les adreces IP estàtiques són fixes i no canvien. Les adreces IP dinàmiques són més freqüents que les adreces estàtiques: les adreces estàtiques només solen utilitzar-se quan es necessiten especialment, com en l'administració d'un servidor.</p>
  </note>

</page>
