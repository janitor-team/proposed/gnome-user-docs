<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="sharing-media" xml:lang="ca">

  <info>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.10" version="0.2" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Compartiu els fitxers multimèdia a la vostra xarxa local usant UPnP.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Compartir la vostra música, fotografies i vídeos</title>

  <p>Podeu navegar, cercar i reproduir els fitxers multimèdia de l'ordinador mitjançant un dispositiu <sys>UPnP</sys> o <sys>DLNA</sys> activat com un telèfon, TV o consola de joc. Configureu <gui>Compartició multimèdia</gui> per a permetre que aquests dispositius accedeixin a les carpetes que contenen música, fotos i vídeos.</p>

  <note style="info package">
    <p>Heu de tenir el paquet <app>Rygel</app> instal·lat perquè <gui>Compartició multimèdia</gui> sigui visible.</p>

    <if:choose xmlns:if="http://projectmallard.org/if/1.0/">
      <if:when test="action:install">
        <p><link action="install:rygel" style="button">Instal·la Rygel</link></p>
      </if:when>
    </if:choose>
  </note>

  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Compartició</gui>.</p>
    </item>
    <item>
      <p>Feu clic a <gui>Compartició</gui> per a obrir el quadre.</p>
    </item>
    <item>
      <p>Si el commutador <gui>Compartició</gui> a la part superior dreta de la finestra està desactivat, activeu-lo.</p>

      <note style="info"><p>Si el text a continuació del <gui>Nom de l'ordinador</gui> us permet editar-lo, podeu <link xref="sharing-displayname">canviar</link> el nom del vostre equip que es mostra a la xarxa.</p></note>
    </item>
    <item>
      <p>Seleccioneu <gui>Compartició multimèdia</gui>.</p>
    </item>
    <item>
      <p>Canvieu <gui>Compartició multimèdia</gui> a <gui>ON</gui>.</p>
    </item>
    <item>
      <p>Per defecte, <file>Música</file>, <file>Imatges</file> i <file>Vídeos</file> es comparteixen. Per a eliminar-ne algun, feu clic a <gui>×</gui> al costat del nom de la carpeta.</p>
    </item>
    <item>
      <p>Per a afegir una altra carpeta, feu clic a <gui style="button">+</gui> per a obrir la finestra <gui>Trieu una carpeta</gui>. Navegueu <em>dins de</em> la carpeta desitjada i feu clic a <gui style="button">Obre</gui>.</p>
    </item>
    <item>
      <p>Feu clic a <gui style="button">×</gui>. Ara podreu navegar o reproduir els arxius multimèdia a les carpetes que heu seleccionat mitjançant un dispositiu extern.</p>
    </item>
  </steps>

  <section id="networks">
  <title>Xarxes</title>

  <p>La secció <gui>Xarxes</gui> llista les xarxes a les quals esteu connectat actualment. Utilitzeu el commutador al costat de cadascuna per a triar en quina es pot compartir el vostre media.</p>

  </section>

</page>
