<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-share" xml:lang="ca">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>
    <link type="guide" xref="sharing"/>
    <link type="seealso" xref="nautilus-connect"/>

    <revision pkgversion="3.8.2" version="0.3" date="2013-05-11" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Transferiu fitxers fàcilment als vostres contactes de correu electrònic des del gestor de fitxers.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>Compartir els fitxers per correu electrònic</title>

<p>Podeu compartir fitxers amb els vostres contactes per correu electrònic directament des del gestor de fitxers.</p>

  <note style="important">
    <p>Abans de començar, assegureu-vos que <app>Evolution</app> o <app>Geary</app> estan instal·lats a l'ordinador i el vostre compte de correu electrònic està configurat.</p>
  </note>

<steps>
  <title>Per a compartir un fitxer per correu electrònic:</title>
    <item>
      <p>Obriu l'aplicació <app>Fitxers </app> des de la vista general d'<gui xref="shell-introduction#activities">Activitats</gui>.</p>
    </item>
  <item><p>Localitzeu el fitxer que voleu transferir.</p></item>
    <item>
      <p>Feu clic sobre el fitxer amb el botó de la dreta i seleccioneu <gui>Envia a…</gui>. Apareixerà una finestra on redactar el correu electrònic amb el fitxer adjunt.</p>
    </item>
  <item><p>Feu clic <gui>A</gui> per a triar un contacte, o introduïu una adreça de correu electrònic on vulgueu enviar el fitxer. Ompliu <gui>Assumpte</gui> i el cos del missatge segons sigui necessari i feu clic a <gui>Envia</gui>.</p></item>
</steps>

<note style="tip">
  <p>Podeu enviar diversos fitxers alhora. Seleccioneu-los mantenint premut <key>Ctrl</key> mentre feu clic als fitxers, aleshores feu clic amb el botó dret a qualsevol fitxer seleccionat.</p>
</note>

</page>
