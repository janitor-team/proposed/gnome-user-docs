<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="clock-calendar" xml:lang="ca">

  <info>
    <link type="guide" xref="clock"/>
    <link type="guide" xref="shell-overview#desktop"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="review"/>
    <revision pkgversion="3.16" date="2015-03-02" status="outdated"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mostreu les vostres cites a l'àrea del calendari a la part superior de la pantalla.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

 <title>Cites de calendari</title>

  <note>
    <p>Això requereix que utilitzeu el calendari des de l'<app>Evolution</app> o el <app>Calendar</app>, o que tingueu configurat un compte en línia compatible amb el <gui>Calendar</gui>.</p>
    <p>La majoria de distribucions inclouen almenys un d'aquests programes instal·lat per defecte. Si no és així, és possible que hàgiu d'instal·lar-lo utilitzant el gestor de paquets de la vostra distribució.</p>
 </note>

  <p>Per a veure les vostres cites:</p>
  <steps>
    <item>
      <p>Feu clic al rellotge del quadre superior.</p>
    </item>
    <item>
      <p>Feu clic a la data per la qual voleu veure les vostres cites del calendari.</p>

    <note>
       <p>Es mostra un punt sota cada data que té una cita.</p>
    </note>

      <p>Les cites existents es mostraran a l'esquerra del calendari. A mesura que s'afegeixin cites al calendari de l'<app>Evolution</app> o al <app>Calendar</app>, apareixeran a la llista de cites del rellotge.</p>
    </item>
  </steps>

 <if:choose>
 <if:when test="!platform:gnome-classic">
 <media type="image" src="figures/shell-appts.png" width="500">
  <p>Rellotge, calendari i cites</p>
 </media>
 </if:when>
 <if:when test="platform:gnome-classic">
 <media type="image" src="figures/shell-appts-classic.png" width="373" height="250">
  <p>Rellotge, calendari i cites</p>
 </media>
 </if:when>
 </if:choose>

</page>
