<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-icon" xml:lang="ca">

  <info>
    <link type="guide" xref="a11y"/>

    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>El menú d'accés universal és la icona de la barra superior que s'assembla a una persona.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Trobar el menú d'accés universal</title>

  <p>El <em>menú d'accés universal</em> és on podeu activar algunes de les opcions d'accessibilitat. Podeu trobar aquest menú fent clic a la icona que s'assembla a una persona envoltada d'un cercle a la barra superior.</p>

  <figure>
    <desc>El menú d'accés universal es troba a la barra superior.</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/preferences-desktop-accessibility-symbolic.svg"/>
  </figure>

  <p>Si no veieu el menú d'accés universal, podeu activar-lo des del quadre de configuració <gui>Accés universal</gui>:</p>

  <steps>
    <item>
      <p>Obriu la vista general <gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Accés universal</gui>.</p>
    </item>
    <item>
      <p>Feu clic a <gui>Accés universal</gui> per a obrir el quadre.</p>
    </item>
    <item>
      <p>Activeu l'opció <gui>Mostra sempre el menú d'accés universal</gui>.</p>
    </item>
  </steps>

  <p>Per a accedir a aquest menú utilitzant el teclat en comptes del ratolí, premeu <keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq> per a moure el focus del teclat a la barra superior. Apareixerà una línia blanca a sota del botó <gui>Activitats</gui> —això indica l'element seleccionat de la barra superior. Utilitzeu les fletxes del teclat per a moure la línia blanca sota la icona del menú d'accés universal i premeu <key>Retorn</key> per a obrir-lo. Podeu utilitzar les fletxes amunt i avall per a seleccionar els elements del menú. Premeu <key>Retorn</key> per a activar o desactivar l'element seleccionat.</p>

</page>
