<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-othercountry" xml:lang="ca">

  <info>
    <link type="guide" xref="power#problems"/>
    <desc>L'ordinador funcionarà, però és possible que necessiteu un altre cable d'alimentació o un adaptador de viatge.</desc>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>Funcionarà el meu ordinador amb una font d'alimentació en un altre país?</title>

<p>Diversos països utilitzen fonts d'alimentació a diferents voltatges (normalment 110V o 220-240V) i freqüències d'AC (normalment 50 Hz o 60 Hz). L'equip ha de funcionar amb una font d'alimentació en un país diferent sempre que tingueu un adaptador de corrent adequat. També és possible que hàgiu de canviar un interruptor.</p>

<p>Si teniu un ordinador portàtil, tot el que haureu de fer és obtenir el connector correcte per a l'adaptador de corrent. Alguns ordinadors portàtils venen empaquetats amb més d'un connector per al seu adaptador, de manera que pot ser que tingueu el correcte. Si no, n'hi ha prou amb connectar el vostre a un adaptador de viatge estàndard.</p>

<p>Si teniu un ordinador de sobretaula, també podeu obtenir un cable amb un endoll diferent o utilitzar un adaptador de viatge. En aquest cas però, és possible que necessiteu canviar l'interruptor de tensió a la font d'alimentació de l'ordinador, si n'hi ha. Molts ordinadors no tenen un interruptor d'aquest tipus, i funcionaran perfectament amb qualsevol voltatge. Mireu la part posterior de l'ordinador i localitzeu el sòcol que connecta el cable d'alimentació. En algun lloc proper, pot haver-hi un petit interruptor marcat com «110V» o «230V» (per exemple). Canvieu-lo si és necessari.</p>

<note style="warning">
  <p>Aneu amb compte quan canvieu els cables d'alimentació o utilitzeu adaptadors de viatge. Apagueu-ho tot primer si és possible.</p>
</note>

</page>
