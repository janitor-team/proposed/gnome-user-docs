<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-order" xml:lang="ca">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Intercalar i revertir l'ordre d'impressió.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Fer que les pàgines s'imprimeixin en un ordre diferent</title>

  <section id="reverse">
    <title>Al revés</title>

    <p>Normalment les impressores imprimeixen primer la primera pàgina i per últim la darrera, de manera que les pàgines acaben en ordre invers quan es recullen. Si és necessari, podeu invertir aquest ordre d'impressió.</p>

    <steps>
      <title>Invertir l'ordre:</title>
      <item>
        <p>Premeu <keyseq><key>Ctrl</key><key>P</key></keyseq> per a obrir el diàleg d'impressió.</p>
      </item>
      <item>
        <p>A la pestanya <gui>General</gui>, a <gui>Còpies</gui>, marqueu <gui>Inverteix</gui>. L'última pàgina s'imprimirà primer, i així successivament.</p>
      </item>
    </steps>

  </section>

  <section id="collate">
    <title>Intercalar</title>

  <p>Si imprimiu més d'una còpia del document, les impressions s'agruparan per número de pàgina de manera predeterminada (és a dir, surten totes les còpies de la pàgina 1, les còpies de la pàgina 2, etc.). <em>Compaginades</em> farà que cada còpia surt amb les seves pàgines agrupades en l'ordre correcte.</p>

  <steps>
    <title>Per a intercalar:</title>
    <item>
     <p>Premeu <keyseq><key>Ctrl</key><key>P</key></keyseq> per a obrir el diàleg d'impressió.</p>
    </item>
    <item>
      <p>A la pestanya <gui>General</gui>, sota el selector de <gui>Còpies</gui>, <gui>Compagina</gui>.</p>
    </item>
  </steps>

</section>

</page>
