<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="music-player-ipodtransfer" xml:lang="ca">
  <info>
    <link type="guide" xref="media#music"/>


    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <desc>Utilitzeu un reproductor multimèdia per a copiar les cançons i retireu l'iPod després de forma segura.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>Les cançons no apareixen al meu iPod quan les copio</title>

<p>Quan connecteu un iPod a l'ordinador, apareixerà a la vostra aplicació de reproducció de música i també al gestor de fitxers (aplicació <app>Fitxers</app> a la vista general d'<gui>Activitats</gui>). Heu de copiar cançons a l'iPod utilitzant el reproductor de música: si les copieu a través del gestor de fitxers, no funcionarà perquè les cançons no es col·loquen a la ubicació correcta. Els iPod tenen una ubicació especial per a emmagatzemar cançons que les aplicacions de reproducció de música saben localitzar, però el gestor de fitxers no.</p>

<p>També heu d'esperar que les cançons acabin de copiar-se a l'iPod abans de desconnectar-lo. Assegureu-vos també de triar l'opció <link xref="files-removedrive">extreure de forma segura</link>. Això assegurarà que totes les cançons s'hagin copiat correctament.</p>

<p>Una altra raó per la qual les cançons poden no aparèixer al vostre iPod és que l'aplicació de la reproducció de música que utilitzeu no sigui compatible amb la conversió de cançons d'un format d'àudio a un altre. Si copieu una cançó que es guarda en un format d'àudio que no suporta el vostre iPod (per exemple, un fitxer Ogg Vorbis (.oga), el reproductor de música intentarà convertir-lo a un format que l'iPod entengui, com ara MP3. Si el programari de conversió (també anomenat codificador o codificador) no està instal·lat, el reproductor de música no podrà realitzar la conversió i, per tant, no copiarà la cançó. Mireu l'instal·lador de programari per a obtenir un còdec adequat.</p>

</page>
