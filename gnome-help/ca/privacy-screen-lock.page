<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-screen-lock" xml:lang="ca">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="seealso" xref="session-screenlocks"/>
    <link type="seealso" xref="shell-exit#lock-screen"/>

    <revision pkgversion="3.8" date="2013-05-21" status="candidate"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>
    <revision pkgversion="3.38.1" date="2020-11-22" status="final"/>
    <revision pkgversion="3.38.4" date="2020-03-07" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit>
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Eviteu que altres usuaris facin servir el vostre escriptori quan marxeu de l'ordinador.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Bloquejar automàticament la pantalla</title>
  
  <p>Quan deixeu l'ordinador, hauríeu de <link xref="shell-exit#lock-screen">bloquejar la pantalla</link> per a evitar que altres persones utilitzin l'escriptori i accedeixin als vostres fitxers. Si de vegades oblideu de bloquejar la vostra pantalla, és possible que vulgueu que la pantalla del vostre ordinador es bloquegi automàticament després d'un període de temps determinat. Això us ajudarà a protegir-lo quan no l'estigui utilitzant.</p>

  <note><p>Quan la pantalla estigui blocada, les aplicacions i els processos del sistema continuaran funcionant, però s'haurà d'introduir la contrasenya per a tornar a utilitzar-lo de nou.</p></note>
  
  <steps>
    <title>Per a configurar el temps abans que la pantalla es bloquegi automàticament:</title>
    <item>
      <p>Obriu la vista general <gui xref="shell-introduction#activities">Activitats</gui> i comenceu a escriure <gui>Bloqueig de pantalla</gui>.</p>
    </item>
    <item>
      <p>Feu clic a <gui>Bloqueja de pantalla</gui> per a obrir el quadre.</p>
    </item>
    <item>
      <p>Assegureu-vos que el <gui>bloqueig automàtic de la pantalla</gui> està activat, i després seleccioneu durant quant de temps des de la llista desplegable <gui>Retard per a blocar la pantalla automàticament</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Les aplicacions poden presentar-vos notificacions que encara es mostren a la pantalla de bloqueig. Això és convenient, per exemple, per a veure si teniu cap correu electrònic sense desbloquejar la pantalla. Si us preocupa que altres persones vegin aquestes notificacions, desactiveu <gui>Mostra les notificacions a la pantalla de bloqueig</gui>. Per a més paràmetres de notificació, consulteu <link xref="shell-notifications"/>.</p>
  </note>

  <p>Quan la pantalla estigui blocada i vulgueu desbloquejar-la, premeu <key>Esc</key>, o feu lliscar el ratolí des de la part inferior de la pantalla. A continuació, introduïu la vostra contrasenya i premeu <key>Retorn</key> o feu clic a <gui>Desbloqueja</gui>. Com a alternativa, simplement comenceu a escriure la vostra contrasenya i la cortina de bloqueig s'aixecarà automàticament mentre escriviu.</p>

</page>
