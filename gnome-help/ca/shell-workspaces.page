<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="ui" version="1.0 if/1.0" id="shell-workspaces" xml:lang="ca">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces" group="#first"/>

    <revision pkgversion="3.8.0" date="2013-04-23" status="review"/>
    <revision pkgversion="3.10.3" date="2014-01-26" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.35.91" date="2020-02-27" status="candidate"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Els espais de treball són una manera d'agrupar finestres al vostre escriptori.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>Què és un espai de treball i com m'ajudarà?</title>

  <p if:test="!platform:gnome-classic">Els espais de treball es refereixen a l'agrupament de finestres a l'escriptori. Podeu crear diversos espais de treball, que actuen com a escriptoris virtuals. Els espais de treball estan dissenyats per a reduir el desordre i fer que sigui més fàcil navegar per l'escriptori.</p>

  <p if:test="platform:gnome-classic">Els espais de treball es refereixen a l'agrupament de finestres a l'escriptori. Podeu crear diversos espais de treball, que actuen com a escriptoris virtuals. Els espais de treball estan dissenyats per a reduir el desordre i fer que sigui més fàcil navegar per l'escriptori.</p>

  <p>Podeu utilitzar els espais de treball per a organitzar el vostre treball. Per exemple, podríeu tenir totes les finestres de comunicació, com ara el correu electrònic i el programa de xat, en un espai de treball, i les vostres tasques en un espai de treball diferent. El vostre gestor de música podria estar en un tercer espai de treball.</p>

<p>Utilitzant espais de treball:</p>

<list>
  <item>
    <p if:test="!platform:gnome-classic">A la vista general <gui xref="shell-introduction#activities">Activitats</gui>, podeu navegar horitzontalment entre els espais de treball.</p>
    <p if:test="platform:gnome-classic">Feu clic al botó de la part inferior esquerra de la pantalla de la llista de finestres o premeu la tecla <key xref="keyboard-key-super">Súper</key> per a obrir la vista <gui>Activitats</gui>.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">Si ja s'utilitza més d'un espai de treball, es mostrarà el selector <em>d'espais de treball</em> entre el camp de cerca i la llista de finestres. Es mostraran els espais de treball utilitzats actualment més un espai de treball buit.</p>
    <p if:test="platform:gnome-classic">A la cantonada inferior dreta, veureu quatre quadres. Aquest és el selector d'espais de treball.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">Per a afegir un espai de treball, arrossegueu i deixeu anar una finestra des d'un espai de treball existent a l'espai de treball buit al selector d'espais de treball. Aquest espai de treball ara conté la finestra que heu deixat anar, i apareixerà un nou espai de treball buit al seu costat.</p>
    <p if:test="platform:gnome-classic">Arrossegueu i deixeu anar una finestra des de l'espai de treball actual a un espai de treball buit al selector d'espais de treball. Aquest espai de treball ara conté la finestra que heu deixat anar.</p>
  </item>
  <item if:test="!platform:gnome-classic">
    <p>Per a eliminar espai de treball, simplement tanqueu totes les finestres o moveu-les a altres espais de treball.</p>
  </item>
</list>

<p if:test="!platform:gnome-classic">Sempre hi ha almenys un espai de treball.</p>

    <media its:translate="yes" type="image" src="figures/shell-workspaces.png" width="940" height="291">
        <p>Selector de l'espai de treball</p>
    </media>

</page>
