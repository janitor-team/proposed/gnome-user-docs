<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="net-wireless-wepwpa" xml:lang="ca">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>WEP i WPA són formes de xifrar dades en xarxes sense fil.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Què vol dir WEP i WPA?</title>

  <p>WEP i WPA (juntament amb WPA2) són noms per a diferents eines de xifrat utilitzades per a garantir seguretat de la vostra connexió sense fil. L'encriptació codifica la connexió de xarxa perquè ningú no pugui "escoltar-la" i observar quines pàgines web esteu veient, per exemple. WEP significa <em>Wired Equivalent Privacy</em>, i WPA significa <em>Wireless Protected Access</em>. WPA2 és la segona versió de l'estàndard WPA.</p>

  <p>Utilitzar <em>algun</em> xifratge sempre és millor que no utilitzar-ne cap, però WEP és el menys segur d'aquests estàndards, i no hauria d'usar-se si es pot evitar. WPA2 és el més segur dels tres. Si la vostra targeta de xarxa i el vostre encaminador admeten WPA2, és el que heu d'utilitzar quan configureu la vostra xarxa sense fil.</p>

</page>
