<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-display" xml:lang="ca">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-display"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>
    <revision pkgversion="40.2" date="2021-08-25" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Controleu les llegendes de les icones que s'utilitzen al gestor de fitxers.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>Preferències de visualització del gestor de fitxers</title>

<p>Podeu controlar com el gestor de fitxers mostra les llegendes sota les icones. Feu clic al botó de menú a la cantonada superior dreta de la finestra, seleccioneu <gui>Preferències</gui>, i aneu a la secció <gui>Títols de les visualitzacions de les icones</gui>.</p>

<section id="icon-captions">
  <title>Llegendes de les icones</title>
  <!-- TODO: update screenshot for 3.18 and above. -->
  <media type="image" src="figures/nautilus-icons.png" width="250" height="110" style="floatend floatright">
    <p>Icones del gestor de fitxers amb llegendes</p>
  </media>
  <p>Quan utilitzi la vista d'icones, pot optar per a tenir informació addicional sobre els fitxers i les carpetes que es mostren amb una llegenda sota cada icona. Això és útil, per exemple, si sovint necessita veure qui és el propietari d'un fitxer o quan s'ha modificat per última vegada.</p>
  <p>Pot fer zoom en una carpeta fent clic al botó d'opcions de visualització de la barra d'eines i triant un nivell de zoom amb el control lliscant. A mesura que s'avança, el gestor de fitxers mostrarà cada vegada més informació a la llegenda. Pot escollir fins a tres coses per a mostrar en els subtítols. El primer es mostrarà a la majoria de nivells de zoom. L'últim només es mostrarà a escales molt grans.</p>
  <p>La informació que podeu mostrar a la llegenda de les icones és la mateixa que les columnes que podeu utilitzar a la vista en llista. Consultar <link xref="nautilus-list"/> per a més informació.</p>
</section>

<section id="list-view">

  <title>Vista en llista</title>

  <p>When viewing files as a list, you can display <gui>Expandable Folders in
  List View</gui>. This shows expanders on each directory in the file list, so
  that the contents of several folders can be shown at once. This is useful if
  the folder structure is relevant, such as if your music files are organized
  with a folder per artist, and a subfolder per album.</p>

</section>

</page>
