<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-apps-favorites" xml:lang="ca">

  <info>
    <link type="seealso" xref="shell-apps-open"/>
    <link type="guide" xref="shell-overview#desktop"/>

    <revision pkgversion="3.6.0" date="2012-10-14" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Afegiu (o elimineu) les icones dels programes que s'utilitzen amb més freqüència al quadre d'aplicacions.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Fixar les aplicacions preferides al quadre d'aplicacions</title>

  <p>Per a afegir una aplicació al <link xref="shell-introduction#activities">tauler</link> per un fàcil accés:</p>

  <steps>
    <item>
      <p if:test="!platform:gnome-classic">Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> fent clic a <gui>Activitats</gui> a la part superior esquerra de la pantalla</p>
      <p if:test="platform:gnome-classic">Feu clic al menú d'<gui xref="shell-introduction#activities">Aplicacions</gui> a la part superior esquerra de la pantalla i trieu l'entrada <gui>Vista general d'Activitats</gui> del menú.</p></item>
    <item>
      <p>Feu clic al botó de quadrícula al quadre d'aplicacions i cerqueu l'aplicació que voleu afegir.</p>
    </item>
    <item>
      <p>Feu clic amb el botó dret sobre la icona de l'aplicació i seleccioneu <gui>Afegeix als preferits</gui>.</p>
      <p>També podeu fer clic i arrossegar la icona al quadre d'aplicacions.</p>
    </item>
  </steps>

  <p>Per a treure la icona d'una aplicació del quadre d'aplicacions, feu clic amb el botó dret a la icona de l'aplicació i seleccioneu <gui>Suprimeix dels preferits</gui>.</p>

  <note style="tip" if:test="platform:gnome-classic"><p>Les aplicacions preferides també apareixen a la secció <gui>Preferides</gui> al menú <gui xref="shell-introduction#activities">Aplicacions</gui>.</p>
  </note>

</page>
