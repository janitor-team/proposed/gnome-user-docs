<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-where" xml:lang="ca">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Alguns consells sobre on emmagatzemar les còpies de seguretat i quin tipus de dispositiu d'emmagatzemament hauríeu d'utilitzar.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>On emmagatzemar una còpia de seguretat</title>

  <p>Hauríeu d'emmagatzemar les còpies de seguretat dels fitxers fora del vostre ordinador, per exemple, en un disc dur extern. D'aquesta manera, si l'ordinador s'espatlla, es perd o el roben, la còpia de seguretat seguirà intacta. Perquè la seguretat sigui màxima, no hauríeu de tenir la còpia de seguretat al mateix edifici de l'ordinador. En cas d'incendi o robatori, es podrien perdre ambdues còpies de les dades si les teniu juntes.</p>

  <p>És important escollir un <em>suport per a les còpies de seguretat</em> adequat. El suport que escolliu ha de tenir prou espai per a emmagatzemar tots els fitxers que vulgueu assegurar.</p>

   <list style="compact">
    <title>Opcions d'emmagatzemament local i remot</title>
    <item>
      <p>Un llapis USB (capacitat baixa)</p>
    </item>
    <item>
      <p>Un disc dur intern (capacitat alta)</p>
    </item>
    <item>
      <p>Un disc dur extern (normalment, capacitat alta)</p>
    </item>
    <item>
      <p>Una unitat en xarxa (capacitat alta)</p>
    </item>
    <item>
      <p>Un servidor de fitxers o de còpies de seguretat (capacitat alta)</p>
    </item>
    <item>
     <p>CD o DVD gravables (capacitat baixa o mitjana)</p>
    </item>
    <item>
     <p>Servei de còpia de seguretat en línia (per exemple <link href="http://aws.amazon.com/s3/">S3 d'Amazon S3</link>; la capacitat depèn del preu)</p>
    </item>
   </list>

  <p>Algunes d'aquestes opcions tenen prou capacitat per a permetre-us fer una còpia de seguretat d'absolutament tots els fitxers del sistema, també coneguda com a <em>còpia de seguretat del sistema complet</em>.</p>
</page>
