<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-2sided" xml:lang="ca">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Impressió del document a dues cares, o diverses pàgines per full.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Imprimeix dissenys a dues cares i en diverses pàgines</title>

  <p>Per a imprimir a ambdós costats de cada full de paper:</p>

  <steps>
    <item>
      <p>Obriu el diàleg d'impressió prement <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Anar a la pestanya <gui>Configuració de pàgina</gui> de la finestra d'impressió i trieu una opció del desplegable <gui>Dues cares</gui>. Si l'opció està deshabilitada, la impressió a dues cares no està disponible per a la vostra impressora.</p>
      <p>Les impressores gestionen la impressió a doble cara de diferents maneres. És una bona idea experimentar amb la impressora per a veure com funciona.</p>
    </item>
    <item>
      <p>Podeu imprimir més d'una pàgina del document per <em>cara</em> de paper també. L'ús de l'opció <gui>Pàgines per cara</gui> ho permeten.</p>
    </item>
  </steps>

  <note>
    <p>La disponibilitat d'aquestes opcions pot dependre del tipus d'impressora que tingueu, així com de l'aplicació que feu servir. Aquesta opció pot no estar sempre disponible.</p>
  </note>

</page>
