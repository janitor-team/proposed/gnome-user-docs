<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-willnotturnon" xml:lang="ca">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="guide" xref="hardware-problems-graphics" group="#last"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Els cables solts i els problemes de maquinari són possibles motius.</desc>
    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>El meu ordinador no s'encén</title>

<p>Hi ha diversos motius pels quals l'ordinador no s'encén. Aquest tema ofereix una breu visió general d'alguns dels possibles motius.</p>
	
<section id="nopower">
  <title>Ordinador no connectat, bateria buida, o cable solt</title>
  <p>Assegureu-vos que els cables d'alimentació de l'ordinador estiguin ben connectats i les preses de corrent estiguin actives. Assegureu-vos que el monitor estigui connectat i engegat també. Si teniu un ordinador portàtil, connecteu el cable del carregador (en cas que s'hagi quedat sense bateria). També és possible que vulgueu comprovar que la bateria estigui instal·lada correctament (consulteu la part inferior del portàtil) si és que es pot treure.</p>
</section>

<section id="hardwareproblem">
  <title>Problema amb el maquinari de l'ordinador</title>
  <p>És possible que un component de l'ordinador estigui trencat o tingui un mal funcionament. Si és aquest el cas, haureu de reparar l'ordinador. Les falles més comunes inclouen una font d'alimentació espatllada, components defectuosos (com la memòria o la RAM) i una placa base defectuosa.</p>
</section>

<section id="beeps">
  <title>L'ordinador emet un so i després s'apaga</title>
  <p>Si l'ordinador emet un so diverses vegades quan s'activa i aleshores s'apaga (o no arrenca), potser indica que ha detectat un problema. Aquests sons són de vegades anomenats <em>codis beep</em>, i el patró de sons està pensat per a indicar quin és el problema de l'ordinador. Diferents fabricants utilitzen codis sonors diferents, de manera que s'haurà de consultar el manual de la placa base de l'ordinador o portar-lo a reparar.</p>
</section>

<section id="fans">
  <title>El ventilador de l'ordinador gira, però no hi ha res a la pantalla</title>
  <p>El primer que heu de verificar és que el vostre monitor estigui connectat i engegat.</p>
  <p>Aquest problema també pot ser degut a un error de maquinari. Els ventiladors poden activar-se quan premeu el botó d'engegada, però altres parts essencials de l'ordinador podrien no activar-se. En aquest cas, porteu-lo a reparar.</p>
</section>

</page>
