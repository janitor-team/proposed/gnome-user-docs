<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-howtoimport" xml:lang="ca">

  <info>
    <link type="guide" xref="color#profiles"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-assignprofiles"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="review"/>
    <revision pkgversion="3.28" date="2018-04-05" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Els perfils de color es poden importar obrint-los.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Com importo perfils de color?</title>

  <p>Podeu importar un perfil de color fent doble clic a <file>.ICC</file> o a <file>.ICM</file> en el navegador de fitxers.</p>

  <p>Alternativament, podeu gestionar els vostres perfils de color a través del quadre de <gui>Color</gui>.</p>

  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Paràmetres</gui>.</p>
    </item>
    <item>
      <p>Feu clic a <gui>Paràmetres</gui>.</p>
    </item>
    <item>
      <p>Feu clic a <gui>Color</gui> en la barra lateral per a obrir el quadre.</p>
    </item>
    <item>
      <p>Seleccioneu el dispositiu.</p>
    </item>
    <item>
      <p>Feu clic a <gui>Afegeix un perfil</gui> per a seleccionar un perfil existent o importar-ne un de nou.</p>
    </item>
    <item>
      <p>Premeu <gui>Afegeix</gui> per a confirmar la vostra selecció.</p>
    </item>
  </steps>

  <p>El fabricant de la vostra pantalla pot proporcionar-vos un perfil que podeu utilitzar. Normalment, aquests perfils es fan per a la visualització mitjana, per tant, pot ser que no sigui perfecte per a la vostè. Per a obtenir el millor calibratge, hauria de <link xref="color-calibrate-screen">crear el seu propi perfil</link> utilitzant un colorímetre o un espectrofotòmetre.</p>

</page>
