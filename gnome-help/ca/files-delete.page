<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-delete" xml:lang="ca">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-recover"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.16" date="2015-02-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Eliminar els fitxers o carpetes que ja no necessitareu.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>Eliminar fitxers i carpetes</title>

  <p>Si ja no voleu un fitxer o una carpeta, podeu suprimir-lo. Quan suprimiu un ítem, es desplaça a la carpeta anomenada <gui>Paperera</gui>, on s'emmagatzema fins que es buidi la paperera. Podeu <link xref="files-recover">restaurar elements</link> de la <gui>Paperera</gui> a la seva ubicació original si decidiu que els necessiteu o si els vàreu eliminar accidentalment.</p>

  <steps>
    <title>Per a enviar un fitxer a la paperera:</title>
    <item><p>Feu un sol clic a l'element que voleu enviar a la paperera per a seleccionar-lo.</p></item>
    <item><p>Premeu <key>Supr</key> al teclat. Com a alternativa, arrossegueu l'element a la <gui>Paperera</gui> a la barra lateral.</p></item>
  </steps>

  <p>El fitxer es traslladarà a la paperera i se us presentarà una opció <gui>Desfés</gui> l'esborrat. El botó <gui>Desfés</gui> apareix uns segons. Si seleccioneu <gui>Desfés</gui>, el fitxer es restaurarà a la seva ubicació original.</p>

  <p>Per a eliminar els fitxers permanentment i alliberar espai al disc a l'ordinador, heu de buidar la paperera. Per a buidar la paperera, feu clic amb el botó de la dreta <gui>Paperera</gui> a la barra lateral i seleccioneu <gui>Buida la paperera</gui>.</p>

  <section id="permanent">
    <title>Suprimeix un fitxer permanentment</title>
    <p>Podeu eliminar immediatament un fitxer de forma permanent, sense haver d'enviar-lo primer a la paperera.</p>

  <steps>
    <title>Per a suprimir un fitxer de forma permanent:</title>
    <item><p>Seleccioneu l'element que voleu suprimir.</p></item>
    <item><p>Mantingueu premuda la tecla <key>Maj</key>, aleshores premeu la tecla <key>Supr</key> del seu teclat.</p></item>
    <item><p>Com que no ho podeu desfer, se us demanarà que confirmeu que voleu eliminar el fitxer o la carpeta.</p></item>
  </steps>

  <note><p>Els fitxers esborrats als <link xref="files#removable">dispositius extraïbles</link> poden no ser visibles en altres sistemes operatius, com Windows o Mac OS. Els fitxers encara hi són i estaran disponibles quan torneu a connectar el dispositiu al vostre ordinador.</p></note>

  </section>

</page>
