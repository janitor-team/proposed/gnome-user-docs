<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-sort" xml:lang="ca">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-25" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Organitzeu els fitxers pel nom, la mida, el tipus o per quan hagin canviat.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>Ordenar fitxers i carpetes</title>

<p>Podeu ordenar els fitxers d'una carpeta de diferents maneres, per exemple segons la seva data o la mida. Consulteu <link xref="#ways"/> a sota per a veure una llista de formes habituals d'ordenar els fitxers. Consulteu <link xref="nautilus-views"/> per a obtenir informació sobre com canviar l'ordre de classificació per defecte.</p>

<p>La manera com podeu ordenar els fitxers depèn de la <em>vista de la carpeta</em> que s'estigui utilitzant. Podeu canviar la vista actual utilitzant els botons de llista o icona a la barra d'eines.</p>

<section id="icon-view">
  <title>Vista d'icona</title>

  <p>Per a ordenar els fitxers en un ordre diferent, feu clic al botó d'opcions de vista de la barra d'eines i trieu <gui>Per Nom</gui>, <gui>Per mida</gui>, <gui>Per Tipus</gui>, <gui>Per data de modificació</gui>, o <gui>Per data d'accés</gui>.</p>

  <p>Com a exemple, si seleccioneu <gui>Per nom</gui>, els fitxers s'ordenaran pels seus noms, alfabèticament. Consultar <link xref="#ways"/> per altres opcions.</p>

  <p>Podeu ordenar en l'ordre invers, seleccionant <gui>Ordre invers</gui> des del menu.</p>

</section>

<section id="list-view">
  <title>Visualització de llista</title>

  <p>Per a ordenar els fitxers en un ordre diferent, feu clic a un dels encapçalaments de columna al gestor de fitxers. Per exemple, feu clic a <gui>Tipus</gui> per a ordenar per tipus de fitxer. Feu clic a l'encapçalament de la columna de nou per a ordenar en ordre invers.</p>
  <p>A la vista de llista, podeu mostrar columnes amb més atributs i ordenar per aquestes columnes. Feu clic al botó Opcions de visualització a la barra d'eines, seleccioneu <gui>Columnes visibles…</gui> i seleccioneu les columnes que voleu que siguin visibles. A continuació, podreu ordenar per aquestes columnes. Consultar <link xref="nautilus-list"/> per a obtenir les descripcions de les columnes disponibles.</p>

</section>

<section id="ways">
  <title>Maneres d'ordenar fitxers</title>

  <terms>
    <item>
      <title>Nom</title>
      <p>Ordena alfabèticament pel nom del fitxer.</p>
    </item>
    <item>
      <title>Mida</title>
      <p>Ordena per la mida del fitxer (quant espai de disc ocupa). Ordena des del més petit al més gran per defecte.</p>
    </item>
    <item>
      <title>Tipus</title>
      <p>Ordre alfabèticament pel tipus de fitxer. Els fitxers del mateix tipus s'agrupen, ordenats per nom.</p>
    </item>
    <item>
      <title>Última modificació</title>
      <p>Ordena per la data i l'hora en què s'ha canviat un fitxer per última vegada. Ordena des del més antic al més recent per defecte.</p>
    </item>
  </terms>

</section>

</page>
