<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="power-batteryoptimal" xml:lang="ca">

  <info>
    <link type="guide" xref="power"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Consells com "No deixis que la càrrega de la bateria estigui massa baixa".</desc>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>Aprofitar al màxim la bateria del portàtil</title>

<p>Com que les bateries del portàtil envelleixen, empitjoren emmagatzemant la càrrega i la seva capacitat disminueix gradualment. Hi ha algunes tècniques que es poden utilitzar per a allargar-ne la vida útil, encara que no s'hagi d'esperar una gran diferència.</p>

<list>
  <item>
    <p>No deixeu que la bateria s'esgoti completament. Recarregueu-la sempre <em>abans</em> que la bateria arribi a estar molt baixa, tot i que la majoria de les bateries porten proteccions integrades per a evitar que això passi. Recarregar quan només està descarregada parcialment és més eficient, però recarregar-la quan només està lleugerament descarregada és pitjor.</p>
  </item>
  <item>
    <p>La calor té un efecte perjudicial en l'eficiència de càrrega de la bateria. No deixeu que la bateria s'escalfi més del que ha d'estar.</p>
  </item>
  <item>
    <p>Les bateries tenen edat, fins i tot si es deixen carregades. Hi ha poc avantatge a l'hora de comprar una bateria de recanvi al mateix temps que la bateria original - sempre compreu els recanvis quan es necessitin.</p>
  </item>
</list>

<note>
  <p>Aquest consell s'aplica específicament a les bateries de liti-ion (Li-Ion), que són el tipus més comú. Altres tipus de bateries poden tenir prestacions diferents.</p>
</note>

</page>
