<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="color-whatisspace" xml:lang="ca">

  <info>
    <link type="guide" xref="color#profiles"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <desc>Un espai de color és un rang definit de colors.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Què és un espai de color?</title>

  <p>Un espai de color és un rang definit de colors. Els espais de colors coneguts inclouen sRGB, AdobeRGB i ProPhotoRGB.</p>

  <p>El sistema visual humà no és un sensor RGB simple, però podem aproximar-nos a com respon l'ull amb un diagrama de cromaticitat CIE 1931 que mostra la resposta visual humana com a forma de ferradura. Podeu veure que a la visió humana hi ha molts més tons de verds detectats que blaus o vermells. Amb un espai de color tricromàtic com el RGB representem els colors de l'ordinador amb tres valors, que es restringeixen fins a la codificació d'un <em>triangle</em> de colors.</p>

  <note>
    <p>L'ús de models com el diagrama de cromaticitat CIE 1931 és una gran simplificació del sistema visual humà, i les expressions reals s'expressen com a forma 3D, en comptes de projeccions 2D. Una projecció 2D d'una forma 3D de vegades pot ser enganyosa, per tant, si voleu veure la forma 3D, utilitzeu l'aplicació <code>gcm-viewer</code>.</p>
  </note>

  <figure>
    <desc>sRGB, AdobeRGB i ProPhotoRGB representats per triangles blancs</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/color-space.png"/>
  </figure>

  <p>Primer, mirant el sRGB, que és l'espai més petit i pot codificar-se amb el mínim de colors. Es tracta una aproximació d'una pantalla CRT de 10 anys d'antiguitat, de manera que la majoria de monitors moderns poden mostrar fàcilment més colors. El sRGB és un estàndard de <em>mínim comú denominador</em> i s'utilitza en una gran quantitat d'aplicacions (inclosa la d'Internet).</p>
  <p>L'AdobeRGB sovint s'utilitza com a <em>espai d'edició</em>. Es poden codificar més colors que amb el sRGB, el que significa que podeu canviar els colors en una fotografia sense preocupar-vos massa si es retallen els colors més vius o els negres s'aixafin.</p>
  <p>El ProPhoto és l'espai més gran disponible i s'utilitza amb freqüència per a l'arxiu de documents. Pot codificar gairebé tota la gamma de colors detectats per l'ull humà, i fins i tot codificar colors que l'ull no pot detectar!</p>

  <p>Ara bé, si ProPhoto és clarament millor, per què no el fem servir per a tot? La resposta és relacionar amb la <em>quantificació</em>. Si només teniu 8 bits (256 nivells) per a codificar cada canal, llavors un rang més gran tindrà diferències més grans entre cada valor.</p>
  <p>Diferències més grans suposen un error més gran entre el color capturat i el color emmagatzemat, i per a alguns colors això és un gran problema. Resulta que els colors clau, com els colors de la pell són molt importants, i fins i tot petits errors poden fer que els espectadors no entrenats notin que alguna cosa de la fotografia sembli incorrecte.</p>
  <p>Per descomptat, utilitzar una imatge de 16 bits deixarà molts més salts i un error de quantificació molt més petit, però això duplica la mida de cada fitxer d'imatge. La majoria dels continguts actuals són 8 bpp, és a dir, 8 bits per píxel.</p>
  <p>La gestió del color és un procés per a convertir un espai de color en un altre, on un espai de color pot ser un espai ben definit com el sRGB, o un espai personalitzat com el monitor o el perfil de la impressora.</p>

</page>
