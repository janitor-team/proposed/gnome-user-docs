<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-constantfan" xml:lang="ca">

  <info>
    <link type="guide" xref="power#problems"/>
    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Pot ser que falti algun programa de control del ventilador o que el vostre ordinador portàtil estigui molt calent.</desc>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>El ventilador de l'ordinador portàtil sempre s'està funcionant</title>

<p>Si el ventilador de refrigeració del vostre ordinador portàtil sempre funciona, podria ser que el maquinari que controli el sistema de refrigeració a l'ordinador portàtil no sigui del tot compatible amb Linux. Alguns portàtils necessiten programari addicional per a controlar els seus ventiladors de manera eficient, però aquest programari pot no estar instal·lat (o no estar disponible per a Linux) i, per tant, els ventiladors només s'executen a tota velocitat contínuament.</p>

<p>Si aquest és el cas, és possible que pugueu canviar algunes configuracions o instal·lar un programari addicional que permeti un control total del ventilador. Per exemple, es pot instal·lar <link href="http://vaio-utils.org/fan/">vaiofand</link> per a controlar els ventiladors d'alguns ordinadors portàtils Sony VAIO. La instal·lació d'aquest programari és un procés bastant tècnic que depèn molt de la marca i el model del vostre ordinador portàtil, de manera que podeu vulgueu obtenir consells específics sobre com fer-ho al vostre ordinador.</p>

<p>També és possible que el portàtil produeixi molta calor. Això no significa necessàriament que estigui sobreescalfant; és possible que només necessiti que el ventilador s'executi contínuament a tota velocitat per a permetre que es mantingui prou fresc. Si aquest és el cas, teniu poques opcions que no sigui deixar que el ventilador funcioni a tota velocitat sempre. De vegades podeu comprar accessoris addicionals de refrigeració per al vostre ordinador portàtil que us poden ajudar.</p>

</page>
