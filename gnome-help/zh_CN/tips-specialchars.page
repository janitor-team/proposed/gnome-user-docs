<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="tips-specialchars" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="tips"/>
    <link type="seealso" xref="keyboard-layouts"/>

    <revision pkgversion="3.8.2" version="0.3" date="2013-05-18" status="review"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.26" date="2017-11-27" status="review"/>
    <revision version="gnome:40" date="2021-03-02" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Type characters not found on your keyboard, including foreign alphabets, mathematical symbols, emoji, and dingbats.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>输入特殊字符</title>

  <p>You can enter and view thousands of characters from most of the world’s
  writing systems, even those not found on your keyboard. This page lists
  some different ways you can enter special characters.</p>

  <links type="section">
    <title>输入字符的方法</title>
  </links>

  <section id="characters">
    <title>Characters</title>
    <p>The character map application allows you to find and insert unusual
    characters, including emoji, by browsing character categories or searching
    for keywords.</p>

    <p>You can launch <app>Characters</app> from the Activities overview.</p>

  </section>

  <section id="emoji">
    <title>Emoji</title>
    <steps>
      <title>Insert emoji</title>
      <item>
        <p>Press <keyseq><key>Ctrl</key><key>;</key></keyseq>.</p>
      </item>
      <item>
        <p>Browse the categories at the bottom of the dialog or start typing a
    description in the search field.</p>
      </item>
      <item>
        <p>Select an emoji to insert.</p>
      </item>
    </steps>
  </section>

  <section id="compose">
    <title>Compose 键</title>
    <p>Compose 键是一种特殊按键，可以按多个键序列得到一个特殊字符，例如，要输入重音符号 <em>é</em>，您可以按住 <key>compose</key> 然后再按 <key>'</key> 和 <key>e</key> 键。</p>
    <p>Keyboards don’t have specific compose keys. Instead, you can define
    one of the existing keys on your keyboard as a compose key.</p>

    <steps>
      <title>定义一个 compose 键</title>
      <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
      </item>
      <item>
        <p>Click on <gui>Settings</gui>.</p>
      </item>
      <item>
        <p>Click <gui>Keyboard</gui> in the sidebar to open the panel.</p>
      </item>
      <item>
        <p>In the <gui>Type Special Characters</gui> section, click <gui>Compose Key</gui>.</p>
      </item>
      <item>
        <p>Turn the switch on for the <gui>Compose Key</gui>.</p>
      </item>
      <item>
        <p>Tick the checkbox of the key that you want to set as the Compose
        key.</p>
      </item>
      <item>
        <p>Close the dialog.</p>
      </item>
    </steps>

    <p>您可以用 compose 键输入多种字符，例如：</p>

    <list>
      <item><p>按住 <key>compose</key> 键，然后按 <key>'</key> 键，可以输入重音符号，像 <em>é</em>。</p></item>
      <item><p>按住 <key>compose</key> 键，然后按  <key>`</key> (反引号)键，可以输入抑音符号，像 <em>è</em>。</p></item>
      <item><p>按住 <key>compose</key> 键，然后按  <key>"</key> ，可以输入变音符号，像 <em>ë</em>。</p></item>
      <item><p>按住 <key>compose</key> 键，然后按  <key>-</key> ，可以输入长音符号，像 <em>ē</em>。</p></item>
    </list>
    <p>For more compose key sequences, see <link href="https://en.wikipedia.org/wiki/Compose_key#Common_compose_combinations">the
    compose key page on Wikipedia</link>.</p>
  </section>

<section id="ctrlshiftu">
  <title>代码点</title>

  <p>You can enter any Unicode character using only your keyboard with the
  numeric code point of the character. Every character is identified by a
  four-character code point. To find the code point for a character, look it up
  in the <app>Characters</app> application. The code point is the four characters
  after <gui>U+</gui>.</p>

  <p>To enter a character by its code point, press
  <keyseq><key>Ctrl</key><key>Shift</key><key>U</key></keyseq>, then type the
  four-character code and press <key>Space</key> or <key>Enter</key>. If you often
  use characters that you can’t easily access with other methods, you might find
  it useful to memorize the code point for those characters so you can enter
  them quickly.</p>

</section>

  <section id="layout">
    <title>键盘布局</title>
    <p>您可以让键盘变成其他语言布局，而不是原来键盘上标识的字母了，您甚至可以使用顶部面板上的一个图标，轻松地在不同语言之间切换，详细介绍请参阅 <link xref="keyboard-layouts"/>。</p>
  </section>

<section id="im">
  <title>输入法</title>

  <p>An Input Method expands the previous methods by allowing to enter
  characters not only with keyboard but also any input devices. For instance
  you could enter characters with a mouse using a gesture method, or enter
  Japanese characters using a Latin keyboard.</p>

  <p>要选择一个输入法，在输入控件上点击右键，在<gui>输入法</gui>菜单中选择一种需要的输入法。尚没有完全成为事实标准的统一的输入法，请参考输入法的文档查看如何使用。</p>

</section>

</page>
