<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="shell-exit" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="shell-overview"/>
    <link type="guide" xref="power"/>
    <link type="guide" xref="index" group="#first"/>

    <revision pkgversion="3.6.0" date="2012-09-15" status="review"/>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.24.2" date="2017-06-11" status="candidate"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>
    <revision pkgversion="3.36.1" date="2020-04-18" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="author">
      <name>Alexandre Franke</name>
      <email>afranke@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David Faour</name>
      <email>dfaour.gnome@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>了解如何退出您的帐号，像注销、切换用户等等。</desc>
    <!-- Should this be a guide which links to other topics? -->
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Log out, power off or switch users</title>

  <p>When you have finished using your computer, you can turn it off, suspend
  it (to save power), or leave it powered on and log out.</p>

<section id="logout">
  <title>退出或切换用户</title>

  <p>要使其他用户使用您的计算机，您可以注销或保持自身的登录，仅切换用户即可。如果您仅切换用户，则所有应用程序将继续运行，您再次登录时，一切将保留原样。</p>

  <p>To <gui>Log Out</gui> or <gui>Switch User</gui>, click the
  <link xref="shell-introduction#systemmenu">system menu</link> on the right
  side of the top bar, expand <gui>Power Off / Log Out</gui>, and select the correct option.</p>

  <note if:test="!platform:gnome-classic">
    <p><gui>注销</gui>和<gui>切换用户</gui>菜单项，仅会在您系统中有两个以上的用户时才会出现。</p>
  </note>

  <note if:test="platform:gnome-classic">
    <p>The <gui>Switch User</gui> entry only appears in the menu if you have
    more than one user account on your system.</p>
  </note>

</section>

<section id="lock-screen">
  <info>
    <link type="seealso" xref="session-screenlocks"/>
  </info>

  <title>锁定屏幕</title>

  <p>If you’re leaving your computer for a short time, you should lock your
  screen to prevent other people from accessing your files or running
  applications. When you return, you will see the
  <link xref="shell-lockscreen">lock screen</link>. Enter your
  password to log back in. If you don’t lock your screen, it will lock
  automatically after a certain amount of time.</p>

  <p>To lock your screen, click the system menu on the right side of the top
  bar and select <gui>Lock</gui> from the menu.</p>

  <p>When your screen is locked, other users can log in to their own accounts
  by clicking <gui>Log in as another user</gui> at the bottom right of the login
  screen. You can switch back to your desktop when they are finished.</p>

</section>

<section id="suspend">
  <info>
    <link type="seealso" xref="power-suspend"/>
  </info>

  <title>挂起</title>

  <p>To save power, suspend your computer when you are not using it. If you use
  a laptop, the system, by default, suspends your computer automatically when
  you close the lid.
  This saves your state to your computer’s memory and powers off most of the
  computer’s functions. A very small amount of power is still used during
  suspend.</p>

  <p>To suspend your computer manually, click the system menu on the right side
  of the top bar, expand <gui>Power Off / Log Out</gui>, and select
  <gui>Suspend</gui>.</p>

</section>

<section id="shutdown">
<!--<info>
  <link type="seealso" xref="power-off"/>
</info>-->

  <title>关机或重启</title>

  <p>If you want to power off your computer entirely, or do a full restart,
  click the system menu on the right side of the top bar, expand <gui>Power Off
  / Log Out</gui>, and select either <gui>Restart…</gui> or <gui>Power Off…</gui>.</p>

  <p>If there are other users logged in, you may not be allowed to power off or
  restart the computer because this will end their sessions.  If you are an
  administrative user, you may be asked for your password to power off.</p>

  <note style="tip">
    <p>在您挪动计算机时，如果没有电池您可能想关闭它，如果您的电池电量低或者不能持续供电。可以使用<link xref="power-batterylife">电量不足</link>时关机代替挂起。</p>
  </note>

</section>

</page>
