<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="user-goodpassword" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>使用长的、复杂的密码。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>选择一个可靠的密码</title>

  <note style="important">
    <p>让您的密码容易记住，但别人(包括计算机程序)又很难猜出来。</p>
  </note>

  <p>设置一个好的密码可以让您的计算机更安全，如果密码容易猜出，某些人可能会破解它，获取您的个人资料信息。</p>

  <p>有些人甚至用计算机来猜取密码，因此，有些对人来说很难的密码，却能能被计算机程序破解，下面给出一些好密码的小提示：</p>
  
  <list>
    <item>
      <p>在密码中使用大小写字母、数字、特殊符号和空格等，这会使密码很难被猜出，可选的符号很多，这就使得想猜出您密码的人不得不花费更多时间来检查。</p>
      <note>
        <p>A good method for choosing a password is to take the first letter of
        each word in a phrase that you can remember. The phrase could be the
        name of a movie, a book, a song or an album. For example, “Flatland: A
        Romance of Many Dimensions” would become F:ARoMD or faromd or f:
        aromd.</p>
      </note>
    </item>
    <item>
      <p>让您的密码足够长，字符越多，被猜出所花的时间也越长。</p>
    </item>
    <item>
      <p>Do not use any words that appear in a standard dictionary in any
      language. Password crackers will try these first. The most common
      password is “password” — people can guess passwords like this very
      quickly!</p>
    </item>
    <item>
      <p>Do not use any personal information such as a date, license plate
      number, or any family member’s name.</p>
    </item>
    <item>
      <p>不要使用任何词语。</p>
    </item>
    <item>
      <p>选择一个可以快速输入的，这样可以减少那些偷看者记住您输入字符的可能性。</p>
      <note style="tip">
        <p>不要把密码记在任何地方，那会很容易被发现！</p>
      </note>
    </item>
    <item>
      <p>不同的地方使用不同的密码。</p>
    </item>
    <item>
      <p>不同的帐号使用不同的密码。</p>
      <p>如果您的所有帐号都使用同一个密码，如果有人猜出您的密码，立即就会访问您所有的帐号。</p>
      <p>It can be difficult to remember lots of passwords, however. Though not
      as secure as using a different passwords for everything, it may be easier
      to use the same one for things that do not matter (like websites), and
      different ones for important things (like your online banking account and
      your email).</p>
   </item>
   <item>
     <p>定期更改密码。</p>
   </item>
  </list>

</page>
