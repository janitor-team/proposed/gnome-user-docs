<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-capacity" xml:lang="zh-CN">
  <info>
    <link type="guide" xref="disk"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Rafael Fontenelle</name>
      <email>rafaelff@gnome.org</email>
    </credit>

    <revision pkgversion="3.4.3" date="2012-06-15" status="review"/>
    <revision pkgversion="3.13.91" date="2014-09-05" status="review"/>

    <desc>Use <gui>Disk Usage Analyzer</gui>, <gui>System Monitor</gui>, or
    <gui>Usage</gui> to check space and capacity.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>检查磁盘剩余空间</title>

  <p>You can check how much disk space is left with <app>Disk Usage Analyzer</app>,
  <app>System Monitor</app>, or <app>Usage</app>.</p>

<section id="disk-usage-analyzer">
<title>使用磁盘使用分析器来检查</title>

  <p>要使用<app>磁盘使用分析器</app>检查空余磁盘空间和磁盘容量：</p>

  <list>
    <item>
      <p>Open <app>Disk Usage Analyzer</app> from the <gui>Activities</gui>
      overview. The window will display a list of file locations together with
      the usage and capacity of each.</p>
    </item>
    <item>
      <p>Click one of the items in the list to view a detailed summary of the
      usage for that item. Click the menu button, and then <gui>Scan
      Folder…</gui> to scan a different location.</p>
    </item>
  </list>
  <p>The information is displayed according to <gui>Folder</gui>,
  <gui>Size</gui>, <gui>Contents</gui> and when the data was last
  <gui>Modified</gui>. See more details in <link href="help:baobab"><app>Disk
  Usage Analyzer</app></link>.</p>

</section>

<section id="system-monitor">

<title>使用系统监视器进行检查</title>

  <p>要使用<app>系统监视器</app>来检查空余磁盘空间和磁盘容量：</p>

<steps>
 <item>
  <p>在<gui>活动</gui>概览中，打开<app>系统监视器</app>程序。</p>
 </item>
 <item>
  <p>Select the <gui>File Systems</gui> tab to view the system’s partitions and
  disk space usage.  The information is displayed according to <gui>Total</gui>,
  <gui>Free</gui>, <gui>Available</gui> and <gui>Used</gui>.</p>
 </item>
</steps>
</section>

<section id="usage">
<title>Check with Usage</title>

  <p>To check the free disk space and disk capacity with <app>Usage</app>:</p>

<steps>
  <item>
    <p>Open the <app>Usage</app> application from the <gui>Activities</gui>
    overview.</p>
  </item>
  <item>
    <p>Select <gui>Storage</gui> tab to view the system’s total <gui>Used</gui>
    and <gui>Available</gui> disk space, as well as the used by the
    <gui>Operating System</gui> and common user’s directories.</p>
  </item>
</steps>

<note style="tip">
  <p>Disk space can be freed from user’s directories and its subdirectories
  by checking the box next to the directory name.</p>
</note>
</section>

<section id="disk-full">

<title>如果磁盘太满怎么办？</title>

  <p>如果磁盘太满您应该：</p>

 <list>
  <item>
   <p>Delete files that aren’t important or that you won’t use anymore.</p>
  </item>
  <item>
   <p>Make <link xref="backup-why">backups</link> of the important files that
   you won’t need for a while and delete them from the hard drive.</p>
  </item>
 </list>
</section>

</page>
