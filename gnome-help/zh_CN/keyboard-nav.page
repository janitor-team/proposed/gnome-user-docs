<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="keyboard-nav" xml:lang="zh-CN">
  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision pkgversion="3.7.5" version="0.2" date="2013-02-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.20" date="2016-08-13" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
       <name>Julita Inca</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
       <name>Ekaterina Gerasimova</name>
       <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>在没有鼠标的情况下单击和移动鼠标指针</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>键盘导航</title>

  <p>本页面将介绍键盘导航特性。本特性使用于无法使用鼠标或其他位置指示设备的用户，以及希望尽量使用键盘进行操作的用户。如需查看键盘快捷键的相关信息，请跳转到<link xref="shell-keyboard-shortcuts"/>。</p>

  <note style="tip">
    <p>如果难以使用鼠标或其他指针设备，则可通过键盘上的数字键盘控制鼠标指针。</p>
  </note>

<table frame="top bottom" rules="rows">
  <title>在用户界面中导航</title>
  <tr>
    <td><p><key>Tab</key> and</p>
    <p><keyseq><key>Ctrl</key><key>Tab</key></keyseq></p>
    </td>
    <td>
      <p>在不同的控件间移动键盘焦点。您可以使用 <keyseq><key>Ctrl</key> <key>Tab</key></keyseq> 在不同的控件组之间移动，比如从侧栏移动到窗口主体部分。也可以使用<keyseq><key>Ctrl</key><key>Tab</key></keyseq>跳出一个本身需要使用 Tab 键的控件，例如文本框。</p>
      <p>按住<key>Shift</key>键可以反向移动焦点。</p>
    </td>
  </tr>
  <tr>
    <td><p>方向键</p></td>
    <td>
      <p>要在单个或多个相关控件之间移动选择项，您可以使用上下左右方向键。例如在文件管理器的列表视图中选择项目等等。</p>
    </td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key>方向键</keyseq></p></td>
    <td><p>在列表视图或图标视图中，可以在不改变选中项的情况下将键盘焦点移动到另一个选项。</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key>方向键</keyseq></p></td>
    <td><p>在列表或图标视图中，选中当前选中项到新焦点项之间的所有项目。</p>
    <p>In a tree view, items that have children can be expanded or collapsed,
    to show or hide their children: expand by pressing
    <keyseq><key>Shift</key><key>→</key></keyseq>, and collapse by
    pressing <keyseq><key>Shift</key><key>←</key></keyseq>.</p></td>
  </tr>
  <tr>
    <td><p><key>空格键</key></p></td>
    <td><p>激活焦点所在项目，例如按钮、复选框或者列表。</p></td>
  </tr>
  <tr>
    <td><p>在工具栏中单击<gui>搜索</gui>，或按 <keyseq><key>Ctrl</key><key>F</key></keyseq> 键。</p></td>
    <td><p>在列表或图标视图中，选中或取消选中焦点项目而不影响其他项目。</p></td>
  </tr>
  <tr>
    <td><p><key>Alt</key></p></td>
    <td><p>按住<key>Alt</key>键以显示快捷键组合：按住<key>Alt</key> 和各项目后面括号内的字母来对项目进行操作。</p></td>
  </tr>
  <tr>
    <td><p><key>Esc</key></p></td>
    <td><p>退出菜单、弹出窗口、切换滑块或者对话框窗口。</p></td>
  </tr>
  <tr>
    <td><p><key>F10</key></p></td>
    <td><p>打开窗口菜单栏的第一个项目，然后在下拉菜单中使用方向键上下浏览。</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key xref="keyboard-key-super">Super</key>
    <key>F10</key></keyseq></p></td>
    <td><p>在顶部面板中打开应用程序菜单。</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key><key>F10</key></keyseq> or</p>
    <p><key xref="keyboard-key-menu">Menu</key></p></td>
    <td>
      <p>弹出当前选项的上下文菜单，行为与右键相同。</p>
    </td>
  </tr>
  <tr>
    <td><p>在工具栏中单击<gui>搜索</gui>，或按 <keyseq><key>Ctrl</key><key>F</key></keyseq> 键。</p></td>
    <td><p>在文件管理器中，弹出当前文件夹的上下文菜单，行为与右键单击相同。</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>PageUp</key></keyseq></p>
    <p>and</p>
    <p><keyseq><key>Ctrl</key><key>PageDown</key></keyseq></p></td>
    <td><p>在多标签界面中，切换到左侧或右侧的标签。</p></td>
  </tr>
</table>

<table frame="top bottom" rules="rows">
  <title>更改桌面背景</title>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="alt-f1"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-tab"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-tick"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="ctrl-alt-tab"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-updown"/>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F6</key></keyseq></p></td>
    <td><p>在同一个程序窗口之间切换，按下 <key>Alt</key> 和 <key>F6</key> 键，直到您想要的窗口高亮显示，然后松开 <key>Alt</key> 键，这个操作也可以按 <keyseq><key>Alt</key><key>`</key></keyseq>。</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
    <td><p>在同一个工作区的各个窗口之间切换。</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
    <td><p><link xref="shell-notifications#notificationlist">Open the
    notification list.</link> Press <key>Esc</key> to close.</p></td>
  </tr>
</table>

<table frame="top bottom" rules="rows">
  <title>窗口导航</title>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
    <td><p>关闭当前窗口。</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F5</key></keyseq> or
    <keyseq><key>Super</key><key>↓</key></keyseq></p></td>
    <td><p>Restore a maximized window to its original size. Use
    <keyseq><key>Alt</key><key>F10</key></keyseq> to maximize.
    <keyseq><key>Alt</key><key>F10</key></keyseq> both maximizes and
    restores.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
    <td><p>要移动当前窗口，按下<keyseq><key>Alt</key><key>F7</key></keyseq>，然后用方向键来移动窗口。</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
    <td><p>要调整当前窗口的尺寸，按下<keyseq><key>Alt</key><key>F8</key></keyseq>，然后用方向键来调整尺寸。按下<key>Enter</key>完成调整，或<key>Esc</key>恢复调整前的尺寸。</p></td>
  </tr>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-updown"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-left"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-right"/>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F10</key></keyseq> or
    <keyseq><key>Super</key><key>↑</key></keyseq></p>
    </td>
    <td><p><link xref="shell-windows-maximize">Maximize</link> a window. Press
    <keyseq><key>Alt</key><key>F10</key></keyseq> or
    <keyseq><key>Super</key><key>↓</key></keyseq> to
    restore a maximized window to its original size.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>H</key></keyseq></p></td>
    <td><p>Minimize a window.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>←</key></keyseq></p></td>
    <td><p>Maximize a window vertically along the left side of the screen.
    Press again to restore the window to its previous size. Press
    <keyseq><key>Super</key><key>→</key></keyseq> to switch
    sides.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>→</key></keyseq></p></td>
    <td><p>Maximize a window vertically along the right side of the screen.
    Press again to restore the window to its previous size. Press
    <keyseq><key>Super</key><key>←</key></keyseq> to
    switch sides.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>空格</key></keyseq></p></td>
    <td><p>弹出窗口菜单，就像在标题栏上右键单击一样。</p></td>
  </tr>
</table>

</page>
