<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="contacts-edit-details" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="contacts"/>

    <revision pkgversion="3.5.5" date="2012-08-13" status="review"/>
    <revision pkgversion="3.8" date="2013-04-27" status="review"/>
    <revision pkgversion="3.12" date="2014-02-26" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.36.2" date="2020-08-11" status="review"/>
    <revision pkgversion="3.38.0" date="2020-11-02" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <credit type="editor">
      <name>Pranali Deshmukh</name>
      <email>pranali21293@gmail.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Edite as informações para cada contato.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2021.</mal:years>
    </mal:credit>
  </info>

<title>Editando detalhes do contato</title>

  <p>Ao editar os detalhes do contato, você consegue manter as informações no seu catálogo de endereços atualizados e completos.</p>

  <steps>
    <item>
      <p>Selecione o contato da sua lista de contatos.</p>
    </item>
    <item>
      <p>Pressione o botão <media its:translate="no" type="image" src="figures/view-more-symbolic.svg">
      <span its:translate="yes">ver mais</span></media> no canto superior direito da janela e selecione <gui style="menuitem">Editar</gui>.</p>
    </item>
    <item>
      <p>Edite os detalhes do contato.</p>
      <p>Para adicionar um <em>detalhe</em>, como um novo número de telefone ou endereço de e-mail, preencha os detalhes no próximo campo vazio do tipo (número de telefone, e-mail etc.) que você deseja adicionar.</p>
      <note style="tip">
        <p>Pressione a opção <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">ver mais</span></media> na parte inferior para expandir as opções disponíveis, revelando campos como <gui>Página web</gui> e <gui>Data de nascimento</gui>.</p>
      </note>
    </item>
    <item>
      <p>Pressione <gui style="button">Concluído</gui> para finalizar a edição do contato.</p>
    </item>
  </steps>

</page>
