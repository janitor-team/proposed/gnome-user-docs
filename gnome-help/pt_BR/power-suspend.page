<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-suspend" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspendfail"/>

    <desc>Suspensão leva seu computador para dormir, para que use menos energia.</desc>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2021.</mal:years>
    </mal:credit>
  </info>

<title>O que acontece quando eu suspendo o meu computador?</title>

<p>Quando você <em>suspende</em> o computador, você leva-o a dormir. Todos os seus aplicativos e documentos permanecem aberto, mas a tela e outras partes do computador são desligadas para economizar energia. O computador ainda está ligado e ainda estará usando uma pequena quantia de energia. Você pode acordá-lo pressionando uma tecla ou clicando no mouse. Se isso não funcionar, tente pressionar o botão de energia.</p>

<p>Alguns computadores têm problema com suporte a alguns hardwares, o que significa que eles <link xref="power-suspendfail">podem não conseguir suspender apropriadamente</link>. É uma boa ideia testar a suspensão de seu computador para ver se ele funciona, antes de depender desta função.</p>

<note style="important">
  <title>Sempre salve seu trabalho antes de suspender</title>
  <p>Você deveria salvar todo o seu trabalho antes de suspender o computador, para o caso de alguma coisa dar errado e seus aplicativos abertos e documentos não possam ser recuperados ao resumir o computador novamente.</p>
</note>

</page>
