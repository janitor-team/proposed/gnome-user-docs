<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="bluetooth-problem-connecting" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="bluetooth#problems"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>O adaptador poderia estar desligado ou você não ter os drivers, ou o Bluetooth pode estar desabilitado ou bloqueado.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2021.</mal:years>
    </mal:credit>
  </info>

  <title>Não consigo me conectar ao meu dispositivo Bluetooth</title>

  <p>Existem uma variedade de razões para que não seja capaz de se conectar a um dispositivo Bluetooth, como um fone de ouvido.</p>

  <terms>
    <item>
      <title>Conexão bloqueada ou não confiável</title>
      <p>Alguns dispositivos Bluetooth bloqueiam conexões por padrão ou exigem que você altere uma configuração para permitir que elas sejam realizadas. Certifique-se de que seu dispositivo está configurado para aceitá-las.</p>
    </item>
    <item>
      <title>O hardware Bluetooth não é reconhecido</title>
      <p>Seu adaptador Bluetooth pode não ter sido reconhecido pelo computador. Uma causa possível são <link xref="hardware-driver">drivers</link> para o adaptador não estarem instalados. Alguns adaptadores Bluetooth não têm suporte no Linux, e então você talvez não seja capaz de conseguir os drivers certos para esses. Nesse caso, você teria que conseguir um adaptador Bluetooth diferente provavelmente.</p>
    </item>
    <item>
      <title>O adaptador não está ligado</title>
        <p>Certifique-se de que seu adaptador Bluetooth esteja ligado. Abra o painel de Bluetooth e verifique se ele não está <link xref="bluetooth-turn-on-off">desabilitado</link>.</p>
    </item>
    <item>
      <title>A conexão Bluetooth do dispositivo está desativada</title>
      <p>Verifique se o Bluetooth está ligado no dispositivo ao qual você está tentando se conectar e que ele é <link xref="bluetooth-visibility">encontrável ou visível</link>. Por exemplo, se você está tentando conectar-se a um celular, certifique-se de que ele não está no modo avião.</p>
    </item>
    <item>
      <title>Não há um adaptador Bluetooth no seu computador</title>
      <p>Muitos computadores não têm adaptadores Bluetooth. Você pode comprar um se quiser usar Bluetooth.</p>
    </item>
  </terms>

</page>
