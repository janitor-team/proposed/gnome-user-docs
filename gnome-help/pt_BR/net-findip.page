<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-findip" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="net-general"/>
    <link type="seealso" xref="net-what-is-ip-address"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.37.3" date="2020-08-05" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Rafael Fontenelle</name>
      <email>rafaelff@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Conhecer seu endereço IP pode ajudar você a resolver alguns problemas de rede.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2021.</mal:years>
    </mal:credit>
  </info>

  <title>Descobrindo o seu endereço de IP</title>

  <p>Conhecer o seu endereço IP pode ajudá-lo a resolver problemas com sua conexão com a Internet. Você pode ficar surpreso em saber que você tem <em>dois</em> endereços IP: um endereço IP para seu computador na rede interna e outro para seu computador na Internet.</p>

  <steps>
    <title>Descobra o endereço IP (da rede interna) da sua conexão cabeada</title>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Configurações</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Configurações</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Rede</gui> na barra lateral para abrir o painel.</p>
    </item>
    <item>
      <p>O endereço IP para uma conexão <gui>Cabeada</gui> será exibido à direita junto com algumas informações.</p>
      
      <p>Clique no botão <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">configurações</span></media> para abrir os detalhes da sua conexão.</p>
    </item>
  </steps>

  <note style="info">
    <p its:locNote="TRANSLATORS: See NetworkManager for 'PCI', 'USB' and 'Ethernet'">Se mais de um tipo de conexão cabeada estiver disponível, você pode ver nomes como <gui>PCI Ethernet</gui> ou <gui>USB Ethernet</gui> em vez de <gui>Cabeada</gui>.</p>
  </note>

  <steps>
    <title>Descobra o endereço IP (da rede interna) da sua conexão sem fio</title>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Configurações</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Configurações</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Wi-Fi</gui> na barra lateral para abrir o painel.</p>
    </item>
    <item>
      <p>Clique no botão <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">configurações</span></media> para ver o endereço IP e mais detalhes da sua conexão.</p>
    </item>
  </steps>

  <steps>
  	<title>Descobrir o seu endereço IP externo</title>
    <item>
      <p>Visite <link href="https://whatismyipaddress.com/">whatismyipaddress.com</link>.</p>
    </item>
    <item>
      <p>O site vai exibir seu endereço IP externo para você.</p>
    </item>
  </steps>

  <p>Dependendo de como o seu computador se conecta com a internet, estes dois endereços podem ser o mesmo.</p>

</page>
