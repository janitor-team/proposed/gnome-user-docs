<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="power-nowireless" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="seealso" xref="power-suspendfail"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Alguns dispositivos sem fio têm problemas em lidar quando o computador é suspenso, não resumindo adequadamente.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2021.</mal:years>
    </mal:credit>
  </info>

  <title>Eu não tenho rede sem fio quando acordo meu computador</title>

  <p>Se você suspendeu seu computador, você pode descobrir que sua conexão Internet sem fio não funciona ao resumi-lo depois. Isso acontece quando o <link xref="hardware-driver">driver</link> do dispositivo sem fio não tem suporte completo a determinados recursos de economia de energia. Normalmente, a conexão sem fio falha em ativar corretamente quando o computador é resumido.</p>

  <p>Se isso acontece, tente desligar sua conexão sem fio e ligar novamente:</p>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Wi-Fi</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Wi-Fi</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Alterne <gui>Wi-Fi</gui> no canto superior direito da janela para desligado e ligue-o novamente.</p>
    </item>
    <item>
      <p>Se a conexão sem fio ainda não funcionar, alterne para o <gui>Modo avião</gui> para ligado e para desligado novamente.</p>
    </item>
  </steps>

  <p>Se isso não funcionar, reiniciar seu computador deveria fazer com que sua conexão sem fio funcione novamente. Se você ainda tiver problemas após isto, conecte à Internet usando um cabo Ethernet e atualize seu computador.</p>

</page>
