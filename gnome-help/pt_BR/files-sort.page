<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-sort" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-25" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Organiza arquivos por nome, tamanho, tipo ou quando eles foram modificados.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2021.</mal:years>
    </mal:credit>
  </info>

<title>Organizando arquivos e pastas</title>

<p>Você pode organizar os arquivos de formas diferentes em uma pasta, como, por exemplo, organizando-os por ordem de data ou tamanho de arquivo. Veja <link xref="#ways"/> abaixo para a lista de formas comuns de se organizar arquivos. Veja <link xref="nautilus-views"/> para informações sobre como alterar a ordem padrão da organização.</p>

<p>A forma como você pode organizar arquivos depende da <em>visão da pasta</em> que você está usando. Você pode alterar a visão atual usando a lista ou botões de ícones na barra de ferramentas.</p>

<section id="icon-view">
  <title>Visão de ícone</title>

  <p>Para organizar arquivos em uma ordem diferente, clique no botão de opções de visualização na barra de ferramentas e escolha <gui>Por nome</gui>, <gui>Por tamanho</gui>, <gui>Por tipo</gui>, <gui>Por data de modificação</gui> ou <gui>Por data de acesso</gui>.</p>

  <p>Como um exemplo, se você seleciona <gui>Por nome</gui>, os arquivos vão ser organizados por seus nomes, em ordem alfabética. Veja <link xref="#ways"/> para outras opções.</p>

  <p>Você pode organizar em ordem reversa selecionando <gui>Ordem inversa</gui> no menu.</p>

</section>

<section id="list-view">
  <title>Visão de lista</title>

  <p>Para organizar arquivos em uma ordem diferente, clique em um dos cabeçalhos de colunas no gerenciador de arquivos. Por exemplo, clique em <gui>Tipo</gui> para organizar por tipo de arquivo. Clique no cabeçalho da coluna novamente para organizar na ordem inversa.</p>
  <p>Na visão de lista, você pode mostrar colunas com mais atributos e organizar essas colunas. Clique no botão de opções de visualização na barra de ferramentas, escolha <gui>Colunas visíveis…</gui> e selecione as colunas que você deseja que estejam visíveis. Então, você poderá organizar por essas colunas. Veja <link xref="nautilus-list"/> para descrições das colunas disponíveis.</p>

</section>

<section id="ways">
  <title>Formas de organizar arquivos</title>

  <terms>
    <item>
      <title>Nome</title>
      <p>Organiza alfabeticamente por nome do arquivo.</p>
    </item>
    <item>
      <title>Tamanho</title>
      <p>Organiza por tamanho do arquivo (quanto espaço em disco ele ocupa). Organizar do menor para o maior, por padrão.</p>
    </item>
    <item>
      <title>Tipo</title>
      <p>Organiza alfabeticamente por tipo de arquivo. Arquivos do mesmo tipo são agrupados e, então, organizados por nome.</p>
    </item>
    <item>
      <title>Última Modificação</title>
      <p>Organiza por data e horário que um arquivo foi modificado pela última vez. Organiza do mais antigo para o mais novo, por padrão.</p>
    </item>
  </terms>

</section>

</page>
