<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-hidden" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-12-05" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Conecte a uma rede sem fio que não é exibido na lista de rede.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2021.</mal:years>
    </mal:credit>
  </info>

<title>Conectando a um rede sem fio oculta</title>

<p>É possível configurar uma rede sem fio de forma que ela seja “oculta”. Redes ocultas não vão aparecer na lista de redes sem fio exibidas nas configurações de <gui>Rede</gui>. Para se conectar a uma rede sem fio oculta:</p>

<steps>
  <item>
    <p>Abra o <gui xref="shell-introduction#systemmenu">menu do sistema</gui> no lado direito da barra superior.</p>
  </item>
  <item>
    <p>Selecione <gui>Wi-Fi não está conectado</gui>. A seção Wi-Fi do menu se expandirá.</p>
  </item>
  <item>
    <p>Clique em <gui>Configurações de Wi-Fi</gui>.</p>
  </item>
  <item><p>o botão de menu no canto superior direito da janela e selecione <gui>Conectar a uma rede oculta…</gui>.</p></item>
 <item>
  <p>Na janela que aparecer, selecione uma rede oculta já conectada anteriormente usando a lista suspensa <gui>Conexão</gui> ou <gui>Nova</gui> para uma nova rede.</p>
 </item>
 <item>
  <p>Para uma nova conexão, digite o nome da rede e escolha o tipo de segurança sem fio da lista suspensa <gui>Segurança Wi-Fi</gui>.</p>
 </item>
 <item>
  <p>Informe sua senha ou outros detalhes de segurança.</p>
 </item>
 <item>
  <p>Clique em <gui>Conectar</gui>.</p>
 </item>
</steps>

  <p>Você pode ter que verificar as configurações do roteador ou ponto acesso sem fio para ver qual é o nome da rede. Se você tem o nome da rede (SSID), você pode usar o <em>BSSID</em> (Basic Service Set Identifier, o endereço MAC do ponto de acesso), o que se parece com <gui>02:00:01:02:03:04</gui> e normalmente pode ser encontrado na parte inferior do ponto de acesso.</p>

  <p>Você deve verificar pelas configurações de segurança do ponto de acesso sem fio. Procure por termos como WEP e WPA.</p>

<note>
 <p>Você pode achar que ocular sua rede sem fio vai aumentar a segurança de forma a evitar que pessoas não saibam sobre ela se conectem a ela. Na prática, isso não é o caso; a rede é um pouco mais difícil de localizar, mas ainda sim é detectável.</p>
</note>

</page>
