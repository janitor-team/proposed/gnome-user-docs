<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-calibrate-screen" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrate-printer"/>
    <link type="seealso" xref="color-calibrate-scanner"/>
    <link type="seealso" xref="color-calibrate-camera"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-05" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Calibrar sua tela é importante para exibir cores de forma acurada.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2021.</mal:years>
    </mal:credit>
  </info>

  <title>Como calibro minha tela?</title>

  <p>Você pode calibrar sua tela de forma que ela mostre cores mais precisas. Isso é especialmente útil se você está envolvido em fotografia digital, design ou trabalhos de arte.</p>

  <p>Você vai precisar de um colorímetro ou de um espectrofotômetro para fazer isso. Ambos dispositivos são usados para fazer perfil de telas, mas eles funcionam de formas um pouco diferentes.</p>

  <steps>
    <item>
      <p>Certifique-se que seu dispositivo de calibração está conectado a seu computador.</p>
    </item>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Configurações</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Configurações</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Cor</gui> na barra lateral para abrir o painel.</p>
    </item>
    <item>
      <p>Selecione sua tela.</p>
    </item>
    <item>
      <p>Pressione <gui style="button">Calibrar…</gui> para começar a calibração.</p>
    </item>
  </steps>

  <p>Telas alteram o tempo todo: a luz de fundo em uma TFT reduz à metade do brilho aproximadamente a cada 18 meses, e vai ficando amarelada à medida em que envelhece. Isso significa que você deveria recalibrar sua tela quando o ícone [!] aparece no painel <gui>Cor</gui>.</p>

  <p>Telas de LED também se alteram com o tempo, mas a uma taxa muito mais lenta que a das TFTs.</p>

</page>
