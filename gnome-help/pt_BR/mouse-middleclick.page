<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mouse-middleclick" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="tips"/>
    <link type="guide" xref="mouse#tips"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.4" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Use o botão do meio do mouse para abrir aplicativos, abrir abas e mais.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2021.</mal:years>
    </mal:credit>
  </info>

<title>Clique com o botão do meio</title>

<p>Muitos mouses e alguns touchpad possuem um botão do meio. Em um mouse com roda, você normalmente pode pressionar para baixo diretamente na roda do mouse para fazer o botão do meio. Se você não possuir um botão do meio, você pode pressionar os botões da esquerda e da direita ao mesmo tempo para realizar o clique do meio.</p>

<p>Em touchpads que têm suporte multitoque, você pode tocar com três dedos de uma só vez para realizar o clique do meio. Você tem que <link xref="mouse-touchpad-click">habilitar toque para clicar</link> nas configurações do touchpad para isso funcionar.</p>

<p>Muitos aplicativos usam clique do meio para atalhos de clique avançados.</p>

<list>
  <item><p>Em aplicativos com barras de rolagem, clicar com o botão esquerdo no espaço vazio da barra move a posição de rolagem diretamente para esse local. Clicar com o botão do meio move-se para uma única página em direção a esse local.</p></item>

  <item><p>No panorama de <gui>Atividades</gui>, você pode rapidamente abrir uma nova janela de um aplicativo com um clique no botão do meio. Basta clicar no botão do meio no ícone do aplicativo, podendo ser no dash à esquerda ou no panorama de atividades. O panorama de aplicativos é exibido por meio do botão de grade no dash.</p></item>

  <item><p>A maioria dos navegadores web permite que você abra links em abas rapidamente com o botão do meio de mouse. Basta clicar em qualquer link com seu botão do meio do mouse e ele abrirá em uma nova aba.</p></item>

  <item><p>No gerenciador de arquivos, o botão do meio tem dois funções. Se você clicar com o botão do meio em uma pasta, ela vai abrir em uma nova aba, imitando assim o comportamento de navegadores web populares. Por outro lado, se você clicar com o botão do meio em um arquivo, ele vai abrir o arquivo, como se você tivesse feito clique duplo.</p></item>
</list>

<p>Alguns aplicativos especializados permitem que você use o botão do meio do mouse para outras funções. Pesquise na ajuda do seu aplicativo por <em>botão do meio do mouse</em> ou, em inglês, <em>middle-click</em> ou <em>middle mouse button</em>.</p>

</page>
