<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="bluetooth-remove-connection" xml:lang="mr">

  <info>
    <link type="guide" xref="bluetooth"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.12" date="2014-03-04" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>ब्लुटुथ उपकरणांच्या यादीतून एक उपकरण वगळा.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>Disconnect a Bluetooth device</title>

  <p>If you do not want to be connected to a Bluetooth device anymore, you can
  remove the connection. This is useful if you no longer want to use a device
  like a mouse or headset, or if you no longer wish to transfer files to or
  from a device.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Bluetooth</gui> to open the panel.</p>
    </item>
    <item>
      <p>Select the device which you want to disconnect from the list.</p>
    </item>
    <item>
      <p>In the device dialog box, switch the <gui>Connection</gui> switch to
      off, or to remove the device from the <gui>Devices</gui> list,
      click <gui>Remove Device</gui>.</p>
    </item>
  </steps>

  <p>तुम्ही नंतर जर इच्छा असेल तर <link xref="bluetooth-connect-device">ब्लुटुथ उपकरण परत जोडा</link>.</p>

</page>
