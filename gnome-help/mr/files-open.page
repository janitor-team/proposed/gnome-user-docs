<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-open" xml:lang="mr">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Open files using an application that isn’t the default one for that
    type of file. You can change the default too.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

<title>वेगळ्या ॲप्लिकेशन्स सोबत फाइल्स उघडा</title>

  <p>When you double-click (or middle-click) a file in the file manager, it
  will be opened with the default application for that file type. You can open
  it in a different application, search online for applications, or set the
  default application for all files of the same type.</p>

  <p>To open a file with an application other than the default, right-click
  the file and select the application you want from the top of the menu. If
  you do not see the application you want, select <gui>Open With Other
  Application</gui>. By default, the file manager only shows applications that
  are known to handle the file. To look through all the applications on your
  computer, click <gui>View All Applications</gui>.</p>

<p>If you still cannot find the application you want, you can search for
more applications by clicking <gui>Find New Applications</gui>. The
file manager will search online for packages containing applications
that are known to handle files of that type.</p>

<section id="default">
  <title>पूर्वनिर्धारित ॲप्लिकेशन बदला</title>
  <p>ठराविक प्रकारच्या फाइल्स उघडण्याकरिता वापरण्याजोगी पूर्वनिर्धारित ॲप्लिकेशन बदलणे शक्य आहे. यामुळे फाइल उघडण्याकरिता दोनवेळा क्लिक केन्यानंतर तुम्ही पसंतीचे शिफारसीय ॲप्लिकेशन उघडू शकाल. उदाहरणार्थ, MP3 फाइलवर दोनवेळा क्लिक केल्यानंतर पसंतीच्या संगीत वादकतर्फे उघडायला आवडेल.</p>

  <steps>
    <item><p>बदलण्याजोगी पूर्वनिर्धारित ॲप्लिकेशनचे फाइल प्रकार निवडा. उदाहरणार्थ, MP3 फाइल्स उघडण्याकरिता ॲप्लिकेशन बदलण्याकरिता, <file>.mp3</file> फाइल निवडा.</p></item>
    <item><p>फाइलवर उजवा-क्लिक करा आणि <gui>गुणधर्म</gui> क्लिक करा.</p></item>
    <item><p><gui>सोबत उघडा</gui> टॅब निवडा.</p></item>
    <item><p>Select the application you want and click
    <gui>Set as default</gui>.</p>
    <p>If <gui>Other Applications</gui> contains an application you sometimes
    want to use, but do not want to make the default, select that application
    and click <gui>Add</gui>. This will add it to <gui>Recommended
    Applications</gui>. You will then be able to use this application by
    right-clicking the file and selecting it from the list.</p></item>
  </steps>

  <p>फक्त निवडलेल्या फाइलकरिताच नाही तर पूर्वनिर्धारित ॲप्लिकेशनकरिता बदलते, परंतु समान प्रकारच्या सर्व फाइल्सकरिता.</p>

<!-- TODO: mention resetting the open with list with the "Reset" button -->

</section>

</page>
