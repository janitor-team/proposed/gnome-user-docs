<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="contacts-edit-details" xml:lang="mr">

  <info>
    <link type="guide" xref="contacts"/>

    <revision pkgversion="3.5.5" date="2012-08-13" status="review"/>
    <revision pkgversion="3.8" date="2013-04-27" status="review"/>
    <revision pkgversion="3.12" date="2014-02-26" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.36.2" date="2020-08-11" status="review"/>
    <revision pkgversion="3.38.0" date="2020-11-02" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <credit type="editor">
      <name>Pranali Deshmukh</name>
      <email>pranali21293@gmail.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>प्रत्येक संपर्काची माहिती संपादित करा.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

<title>संपर्काची माहिती संपादित करा</title>

  <p>संपर्क तपशील संपादित केल्याने पत्ता पुस्तिकामधील माहिती अपटूडेट आणि परिपूर्ण राहते.</p>

  <steps>
    <item>
      <p>संपर्क यादीतून संपर्क निवडा.</p>
    </item>
    <item>
      <p>Press the <media its:translate="no" type="image" src="figures/view-more-symbolic.svg">
      <span its:translate="yes">view more</span></media> 
      button in the top-right corner of the window and
      select <gui style="menuitem">Edit</gui>.</p>
    </item>
    <item>
      <p>संपर्काची माहिती संपादित करा.</p>
      <p>To add a <em>detail</em> such as a new phone number or email
      address, just fill in the details on the next empty field of the
      type (phone number, email, etc.) you want to add.</p>
      <note style="tip">
        <p>Press the <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">view more</span></media>
        option at the bottom to expand available options,
        revealing fields like <gui>Website</gui> and <gui>Birthday</gui>.</p>
      </note>
    </item>
    <item>
      <p>संपर्काची माहिती संपादित करणे पूर्ण करण्यासाठी <gui style="button">पूर्ण झाले</gui> दाबा.</p>
    </item>
  </steps>

</page>
