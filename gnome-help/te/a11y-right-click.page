<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-right-click" xml:lang="te">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-21" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>షాన్ మెక్‌కేన్స్</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="author">
      <name>ఫిల్ బుల్</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>మైకేల్ హిల్</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>కుడి-నొక్కు నొక్కుటకు ఎడమ మౌస్ బటన్‌ను వత్తి పట్టి వుంచు.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Praveen Illa</mal:name>
      <mal:email>mail2ipn@gmail.com</mal:email>
      <mal:years>2011, 2014. </mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>కృష్ణబాబు క్రొత్తపల్లి</mal:name>
      <mal:email>kkrothap@redhat.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>కుడి మౌస్ నొక్కు సిమ్యులేట్ చేయి</title>

  <p>ఎడమ మౌస్ బటన్‌ను నొక్కిపట్టి మీరు కుడి-నొక్కు నొక్కవచ్చు. మీరు మీ చేతి వేళ్ళను విడివిడిగా కదిలించుటలో సమస్య వున్నా లేగా మీ సూచకి పరికరం వొక బటన్ మాత్రమే కలిగివున్నా ఇది మీకు వుపయోగకరంగా వుంటుంది.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Accessibility</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Accessibility</gui> to open the panel.</p>
    </item>
    <item>
      <p>Press <gui>Click Assist</gui> in the <gui>Pointing &amp; Clicking</gui>
      section.</p>
    </item>
    <item>
      <p>In the <gui>Click Assist</gui> window, switch the <gui>Simulated
      Secondary Click</gui> switch to on.</p>
    </item>
  </steps>

  <p>You can change how long you must hold down the left mouse button before it
  is registered as a right click by changing the <gui>Acceptance
  delay</gui>.</p>

  <p>To right-click with simulated secondary click, hold down the left mouse
  button where you would normally right-click, then release. The pointer fills
  with a different color as you hold down the left mouse button. Once it will
  change this color entirely, release the mouse button to right-click.</p>

  <p>Some special pointers, such as the resize pointers, do not change colors.
  You can still use simulated secondary click as normal, even if you do not get
  visual feedback from the pointer.</p>

  <p>ఒకవేళ మీరు <link xref="mouse-mousekeys">మాస్ కీలు</link> వుపయోగించితే, మీ కీపాడ్ పైని <key>5</key> కీను నొక్కివుంచుట ద్వారా మీరు కుడి-నొక్కు నొక్కవచ్చు.</p>

  <note>
    <p>In the <gui>Activities</gui> overview, you are always able to long-press
    to right-click, even with this feature disabled. Long-press works slightly
    differently in the overview: you do not have to release the button to
    right-click.</p>
  </note>

</page>
