<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-removedrive" xml:lang="te">

  <info>
    <link type="guide" xref="files#removable"/>

    <credit type="author">
      <name>షాన్ మెక్‌కేన్స్</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>జిమ్ క్యాంబెల్</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>మైకేల్ హిల్</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <revision pkgversion="3.6.0" version="0.2" date="2012-10-08" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>USB ఫ్లాష్ డ్రైవు, CD, DVD, లేదా ఇతర పరికరం బయటకు నెట్టు లేదా అన్‌మౌంట్ చేయి.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Praveen Illa</mal:name>
      <mal:email>mail2ipn@gmail.com</mal:email>
      <mal:years>2011, 2014. </mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>కృష్ణబాబు క్రొత్తపల్లి</mal:name>
      <mal:email>kkrothap@redhat.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>బాహ్య డ్రైవు సురక్షితంగా తీసివేయి</title>

  <p>USB ఫ్లాష్ డ్రైవుల వంటి బాహ్య నిల్వ పరికరాలు వుపయోగించునప్పుడు, మీరు వాటిని పీకువేయుటకు ముందుగా వాటిని సురక్షితంగా తీసివేయాలి. మీరు ఒక పరికరం పీకివేస్తే, ఒక అనువర్తనం దానిని వుపయోగించునప్పుడు పీకివేసే ప్రమాదం వుంటుంది. దీని వలన మీ ఫైళ్ళు కొన్ని పాడవ్వొచ్చు లేదా పోవచ్చు. మీరు CD, DVD వంటి ఆప్టికల్ డిస్కు ఉపయోగించునప్పుడు, మీ కంప్యూటర్ నుండి డిస్కును బయటకు నెట్టుటకు మీరు అవే స్టెప్పులు వాడవచ్చు.</p>

  <steps>
    <title>తీసివేయదగు పరికరం బయటకు నెట్టుటకు:</title>
    <item>
      <p>From the <gui xref="shell-introduction#activities">Activities</gui> overview,
      open <app>Files</app>.</p>
    </item>
    <item>
      <p>పరికరంను పక్కపట్టీనందు గుర్తించుము. అది పేరుకు పక్కన ఒక చిన్న బయటకునెట్టు ప్రతిమ కలిగివుండాలి. పరికరం సురక్షితంగా తీసివేయుటకు లేదా బయటకునెట్టుటకు  బయటకునెట్టు  ప్రతిమ నొక్కండి.</p>
      <p>ప్రత్యామ్నాయంగా, పక్కపట్టీ నందలి పరికరపు పైరుపై కుడి-నొక్కు నొక్కి <gui>బయటకునెట్టు</gui> ఎంపికచేయి.</p>
    </item>
  </steps>

  <section id="remove-busy-device">
    <title>ఉపయోగంలో వున్న పరికరం సురక్షితంగా తీసివేయి</title>

  <p>పరకరం పైని ఏదేని ఫైళ్ళు తెరిచివున్నా మరియు వేరొక అనువర్తనంచే వుపయోగంలోవున్నా, మీరు పరికరంను సురక్షితంగా తీసివేయలేరు. <gui>వాల్యూమ్ బ్యుజీగా వుంది</gui> అని ఒక విండో మీకు చెప్పును. పరికరం సురక్షితంగా తీసివేయుటకు:</p>

  <steps>
    <item><p><gui>రద్దుచేయి</gui> నొక్కుము.</p></item>
    <item><p>పరికరంపైని అన్ని ఫైళ్ళను మూసివేయి.</p></item>
    <item><p>పరికరం సురక్షితంగా తీసివేయుటకు లేదా బయటకునెట్టుటకు బయటకునెట్టు ప్రతిమపై నొక్కుము.</p></item>
    <item><p>ప్రత్యామ్నాయంగా, పక్కపట్టీ నందలి పరికరపు పైరుపై కుడి-నొక్కు నొక్కి <gui>బయటకునెట్టు</gui> ఎంపికచేయి.</p></item>
  </steps>

  <note style="warning"><p>ఫైళ్ళను మూయకుండా పరికరం తీసివేయుటకు మీరు <gui>ఏమైనాసరే బయటకునెట్టు</gui> కూడా ఎంచుకొనవచ్చు. ఇది ఆ ఫైళ్ళను తెరచిన అనువర్తనాల నందు దోషములకు కారణం కావచ్చు.</p></note>

  </section>

</page>
