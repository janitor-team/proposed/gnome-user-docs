<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="backup-how" xml:lang="te">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit>
      <name>గ్నోమ్ పత్రీకరణ పరియోజన</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>మైకేల్ హిల్</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Klein Kravis</name>
      <email>kleinkravis44@outlook.com</email>
      <years>2020</years>
    </credit>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Use Déjà Dup (or another backup application) to make copies of
    your valuable files and settings to protect against loss.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Praveen Illa</mal:name>
      <mal:email>mail2ipn@gmail.com</mal:email>
      <mal:years>2011, 2014. </mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>కృష్ణబాబు క్రొత్తపల్లి</mal:name>
      <mal:email>kkrothap@redhat.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

<title>బ్యాక్అప్ తీయడం ఎలా</title>

  <p>మీ ఫైళ్ళను మరియు అమరికలను బ్యాకప్ తీయుటకు సుళువైన మార్గం ఏదేని బ్యాకప్ అనువర్తనాన్ని మీ బ్యాకప్ కార్యక్రమం నిర్వహించుటకు అనుమతించడమే. చాలా విభిన్న బ్యాకప్ అనువర్తనాలు అందుబాటులోవున్నాయి, ఉదాహరణకు <app>Déjà Dup</app>.</p>

  <p>మీరు ఎంచుకొనిన బ్యాకప్ అనువర్తనపు సహాయం అనునది బ్యాకప్ కొరకు మీ అభీష్టాలను అమర్చుటకు, అదేవిదంగా మీ దత్తాంశం తిరిగిపొందుటకు మార్గదర్శనంచేయును.</p>

  <p>An alternative option is to <link xref="files-copy">copy your files</link>
 to a safe location, such as an external hard drive, an online storage service,
 or a USB drive. Your <link xref="backup-thinkabout">personal files</link>
 and settings are usually in your Home folder, so you can copy them from there.</p>

  <p>మీరు బ్యాకప్ తీయాలని అనుకుంటున్న పరిమాణం మీ నిల్వ పరికరం పరిమాణంకు పరిమితమై వుంటుంది. మీ బ్యాకప్ పరికరం పైన మీకు జాగా వుంటే, కింది వాటిని విస్మరించి మీ మొత్త నివాస సంచయం బ్యాకప్ తీయుట మంచిది.</p>

<list>
 <item><p>Files that are already backed up somewhere else, such as to a USB drive,
 or other removable media.</p></item>
 <item><p>Files that you can recreate easily. For example, if you are a
 programmer, you do not have to back up the files that get produced when you
 compile your programs. Instead, just make sure that you back up the original
 source files.</p></item>
 <item><p>చెత్త సంచయం నందలి ఏ ఫైళ్ళైనా. మీ చెత్త సంచయం <file>~/.local/share/Trash</file> నందు వుంటుంది.</p></item>
</list>

</page>
