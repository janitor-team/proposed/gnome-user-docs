<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-hidden" xml:lang="te">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-29" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>ఫిల్ బుల్</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>మైకేల్ హిల్</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Make a file invisible, so you cannot see it in the file
    manager.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Praveen Illa</mal:name>
      <mal:email>mail2ipn@gmail.com</mal:email>
      <mal:years>2011, 2014. </mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>కృష్ణబాబు క్రొత్తపల్లి</mal:name>
      <mal:email>kkrothap@redhat.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

<title>ఒక ఫైలును మరుగునపెట్టు</title>

  <p>The <app>Files</app> file manager gives you the ability to hide and unhide
  files at your discretion. When a file is hidden, it is not displayed by the
  file manager, but it is still there in its folder.</p>

  <p>To hide a file, <link xref="files-rename">rename it</link> with a
  <file>.</file> at the beginning of its name. For example, to hide a file
  named <file>example.txt</file>, you should rename it to
  <file>.example.txt</file>.</p>

<note>
  <p>You can hide folders in the same way that you can hide files. Hide a
  folder by placing a <file>.</file> at the beginning of the folder’s name.</p>
</note>

<section id="show-hidden">
 <title>మరుగునవున్న అన్ని ఫైళ్ళను చూపుము</title>

  <p>If you want to see all hidden files in a folder, go to that folder and
  either click the
  <!-- FIXME: Get a tooltip added for "View options" -->
  view options button in the toolbar and pick <gui>Show Hidden Files</gui>, or press
  <keyseq><key>Ctrl</key><key>H</key></keyseq>. You will see all hidden files,
  along with regular files that are not hidden.</p>

  <p>To hide these files again, either click the
  <!-- FIXME: Get a tooltip added for "View options" -->
  view options button in the toolbar and pick <gui>Show Hidden Files</gui>, or press
  <keyseq><key>Ctrl</key><key>H</key></keyseq> again.</p>

</section>

<section id="unhide">
 <title>ఒకఫైలును బయల్పరచుట</title>

  <p>To unhide a file, go to the folder containing the hidden file and click
  the
  <!-- FIXME: Get a tooltip added for "View options" -->
  view options button in the toolbar and pick <gui>Show Hidden Files</gui>.
  Then, find the hidden file and rename it so that it does not have a
  <file>.</file> in front of its name. For example, to unhide a file called
  <file>.example.txt</file>, you should rename it to
  <file>example.txt</file>.</p>

  <p>Once you have renamed the file, you can either click the
  <!-- FIXME: Get a tooltip added for "View options" -->
  view options button in the toolbar and pick <gui>Show Hidden Files</gui>, or
  press <keyseq><key>Ctrl</key><key>H</key></keyseq> to hide any other hidden
  files again.</p>

  <note><p>అప్రమేయంగా, మీరు ఫైల్ నిర్వాహికను మూసివేయునంతవరకు మీరు మరుగునవున్న ఫైళ్ళను ఫైల్ నిర్వాహికనందు మాత్రమే చూస్తారు. ఫైల్ నిర్వాహిక ఎల్లప్పుడూ మరుగునవున్న ఫైళ్ళను చూపునట్లు ఈ అమరికను మార్చుటకు, <link xref="nautilus-views"/> చూడండి.</p></note>

  <note><p>Most hidden files will have a <file>.</file> at the
  beginning of their name, but others might have a <file>~</file> at the end of
  their name instead. These files are backup files. See
  <link xref="files-tilde"/> for more information.</p></note>

</section>

</page>
