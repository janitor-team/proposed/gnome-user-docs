<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-browse" xml:lang="te">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-copy"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.38" date="2020-10-16" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>షాన్ మెక్‌కేన్స్</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>ఫిల్ బుల్</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>మైకేల్ హిల్</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>ఫైల్ నిర్వాహికతో ఫైళ్ళను నిర్వహించు మరియు సర్దు.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Praveen Illa</mal:name>
      <mal:email>mail2ipn@gmail.com</mal:email>
      <mal:years>2011, 2014. </mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>కృష్ణబాబు క్రొత్తపల్లి</mal:name>
      <mal:email>kkrothap@redhat.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

<title>ఫైళ్ళను మరియు సంచయాలను విహరించండి</title>

<p>మీ కంప్యూటర్ పైన ఫైళ్ళను బ్రౌజ్ చేయుటకు మరియు నిర్వహించుటకు <app>ఫైళ్ళు</app> ఫైల్ నిర్వాహిక ఉపయోగించుము. <link xref="nautilus-connect">ఫైల్ సేవికలు</link> పైని, మరియు నెట్వర్కు భాగస్వామ్యాలపైని, నిల్వ పరికరాలు (బాహ్య హార్గు డిస్కులు వంటివి) పైని ఫైళ్ళను నిర్వహించుటకు మీరు దీనిని ఉపయోగించవచ్చు.</p>

<p>To start the file manager, open <app>Files</app> in the
<gui xref="shell-introduction#activities">Activities</gui> overview. You can also search
for files and folders through the overview in the same way you would
<link xref="shell-apps-open">search for applications</link>.
</p>

<section id="files-view-folder-contents">
  <title>సంచయాల కాంటెంట్లను గాలించుట</title>

<p>In the file manager, double-click any folder to view its contents, and
double-click or <link xref="mouse-middleclick">middle-click</link> any file to
open it with the default application for that file. Middle-click a folder to
open it in a new tab. You can also right-click a folder to open it in a new tab
or new window.</p>

<p>సంచయం నందలి ఫైళ్ళు చూడునప్పుడు, స్పేస్ బార్ నొక్కి త్వరితంగా <link xref="files-preview">ప్రతి ఫైలు ముందస్తుదర్శనం</link> చేసి  మీరు తెరవాలని, నకలుతీయాలని, లేదా తొలగించాలని అనుకొన్న ఫైలు అదేనా కాదా అనేది నిర్థారించుకోవచ్చు.</p>

<p>The <em>path bar</em> above the list of files and folders shows you which
folder you’re viewing, including the parent folders of the current folder.
Click a parent folder in the path bar to go to that folder. Right-click any
folder in the path bar to open it in a new tab or window, or access its
properties.</p>

<p>If you want to quickly <link xref="files-search">search for a file</link>,
in or below the folder you are viewing, start typing its name. A <em>search
bar</em> will appear at the top of the window and only files which match your
search will be shown. Press <key>Esc</key> to cancel the search.</p>

<p>You can quickly access common places from the <em>sidebar</em>. If you do
not see the sidebar, press the menu button in the top-right corner of the window
and then select <gui>Sidebar</gui>. You can <link xref="nautilus-bookmarks-edit">add
bookmarks to folders that you use often</link> and they will appear in the sidebar.</p>

</section>

</page>
