<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-select" xml:lang="te">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>ఫిల్ బుల్</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Print only specific pages, or only a range of pages.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Praveen Illa</mal:name>
      <mal:email>mail2ipn@gmail.com</mal:email>
      <mal:years>2011, 2014. </mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>కృష్ణబాబు క్రొత్తపల్లి</mal:name>
      <mal:email>kkrothap@redhat.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>Print only certain pages</title>

  <p>To only print certain pages from the document:</p>

  <steps>
    <item>
      <p>Open the print dialog by pressing
      <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>In the <gui>General</gui> tab, choose <gui>Pages</gui> from the
      <gui>Range</gui> section.</p>
    </item>
    <item><p>Type the numbers of the pages you want to print in the text box,
    separated by commas. Use a dash to denote a range of pages.</p></item>
  </steps>

  <note>
    <p>For example, if you enter “1,3,5-7” in the <gui>Pages</gui> text box,
    pages 1,3,5,6 and 7 will be printed.</p>
    <media type="image" src="figures/printing-select.png"/>
  </note>

</page>
