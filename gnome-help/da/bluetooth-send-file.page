<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="bluetooth-send-file" xml:lang="da">

  <info>
    <link type="guide" xref="bluetooth"/>
    <link type="guide" xref="sharing"/>
    <link type="seealso" xref="files-share"/>

    <revision pkgversion="3.8" date="2013-05-16" status="review"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Del filer til Bluetooth-enheder såsom din telefon.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  </info>

  <title>Send filer til en Bluetooth-enhed</title>

  <p>Du kan sende filer til tilsluttede Bluetooth-enheder såsom nogle mobiltelefoner eller andre computere. Nogle typer enheder tillader ikke overførsel af filer eller bestemte typer filer. Du kan sende filer med Bluetooth-indstillingsvinduet.</p>

  <note style="important">
    <p><gui>Send filer</gui> virker ikke på enheder der ikke understøttes såsom iPhones.</p>
  </note>

  <steps>
    <item>
      <p>Åbn <gui xref="shell-introduction#activities">Aktivitetsoversigten</gui> og begynd at skrive <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Klik på <gui>Bluetooth</gui> for at åbne panelet.</p>
    </item>
    <item>
      <p>Sørg for at Bluetooth er aktiveret: kontakten i titellinjen skal være tændt.</p>
    </item>
    <item>
      <p>I listen <gui>Enheder</gui> vælges den enhed der skal sendes filer til. Hvis den ønskede enhed ikke vises som <gui>Forbundet</gui> i listen, så skal du <link xref="bluetooth-connect-device">oprette forbindelse</link> til den.</p>
      <p>Der vises et panel specifikt til den eksterne enhed.</p>
    </item>
    <item>
      <p>Klik på <gui>Send filer …</gui>, så vises filvælgeren.</p>
    </item>
    <item>
      <p>Vælg den fil du vil sende og klik på <gui>Vælg</gui>.</p>
      <p>For at sende end mere en fil i en mappe, holdes <key>Ctrl</key> nede mens du vælger hver fil.</p>
    </item>
    <item>
      <p>Ejeren af den modtagende enhed skal gerne trykke på en knap for at acceptere filen. Dialogen <gui>Bluetooth-filoverførsel</gui> viser forløbslinjen. Klik på <gui>Luk</gui> når overførslen er færdig.</p>
    </item>
  </steps>

</page>
