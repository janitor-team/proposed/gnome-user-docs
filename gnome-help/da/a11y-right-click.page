<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-right-click" xml:lang="da">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-21" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Tryk på og hold den venstre museknap nede for at højreklikke.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  </info>

  <title>Simulér et højreklik med musen</title>

  <p>Du kan højreklikke ved at holde den venstre museknap nede. Det er nyttigt hvis du har svært ved at flytte dine fingre hver for sig på den ene hånd eller hvis din pegeenhed kun har én knap.</p>

  <steps>
    <item>
      <p>Åbn <gui xref="shell-introduction#activities">Aktivitetsoversigten</gui> og begynd at skrive <gui>Tilgængelighed</gui>.</p>
    </item>
    <item>
      <p>Klik på <gui>Tilgængelighed</gui> for at åbne panelet.</p>
    </item>
    <item>
      <p>Tryk på <gui>Klikhjælp</gui> i afsnittet <gui>Pege og klikke</gui>.</p>
    </item>
    <item>
      <p>Tænd for <gui>Simuleret sekundært klik</gui> i vinduet <gui>Klikhjælp</gui>.</p>
    </item>
  </steps>

  <p>Du kan ændre hvor længe du skal holde den venstre museknap nede inden det registreres som et højreklik ved at ændre <gui>Ventetid for accept</gui>.</p>

  <p>For at højreklikke med simuleret sekundært klik holdes den venstre museknap nede, hvor du normalt ville højreklikke, og herefter slippes den. Markøren udfyldes med en anden farve mens du holder den venstre museknap nede. Når farven er ændret helt, så slip museknappen for at højreklikke.</p>

  <p>Visse specielle markører såsom markørene til at tilpasse størrelse ændrer ikke farve. Du kan stadigvæk bruge simuleret sekundært klik som normalt, selv hvis du ikke får visuel feedback fra markøren.</p>

  <p>Hvis du bruger <link xref="mouse-mousekeys">musetaster</link>, så giver det dig også mulighed for at højreklikke ved at holde <key>5</key>-tasten nede på dit numeriske tastatur.</p>

  <note>
    <p>I <gui>Aktivitetsoversigten</gui> kan du også bruge langvarigt tryk til at højreklikke, selv hvis funktionaliteten er deaktiveret. Langvarigt tryk virker en smule anderledes i oversigten: du behøver ikke slippe knappen for at højreklikke.</p>
  </note>

</page>
