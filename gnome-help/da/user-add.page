<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-add" xml:lang="da">

  <info>
    <link type="guide" xref="user-accounts#manage" group="#first"/>

    <revision pkgversion="3.8" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2013-11-01" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>GNOMEs dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Tilføj nye brugere så andre personer kan logge ind på computeren.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  </info>

  <title>Tilføj en ny brugerkonto</title>

  <p>Du kan tilføje flere brugerkontoer til din computer. Giv en konto til hver person i dit hjem eller virksomhed. Hver bruger har deres egen hjemmemappe, dokumenter og indstillinger.</p>

  <p>Du skal have <link xref="user-admin-explain">administratorrettigheder</link> for at tilføje brugerkontoer.</p>

  <steps>
    <item>
      <p>Åbn <gui xref="shell-introduction#activities">Aktivitetsoversigten</gui> og begynd at skrive <gui>Brugere</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Users</gui> to open the panel.</p>
    </item>
    <item>
      <p>Tryk på <gui style="button">Lås op</gui> i øverste højre hjørne og skriv din adgangskode, når du bliver spurgt.</p>
    </item>
    <item>
      <p>Tryk på knappen <gui style="button">+</gui> under listen over kontoer til venstre og tilføj en ny brugerkonto.</p>
    </item>
    <item>
      <p>Hvis den nye bruger skal have <link xref="user-admin-explain">administrativ adgang</link> til computeren, så vælg <gui>Administrator</gui> som kontotype.</p>
      <p>Administratorer kan gøre ting som at tilføje og slette brugere, installere software og drivere og ændre dato og klokkeslæt.</p>
    </item>
    <item>
      <p>Indtast den nye brugers fulde navn. Brugernavnet udfyldes automatisk baseret på det fulde navn. Hvis du ikke kan lide det foreslået brugernavn, så kan du ændre det.</p>
    </item>
    <item>
      <p>Du kan vælge at indstille en adgangskode til den nye bruger eller lade dem indstille det selv første gang de logger ind.</p>
      <p>Hvis du vælger at indstille adgangskoden nu, så kan du trykke på <gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">generér adgangskode</span></media></gui>-ikonet for automatisk at generere en tilfældig adgangskode.</p>
    </item>
    <item>
      <p>Klik på <gui>Tilføj</gui>.</p>
    </item>
  </steps>

  <p>Hvis du vil ændre adgangskoden efter kontoen er blevet oprettet, så vælg kontoen, <gui style="button">Lås op</gui> og tryk på statussen for den nuværende adgangskode.</p>

  <note>
    <p>I panelet <gui>Brugere</gui> kan du klikke på billedet ved siden af brugerens navn til højre for at indstille et billede til kontoen. Billedet vises i loginvinduet. Systemet leverer nogle stockbilleder som du kan bruge eller du kan vælge dine egne eller tage et billede med dit kamera.</p>
  </note>

</page>
