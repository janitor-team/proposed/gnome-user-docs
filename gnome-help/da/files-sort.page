<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-sort" xml:lang="da">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-25" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Opstil filer efter navn, størrelse, type eller hvornår de blev ændret.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  </info>

<title>Sortér filer og mapper</title>

<p>You can sort files in different ways in a folder, for example by sorting them
in order of date or file size. See <link xref="#ways"/> below for a list of
common ways to sort files. See <link xref="nautilus-views"/> for information on
how to change the default sort order.</p>

<p>The way that you can sort files depends on the <em>folder view</em> that you
are using. You can change the current view using the list or icon buttons in the
toolbar.</p>

<section id="icon-view">
  <title>Ikonvisning</title>

  <p>To sort files in a different order, click the view options button in the
  <!-- FIXME: Get a tooltip added for "View options" -->
  toolbar and choose <gui>By Name</gui>, <gui>By Size</gui>, <gui>By
  Type</gui>, <gui>By Modification Date</gui>, or <gui>By Access
  Date</gui>.</p>

  <p>As an example, if you select <gui>By Name</gui>, the files will be sorted
  by their names, in alphabetical order. See <link xref="#ways"/> for other
  options.</p>

  <p>You can sort in the reverse order by selecting <gui>Reversed Order</gui>
  from the menu.</p>

</section>

<section id="list-view">
  <title>Listevisning</title>

  <p>To sort files in a different order, click one of the column headings in
  the file manager. For example, click <gui>Type</gui> to sort by file type.
  Click the column heading again to sort in the reverse order.</p>
  <p>In list view, you can show columns with more attributes and sort on those
  columns. Click the view options button in the toolbar, pick <gui>Visible
  <!-- FIXME: Get a tooltip added for "View options" -->
  Columns…</gui> and select the columns that you want to be visible. You will
  then be able to sort by those columns. See <link xref="nautilus-list"/> for
  descriptions of available columns.</p>

</section>

<section id="ways">
  <title>Filsortering</title>

  <terms>
    <item>
      <title>Navn</title>
      <p>Sorts alphabetically by the name of the file.</p>
    </item>
    <item>
      <title>Størrelse</title>
      <p>Sorts by the size of the file (how much disk space it takes up). Sorts
      from smallest to largest by default.</p>
    </item>
    <item>
      <title>Type</title>
      <p>Sorts alphabetically by the file type. Files of the same type are
      grouped together, then sorted by name.</p>
    </item>
    <item>
      <title>Sidst ændret</title>
      <p>Sorts by the date and time that a file was last changed. Sorts from
      oldest to newest by default.</p>
    </item>
  </terms>

</section>

</page>
