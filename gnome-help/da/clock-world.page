<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="clock-world" xml:lang="da">

  <info>
    <link type="guide" xref="clock" group="#last"/>
    <link type="seealso" href="help:gnome-clocks/index"/>

    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.28" date="2018-07-30" status="review"/>
    <revision pkgversion="3.37" date="2020-08-06" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhill@gnome.org</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Vis klokkeslæt fra andre byer under kalenderen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  </info>

  <title>Tilføj et verdensur</title>

  <p>Brug <app>Ure</app> for at tilføje klokkeslæt fra andre byer.</p>

  <note>
    <p>Det kræver at programmet <app>Ure</app> er installeret.</p>
    <p>De fleste distributioner kommer med <app>Ure</app> installeret som standard. Hvis din ikke gør, så kan det være nødvendigt at installere det med din distributions pakkehåndtering.</p>
  </note>

  <p>Tilføj et verdensur:</p>

  <steps>
    <item>
      <p>Klik på uret på toplinjen.</p>
    </item>
    <item>
      <p>Klik på knappen <gui>Tilføj verdensure …</gui> under kalenderen for at starte <app>Ure</app>.</p>

    <note>
       <p>Hvis du allerede har et eller flere verdensure, så klik på et af dem og <app>Ure</app> starter.</p>
    </note>

    </item>
    <item>
      <p>I vinduet <app>Ure</app> skal du klikke på <gui style="button">+</gui>-knappen, eller trykke på <keyseq><key>Ctrl</key><key>N</key></keyseq> for at tilføje en ny by.</p>
    </item>
    <item>
      <p>Begynd at skrive navnet på byen i søgning.</p>
    </item>
    <item>
      <p>Vælg den rette by eller den placering der er nærmest dig i listen.</p>
    </item>
    <item>
      <p>Tryk på <gui style="button">Tilføj</gui> for at færdiggøre tilføjelsen af byen.</p>
    </item>
  </steps>

  <p>Se <link href="help:gnome-clocks">Hjælp til Ure</link> for mere information om hvad <app>Ure</app> er i stand til.</p>

</page>
