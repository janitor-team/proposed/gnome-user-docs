<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-autologin" xml:lang="da">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="shell-exit"/>

    <revision pkgversion="3.8" date="2013-04-04" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Opsæt automatisk login til når du tænder din computer.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  </info>

  <title>Automatisk indlogning</title>

  <p>You can change your settings so that you are automatically logged in to
  your account when you start up your computer:</p>

  <steps>
    <item>
      <p>Åbn <gui xref="shell-introduction#activities">Aktivitetsoversigten</gui> og begynd at skrive <gui>Brugere</gui>.</p>
    </item>
    <item>
      <p>Klik på <gui>Brugere</gui> for at åbne panelet.</p>
    </item>
    <item>
      <p>Vælg brugerkontoen som automatisk skal logge ind ved opstart.</p>
    </item>
    <item>
      <p>Tryk på <gui style="button">Lås op</gui> i øverste højre hjørne og skriv din adgangskode, når du bliver spurgt.</p>
    </item>
    <item>
      <p>Tænd for <gui>Automatisk login</gui>-kontakten.</p>
    </item>
  </steps>

  <p>When you next start up your computer, you will be logged in automatically.
  If you have this option enabled, you will not need to type in your password
  to log in to your account which means that if someone else starts up your
  computer, they will be able to access your account and your personal data
  including your files and browser history.</p>

  <note>
    <p>If your account type is <em>Standard</em>, you cannot change this
    setting. Contact your system administrator who can change this setting
    for you.</p>
  </note>

</page>
