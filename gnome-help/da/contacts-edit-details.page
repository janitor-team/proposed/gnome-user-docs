<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="contacts-edit-details" xml:lang="da">

  <info>
    <link type="guide" xref="contacts"/>

    <revision pkgversion="3.5.5" date="2012-08-13" status="review"/>
    <revision pkgversion="3.8" date="2013-04-27" status="review"/>
    <revision pkgversion="3.12" date="2014-02-26" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.36.2" date="2020-08-11" status="review"/>
    <revision pkgversion="3.38.0" date="2020-11-02" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <credit type="editor">
      <name>Pranali Deshmukh</name>
      <email>pranali21293@gmail.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Rediger informationen for hver kontakt.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  </info>

<title>Rediger kontaktdetaljer</title>

  <p>Redigering af kontaktdetaljer hjælper dig med at holde informationen i din adressebog opdateret og komplet.</p>

  <steps>
    <item>
      <p>Vælg kontakten i din kontaktliste.</p>
    </item>
    <item>
      <p>Press the <media its:translate="no" type="image" src="figures/view-more-symbolic.svg">
      <span its:translate="yes">view more</span></media> 
      button in the top-right corner of the window and
      select <gui style="menuitem">Edit</gui>.</p>
    </item>
    <item>
      <p>Rediger kontaktdetaljerne.</p>
      <p>For at tilføje en <em>detalje</em> såsom et nyt telefonnummer eller e-mailadresse skal du blot udfylde detaljerne i det næste tomme felt med den type (telefonnummer, e-mail osv.) du vil tilføje.</p>
      <note style="tip">
        <p>Press the <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">view more</span></media>
        option at the bottom to expand available options,
        revealing fields like <gui>Website</gui> and <gui>Birthday</gui>.</p>
      </note>
    </item>
    <item>
      <p>Tryk på <gui style="button">Udført</gui> for at færdiggøre redigeringen af kontakten.</p>
    </item>
  </steps>

</page>
