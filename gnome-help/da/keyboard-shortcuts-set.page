<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="keyboard-shortcuts-set" xml:lang="da">

  <info>
    <link type="guide" xref="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-23" status="review"/>
    <revision pkgversion="3.9.92" date="2013-10-11" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.17.90" date="2015-08-30" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>
    <revision version="gnome:40" date="2021-03-02" status="review"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Juanjo Marín</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Angiv eller ændr tastaturgenveje i indstillingerne <gui>Tastatur</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  </info>

  <title>Indstil tastaturgenveje</title>

<p>Ændr den tast eller de taster der skal trykkes på for en tastaturgenvej:</p>

  <steps>
    <item>
      <p>Åbn <gui xref="shell-introduction#activities">Aktivitetsoversigten</gui> og begynd at skrive <gui>Indstillinger</gui>.</p>
    </item>
    <item>
      <p>Klik på <gui>Indstillinger</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Keyboard</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Keyboard Shortcuts</gui> section, select <gui>Customize Shortcuts</gui>.</p>
    </item>
    <item>
      <p>Select the desired category, or enter a search term.</p>
    </item>
    <item>
      <p>Klik på rækken for den ønskede handling. Vinduet <gui>Angiv genvej</gui> vises.</p>
    </item>
    <item>
      <p>Hold den ønskede tastekombination nede eller tryk på <key>Backspace</key> for at nulstille, eller tryk på <key>Esc</key> for at annullere.</p>
    </item>
  </steps>


<section id="defined">
<title>Prædefinerede genveje</title>
  <p>Der findes flere prækonfigurerede genveje som kan ændres, grupperet i følgende kategorier:</p>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Tilgængelighed</title>
  <tr>
	<td><p>Mindre tekst</p></td>
	<td><p>Deaktiveret</p></td>
  </tr>
  <tr>
	<td><p>Høj kontrast til eller fra</p></td>
	<td><p>Deaktiveret</p></td>
  </tr>
  <tr>
	<td><p>Større tekst</p></td>
	<td><p>Deaktiveret</p></td>
  </tr>
  <tr>
	<td><p>Slå skærmtastatur til eller fra</p></td>
	<td><p>Deaktiveret</p></td>
  </tr>
  <tr>
	<td><p>Slå skærmlæser til eller fra</p></td>
	<td><p><keyseq><key>Alt</key><key>Super</key><key>S</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Slå zoom til eller fra</p></td>
	<td><p><keyseq><key>Alt</key><key>Super</key><key>8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Zoom ind</p></td>
  <td><p><keyseq><key>Alt</key><key>Super</key><key>=</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Zoom ud</p></td>
  <td><p><keyseq><key>Alt</key><key>Super</key><key>-</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Programstartere</title>
  <tr>
	<td><p>Hjemmemappe</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-folder.svg"> <key>Explorer</key> key symbol</media> eller <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-computer.svg"> <key>Explorer</key> key symbol</media> eller <key>Explorer</key></p></td>
  </tr>
  <tr>
	<td><p>Start lommeregner</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-calculator.svg"> <key>Calculator</key> key symbol</media> eller <key>Calculator</key></p></td>
  </tr>
  <tr>
	<td><p>Start e-mailklient</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mail.svg"> <key>Mail</key> key symbol</media> eller <key>Mail</key></p></td>
  </tr>
  <tr>
	<td><p>Start hjælpebrowser</p></td>
	<td><p>Deaktiveret</p></td>
  </tr>
  <tr>
	<td><p>Start webbrowser</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-world.svg"> <key>WWW</key> key symbol</media> eller <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-home.svg"> <key>WWW</key> key symbol</media> eller <key>WWW</key></p></td>
  </tr>
  <tr>
	<td><p>Søg</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-search.svg"> <key>Search</key> key symbol</media> eller <key>Search</key></p></td>
  </tr>
  <tr>
	<td><p>Indstillinger</p></td>
	<td><p><key>Tools</key></p></td>
  </tr>

</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Navigation</title>
  <tr>
	<td><p>Skjul alle normale vinduer</p></td>
	<td><p>Deaktiveret</p></td>
  </tr>
  <tr>
	<td><p>Move to workspace on the left</p></td>
	<td><p><keyseq><key>Super</key><key>Page Up</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move to workspace on the right</p></td>
	<td><p><keyseq><key>Super</key><key>Page Down</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Flyt vindue én skærm ned</p></td>
	<td><p><keyseq><key>Skift</key><key xref="keyboard-key-super">Super</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Flyt vindue én skærm til venstre</p></td>
	<td><p><keyseq><key>Skift</key><key>Super</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Flyt vindue én skærm til højre</p></td>
	<td><p><keyseq><key>Skift</key><key>Super</key><key>→</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Flyt vindue én skærm op</p></td>
	<td><p><keyseq><key>Skift</key><key>Super</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window one workspace to the left</p></td>
	<td><p><keyseq><key>Skift</key><key>Super</key> <key>Page Up</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window one workspace to the right</p></td>
	<td><p><keyseq><key>Skift</key><key>Super</key><key>Page Down</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Flyt vindue til sidste arbejdsområde</p></td>
	<td><p><keyseq><key>Skift</key><key>Super</key><key>End</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Flyt vindue til arbejdsområde 1</p></td>
	<td><p><keyseq><key>Skift</key><key>Super</key><key>Home</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Flyt vindue til arbejdsområde 2</p></td>
	<td><p>Deaktiveret</p></td>
  </tr>
  <tr>
	<td><p>Flyt vindue til arbejdsområde 3</p></td>
	<td><p>Deaktiveret</p></td>
  </tr>
  <tr>
	<td><p>Flyt vindue til arbejdsområde 4</p></td>
	<td><p>Deaktiveret</p></td>
  </tr>
  <tr>
	<td><p>Skift programmer</p></td>
	<td><p><keyseq><key>Super</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Skift systemstyring</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Skift systemstyring direkte</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Skift til sidste arbejdsområde</p></td>
	<td><p><keyseq><key>Super</key><key>End</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Skift til arbejdsområde 1</p></td>
	<td><p><keyseq><key>Super</key><key>Home</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Skift til arbejdsområde 2</p></td>
	<td><p>Deaktiveret</p></td>
  </tr>
  <tr>
	<td><p>Skift til arbejdsområde 3</p></td>
	<td><p>Deaktiveret</p></td>
  </tr>
  <tr>
	<td><p>Skift til arbejdsområde 4</p></td>
	<td><p>Deaktiveret</p></td>
  </tr>
  <tr>
        <td><p>Skift vinduer</p></td>
        <td><p>Deaktiveret</p></td>
  </tr>
  <tr>
	<td><p>Skift vinduer direkte</p></td>
	<td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Skift vinduer af et program direkte</p></td>
	<td><p><keyseq><key>Alt</key><key>F6</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Skift vinduer af et program</p></td>
	<td><p>Deaktiveret</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Skærmbilleder</title>
  <tr>
	<td><p>Kopiér et skærmbillede af et vindue til udklipsholderen</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Print</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Kopiér et skærmbillede af et område til udklipsholderen</p></td>
	<td><p><keyseq><key>Skift</key><key>Ctrl</key><key>Print</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Kopiér et skærmbillede til udklipsholderen</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Print</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Optag en kort skærmoptagelse</p></td>
        <td><p><keyseq><key>Skift</key><key>Ctrl</key><key>Alt</key><key>R</key> </keyseq></p></td>
  </tr>
  <tr>
	<td><p>Gem et skærmbillede af et vindue til Billeder</p></td>
	<td><p><keyseq><key>Alt</key><key>Print</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Gem et skærmbillede af et område til Billeder</p></td>
	<td><p><keyseq><key>Skift</key><key>Print</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Gem et skærmbillede til Billeder</p></td>
	<td><p><key>Print</key></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Lyd og medie</title>
  <tr>
	<td><p>Skub ud</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-eject.svg"> <key>Eject</key> key symbol</media> (skub ud)</p></td>
  </tr>
  <tr>
	<td><p>Start medieafspiller</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-media.svg"> <key>Media</key> key symbol</media> (lyd medie)</p></td>
  </tr>
  <tr>
	<td><p>Microphone mute/unmute</p></td>
	<td/>
  </tr>
  <tr>
	<td><p>Næste spor</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-next.svg"> <key>Next</key> key symbol</media> (lyd næste)</p></td>
  </tr>
  <tr>
	<td><p>Sæt afspilning på pause</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-pause.svg"> <key>Pause</key> key symbol</media> (lyd pause)</p></td>
  </tr>
  <tr>
	<td><p>Afspil (eller afspil/pause)</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-play.svg"> <key>Play</key> key symbol</media> (lyd afspil)</p></td>
  </tr>
  <tr>
	<td><p>Forrige spor</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-previous.svg"> <key>Previous</key> key symbol</media> (lyd forrige)</p></td>
  </tr>
  <tr>
	<td><p>Stop afspilning</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-stop.svg"> <key>Stop</key> key symbol</media> (lyd stop)</p></td>
  </tr>
  <tr>
	<td><p>Lavere</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-voldown.svg"> <key>Volume Down</key> key symbol</media> (lyd lavere)</p></td>
  </tr>
  <tr>
	<td><p>Lydløs</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mute.svg"> <key>Mute</key> key symbol</media> (lyd lydløs)</p></td>
  </tr>
  <tr>
	<td><p>Højere</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-volup.svg"> <key>Volume Up</key> key symbol</media> (lyd højere)</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>System</title>
  <tr>
        <td><p>Giv fokus til den aktive påmindelse</p></td>
        <td><p><keyseq><key>Super</key><key>N</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Lås skærm</p></td>
	<td><p><keyseq><key>Super</key><key>L</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Show the Power Off dialog</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Delete</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Åbn programmenuen</p></td>
        <td><p><keyseq><key>Super</key><key>F10</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Gendan tastaturgenvejene</p></td>
        <td><p><keyseq><key>Super</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Vis alle programmer</p></td>
        <td><p><keyseq><key>Super</key><key>A</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Vis aktivitetsoversigten</p></td>
	<td><p><keyseq><key>Alt</key><key>F1</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Vis påmindelseslisten</p></td>
	<td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Vis oversigten</p></td>
	<td><p><keyseq><key>Super</key><key>S</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Vis prompten Kør kommando</p></td>
	<td><p><keyseq><key>Alt</key><key>F2</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Skrivning</title>
  <tr>
  <td><p>Skift til den næste indtastningskilde</p></td>
  <td><p><keyseq><key>Super</key><key>Mellemrum</key></keyseq></p></td>
  </tr>

  <tr>
  <td><p>Skift til den forrige indtastningskilde</p></td>
  <td><p><keyseq><key>Skift</key><key>Super</key><key>Mellemrum</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Vinduer</title>
  <tr>
	<td><p>Aktivér vinduesmenuen</p></td>
	<td><p><keyseq><key>Alt</key><key>Mellemrum</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Luk vinduet</p></td>
	<td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Skjul vinduet</p></td>
        <td><p><keyseq><key>Super</key><key>H</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Sænk vinduet under de andre vinduer</p></td>
	<td><p>Deaktiveret</p></td>
  </tr>
  <tr>
	<td><p>Maksimer vinduet</p></td>
	<td><p><keyseq><key>Super</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Maksimer vinduet vandret</p></td>
	<td><p>Deaktiveret</p></td>
  </tr>
  <tr>
	<td><p>Maksimer vinduet lodret</p></td>
	<td><p>Deaktiveret</p></td>
  </tr>
  <tr>
	<td><p>Flyt vinduet</p></td>
	<td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Løft vinduet over de andre vinduer</p></td>
	<td><p>Deaktiveret</p></td>
  </tr>
  <tr>
	<td><p>Løft vinduet hvis det er tildækket, ellers sænk det</p></td>
	<td><p>Deaktiveret</p></td>
  </tr>
  <tr>
	<td><p>Tilpas størrelsen på vinduet</p></td>
	<td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Gendan vinduet</p></td>
        <td><p><keyseq><key>Super</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Fuldskærmstilstand til/fra</p></td>
	<td><p>Deaktiveret</p></td>
  </tr>
  <tr>
	<td><p>Maksimeret tilstand til/fra</p></td>
	<td><p><keyseq><key>Alt</key><key>F10</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Vindue på alle arbejdsområder eller ét til/fra</p></td>
	<td><p>Deaktiveret</p></td>
  </tr>
  <tr>
        <td><p>Opdelt visning til venstre</p></td>
        <td><p><keyseq><key>Super</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Opdelt visning til højre</p></td>
        <td><p><keyseq><key>Super</key><key>→</key></keyseq></p></td>
  </tr>
</table>

</section>

<section id="custom">
<title>Tilpassede genveje</title>

  <p>To create your own application keyboard shortcut in the
  <gui>Keyboard</gui> settings:</p>

  <steps>
    <item>
      <p>Select <gui>Custom Shortcuts</gui>.</p>
    </item>
    <item>
      <p>Click the <gui style="button">Add Shortcut</gui> button if no custom
      shortcut is set yet. Otherwise click the <gui style="button">+</gui>
      button. The <gui>Add Custom Shortcut</gui> window will appear.</p>
    </item>
    <item>
      <p>Skriv et <gui>Navn</gui> til at identificere genvejen og en <gui>Kommando</gui> til at køre et program. Hvis du f.eks. vil have genvejen til at åbne <app>Rhythmbox</app>, så kan du kalde den <input>Musik</input> og bruge kommandoen <input>rhythmbox</input>.</p>
    </item>
    <item>
      <p>Click the <gui style="button">Add Shortcut…</gui> button. In the
      <gui>Add Custom Shortcut</gui> window, hold down the desired
      shortcut key combination.</p>
    </item>
    <item>
      <p>Klik på <gui>Tilføj</gui>.</p>
    </item>
  </steps>

  <p>Det kommandonavn du skriver skal være en gyldig systemkommando. Du kan tjekke at kommandoen virker ved at åbne en terminal og skrive den der. Kommandoen der åbner et program må ikke have samme navn som selve programmet.</p>

  <p>If you want to change the command that is associated with a custom
  keyboard shortcut, click the row of the shortcut. The
  <gui>Set Custom Shortcut</gui> window will appear, and you can edit the
  command.</p>

</section>

</page>
