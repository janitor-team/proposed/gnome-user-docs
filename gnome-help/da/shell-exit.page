<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="shell-exit" xml:lang="da">

  <info>
    <link type="guide" xref="shell-overview"/>
    <link type="guide" xref="power"/>
    <link type="guide" xref="index" group="#first"/>

    <revision pkgversion="3.6.0" date="2012-09-15" status="review"/>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.24.2" date="2017-06-11" status="candidate"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>
    <revision pkgversion="3.36.1" date="2020-04-18" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="author">
      <name>Alexandre Franke</name>
      <email>afranke@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David Faour</name>
      <email>dfaour.gnome@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Læs om at forlade din brugerkonto ved at logge ud, skifte bruger osv.</desc>
    <!-- Should this be a guide which links to other topics? -->
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  </info>

  <title>Log ud, sluk eller skift bruger</title>

  <p>Når du er færdig med at bruge din computer, så kan du slukke den, sætte den i hviletilstand (for at spare på strømmen) eller lade den være tændt og logge ud.</p>

<section id="logout">
  <title>Log ud eller skift bruger</title>

  <p>For at lade andre brugere bruge din computer kan du enten logge ud eller forblive logget ind og bare skifte bruger. Hvis du skifter bruger, så vil alle dine programmer fortsat køre, og alt vil være som du forlod det, næste gang du logger ind.</p>

  <p>To <gui>Log Out</gui> or <gui>Switch User</gui>, click the
  <link xref="shell-introduction#systemmenu">system menu</link> on the right
  side of the top bar, expand <gui>Power Off / Log Out</gui>, and select the correct option.</p>

  <note if:test="!platform:gnome-classic">
    <p>Punkterne <gui>Log ud</gui> og <gui>Skift bruger</gui> vises kun i menuen, hvis der er mere end én brugerkonto på dit system.</p>
  </note>

  <note if:test="platform:gnome-classic">
    <p>Punktet <gui>Skift bruger</gui> vises kun i menuen, hvis der er mere end én brugerkonto på dit system.</p>
  </note>

</section>

<section id="lock-screen">
  <info>
    <link type="seealso" xref="session-screenlocks"/>
  </info>

  <title>Lås skærmen</title>

  <p>Hvis du forlader din computer i et stykke tid, så bør du låse skærmen for at forhindre andre i at få adgang til dine filer og køre programmer. Når du vender tilbage ses <link xref="shell-lockscreen">låseskærmen</link>. Indtast din adgangskode for at logge ind igen. Hvis du undlader at låse skærmen, så låses den automatisk efter et stykke tid.</p>

  <p>For at låse din skærm skal du klikke på systemmenuen i højre side af toplinjen og vælge <gui>Lås</gui> i menuen.</p>

  <p>Når din skærm er låst, kan andre brugere logge ind på deres egne konti ved at klikke på <gui>Log ind som en anden bruger</gui> nederst til højre på loginskærmen. Du kan skifte tilbage til dit skrivebord når de er færdige.</p>

</section>

<section id="suspend">
  <info>
    <link type="seealso" xref="power-suspend"/>
  </info>

  <title>Hviletilstand</title>

  <p>For at spare på strømmen kan du sætte din computer i hviletilstand når du ikke bruger den. Hvis du bruger en bærbar computer, så sætter systemet automatisk din computer i hviletilstand når du lukker låget som standard. Det gemmer tilstanden i din computers hukommelse og slukker for de fleste af computerens funktioner. Der bruges fortsat en lille mængde strøm i hviletilstand.</p>

  <p>For at sætte din computer i hviletilstand manuelt kan du klikke på systemmenuen i højre side af toplinjen, udfolde <gui>Sluk/log ud</gui> og vælge <gui>Hviletilstand</gui>.</p>

</section>

<section id="shutdown">
<!--<info>
  <link type="seealso" xref="power-off"/>
</info>-->

  <title>Sluk eller genstart</title>

  <p>If you want to power off your computer entirely, or do a full restart,
  click the system menu on the right side of the top bar, expand <gui>Power Off
  / Log Out</gui>, and select either <gui>Restart…</gui> or <gui>Power Off…</gui>.</p>

  <p>Hvis der er andre brugere som er logget ind, så vil du måske ikke have tilladelse til at slukke eller genstarte computeren da det vil afslutte deres session. Hvis du er en administrativ bruger, så kan du blive spurgt om din adgangskode for at slukke.</p>

  <note style="tip">
    <p>Du vil måske slukke din computer hvis du skal flytte den eller ikke har et batteri, hvis batteriet er lavt eller har ringe kapacitet. En slukket computer bruger også <link xref="power-batterylife">mindre energi</link> end en som er i hviletilstand.</p>
  </note>

</section>

</page>
