<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-percentage" xml:lang="da">

  <info>

    <link type="guide" xref="power"/>
    <link type="guide" xref="status-icons"/>
    <link type="seealso" xref="power-status"/>

    <revision version="gnome:40" date="2021-03-21" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2021</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Display the battery percentage in the top bar.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  </info>
  
  <title>Show the battery status as a percentage</title>

  <p>The <link xref="status-icons#batteryicons">status icon</link> in the top
    bar shows the charge level of the main internal battery, and whether it is
    currently charging or not. It can also display the charge as a
    <link xref="power-percentage">percentage</link>.</p>

  <steps>

    <title>Display the battery percentage in the top bar</title>

    <item>
      <p>Åbn <gui xref="shell-introduction#activities">Aktivitetsoversigten</gui> og begynd at skrive <gui>Strøm</gui>.</p>
    </item>
    <item>
      <p>Klik på <gui>Strøm</gui> for at åbne panelet.</p>
    </item>
    <item>
      <p>In the <gui>Suspend &amp; Power Button</gui> section, set <gui>Show
      Battery Percentage</gui> to on.</p>
    </item>

  </steps>

</page>
