<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-usemic" xml:lang="as">

  <info>
    <link type="guide" xref="media#sound"/>

    <revision version="gnome:40" date="2021-02-26" status="candidate"/>

    <credit type="author">
      <name>শ্বণ মেকেন্স</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>একাটেৰিনা গেৰাচিমভা</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>এটা এনালগ অথবা USB মাইক্ৰফোন ব্যৱহাৰ কৰক আৰু এটা অবিকল্পিত ইনপুট ডিভাইচ বাছক।</desc>
  </info>

  <title>এটা অন্য মাইক্ৰফোন ব্যৱহাৰ কৰক</title>

  <p>You can use an external microphone for chatting with friends, speaking
  with colleagues at work, making voice recordings, or using other multimedia
  applications. Even if your computer has a built-in microphone or a webcam
  with a microphone, a separate microphone usually provides better audio
  quality.</p>

  <p>If your microphone has a circular plug, just plug it into the appropriate
  audio socket on your computer. Most computers have two sockets: one for
  microphones and one for speakers. This socket is usually light red in color
  or is accompanied by a picture of a microphone. Microphones plugged
  into the appropriate socket are usually used by default. If not, see the
  instructions below for selecting a default input device.</p>

  <p>যদি আপোনাৰ এটা USB মাইক্ৰফোন আছে, ইয়াক আপোনাৰ কমপিউটাৰৰ যিকোনো USB পৰ্টত প্লাগ কৰক। USB মাইক্ৰফোনসমূহ পৃথক অডিঅ' ডিভাইচ ৰূপত ব্যৱহাৰ কৰে, আৰু আপুনি অবিকল্পিতভাৱে কোন মাইক্ৰফোন ব্যৱহাৰ কৰিব ধাৰ্য্য কৰিব লাগিব।</p>

  <steps>
    <title>এটা অবিকল্পিত অডিঅ' ইনপুট ডিভাইচ বাছক</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>Sound</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Sound</gui> to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Input</gui> section, select the device that you want to
      use. The input level indicator should respond when you speak.</p>
    </item>
  </steps>

  <p>You can adjust the volume and switch the microphone off from this
  panel.</p>

</page>
