<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-disable-service" xml:lang="as">

  <info>
    <link type="guide" xref="accounts"/>

    <revision pkgversion="3.5.5" date="2012-08-14" status="review"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>ফিল বুল</name>
      <email>philbull@gmail.com</email>
    </credit>
<credit type="editor">
      <name>Klein Kravis</name>
      <email>kleinkravis44@outlook.com</email>
      <years>2020</years>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>কিছুমান অনলাইন একাওন্টক একাধিক সেৱা অভিগম কৰিবলৈ ব্যৱহাৰ কৰিব পাৰি (যেনে কেলেন্ডাৰ আৰু ইমেইল)। আপুনি এই সেৱাসমূহৰ কোনবোৰ এপ্লিকেচনসমূহ দ্বাৰা ব্যৱহাৰ কৰিব পৰা যাব নিয়ন্ত্ৰণ কৰিব পাৰিব।</desc>
  </info>

  <title>এটা একাওন্ট ব্যৱহাৰ কৰি কোনবোৰ অনলাইন সেৱা অভিগম কৰিব পৰা যাব নিয়ন্ত্ৰণ কৰক</title>

  <p>Some types of online account providers allow you to access several services
  with the same user account. For example, Google accounts provide access to
  calendar, email, and contacts. You may want to use your account for some
  services, but not others. For example, you may want to use your Google account
  for email but not calendar if you have a different online account that you use
  for calendar.</p>

  <p>প্ৰতিটো অনলাইন একাওন্ট দ্বাৰা প্ৰদান কৰা কিছুমান সেৱা আপুনি অসামৰ্থবান কৰিব পাৰিব:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Online Accounts</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Online Accounts</gui> to open the panel.</p>
    </item>
    <item>
      <p>Select the account which you want to change from the list on the
      right.</p>
    </item>
    <item>
      <p>A list of services that are available with this account will be
      shown under <gui>Use for</gui>. See <link xref="accounts-which-application"/>
      to see which applications access which services.</p>
    </item>
    <item>
      <p>Switch off any of the services that you do not want to use.</p>
    </item>
  </steps>

  <p>Once a service has been disabled for an account, applications on your
  computer will not be able to use the account to connect to that service any
  more.</p>

  <p>To turn on a service that you disabled, just go back to the <gui>Online
  Accounts</gui> panel and switch it on.</p>

</page>
