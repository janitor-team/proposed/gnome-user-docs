<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-templates" xml:lang="as">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>অনিতা ৰাইটেৰে</name>
      <email>nitalynx@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>স্বনিৰ্বাচিত ফাইল টেমপ্লেইটসমূহৰ পৰা দ্ৰুত নতুন দস্তাবেজসমূহ সৃষ্টি কৰক।</desc>
  </info>

  <title>সাধাৰণত-ব্যৱহৃত দস্তাবেজ দৰণসমূহৰ বাবে টেমপ্লেইটসমূহ</title>

  <p>If you often create documents based on the same content, you might
  benefit from using file templates. A file template can be a document
  of any type with the formatting or content you would like to reuse.
  For example, you could create a template document with your letterhead.</p>

  <steps>
    <title>এটা নতুন টেমপ্লেইট নিৰ্মাণ কৰক</title>
    <item>
      <p>আপুনি এটা টেমপ্লেইট ৰূপে ব্যৱহাৰ কৰিব খোজা এটা দস্তাবেজ সৃষ্টি কৰক। উদাহৰণস্বৰূপ, আপুনি আপোনাৰ লেটাৰহেড এটা শব্দ প্ৰক্ৰিয়াকৰণ এপ্লিকেচনত বনাব পাৰিব।</p>
    </item>
    <item>
      <p>Save the file with the template content in the <file>Templates</file>
      folder in your <file>Home</file> folder. If the <file>Templates</file>
      folder does not exist, you will need to create it first.</p>
    </item>
  </steps>

  <steps>
    <title>এটা দস্তাবেজ সৃষ্টি কৰিবলৈ এটা টেমপ্লেইট ব্যৱহাৰ কৰক</title>
    <item>
      <p>আপুনি নতুন দস্তাবেজ উপস্থাপন কৰিব বিচৰা ফোল্ডাৰ খোলক।</p>
    </item>
    <item>
      <p>Right-click anywhere in the empty space in the folder, then choose
      <gui style="menuitem">New Document</gui>. The names of available
      templates will be listed in the submenu.</p>
    </item>
    <item>
      <p>তালিকাৰ পৰা আপোনাৰ পছন্দৰ টেমপ্লেইট বাছক।</p>
    </item>
    <item>
      <p>Double-click the file to open it and start editing. You may wish to
      <link xref="files-rename">rename the file</link> when you are
      finished.</p>
    </item>
  </steps>

</page>
