<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-delete" xml:lang="as">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="user-add"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>টিফানি এন্টপলস্কি</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>GNOME তথ্যচিত্ৰ প্ৰকল্প</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>শ্বণ মেকেন্স</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>একাটেৰিনা গেৰাচিমভা</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>আপোনাৰ কমপিউটাৰ ব্যৱহাৰ নকৰা ব্যৱহাৰকাৰীসকলক আতৰাওক।</desc>
  </info>

  <title>এটা ব্যৱহাৰকাৰী একাওন্ট মচি পেলাওক</title>

  <p>You can <link xref="user-add">add multiple user accounts to your
  computer</link>. If somebody is no longer using your computer, you can delete
  that user’s account.</p>

  <p>You need <link xref="user-admin-explain">administrator privileges</link>
  to delete user accounts.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Users</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Users</gui> to open the panel.</p>
    </item>
    <item>
      <p>Press <gui style="button">Unlock</gui> in the top right corner and
      type in your password when prompted.</p>
    </item>
    <item>
      <p>Select the user that you want to delete and press the
      <gui style="button">-</gui> button, below the list of accounts on the
      left, to delete that user account.</p>
    </item>
    <item>
      <p>Each user has their own home folder for their files and settings. You
      can choose to keep or delete the user’s home folder. Click <gui>Delete
      Files</gui> if you are sure they will not be used anymore and you need to
      free up disk space. These files are permanently deleted. They cannot be
      recovered. You may want to back up the files to an external storage device
      before deleting them.</p>
    </item>
  </steps>

</page>
