<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="look-background" xml:lang="as">

  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-09" status="review"/>
    <revision pkgversion="3.34" date="2019-11-12" status="review"/>
    <revision pkgversion="3.38.1" date="2020-11-04" status="review"/>

    <credit type="author">
      <name>GNOME তথ্যচিত্ৰ প্ৰকল্প</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>এপ্ৰিল গনজালিচ</name>
      <email>loonycookie@gmail.com</email>
    </credit>
    <credit type="author">
      <name>নাটালিয়া ৰুজ লিভা</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="author">
      <name>শ্বণ মেকেন্স</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>আন্দ্ৰে ক্লেপাৰ</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>শোভা ত্যাগী</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>একাটেৰিনা গেৰাচিমভা</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ivan Stanton</name>
      <email>northivanastan@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Set an image as your desktop background.</desc>
  </info>

  <title>Change the desktop background</title>

  <p>To change the image used for your backgrounds:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>Background</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Background</gui> to open the panel. The currently selected
      wallpaper is shown at the top.</p>
    </item>
    <item>
      <p>There are two ways to change the image used for your backgrounds:</p>
      <list>
        <item>
          <p>Click one of the background images which are shipped with the
          system.</p>
        <note style="info">
          <p>Some wallpapers change throughout the day. These wallpapers have a
          small clock icon in the bottom-right corner.</p>
        </note>
        </item>
        <item>
          <p>Click <gui>Add Picture…</gui> to use one of your own photos. By
          default, the <file>Pictures</file> folder will be opened, since most
          photo management applications store photos there.</p>
        </item>
      </list>
    </item>
    <item>
      <p>সংহতিসমূহ তৎক্ষনাত প্ৰয়োগ কৰা হয়।</p>
        <note style="tip">
          <p>For another way to set one of your own photos as the background,
          right-click on the image file in <app>Files</app> and select <gui>Set
          as Wallpaper</gui>, or open the image file in <app>Image Viewer</app>,
          click the menu button in the titlebar and select <gui>Set as
          Wallpaper</gui>.</p>
        </note>
    </item>
    <item>
      <p>আপোনাৰ সম্পূৰ্ণ ডেস্কটপ দৰ্শন কৰিবলৈ <link xref="shell-workspaces-switch"> এটা ৰিক্ত কৰ্মস্থানলৈ যাওক</link>।</p>
    </item>
  </steps>

</page>
