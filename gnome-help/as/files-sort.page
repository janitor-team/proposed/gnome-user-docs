<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-sort" xml:lang="as">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-25" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>ফিল বুল</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>জিম কেম্পবেল</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>ফাইলসমূহ সিহতৰ নাম, আকাৰ, ধৰণ, অথবা সিহতক যেতিয়া সলনি কৰা হৈছিল সেই হিচাপে সজাঁওক।</desc>
  </info>

<title>ফাইলসমূহ আৰু ফোল্ডাৰসমূহ সজাঁওক</title>

<p>You can sort files in different ways in a folder, for example by sorting them
in order of date or file size. See <link xref="#ways"/> below for a list of
common ways to sort files. See <link xref="nautilus-views"/> for information on
how to change the default sort order.</p>

<p>The way that you can sort files depends on the <em>folder view</em> that you
are using. You can change the current view using the list or icon buttons in the
toolbar.</p>

<section id="icon-view">
  <title>আইকন দৰ্শন</title>

  <p>To sort files in a different order, click the view options button in the
  <!-- FIXME: Get a tooltip added for "View options" -->
  toolbar and choose <gui>By Name</gui>, <gui>By Size</gui>, <gui>By
  Type</gui>, <gui>By Modification Date</gui>, or <gui>By Access
  Date</gui>.</p>

  <p>উদাহৰণস্বৰূপ, যদি আপুনি <gui>নামৰ দ্বাৰা</gui> নিৰ্বাচন কৰে, ফাইলসমূহ সিহতৰে নামৰে সজোৱা হব, আক্ষৰিক ক্ৰমত। অন্য বিকল্পৰ বাবে <link xref="#ways"/> চাওক।</p>

  <p>You can sort in the reverse order by selecting <gui>Reversed Order</gui>
  from the menu.</p>

</section>

<section id="list-view">
  <title>তালিকা দৰ্শন</title>

  <p>To sort files in a different order, click one of the column headings in
  the file manager. For example, click <gui>Type</gui> to sort by file type.
  Click the column heading again to sort in the reverse order.</p>
  <p>In list view, you can show columns with more attributes and sort on those
  columns. Click the view options button in the toolbar, pick <gui>Visible
  <!-- FIXME: Get a tooltip added for "View options" -->
  Columns…</gui> and select the columns that you want to be visible. You will
  then be able to sort by those columns. See <link xref="nautilus-list"/> for
  descriptions of available columns.</p>

</section>

<section id="ways">
  <title>ফাইলসমূহ সজোৱাৰ ধৰণবোৰ</title>

  <terms>
    <item>
      <title>নাম</title>
      <p>ফাইল নামৰ সহায়ত আক্ষৰিকভাৱে সজায়।</p>
    </item>
    <item>
      <title>আকাৰ</title>
      <p>ফাইলৰ আকাৰ দ্বাৰা সমন্বয় কৰক (ই কিমান ডিস্ক স্থান লয়)। অবিকল্পিতভাৱে সৰুৰ পৰা ডাঙৰলৈ সমন্বয় কৰে।</p>
    </item>
    <item>
      <title>ধৰণ</title>
      <p>ফাইলৰ ধৰণে আক্ষৰিকভাৱে সমন্বয় কৰে। একে ধৰণৰ ফাইলক এলগলে ৰখা হয়, তাৰ পিছত নামৰে সমন্বয় কৰা হয়।</p>
    </item>
    <item>
      <title>Last Modified</title>
      <p>এটা ফাইল সৰ্বশেষ পৰিবৰ্তন কৰাৰ তাৰিখ আৰু সময়ৰে সমন্বয় কৰে। পুৰনিৰ পৰা নতুনলৈ অবিকল্পিতভাৱে সমন্বয় কৰে।</p>
    </item>
  </terms>

</section>

</page>
