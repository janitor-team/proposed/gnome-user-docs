<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-sensitivity" xml:lang="as">

  <info>
    <link type="guide" xref="mouse" group="#first"/>
    <link type="guide" xref="a11y#mobility" group="pointing"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.10" date="2013-10-29" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-05" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>ফিল বুল</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit>
      <name>টিফানি এন্টপলস্কি</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit>
      <name>শ্বণ মেকেন্স</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>যেতিয়া আপুনি আপোনাৰ মাউছ অথবা টাচপেড ব্যৱহাৰ কৰে পোইন্টাৰ স্থানান্তৰ হোৱাৰ গতি পৰিবৰ্তন কৰক।</desc>
  </info>

  <title>Adjust the speed of the mouse and touchpad</title>

  <p>যদি আপুনি মাউছ অথবা টাচপেড লৰাওতে আপোনাৰ পইন্টাৰ অতি দ্ৰুত অথবা অতি লেহেম গতিত লৰে, আপুনি এই ডিভাইচসমূহৰ বাবে পইন্টাৰৰ গতি সমন্বয় কৰিব পাৰিব।</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Mouse &amp; Touchpad</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Mouse &amp; Touchpad</gui> to open the panel.</p>
    </item>
    <item>
      <p>Adjust the <gui>Mouse Speed</gui> or <gui>Touchpad Speed</gui> slider
      until the pointer motion is comfortable for you. Sometimes the most
      comfortable settings for one type of device are not the best for
      the other.</p>
    </item>
  </steps>

  <note>
    <p>The <gui>Touchpad</gui> section only appears if your system has a
    touchpad, while the <gui>Mouse</gui> section is only visible when a mouse
    is connected.</p>
  </note>

</page>
