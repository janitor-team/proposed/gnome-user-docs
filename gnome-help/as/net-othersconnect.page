<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-othersconnect" xml:lang="as">

  <info>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="net-othersedit"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>ফিল বুল</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>একাটেৰিনা গেৰাচিমভা</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>আপুনি এটা নেটৱাৰ্ক সংযোগৰ বাবে সংহতিসমূহ (যেনে পাছৱাৰ্ড) সংৰক্ষণ কৰিব পাৰিব যাতে যোনে কমপিউটাৰ ব্যৱহাৰ কৰে সিয়ে ইয়াৰ সৈতে সংযোগ কৰিব পাৰে।</desc>
  </info>

  <title>Other users can’t connect to the internet</title>

  <p>When you set up a network connection, all other users on your computer
  will normally be able to use it. If the connection information is not shared,
  you should check the connection settings.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Network</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Network</gui> to open the panel.</p>
    </item>
    <item>
      <p>Select <gui>Wi-Fi</gui> from the list on the left.</p>
    </item>
    <item>
      <p>Click the 
      <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">settings</span></media> button to open the connection
      details.</p>
    </item>
    <item>
      <p>Select <gui>Identity</gui> from the pane on the left.</p>
    </item>
    <item>
      <p>At the bottom of the <gui>Identity</gui> panel, check the <gui>Make
      available to other users</gui> option to allow other users to use the
      network connection.</p>
    </item>
    <item>
      <p>Press <gui style="button">Apply</gui> to save the changes.</p>
    </item>
  </steps>

  <p>কমপিউটাৰৰ অন্য ব্যৱহাৰকাৰীসকলে এই সংযোগক ততোধিক বিৱড়ন নিদিয়াকৈ ব্যৱহাৰ কৰিব পাৰিব।</p>

  <note>
    <p>Any user can change this setting.</p>
  </note>

</page>
