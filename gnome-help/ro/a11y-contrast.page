<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-contrast" xml:lang="ro">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Face ferestrele și butoanele de pe ecran mai mult (sau mai puțin) vioaie, pentru a fi mai ușor de văzut.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Șerbănescu</mal:name>
      <mal:email>daniel [at] serbanescu [dot] dk</mal:email>
      <mal:years>2016, 2019</mal:years>
    </mal:credit>
  </info>

  <title>Ajustarea contrastului</title>

  <p>Puteți ajusta contrastul ferestrelor și butoanelor pentru a fi mai ușor de văzut. Această acțiune nu este la fel cu schimbarea luminozității întregului ecran; doar părți ale <em>interfeței pentru utilizator</em> se vor schimba.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Accessibility</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Accessibility</gui> to open the panel.</p>
    </item>
    <item>
      <p>Switch the <gui>High Contrast</gui> switch in the <gui>Seeing</gui>
      section to on.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Comutarea rapidă a contrastului ridicat</title>
    <p>Puteți comuta contrastul ridicat apăsând pe <link xref="a11y-icon">iconița de accesibilitate</link> de pe bara de sus și selectând <gui>Contrast ridicat</gui>.</p>
  </note>

</page>
