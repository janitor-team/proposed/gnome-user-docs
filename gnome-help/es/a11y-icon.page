<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-icon" xml:lang="es">

  <info>
    <link type="guide" xref="a11y"/>

    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>El menú del accesibilidad es el icono que parece una persona en la barra superior.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Encontrar el menú de accesibilidad</title>

  <p>El <em>menú de accesibilidad</em> es donde puede activar varios ajustes de accesibilidad. Puede encontrar este menú pulsando sobre el icono que parece una persona rodeada de un círculo, en la barra superior.</p>

  <figure>
    <desc>El menú de accesibilidad se puede encontrar en la barra superior.</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/preferences-desktop-accessibility-symbolic.svg"/>
  </figure>

  <p>Si no ve el menú del accesibilidad en el menú, puede activarlo en la configuración del panel de <gui>Accesibilidad</gui>:</p>

  <steps>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Accesibilidad</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Accesibilidad</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>Active la opción <gui>Mostrar siempre el menú de accesibilidad</gui>.</p>
    </item>
  </steps>

  <p>Para acceder a este menú mediante el teclado en lugar del ratón, pulse <keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq> para mover el foco del teclado a la barra superior. Aparecerá una línea blanca debajo del botón <gui>Actividades</gui>; le indica qué elemento de la barra superior se selecciona. Utilice las teclas de flecha del teclado para mover la línea blanca en el icono de menú de accesibilidad y luego pulse <key>Intro</key> para abrirlo. Puede usar las teclas de flecha para seleccionar elementos del menú. Pulse <key>Intro</key> para cambiar al elemento seleccionado.</p>

</page>
