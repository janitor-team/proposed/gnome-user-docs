<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="bluetooth-connect-device" xml:lang="es">

  <info>
    <link type="guide" xref="bluetooth"/>
    <link type="guide" xref="hardware-phone#setup"/>
    <link type="seealso" xref="sharing-bluetooth"/>
    <link type="seealso" xref="bluetooth-remove-connection"/>

    <revision pkgversion="3.8" date="2013-05-16" status="review"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-22" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>emparejar dispositivos Bluetooth.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Conectar su equipo a un dispositivo Bluetooth</title>

  <p>Antes de poder usar un dispositivo Bluetooth como un ratón o unos auriculares, primero tendrá que conectarlos al equipo. A esto se le denomina <em>emparejar</em> dispositivos Bluetooth.</p>

  <steps>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Bluetooth</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>Asegúrese de que Bluetooth está activado: el botón de la barra superior debe estar activado. Con el panel abierto y el interruptor encendido, el equipo debería empezar a buscar dispositivos.</p>
    </item>
    <item>
      <p>Haga que el otro dispositivo Bluetooth <link xref="bluetooth-visibility">se pueda descubrir o que sea visible</link> y colóquelo a unos 5-10 metros de su equipo.</p>
    </item>
    <item>
      <p>Pulse en el dispositivo en la lista de <gui>Dispositivos</gui>. Se abrirá el panel para el dispositivo.</p>
    </item>
    <item>
      <p>Si es necesario, confirme el PIN en el otro dispositivo. El dispositivo debería mostrarle el PIN que ve en la pantalla de su equipo. Confirme el PIN en el dispositivo (puede tener que pulsar <gui>Emparejar</gui> o <gui>Confirmar</gui>), y luego pulse <gui>Confirmar</gui> en el equipo.</p>
      <p>En la mayoría de los dispositivos, deberá introducirlo la entrada en menos de veinte segundos, o la conexión no se completará. Si pasara esto, vuelva a la lista de dispositivos y empiece de nuevo.</p>
    </item>
    <item>
      <p>La entrada para el dispositivo en la lista de <gui>Dispositivos</gui> mostrará ahora el estado <gui>Conectado</gui>.</p>
    </item>
    <item>
      <p>Para editar el dispositivo, pulse sobre él en la lista de <gui>Dispositivos</gui>. Verá un panel específico para el dispositivo. Puede mostrar opciones adicionales, aplicables al tipo de dispositivo al que se está conectando.</p>
    </item>
    <item>
      <p>Cierre el panel cuando haya cambiado la configuración.</p>
    </item>
  </steps>

  <media its:translate="no" type="image" src="figures/bluetooth-symbolic.svg" style="floatend">
    <p its:translate="yes">El icono de Bluetooth en la barra superior</p>
  </media>
  <p>Cuando hay uno o más dispositivos Bluetooth conectados, el icono de Bluetooth aparece en el área de estado del sistema.</p>

</page>
