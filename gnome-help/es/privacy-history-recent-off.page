<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-history-recent-off" xml:lang="es">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.8" date="2013-03-11" status="final"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>
    <revision pkgversion="3.38.1" date="2020-11-22" status="candidate"/>
    <revision pkgversion="3.38.4" date="2021-03-07" status="review"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Dejar de registrar o limitar el registro de archivos usados recientemente.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Desactivar o limitar el registro del histórico</title>
  
  <p>Rastrear los archivos y carpetas usados recientemente puede hacer que sea más fácil encontrar elementos con los que ha estado trabajando en el gestor de archivos y el diálogos de archivos en aplicaciones. Puede querer mantener esos elementos en privado, o rastrear solo el histórico más reciente.</p>

  <steps>
    <title>Desactivar el registro del histórico</title>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> empiece a escribir <gui>Privacidad</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>File History &amp; Trash</gui> to open the panel.</p>
    </item>
    <item>
     <p>Switch the <gui>File History</gui> switch to off.</p>
     <p>To re-enable this feature, switch the <gui>File History</gui> switch
     to on.</p>
    </item>
    <item>
      <p>Use the <gui>Clear History…</gui> button to purge the history
      immediately.</p>
    </item>
  </steps>
  
  <note><p>Esta configuración no afectará a cómo su navegador guarda información sobre las páginas web que visita.</p></note>

  <steps>
    <title>Restringir la cantidad de tiempo que se rastrea el histórico de archivos</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>File History &amp; Trash</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>File History &amp; Trash</gui> to open the panel.</p>
    </item>
    <item>
     <p>Ensure the <gui>File History</gui> switch is set to on.</p>
    </item>
    <item>
     <p>Under <gui>File History Duration</gui>, select how long to retain your file history.
     Choose from options <gui>1 day</gui>, <gui>7 days</gui>, <gui>30 days</gui>, or
     <gui>Forever</gui>.</p>
    </item>
    <item>
      <p>Use the <gui>Clear History…</gui> button to purge the history
      immediately.</p>
    </item>
  </steps>

</page>
