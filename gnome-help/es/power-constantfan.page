<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-constantfan" xml:lang="es">

  <info>
    <link type="guide" xref="power#problems"/>
    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Puede faltar algún software controlador de los ventiladores o su portátil puede estar recalentando.</desc>

    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>El ventilador de mi portátil siempre está funcionando</title>

<p>Si el ventilador de refrigeración de su portátil siempre está funcionando, es posible que el hardware que controla el sistema de refrigeración del portátil no esté muy bien soportado en Linux. Algunos portátiles necesitan software adicional para controlar sus ventiladores de refrigeración de manera eficiente, pero este software puede no estar instalado (o no estar disponible para Linux) y por eso los ventiladores funcionan a toda velocidad todo el tiempo.</p>

<p>Si este es el caso, puede ser capaz de cambiar algunos ajustes o instalar software adicional que le permita el control total del ventilador. Por ejemplo, <link href="http://vaio-utils.org/fan/">vaiofan</link> se puede instalar para controlar los ventiladores de algunos portátiles Sony VAIO. La instalación de este software es un proceso bastante técnico que depende en gran medida de la marca y modelo de su portátil, por lo que pudiera tener que obtener asesoramiento específico sobre cómo hacerlo para su equipo.</p>

<p>También es posible que su portátil produzca una gran cantidad de calor. Esto no significa necesariamente que se esté recalentando, sino que puede ser que necesite que el ventilador funcione a toda velocidad todo el tiempo para permitirle mantenerse frío. Si este es el caso, tiene pocas opciones para evitar que el ventilador funcione a toda velocidad todo el tiempo. A veces se pueden comprar accesorios adicionales de refrigeración para su portátil que le pueden ayudar.</p>

</page>
