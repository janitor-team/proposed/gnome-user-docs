<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="backup-how" xml:lang="es">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit>
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Klein Kravis</name>
      <email>kleinkravis44@outlook.com</email>
      <years>2020</years>
    </credit>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Use Déjà Dup (u otra aplicación de copias de respaldo) para hacer copias de sus archivos valiosos y de su configuración y evitar pérdidas.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>Cómo hacer copias de respaldo</title>

  <p>La manera más sencilla de hacer copias de seguridad de sus archivos y configuración es dejar a una aplicación de copia de seguridad que gestiones los procesos de respaldo. Hay un cierto número de aplicaciones de copia de seguridad, por ejemplo <app>Déjà Dup</app>.</p>

  <p>La ayuda para la aplicación de copia de respaldo elegida le guiará mientras establece sus preferencias de copia de respaldo, así como para restaurar sus datos.</p>

  <p>Una opción alternativa consiste en <link xref="files-copy">copiar sus archivos</link> a un lugar seguro, como un disco duro externo, un servicio de almacenamiento en línea o una unidad USB. Sus <link xref="backup-thinkabout">archivos personales</link> y preferencias suelen estar en su carpeta personal, por lo que puede copiarlos de ahí.</p>

  <p>La cantidad de datos que puede respaldar está limitada por el tamaño del dispositivo de almacenamiento. Si tiene espacio suficiente en su dispositivo de respaldo, es mejor copiar la carpeta personal completa con las siguientes excepciones:</p>

<list>
 <item><p>Archivos que ya están respaldados en otro sitio, como una unidad USB u otro medio extraíble.</p></item>
 <item><p>Los archivos que puede volver a crear fácilmente. Por ejemplo, si es programador, no tiene que respaldar todos los archivos que ha generado al compilar sus programas. En su lugar, simplemente asegúrese de que respalda los archivos fuente originales.</p></item>
 <item><p>Cualquier archivo en la carpeta Papelera. Puede encontrar su carpeta de Papelera en <cmd>~/.local/share/Trash</cmd>.</p></item>
</list>

</page>
