<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="ui" version="1.0 if/1.0" id="shell-workspaces" xml:lang="es">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces" group="#first"/>

    <revision pkgversion="3.8.0" date="2013-04-23" status="review"/>
    <revision pkgversion="3.10.3" date="2014-01-26" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.35.91" date="2020-02-27" status="candidate"/>

    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Las áreas de trabajo son una manera de agrupar ventanas en su escritorio.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>¿Qué es un área de trabajo y cómo me ayudará?</title>

  <p if:test="!platform:gnome-classic">Las áreas de trabajo se refieren a la agrupación de ventanas en su escritorio. Puede crear varias áreas de trabajo, que actúan como escritorios virtuales. Las áreas de trabajo están destinadas a reducir el desorden y hacer que el escritorio sea sencillo de examinar.</p>

  <p if:test="platform:gnome-classic">Las áreas de trabajo se refieren a la agrupación de ventanas en su escritorio. Puede usar varias áreas de trabajo, que actúan como escritorios virtuales. Las áreas de trabajo están destinadas a reducir el desorden y hacer que el escritorio sea sencillo de examinar.</p>

  <p>Podría utilizar las áreas de trabajo para organizar su trabajo. Por ejemplo, podría tener todas sus ventanas de comunicación, tales como el correo electrónico y su programa de chat en un área de trabajo y el trabajo que está haciendo en un área de trabajo diferente. Su gestor de música podría estar en una tercera área de trabajo.</p>

<p>Usando áreas de trabajo:</p>

<list>
  <item>
    <p if:test="!platform:gnome-classic">In the
    <gui xref="shell-introduction#activities">Activities</gui> overview, you can
    horizontally navigate between the workspaces.</p>
    <p if:test="platform:gnome-classic">Pulse el botón en la parte inferior izquierda de la pantalla en la lista de ventanas o pulse la tecla <key xref="keyboard-key-super">Super</key> para abrir la vista de <gui>Actividades</gui>.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">If more than one workspace is already
    in use, the <em>workspace selector</em> is shown between the search field and
    the window list. It will display currently used workspaces plus an empty workspace.</p>
    <p if:test="platform:gnome-classic">En la esquina inferior derecha puede ver cuatro cajas. Esto es el selector de áreas de trabajo.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">To add a workspace, drag and drop a
    window from an existing workspace onto the empty workspace in the workspace
    selector. This workspace now contains the window you have dropped, and a new
    empty workspace will appear next to it.</p>
    <p if:test="platform:gnome-classic">Arrastre y suelte una ventana de su área de trabajo actual hasta una vacía en el selector de áreas de trabajo. Esta área de trabajo contiene ahora la ventana que se ha arrastrado.</p>
  </item>
  <item if:test="!platform:gnome-classic">
    <p>Para quitar un área de trabajo, simplemente cierre todas las ventanas que tenga, o muévalas a otras áreas de trabajo.</p>
  </item>
</list>

<p if:test="!platform:gnome-classic">Siempre hay al menos un área de trabajo.</p>

    <media its:translate="yes" type="image" src="figures/shell-workspaces.png" width="940" height="291">
        <p>Selector de áreas de trabajo</p>
    </media>

</page>
