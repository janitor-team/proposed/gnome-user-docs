<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sharing-bluetooth" xml:lang="es">

  <info>
    <link type="guide" xref="bluetooth"/>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.8.2" date="2013-05-13" status="draft"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-08" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="author">
      <name>David King</name>
      <email>davidk@gnome.org</email>
      <years>2014-2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Permitir subir archivos al equipo mediante Bluetooth</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Controlar la compartición por Bluetooth</title>

  <p>Puede activar la compartición <gui>Bluetooth</gui> para recibir archivos mediante Bluetooth en la carpeta <file>Descargas</file></p>

  <steps>
    <title>Permitir que se compartan los archivos de su carpeta <file>Descargas</file></title>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Bluetooth</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>Asegúrese de que el <link xref="bluetooth-turn-on-off"><gui>Bluetooth</gui> está activado</link>.</p>
    </item>
    <item>
      <p>Los dispositivos con Bluetooth pueden enviar archivos a su carpeta de <file>Descargas</file> sólo cuando el panel de <gui>Bluetooth</gui> está abierto.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Puede <link xref="sharing-displayname">cambiar</link> el nombre que el equipo muestra a otros dispositivos.</p>
  </note>

</page>
