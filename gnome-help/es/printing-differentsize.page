<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-differentsize" xml:lang="es">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <its:rules xmlns:its="http://www.w3.org/2005/11/its" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <desc>Imprimir un documento en un tamaño de papel u orientación diferente.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Cambiar el tamaño del papel al imprimir</title>

  <p>Si quiere cambiar el tamaño de papel del documento (por ejemplo, imprimir un PDF de tamaño carta americana en papel A4), puede cambiar el formato de impresión para el documento.</p>

  <steps>
    <item>
      <p>Abra el diálogo de impresión pulsando <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Seleccione la pestaña <gui>Configuración de página</gui>.</p>
    </item>
    <item>
      <p>Bajo la columna <em>Papel</em>, seleccione su <gui>Tamaño de papel</gui> de la lista desplegable.</p>
    </item>
    <item>
      <p>Pulse <gui>Imprimir</gui> para imprimir su documento.</p>
    </item>
  </steps>

  <p>También puede usar la lista desplegable <gui>Orientación</gui> para elegir una orientación diferente:</p>

  <list>
    <item><p><gui>Vertical</gui></p></item>
    <item><p><gui>Apaisado</gui></p></item>
    <item><p><gui>Retrato invertido</gui></p></item>
    <item><p><gui>Apaisado invertido</gui></p></item>
  </list>

</page>
