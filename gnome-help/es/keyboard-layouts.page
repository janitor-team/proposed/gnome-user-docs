<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="keyboard-layouts" xml:lang="es">

  <info>
    <link type="guide" xref="prefs-language"/>
    <link type="guide" xref="keyboard" group="i18n"/>

    <revision pkgversion="3.8" version="0.3" date="2013-04-30" status="review"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="author">
       <name>Julita Inca</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Juanjo Marín</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Añadir distribuciones del teclado y cambiar entre ellas.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Usar distribuciones de teclado alternativas</title>

  <p>Los teclados vienen en cientos de distribuciones diferentes para distintos idiomas. Incluso para un solo idioma pueden existir muchas distribuciones de teclado, como la distribución Dvorak para el inglés. Puede hacer que su teclado se comporte como un teclado con una distribución diferente, independientemente de qué letras y símbolos tenga escritos en las teclas. Esto es útil si tiene que cambiar entre varios idiomas con frecuencia.</p>

  <steps>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Configuración</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Configuración</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Teclado</gui> en la barra lateral para abrir el panel.</p>
    </item>
    <item>
      <p>Pulse el botón <key>+</key> en la sección <gui>Fuentes de entrada</gui>, seleccione el idioma asociado a la distribución, seleccione una distribución y pulse <gui>Añadir</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Si hay varias cuentas de usuario en su sistema, hay una instancia separada de la <gui>Región e idioma </gui> para la pantalla de inicio de sesión. Haga clic en el botón <gui>Pantalla de inicio de sesión</gui> en la esquina superior derecha para alternar entre las dos instancias.</p>

    <p>Algunas variantes de diseño de teclado raramente utilizadas no están disponibles de forma predeterminada cuando pulsa en el botón <gui>+</gui>. Para que también estén disponibles esas fuentes de entrada, puede abrir una ventana de la terminal presionando <keyseq><key>Ctrl</key><key>Alt</key><key>T</key> </keyseq> y ejecutar este comando :</p>
    <p><cmd its:translate="no">gsettings set org.gnome.desktop.input-sources
    show-all-sources true</cmd></p>
  </note>

  <note style="sidebar">
    <p>Puede previsualizar una imagen de cualquier distribución seleccionándola en la lista de <gui>Fuentes de entrada</gui> y pulsando <gui><media its:translate="no" type="image" src="figures/input-keyboard-symbolic.png" width="16" height="16"><span its:translate="yes">vista previa</span></media></gui></p>
  </note>

  <p>Ciertos idiomas ofrecen algunas opciones de configuración adicionales. Puede identificar estos idiomas porque tienen un icono de <gui><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16"><span its:translate="yes">vista previa</span></media></gui>. Si quiere acceder a estos parámetros adicionales, seleccione el idioma en la lista de <gui>Fuentes de entrada</gui> y se mostrará un botón de <gui style="button"><media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg" width="16" height="16"><span its:translate="yes">preferencias</span></media></gui> que le dará acceso a la configuración adicional.</p>

  <p>Cuando usa varias distribuciones, puede elegir entre tener todas las ventanas con la misma distribución o asignar una distribución diferente a cada ventana. Esto último resulta interesante, por ejemplo, si está escribiendo un artículo en otro idioma en una ventana del procesador de textos. Cada ventana recordará su selección de teclado conforme vaya pasando de una ventana a otra. Pulse el botón <gui style="button">Opciones</gui> para seleccionar cómo quiere gestionar varias distribuciones.</p>

  <p>La barra superior mostrará un identificador corto para la distribución actual, como <gui>es</gui> para la distribución estándar de español. Pulse en el indicador de la distribución y seleccione la distribución que quiere usar en el menú. Si el idioma seleccionado tiene alguna configuración adicional, se mostrará debajo de la lista de distribuciones disponibles. Esto le da una vista general rápida de la configuración. También puede abrir una imagen con la distribución actual del teclado para usarla como referencia.</p>

  <p>La manera más rápida de cambiar a otra distribución es usando los <gui>Atajos del teclado</gui> de las <gui>Fuentes de entrada</gui>. Estos atajos abren el selector de <gui>Fuentes de entrada</gui>, donde puede moverse hacia adelante o hacia atrás. De manera predeterminada, puede cambiar a la siguiente fuente de entrada pulsando <keyseq><key xref="keyboard-key-super">Super</key><key>Espacio</key></keyseq> y a la fuente de entrada anterior pulsando <keyseq><key>Mayús</key><key>Super</key> <key>Espacio</key></keyseq>. Puede cambiar estos atajos en la configuración del <gui>Teclado</gui>, en <guiseq><gui>Atajos del teclado</gui><gui>Personalizar atajos</gui><gui>Escritura</gui></guiseq>.</p>

  <p><media type="image" src="figures/input-methods-switcher.png"/></p>

</page>
