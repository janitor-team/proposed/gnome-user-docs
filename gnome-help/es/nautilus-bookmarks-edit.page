<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="nautilus-bookmarks-edit" xml:lang="es">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="review"/>
    <revision pkgversion="3.38" date="2020-10-16" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Añadir, eliminar y renombrar marcadores en el gestor de archivos.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Editar marcadores de carpeta</title>

  <p>Sus marcadores se listan en la barra lateral del gestor de archivos.</p>

  <steps>
    <title>Añadir un marcador:</title>
    <item>
      <p>Abra la carpeta (o la ubicación) que quiera añadir a los marcadores.</p>
    </item>
    <item>
      <p>Click the current folder in the path bar and then select
      <gui style="menuitem">Add to Bookmarks</gui>.</p>
      <note>
        <p>You can also drag a folder to the sidebar, and drop it
        over <gui>New bookmark</gui>, which appears dynamically.</p>
      </note>
    </item>
  </steps>

  <steps>
    <title>Eliminar un marcador:</title>
    <item>
      <p>Pulse con el botón derecho del ratón sobre el marcador en la barra lateral y elija <gui>Eliminar</gui> en el menú.</p>
    </item>
  </steps>

  <steps>
    <title>Renombrar un marcador:</title>
    <item>
      <p>Pulse con el botón derecho del ratón sobre el marcador en la barra lateral y seleccione <gui>Renombrar…</gui>.</p>
    </item>
    <item>
      <p>En la casilla <gui>Nombre</gui> escriba el nombre nuevo para el marcador.</p>
      <note>
        <p>Al renombrar un marcador no se renombra la carpeta. Si tiene marcadores de dos carpetas distintas en dos ubicaciones distintas, pero cada una con el mismo nombre, el marcador tendrá el mismo nombre, y no podrá distinguirlos. En esos casos, es útil darle al marcador un nombre distinto al de la carpeta a la que apunta.</p>
      </note>
    </item>
  </steps>

</page>
