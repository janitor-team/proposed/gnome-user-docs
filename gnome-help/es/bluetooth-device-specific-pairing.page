<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="bluetooth-device-specific-pairing" xml:lang="es">

  <info>
    <link type="guide" xref="bluetooth" group="#last"/>

    <credit type="author">
      <name>Bastien Nocera</name>
      <email>hadess@hadess.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Cómo emparejar dispositivos específicos a su equipo.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Instrucciones de emparejado para dispositivos específicos</title>

  <p>Incluso si logra obtener el manual de un dispositivo, es posible que no contenga suficiente información para lograr el emparejamiento. Aquí hay detalles para algunos dispositivos comunes.</p>

  <terms>
    <item>
      <title>Mandos 3 de PlayStation</title>
      <p>Esos dispositivos usan «emparejamiento por cable». Conecte los mandos a través de USB con la <gui> Configuración de Bluetooth </gui> abierta y Bluetooth activado. Después de pulsar el botón «PS», se le preguntará si quiere configurar esos mandos. Desconéctelos y pulse el botón «PS» para usarlos a través de Bluetooth.</p>
    </item>
    <item>
      <title>Mandos 4 de PlayStation</title>
      <p>Esos dispositivos también usan «emparejamiento por cable». Conecte los mandos a través de USB con la <gui>Configuración de Bluetooth</gui> abierta y Bluetooth activado. Se le preguntará si quiere configurar esos mandos sin necesidad de pulsar el botón PS. Desconéctelos y pulse el botón PS para usarlos a través de Bluetooth.</p>
      <p>El uso de la combinación de botones «PS» y «Share» para emparejar el mando también se puede usar para hacer que el mando sea visible y emparejarlo como cualquier otro dispositivo Bluetooth si no tiene un cable USB a mano.</p>
    </item>
    <item>
      <title>Control remoto de PlayStation 3 BD</title>
      <p>Pulse los botones «Start» y «Enter» a la vez durante unos 5 segundos. Puede seleccionar el control remoto en la lista de dispositivos de la manera habitual.</p>
    </item>
    <item>
      <title>Mandos de Nintendo Wii y Wii U</title>
      <p>Use el botón rojo «Sync» dentro del compartimento de la batería para iniciar el proceso de emparejamiento. Otras combinaciones de botones no mantendrán la información de emparejamiento, por lo que deberá volver a hacerlo en poco tiempo. También tenga en cuenta que algunos programas quieren acceso directo a los controles remotos y, en esos casos, no debe configurarlos en el panel Bluetooth. Consulte el manual de la aplicación para obtener instrucciones.</p>
    </item>
    <item>
      <title>ION iCade</title>
      <p>Mantenga pulsados los 4 botones inferiores y el botón blanco superior para empezar el proceso de emparejamiento. Cuando aparezcan las instrucciones de emparejamiento, asegúrese de usar solamente direcciones cardinales para introducir el código, seguidas de cualquiera de los 2 botones blancos del extremo derecho del mando de la máquina recreativa para confirmar.</p>
    </item>
  </terms>

</page>
