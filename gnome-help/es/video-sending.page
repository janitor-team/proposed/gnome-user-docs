<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="video-sending" xml:lang="es">

  <info>
    <link type="guide" xref="media#videos"/>
    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.12.1" date="2014-03-30" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.17.90" date="2015-08-30" status="final"/>

    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Comprobar que tiene los códecs de vídeo adecuados instalados.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Otras personas no pueden reproducir los vídeos que yo hice</title>

  <p>Si ha realizado un vídeo en su equipo GNU/Linux y lo envió a alguien que use Windows o Mac OS, es posible que tengan problemas para reproducir el vídeo.</p>

  <p>Para poder reproducir su vídeo, la persona a la que lo envió debe tener los <em>códecs</em> adecuados instalados. Un códec es una pequeña pieza de software que sabe cómo sacar el vídeo y mostrarlo en la pantalla. Hay muchos formatos de vídeo diferentes y cada uno requiere un códec para reproducirse. Puede comprobar el formato de su vídeo haciendo lo siguiente:</p>

  <list>
    <item>
      <p>Abra <app>Archivos</app> desde la vista de <gui xref="shell-introduction#activities">Activities</gui>.</p>
    </item>
    <item>
      <p>Pulse con el botón derecho del ratón sobre el archivo de vídeo y seleccione <gui>Propiedades</gui>.</p>
    </item>
    <item>
      <p>Vaya a la pestaña <gui>Sonido/Vídeo</gui>o <gui>Vídeo</gui> y compruebe qué <gui>Codificador</gui> aparece en <gui>Vídeo</gui> y en <gui>Sonido</gui> (si el vídeo también tiene sonido).</p>
    </item>
  </list>

  <p>Pregunte a la persona que tiene problemas con la reproducción si tienen el códec correcto instalado. Es posible que le resulte útil buscar en la web el nombre del códec más el nombre de su aplicación de reproducción de vídeo. Por ejemplo, si el vídeo utiliza el formato <em>Theora</em> y tiene un amigo que usa Windows Media Player para tratar de verlo, busque «theora Windows Media Player». Normalmente será capaz de descargar el códec adecuado de manera gratuita, si no está instalado.</p>

  <p>Si no puede encontrar el códec adecuado, inténtelo con el reproductor multimedia <link href="http://www.videolan.org/vlc/">VLC</link>. Funciona en Windows, Mac OS y GNU/Linux, y permite una gran cantidad de formatos de vídeo diferentes. De no ser así, intente convertir el vídeo en un formato diferente. La mayoría de los editores de vídeo son capaces de hacer esto y las aplicaciones específicas de convertidor de vídeo están disponibles. Compruebe la aplicación de instalación de software para ver lo que está disponible.</p>

  <note>
    <p>Hay algunos pocos problemas más, que pueden evitar que alguien reproduzca el vídeo. El vídeo podría haberse dañado cuando se envió (a veces los archivos grandes no se copian a la perfección), puede haber problemas con su aplicación de reproducción de vídeo o el vídeo puede no haberse creado correctamente (podría haber habido algunos errores en el momento de guardar el vídeo).</p>
  </note>

</page>
