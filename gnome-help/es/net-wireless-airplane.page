<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-airplane" xml:lang="es">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.24" date="2017-03-26" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>

    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>

    <desc>Abra la configuración de red y active el modo avión.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>Apagar el modo inalámbrico (modo avión)</title>

<p>Si tiene su equipo en un avión (o en alguna otra área donde no estén permitidas las conexiones inalámbricas), debe apagar su sistema inalámbrico. También debería apagarlo por otras razones: (para ahorrar batería, por ejemplo). Para hacer esto:</p>

  <note>
    <p>Usar el <em>Modo avión</em> desactivará completamente todas las conexiones inalámbricas, incluyendo las conexiones inalámbricas, 3G y de Bluetooth.</p>
  </note>

  <p>Para activar el modo avión</p>

  <steps>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Inalámbrica</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Inalámbrica</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>Active el <gui>Modo avión</gui>. Esto apagará su conexión inalámbrica hasta que apague el modo avión.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Puede desactivar su conexión inalámbrica desde el <gui xref="shell-introduction#systemmenu">menú del sistema</gui> pulsando en el nombre de la conexión y eligiendo <gui>Apagar</gui>.</p>
  </note>

</page>
