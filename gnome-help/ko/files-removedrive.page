<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-removedrive" xml:lang="ko">

  <info>
    <link type="guide" xref="files#removable"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <revision pkgversion="3.6.0" version="0.2" date="2012-10-08" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>USB 플래시 드라이브, CD, DVD, 기타 장치를 꺼내거나 마운트 해제합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>외장 드라이브 안전하게 제거하기</title>

  <p>USB 플래시 드라이브 같은 외부 저장장치를 사용할 때는 뽑기 전에 안전하게 제거해야 합니다. 장치를 바로 뽑아버리면 프로그램에서 여전히 활용하고 있을 때 뽑는 동작으로 인해 감수할 손해에 직면합니다. 결과적으로 일부 파일이 없어지거나 깨집니다. CD 또는 DVD 광 디스크를 사용하는 경우 컴퓨터에서 디스크를 꺼낼 때 동일한 과정을 거칠 수 있습니다.</p>

  <steps>
    <title>회장 드라이브를 꺼내려면:</title>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요에서 <app>파일</app>을 엽니다.</p>
    </item>
    <item>
      <p>가장자리 창에서 장치를 찾습니다. 이름 옆에 작은 꺼내기 아이콘이 있습니다. 꺼내기 아이콘을 누르면 장치를 안전하게 제거하거나 꺼냅니다.</p>
      <p>또는, 가장자리 창의 이름에 포인터 커서를 올려다 놓고 오른쪽 단추를 누른 후 <gui>꺼내기</gui>를 선택하여 꺼낼 수 있습니다.</p>
    </item>
  </steps>

  <section id="remove-busy-device">
    <title>사용중인 장치를 안전하게 제거</title>

  <p>프로그래에서 장치의 임의의 파일을 열어 사용중이라면 장치를 안전하게 제거할 수 없습니다. <gui>볼륨을 사용중입니다</gui>라는 창이 뜹니다. 장치를 안전하게 제거하려면:</p>

  <steps>
    <item><p><gui>취소</gui>를 누릅니다.</p></item>
    <item><p>장치의 모든 파일을 닫습니다.</p></item>
    <item><p>꺼내기 아이콘을 눌러 장치를 안전하게 제거하거나 꺼냅니다.</p></item>
    <item><p>또는, 가장자리 창의 이름에 포인터 커서를 올려다 놓고 오른쪽 단추를 누른 후 <gui>꺼내기</gui>를 선택하여 꺼낼 수 있습니다.</p></item>
  </steps>

  <note style="warning"><p><gui>어쨌든 꺼내기</gui>를 선택하여 파일을 닫지 않고도 장치를 제거할 수도 있습니다. 이럴 경우 프로그램에서 해당 파일을 열었을 때 오류가 나타날 수 있습니다.</p></note>

  </section>

</page>
