<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-admin-change" xml:lang="ko">

  <info>
    <link type="guide" xref="user-accounts#privileges"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>그놈 문서 프로젝트</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>사용자에게 관리자 권한을 부여하여 시스템의 중요한 부분을 바꿀 수 있게 합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>관리자 권한 소유자 바꾸기</title>

  <p>관리자 권한은 시스템의 중요한 일부 설정을 바꿀 수 있는 수단입니다. 여러분이라면 어떤 사용자에게 관리자 권한을 주어야 할 지 말아야 할 지 바꿀 수 있습니다. 시스템을 안전하게 지키고 허락하지 않은 상황에서 시스템 설정을 바꾸는 행위으로 시스템을 망가뜨리는 경우의 수를 막는 방법이 있습니다.</p>

  <p>계정 형식을 바꾸려면 <link xref="user-admin-explain">관리자 권한</link>이 필요합니다.</p>

  <steps>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>사용자</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>사용자</gui>를 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p>우측 상단 구석의 <gui style="button">잠금 해제</gui>를 누르고 암호를 물어보면 입력합니다.</p>
    </item>
    <item>
      <p>권한을 바꾸고자 하는 사용자를 선택합니다.</p>
    </item>
    <item>
      <p><gui>계정 형식</gui> 옆 <gui>표준</gui> 레이블을 누르고 <gui>관리자</gui>를 선택합니다.</p>
    </item>
    <item>
      <p>사용자 권하는 다음에 로그인할 떄 바뀝니다.</p>
    </item>
  </steps>

  <note>
    <p>시스템의 처음 사용자 계정은 보통 관리자 권한을 가진 계정입니다. 이 계정은 시스템을 처음 설치할 때 만든 사용자 계정입니다.</p>
    <p>시스템에 <gui>관리자</gui> 권한을 많은 사용자에게 남용하는건 현명하지 않습니다.</p>
  </note>

</page>
