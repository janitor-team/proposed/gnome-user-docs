<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="hardware-cardreader" xml:lang="ko">

  <info>
    <link type="guide" xref="media#photos"/>
    <link type="guide" xref="hardware#problems"/>

    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>그놈 문서 프로젝트</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>미디어 카드 읽기 장치의 문제를 해결합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

<title>미디어 카드 리더 문제</title>

<p>상당수의 컴퓨터에는 SD, MMC, SM, MS, CF 등 기타 저장소 미디어 카드 읽기 장치가 붙어있습니다. 저장소 미디어 카드는 자동으로 발견하고 <link xref="disk-partitions">마운트</link>합니다. 마운트가 안되는 경우 몇가지 문제 해결 단계는 다음과 같습니다:</p>

<steps>
<item>
<p>카드를 제대로 넣었는지 확인합니다. 상당수의 카드는 제대로 넣었을 때 뒤집어져있는 것처럼 보입니다. 또한 슬롯에 충분히 들어갔는지 확인합니다. 특히 CF 카드의 경우 제대로 넣으려면 약간의 힘을 주어야 합니다. (너무 세게 밀어넣지 않게 주의합니다! 무언가 단단한게 걸린다면 그 이상 힘을 주지 않습니다.)</p>
</item>

<item>
  <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요에서 <app>파일</app>을 엽니다. 왼쪽 가장자리 창에 장착한 카드가 나타납니까? 때로는 목록에 카드가 나타나지만 마운트한 상태가 아닐 수도 있습니다. 이때 한 번 눌러주면 마운트합니다. 가장자리 창에 나타나지 않으면, <key>F9</key> 키를 누르거나 상단 표시줄의 <gui style="menu">파일</gui>을 누른 후 <gui style="menuitem">가장자리 창</gui>을 선택합니다.</p>
</item>

<item>
  <p>가장자리 창에 카드가 나타나지 않는다면, <keyseq><key>Ctrl</key><key>L</key></keyseq>을 누르고 <input>computer:///</input>를 입력한 다음 <key>Enter</key> 키를 누릅니다. 카드 읽기 장치를 제대로 구성했다면 읽기 장치가 카드 없는 드라이브로 나타나며, 카드를 장착하면 카드 내용이 나타납니다.</p>
</item>

<item>
<p>카드가 아닌 카드 읽기 장치를 본다면 카드 자체에 문제가 있을 수 있습니다. 다른 카드를 넣어보거나 가능한 경우 다른 카드 읽기 장치를 사용해봅니다.</p>
</item>
</steps>

<p><gui>컴퓨터</gui> 위치를 찾아볼 때 카드 또는 드라이브가 나타나지 않는다면 드라이버 문제로 리눅스에서 카드 읽기 장치가 동작하지 않을 수도 있습니다. 카드 읽기 장치가 내장형(컴퓨터 외부에 두는 방식이 아닌 안에 붙어있는 방식)이라면 거의 그렇습니다. 최선의 방법이라면 장치 (카메라, 휴대폰 등)를 컴퓨터의 USB에 직접 연결하는 방법입니다. 외장 USB 카드 읽기 장치도 있으며, 리눅스 시스템과 더 잘 동작합니다.</p>

</page>
