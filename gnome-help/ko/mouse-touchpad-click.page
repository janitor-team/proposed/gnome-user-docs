<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-touchpad-click" xml:lang="ko">

  <info>
    <link type="guide" xref="mouse"/>

    <revision pkgversion="3.7" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-29" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <!--
    For 41: https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/121
    -->
    <revision version="gnome:40" date="2021-03-18" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>터치패드에서, 누르고 끌거나 두드리기 동작으로 스크롤, 손가락 움직임을 취합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>터치패드로 누름, 끌기, 스크롤 동작</title>

  <p>별도의 하드웨어 단추를 사용하지 않고도 터치패드 만으로 누르기, 두 번 누르기, 끌기, 스크롤이 가능합니다.</p>

  <note>
    <p><link xref="touchscreen-gestures">터치패드 손가락 동작</link>은 별도로 다룹니다.</p>
  </note>

<section id="tap">
  <title>두드려서 누르기</title>

  <media src="figures/touch-tap.svg" its:translate="no" style="floatend"/>

  <p>단추를 사용하는 대신 터치패드를 두드려 누를 수 있습니다.</p>

  <list>
    <item>
      <p>한 번 누르려면, 터치 패드를 한 번 두드립니다.</p>
    </item>
    <item>
      <p>두 번 누르려면, 두 번 두드립니다.</p>
    </item>
    <item>
      <p>항목을 끌어 이동할 때는 두번 두드리되, 두번째 두드림 동작 다음에는 손가락을 들어올리지 않습니다. 항목을 원하는 위치로 끌어둔 다음 놓아둘 위치에서 손가락을 들어올립니다.</p>
    </item>
    <item>
      <p>터치 패드에서 여러 손가락 두드리기 기능을 지원한다면 한번에 두 손가락을 두드려 오른쪽 단추 누름을 진행합니다. 다른 방법을 활용한다면, 오른쪽 단추 누름시 하드웨어 단추가 여전히 필요합니다. 두번째 마우스 단추가 아닌 오른쪽 단추 누름 방식에 대해서는 <link xref="a11y-right-click"/>를 살펴보십시오.</p>
    </item>
    <item>
      <p>터치 패드에서 여러 손가락 두드리기 기능을 지원한다면 한번에 세 손가락을 두드려 <link xref="mouse-middleclick">가운데 단추 누름</link>을 진행합니다.</p>
    </item>
  </list>

  <note>
    <p>여러 손가락으로 두들기거나 끌 때 손가락 사이에 충분한 거릴르 두었는지 확인합니다. 손가락이 너무 가까이 붙어있으면, 손가락 하나로 인식할 수도 있습니다.</p>
  </note>

  <steps>
    <title>두드리면 단추 누름 사용</title>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>마우스 및 터치패드</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>마우스 및 터치패드</gui>를 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p><gui>터치패드</gui> 섹션에서 <gui>터치패드</gui> 스위치를 켰는지 확인합니다.</p>
      <note>
        <p><gui>터치패드</gui> 섹션은 시스템에 터치패드가 있을 경우에만 나타납니다.</p>
      </note>
    </item>
   <item>
      <p><gui>두드리면 단추 누름</gui> 스위치를 켭니다.</p>
    </item>
  </steps>
</section>

<section id="twofingerscroll">
  <title>두 손가락 스크롤</title>

  <media src="figures/touch-scroll.svg" its:translate="no" style="floatend"/>

  <p>두 손가락으로 터치패드에서 스크롤할 수 있습니다.</p>

  <p>이 항목을 선택하면, 한 손가락으로 두드려 끌기 동작은 일상적으로 동작하나, 터치패드 일부분을 두 손가락으로 끌면 스크롤 동작을 진행합니다. 터치패드의 상단 또는 하단 사이에서 손가락을 움직여 위아래로 스크롤하거나, 스크롤 가장자리르 활용하여 손가락을 움직여 스크롤합니다. 손가락 두는 위치에 유의합니다. 손가락을 너무 가깝게 붙이면 큰 손가락 하나로 인식합니다.</p>

  <note>
    <p>모든 터치패드에서 두 손가락 스크롤이 동작하는건 아닙니다.</p>
  </note>

  <steps>
    <title>두 손가락 스크롤 사용</title>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>마우스 및 터치패드</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>마우스 및 터치패드</gui>를 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p><gui>터치패드</gui> 섹션에서 <gui>터치패드</gui> 스위치를 켰는지 확인합니다.</p>
    </item>
    <item>
      <p><gui>두 손가락 스크롤</gui> 스위치를 켭니다.</p>
    </item>
  </steps>
</section>

<section id="contentsticks">
  <title>자연스러운 스크롤</title>

  <p>터치패드로 종이를 물리적으로 제끼듯이 내용을 끌어 움직일 수 있습니다.</p>

  <steps>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>마우스 및 터치패드</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>마우스 및 터치패드</gui>를 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p><gui>터치패드</gui> 섹션에서, <gui>터치패드</gui> 스위치를 켰는지 확인합니다.</p>
    </item>
    <item>
      <p><gui>자연스러운 스크롤</gui> 스위치를 켭니다.</p>
    </item>
  </steps>

  <note>
    <p>이 기능은 <em>스크롤 방향 뒤바꾸기</em>라고도 합니다.</p>
  </note>

</section>

</page>
