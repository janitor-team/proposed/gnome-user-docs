<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-default-browser" xml:lang="ko">

  <info>
    <link type="guide" xref="net-browser"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.36" date="2020-06-24" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc><gui>설정</gui>의 <gui>자세히 보기</gui>로 이동하여 기본 웹 브라우저를 바꿉니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>웹 사이트를 열 기본 웹 브라우저 바꾸기</title>

  <p>어떤 프로그램에서 웹 페이지 링크를 누르면 웹 브라우저를 자동으로 열고 해당 웹 페이지를 엽니다. 그러나 하나 이상의 브라우저를 설치했다면, 원하는 브라우저에서 페이지를 열지 않을 수도 있습니다. 이 문제를 해결하려면 기본 웹 브라우저를 바꿉니다:</p>

  <steps>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <input>기본 프로그램</input> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>기본 프로그램</gui>을 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p><gui>웹</gui> 옵션을 바꾸어 링크를 열 웹 브라우저를 선택합니다.</p>
    </item>
  </steps>

  <p>다른 웹 브라우저로 열면 기본 웹 브라우저가 아님을 알려줍니다. 이 메시지가 나타나면, <gui>취소</gui>(또는 비슷한) 단추를 눌러 기본 브라우저를 다시 설정하지 않게 합니다.</p>

</page>
