<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-which-application" xml:lang="ko">

  <info>
    <link type="guide" xref="accounts"/>
    <link type="seealso" xref="accounts-disable-service"/>

    <revision pkgversion="3.8.2" date="2013-05-22" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="incomplete"/>

    <credit type="author copyright">
      <name>Baptiste Mille-Mathias</name>
      <email>baptistem@gnome.org</email>
      <years>2012, 2013</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>프로그램에서는 <app>온라인 계정</app>에서 만든 계정과 개척한 서비스를 활용할 수 있습니다.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>온라인 서비스 및 프로그램</title>

  <p>온라인 계정을 추가하고 나면, 어떤 프로그램이든 <link xref="accounts-disable-service">비활성</link> 상태가 아닌 가용 서비스에 접근할 수 있습니다. 다른 제공자에서 다양한 서비스를 제공합니다. 이 페이지에서는 다른 서비스 목록을 보여주며 알려진 일부 프로그램에서 활용할 수 있습니다.</p>

  <terms>
    <item>
      <title>달력</title>
      <p>달력 서비스에서는 온라인 달력에서 행사를 보고, 추가하고 편집할 수 있습니다. <app>달력</app>, <app>에볼루션</app>, <app>캘리포니아</app> 프로그램에서 활용합니다.</p>
    </item>

    <item>
      <title>대화</title>
      <p>대화 서비스는 잘 알려진 인스턴트 메시지 플랫폼의 연락처로 대화할 수 있게 해줍니다. <app>엠퍼시</app> 프로그램에서 활용합니다.</p>
    </item>

    <item>
      <title>연락처</title>
      <p>연락서 서비스는 다양한 서비스에 올려둔 여러분의 연락처 세부 정보를 볼 수 있게 해줍니다. <app>연락처</app>와 <app>에볼루션</app>과 같은 프로그램에서 활용합니다.</p>
    </item>

    <item>
      <title>문서</title>
      <p>문서 서비스에서는 구글 닥스와 같은 온라인 문서를 볼 수 있습니다. <app>문서</app> 프로그램에서 문서를 볼 수 있습니다.</p>
    </item>

    <item>
      <title>파일</title>
      <p>파일 서비스에서는 파일 관리자에서 <link xref="nautilus-connect">서버 연결</link> 기능을 활용하여 원격 파일 위치를 추가합니다. 파일 관리자에서 원격 파일에 접근할 수 있으며, 다른 어떤 프로그램에서도 파일 열기 및 저장 대화상자로 파일에 접근할 수 있습니다.</p>
    </item>

    <item>
      <title>메일</title>
      <p>메일 서비스에서는 구글과 같은 전자메일 제공자를 통해 전자메일을 보내고 받을 수 있습니다. <app>에볼루션</app>에서 활용합니다.</p>
    </item>

<!-- TODO: Not sure what this does. Doesn't seem to do anything in Maps app.
    <item>
      <title>Maps</title>
    </item>
-->

    <item>
      <title>사진</title>
      <p>사진 서비스에서는 페이스북에 게시한 사진 같은 온라인 사진을 볼 수 있습니다. <app>사진</app> 프로그램으로 해당 사진을 살펴볼 수 있습니다.</p>
    </item>

    <item>
      <title>프린터</title>
      <p>프린터 서비스에서는 어떤 프로그램에서든 나타나는 인쇄 대화상자에서 PDF 사본을 프린터로 보낼 수 있게 합니다. 제공자는 아마도 인쇄 서비스를 제공하거나 PDF를 나중에 다운로드하여 인쇄할 수 있도록 하는 저장소를 제공할 수도 있습니다.</p>
    </item>

    <item>
      <title>다음에 읽기</title>
      <p>다음에 읽기 서비스는 다른 장치에서 나중에 읽을 수 있도록 외부 서비스에 웹 페이지를 저장할 수 있습니다. 이 서비스를 활용하는 프로그램은 아직 없습니다.</p>
    </item>

  </terms>

</page>
