<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task a11y" id="mouse-doubleclick" xml:lang="ko">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9" date="2013-10-16" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="20156-06-15" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>마우스 단추를 얼마나 빨리 눌러서 두 번 누르기 동작으로 취급할 지 제어합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>두 번 누르기 속도 조절</title>

  <p>두번 누르기는 마우스 단추를 충분히 빠르게 두 번 눌러 수행합니다. 두번째 누름이 첫번째 누름보다 너무 길거나 하면, 두번 누르기가 아닌 별개의 누름 동작으로 인식합니다. 마우스 단추를 빨리 누르는데 어려움이 있다면, 지연 시간을 늘려야합니다.</p>

  <steps>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>접근성</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>접근성</gui>을 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p><gui>마우스 이동 및 누르기</gui>에서 <gui>두 번 누르기 지연시간</gui> 슬라이더를 편하게 느껴지는 값으로 끌어 조절합니다.</p>
    </item>
  </steps>

  <p>두번 누르기 제한 시간을 늘렸음에도 불구하고 한번 눌렀을 때 두번 눌린다면, 마우스가 고장난 상태일 가능성이 있습니다. 다른 마우스를 컴퓨터에 연결하고 제대로 동작하는지 확인합니다. 대신, 마우스를 다른 컴퓨터에 연결하여 동일한 문제가 나타나는지도 확인합니다.</p>

  <note>
    <p>이 설정은 마우스 및 터치패드, 기타 포인팅 장치에 영향을 줍니다.</p>
  </note>

</page>
