<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="keyboard-osk" xml:lang="ko">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>

    <credit type="author">
      <name>Jeremy Bicha</name>
      <email>jbicha@ubuntu.com</email>
    </credit>
    <credit type="author">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>마우스 단추로 누르거나 터치 스크린 활용 방식으로 글자를 입력하는 화면 키보드를 활용합니다.</desc>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>화면 키보드 활용하기</title>

  <p>컴퓨터에 키보드를 연결하지 않았거나 사용하고 싶지 않으면 <em>화면 키보드</em>를 켜서 텍스트를 입력할 수 있습니다.</p>

  <note>
    <p>터치스크린을 활용한다면 화면 키보드는 자동으로 켜집니다</p>
  </note>

  <steps>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>설정</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>설정</gui>을 누릅니다.</p>
    </item>
    <item>
      <p>가장자리 창에서 <gui>접근성</gui>을 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p><gui>자판 입력</gui> 섹션에서 <gui>화면 키보드</gui> 스위치를 켭니다.</p>
    </item>
  </steps>

  <p>다음에 입력할 상황이 주어지면, 화면 키보드가 화면 하단에 나타납니다.</p>

  <p><gui style="button">?123</gui> 단추를 누르면 숫자와 기호를 입력합니다. <gui style="button">=/&lt;</gui> 단추를 누르면 다른 추가 기호가 나타납니다. 알파벳 키보드로 돌아가려면 <gui style="button">ABC</gui> 단추를 누릅니다.</p>

  <p><gui style="button"><media its:translate="no" type="image" src="figures/go-down-symbolic.svg" width="16" height="16"><span its:translate="yes">down</span></media></gui> 단추를 눌러 키보드를 잠깐 숨길 수 있습니다. 키보드를 사용할 수 있는 어딘가를 누르면 키보드가 다시 나타납니다. 터치스크린에서는 화면 키보드를 <link xref="touchscreen-gestures">하단 모서리에서 끌어올릴 수 있습니다</link>.</p>
  <p><gui style="button"><media its:translate="no" type="image" src="figures/emoji-flags-symbolic.svg" width="16" height="16"><span its:translate="yes">flag</span></media></gui> 단추를 눌러 <link xref="session-language">언어</link> 또는 <link xref="keyboard-layouts">입력기</link> 설정을 바꿉니다.</p>

</page>
