<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-night-light" xml:lang="ko">
  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="3.28" date="2018-07-28" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>
    <revision pkgversion="3.34" date="2019-11-11" status="review"/>
    <revision pkgversion="3.38.2" date="2021-03-06" status="review"/>
    <revision pkgversion="40.1" date="2021-06-09" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2018</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>그 날짜의 시간대에 따라 야간조명이 디스플레이 색상을 바꿉니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>화면 색 온도 조절하기</title>

  <p>컴퓨터 모니터에서는 어두워지고 나면 졸음과 피곤을 유발하는 청색광을 냅니다. <gui>야간 모드</gui>는 하루 시간대에 따라 색상을 바꾸어 저녁시간대에 표시하는 색상을 따뜻하게 느끼게 합니다. <gui>야간 모드</gui>를 활성화하려면:</p>

  <steps>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>디스플레이</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>디스플레이</gui>를 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p>헤더 모음의 <gui>야간 모드</gui>를 눌러 설정을 엽니다.</p>
    </item>
    <item>
      <p><gui>야간 모드</gui> 스위치를 켰는지 확인합니다.</p>
    </item>
    <item>
      <p><gui>일정</gui>에서 <gui>일몰에서 일출</gui>을 선택하여 거주 지역의 일몰 및 일출 시간대를 따라 화면 색상이 따라가게 합니다. 일정을 개별 지정하려면 <gui>일정 수동 설정</gui>을 선택하여 <gui>시각</gui>을 설정합니다.</p>
    </item>
    <item>
      <p>슬라이더를 활용하여 <gui>색 온도</gui> 더 또는 덜 따듯하게 조절합니다.</p>
    </item>
  </steps>
      <note>
        <p><link xref="shell-introduction">상단 표시줄</link>에서는 언제 <gui>야간 모드</gui>를 켤지 보여줍니다. 시스템 메뉴에서 임시로 끌 수 있습니다.</p>
      </note>



</page>
