<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-recover" xml:lang="ko">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>
    <link type="seealso" xref="files-lost"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="review"/>

    <credit type="author">
      <name>그놈 문서 프로젝트</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>삭제한 파일은 보통 휴지통에 들어가지만, 복원할 수 있습니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>휴지통에서 파일 복원하기</title>

  <p>파일 관리자에서 파일을 삭제했다면 파일은 보통 <gui>휴지통</gui>에 들어가며 복원할 수 있습니다.</p>

  <steps>
    <title>휴지통에서 파일을 복원하려면:</title>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <app>파일</app> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><app>파일</app>을 눌러 파일 관리자를 엽니다.</p>
    </item>
    <item>
      <p>가장자리 창에서 <gui>휴지통</gui>을 누릅니다. 가장자리 창에 휴지통이 나타나지 않으면, 창 우측 상단의 메뉴 단추를 누른 후 <gui>가장자리 창</gui>을 선택합니다.</p>
    </item>
    <item>
      <p>삭제한 파일이 있다면, 해당 파일을 누른 후 <gui>복원</gui>을 선택합니다. 그러면 파일을 지웠던 폴더에 해당 파일을 복원해줍니다.</p>
    </item>
  </steps>

  <p><keyseq><key>Shift</key><key>Delete </key></keyseq> 키를 눌러 파일을 삭제했거나 명령행에서 파일을 삭제했다면, 파일을 완전히 삭제한 상황입니다. 완전히 삭제한 파일은 <gui>휴지통</gui>에서 복원할 수 없습니다.</p>

  <p>완전히 삭제한 파일으 복원하는 수많은 복원 도구가 있습니다. 그러나 이 도구는 보통 사용하기 쉽지 않습니다. 실수로 파일을 완전히 삭제했다면, 파일을 복원할 수 있는지 지원 포럼에서 조언을 구하는게 가장 좋습니다.</p>

</page>
