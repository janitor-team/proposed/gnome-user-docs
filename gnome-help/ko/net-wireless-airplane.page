<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-airplane" xml:lang="ko">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.24" date="2017-03-26" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>

    <credit type="author">
      <name>그놈 문서 프로젝트</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>

    <desc>네트워크 설정을 열어 비행 모드를 '켬'으로 전환합니다.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

<title>무선 통신 끄기(비행 모드)</title>

<p>컴퓨터가 비행기 안에 있다면(또는 무선 연결을 할 수 없는 장소에 있을 경우), 무선 연결을 끊어야 합니다. 또한 다른 이유(배터리 절전 등)로 무선 연결을 끊어야 할 때도 있습니다.</p>

  <note>
    <p><em>비행기 모드</em>를 활용하면 와이파이, 3G, 블루투스 연결 등의 무선 연결을 완전히 끊어줍니다.</p>
  </note>

  <p>비행 모드를 켜려면:</p>

  <steps>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>와이파이</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>와이파이</gui>를 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p><gui>비행기 모드</gui> 스위치를 켭니다. 이렇게 하면 비행기 모드를 끄기 전까지 무선 연결을 꺼줍니다.</p>
    </item>
  </steps>

  <note style="tip">
    <p>연결 이름을 누르고 <gui>끄기</gui>를 선택하여 <gui xref="shell-introduction#systemmenu">시스템 메뉴</gui>에서 와이파이 연결을 끌 수 있습니다.</p>
  </note>

</page>
