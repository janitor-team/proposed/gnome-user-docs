<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="a11y task" id="a11y-visualalert" xml:lang="ko">

  <info>
    <link type="guide" xref="a11y#sound"/>
    <link type="seealso" xref="sound-alert"/>

    <revision pkgversion="3.7.1" date="2012-11-10" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>경고음을 재생할 때 화면 또는 창을 깜빡이는 시각 알림을 켭니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>경고음을 낼 때 화면 깜빡이기</title>

  <p>컴퓨터에서는 다양한 형식의 메시지와 이벤트에 대해 간단한 경고음을 재생합니다. 이 경고음을 듣기 어려울 때, 경고 음을 재생할 때마다 전체 화면 또는 현재 창을 깜빡이게 할 수 있습니다.</p>

  <p>이 기능을 켜면 도서관 같은 환경에서 컴퓨터를 조용하게 활용해야 할 경우 쓸만합니다. 경고음을 끄고 시각 알림을 켜는 방법을 알아보려면 <link xref="sound-alert"/>를 살펴봅니다.</p>

  <steps>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>접근성</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>접근성</gui>을 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p><gui>듣기</gui> 섹션에서 <gui>시각 알림</gui>을 누릅니다.</p>
    </item>
    <item>
      <p><gui>시각 알림</gui> 스위치를 켭니다.</p>
    </item>
    <item>
      <p>전체 화면을 깜빡일 지 현재 창 제목 부분믄 깜빡일 지 선택합니다.</p>
    </item>
  </steps>

  <note style="tip">
    <p>상단 표시줄의 <link xref="a11y-icon">접근성 아이콘</link>을 누른 후 <gui>시각 알림</gui>를 선택하여 간단하게 시각 알림을 켜고 끌 수 있습니다.</p>
  </note>

</page>
