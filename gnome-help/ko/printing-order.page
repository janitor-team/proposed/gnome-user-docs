<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-order" xml:lang="ko">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>인쇄 순서를 모으거나 뒤집습니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>다른 순서로 인쇄 순서 지정</title>

  <section id="reverse">
    <title>순서 뒤집기</title>

    <p>프린터는 보통 첫 페이지를 우선 나중 페이지를 나중에 인쇄하기 때문에, 인쇄물을 집어들면 순서가 반대로 되기도 합니다. 필요한 경우 인쇄 순서를 반대로 지정할 수 있습니다.</p>

    <steps>
      <title>순서를 뒤집으려면:</title>
      <item>
        <p><keyseq><key>Ctrl</key><key>P</key></keyseq> 키를 눌러 인쇄 대화 상자를 엽니다.</p>
      </item>
      <item>
        <p><gui>일반</gui> 탭의 <gui>사본</gui>에서 <gui>역순</gui>을 표시합니다. 마지막 페이지부터 먼저 그 다음 역순으로 인쇄합니다.</p>
      </item>
    </steps>

  </section>

  <section id="collate">
    <title>한 부씩 인쇄</title>

  <p>문서 사본을 하나 이상 인쇄할 경우 페이지 수 별로 모아서 인쇄합니다(이 말인 즉슨, 임의 페이지 사본이 다 나온 후 두 번째 페이지 사본이 다 나오는 식입니다). <em>한 부씩 인쇄</em>를 하면 페이지별로 모아서 인쇄하는 대신 올바른 페이지 순서대로 인쇄합니다.</p>

  <steps>
    <title>모아 찍으려면:</title>
    <item>
     <p><keyseq><key>Ctrl</key><key>P</key></keyseq> 키를 눌러 인쇄 대화 상자를 엽니다.</p>
    </item>
    <item>
      <p><gui>일반</gui> 탭의 <gui>사본</gui>에서 <gui>모아찍기</gui>에 표시합니다.</p>
    </item>
  </steps>

</section>

</page>
