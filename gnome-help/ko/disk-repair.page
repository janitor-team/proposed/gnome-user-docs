<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-repair" xml:lang="ko">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>그놈 문서 프로젝트</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <revision pkgversion="3.25.90" date="2017-08-17" status="review"/>

    <desc>파일 시스템이 손상되었는지 사용가능한 상태로 복원할 수 있는지 확인합니다.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

<title>손상 파일 시스템 복원하기</title>

  <p>예기치 못한 전원 손실, 시스템 오류, 안전하지 못한 드라이브 제거 등으로 인해 파일 시스템이 깨질 수 있습니다. 이런 일이 발생한 후, 앞으로의 데이터 손실을 예방하려면 최소한의 파일 시스템 <em>검사</em> 또는 <em>복구</em>를 권합니다.</p>
  <p>때호는 파일 시스템을 마운트하거나 수정하려할 때 복구가 필요할 때가 있습니다. <em>검사</em> 과정에서 파일 시스템의 손상 여부를 보고하지 않더라도 내부적으로 'dirty' 표시를 하며 복구가 필요합니다.</p>

<steps>
  <title>파일 시스템 손상 여부 검사하기</title>
  <item>
    <p><gui>현재 활동</gui> 개요에서 <app>디스크</app>를 엽니다.</p>
  </item>
  <item>
    <p>좌측 저장소 장치 목록에서 파일 시스템이 들어간 디스크를 선택합니다. 디스크에 볼륨이 하나 이상 있다면, 파일 시스템이 들어있는 볼륨을 선택합니다.</p>
  </item>
  <item>
    <p><gui>볼륨</gui> 섹션의 도구 모음에서 메뉴 단추를 누릅니다. 그 다음 <gui>파일 시스템 검사…</gui>를 누릅니다.</p>
  </item>
  <item>
    <p>파일 시스템에 데이터가 얼마나 많이 들어있느냐에 따라 검사 시간이 오래 걸릴 수 있습니다. 대화 상자가 뜨면 과정 시작을 확인합니다.</p>
   <p>파일 시스템을 수정하는 동작은 일어나지 않겠지만, 필요한 경우 마운트 해제합니다. 파일 시스템을 검사하는 동안 인내심을 가지고 기다립니다.</p>
  </item>
  <item>
    <p>검사가 끝나면 파일 시스템이 깨졌는지 여부를 보고받습니다. 참고로 파일 시스템이 깨지지 않았을 경우에도 내부적으로 'dirty' 표시를 초기화 하는 복구 과정이 필요합니다.</p>
  </item>
</steps>

<note style="warning">
 <title>복구 과정에서 가능한 데이터 손실</title>
  <p>파일 시스템 구조가 깨졌다면 저장한 파일에도 영향을 줍니다. 이 경우 파일을 제대로 된 구조로 돌려내지 못하며, 삭제하거나 특수 디렉터리로 이동합니다. 보통 파일 시스템 최상단 디렉터리의 <em>lost+found</em> 폴더이며, 복원한 파일 일부를 이 곳에서 찾아볼 수 있습니다.</p>
  <p>처리 기간동안 데이터를 잃기에 상당히 아깝다면 복원하기 전 볼륨의 이미지를 저장하여 백업하시는게 좋습니다.</p>
  <p>이 이미지는 복구하는 동안 복원하지 못한 데이터 부분이나 빠진 파일 뿐만 아니라 이전에 제거한 파일을 복원할 때 <app>sleuthkit</app>와 같은 포렌식 분석 도구로 처리할 수 있습니다.</p>
</note>

<steps>
  <title>파일 시스템 복구하기</title>
  <item>
    <p><gui>현재 활동</gui> 개요에서 <app>디스크</app>를 엽니다.</p>
  </item>
  <item>
    <p>좌측 저장소 장치 목록에서 파일 시스템이 들어간 디스크를 선택합니다. 디스크에 볼륨이 하나 이상 있다면, 파일 시스템이 들어있는 볼륨을 선택합니다.</p>
  </item>
  <item>
    <p><gui>볼륨</gui> 섹션의 도구 모음에서 메뉴 단추를 누릅니다. 그 다음 <gui>파일 시스템 복구…</gui>를 누릅니다.</p>
  </item>
  <item>
    <p>파일 시스템에 데이터가 얼마나 많이 들어있느냐에 따라 복구 시간이 오래 걸릴 수 있습니다. 대화 상자가 뜨면 과정 시작을 확인합니다.</p>
   <p>동작을 진행하는 동안 필요한 경우 마운트를 해제합니다. 목원 동작은 파일 시스템을 일관된 상태로 두고 깨진 파일을 특수 폴더에 옮깁니다. 파일 시스템을 복원하는 동안 인내심을 가지고 기다립니다.</p>
  </item>
  <item>
    <p>복원이 끝나면 파일 시스템을 제대로 복원할 수 있는지 여부를 보고받습니다. 성공할 경우 평상시 처럼 다시 활용할 수 있습니다.</p>
    <p>파일 시스템을 복원하지 못할 경우, 중요한 파일을 가져올 수 있기 전 볼륨의 이미지를 저장하여 백업합니다. <app>sleuthkit</app>과 같은 포렌식 분석 도구로 읽기 전용 이미지를 마운트하여 진행할 수 있습니다.</p>
    <p>볼륨을 다시 활용할 수 있게 하려면 <link xref="disk-format">포맷</link>한 새 파일 시스템이어야 합니다. 모든 데이터는 사라집니다.</p>
  </item>
</steps>

</page>
