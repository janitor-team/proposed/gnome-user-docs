<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-stylus" xml:lang="ko">

  <info>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>와콥 스타일러스의 단추 기능과 압력 감도를 정의합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>스타일러스 설정</title>

<steps>
  <item>
    <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>설정</gui> 입력을 시작합니다.</p>
  </item>
  <item>
    <p><gui>설정</gui>을 누릅니다.</p>
  </item>
  <item>
    <p>가장자리 창에서 <gui>와콤 디지타이저</gui>를 눌러 창을 엽니다.</p>
  </item>
  <item>
    <p>헤더 모음에서 <gui>스타일러스</gui> 단추를 누릅니다.</p>
    <note style="tip"><p>스타일러스를 찾지 못하면 <gui>스타일러스를 디지타이저에 가깝게 움직여서 설정하십시오</gui> 라는 메시지를 볼 수 있습니다.</p></note>
  </item>
  <item><p>창에는 장치 이름(스타일러스 계열) 및 좌측의 다이어그램과 함께 스타일러스별 세부 항목과 설정 항목이 들어있습니다. 다음 설정을 조절할 수 있습니다:</p>
    <list>
      <item><p><gui>지우개 압력 감도:</gui> 슬라이더를 활용하여 <gui>부드럽게</gui>와 <gui>단단하게</gui> 사이에서 “감도” (물리 압력을 디지털 값으로 변환하는 방식)을 조절합니다.</p></item>
      <item><p><gui>단추/스크롤 휠</gui> 설정 (바꾸면 스타일러스에 반영). 각 레이블 옆의 메뉴를 눌러 동작 안 함, 왼쪽 마우스 단추 누르기, 가운데 마우스 단추 누르기, 오른쪽 마우스 단추 누르기, 스크롤 위로, 스크롤 아래로, 스크롤 왼쪽, 스크롤 오른쪽, 뒤로, 앞으로 중 하나를 선택합니다.</p></item>
      <item><p><gui>팁 압력 감도:</gui> 슬라이더를 활용하여 <gui>부드럽게</gui>와 <gui>단단하게</gui> 사이에서 “감도”를 조절합니다.</p></item>
    </list>
  </item>
</steps>

<note style="info">
  <p>스타일러스가 하나 이상일 경우 스타일러스 장치 이름 옆 페이저를 활용하여 어떤 스타일러스를 설정할 지 선택합니다.</p>
</note>

</page>
