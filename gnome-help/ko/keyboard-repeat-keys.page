<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="keyboard-repeat-keys" xml:lang="ko">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-10-11" status="candidate"/>
    <revision pkgversion="3.13.92" date="2013-10-11" status="candidate"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
       <name>Natalia Ruz Leiva</name>
       <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="author">
       <name>Julita Inca</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>키를 누르고 있을 때 글자 입력을 반복하지 않게 하거나 반복 키 반복 시간 간격 또는 속도를 바꿉니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>반복 키 누름 관리</title>

  <p>기본적으로 키보드의 키를 누르고 있으면, 키를 뗄 때까지 문자 또는 기호 출력을 반복합니다. 손가락을 빨리 떼기 어렵다면 이 기능을 끄거나 키 누름으로 반복 입력을 시작하기 전 걸리는 시간을 바꾸거나, 키 누름 반복 속도를 바꿀 수 있습니다.</p>

  <steps>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>설정</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>설정</gui>을 누릅니다.</p>
    </item>
    <item>
      <p>가장자리 창에서 <gui>접근성</gui>을 눌러 창을 엽니다.</p>
    </item>
    <item>
     <p><gui>자판 입력</gui> 섹션에서 <gui>반복 키</gui>를 누릅니다.</p>
    </item>
    <item>
      <p><gui>반복 키</gui> 스위치를 끕니다.</p>
      <p>대신 <gui>지연 시간</gui> 슬라이더를 조절하여 반복을 시작하기 전 키 누름 유지 시간을 조종하며, <gui>속도</gui> 슬라이더를 조절하여 키 누름 반복 속도를 조종할 수 있습니다.</p>
    </item>
  </steps>

</page>
