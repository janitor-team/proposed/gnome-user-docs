<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-open" xml:lang="ko">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>파일 형식에 대해 기본 프로그램이 아닌 다른 프로그램으로 파일을 엽니다. 기본 프로그램을 바꿀 수도 있습니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

<title>다른 프로그램으로 파일 열기</title>

  <p>파일 관리자에서 파일을 두번 누르면(또는 가운데 단추를 누르면), 파일 형식에 맞는 기본 프로그램으로 파일을 엽니다. 다른 프로그램에서 열거나, 프로그램에서 온라인 검색을 하거나, 동일한 파일 형식을 지닌 모든 파일에 대한 기본 프로그램을 설정합니다.</p>

  <p>기본 프로그램이 아닌 다른 프로그램으로 파일을 열려면, 파일에서 오른쪽 단추를 누른 다음 메뉴 상단에서 원하는 프로그램을 선택합니다. 원하는 프로그램이 보이지 않을 경우 <gui>다른 프로그램으로 열기</gui>를 선택합니다. 기본적으로 파일 관리자에서는 파일을 처리하는 프로그램으로 알고 있는 프로그램만 나타냅니다. 컴퓨터의 모든 프로그램을 살펴보려면 <gui>모든 프로그램 보기</gui>를 누릅니다.</p>

<p>원하는 프로그램을 여전히 찾지 못했다면 <gui>새 프로그램 찾기</gui>를 눌러 더 많은 프로그램을 검색할 수 있습니다. 파일 관리자에서는 해당 형식의 파일을 처리할 수 있다고 알려진 프로그램 패키지를 온라인에서 검색합니다.</p>

<section id="default">
  <title>기본 프로그램 바꾸기</title>
  <p>주어진 파일 형식을 열 기본 프로그램을 바꿀 수 있습니다. 파일을 두 번 눌렀을 때 원하는 프로그램으로 열 수 있습니다. 예를 들면 MP3 파일을 두 번 눌렀을 때 원하는 음악 재생 프로그램으로 열고 싶을 때가 있습니다.</p>

  <steps>
    <item><p>바꾸고자 하는 기본 프로그램의 처리 형식을 지닌 파일을 선택합니다. 예를 들어 MP3 파일을 열 프로그램을 바꾸려면 <file>.mp3</file> 파일을 선택합니다.</p></item>
    <item><p>파일에 오른쪽 단추를 누르고 <gui>속성</gui>을 선택합니다.</p></item>
    <item><p><gui>열 프로그램</gui> 탭을 선택합니다.</p></item>
    <item><p>원하는 프로그램을 선택한 후 <gui>기본값으로 설정</gui>을 누릅니다.</p>
    <p><gui>다른 프로그램</gui>에 가끔 쓰고 싶지만 기본 프로그램으로 설정하고 싶지 않으면 해당 프로그램을 선택하고 <gui>추가</gui>를 누릅니다. 이렇게 하면 <gui>추천 프로그램</gui>에 들어갑니다. 이렇게 하면 파일에 포인터 커서를 놓고 마우스 오른쪽 단추를 누른 후 목록에서 이 프로그램을 선택하여 사용할 수 있습니다.</p></item>
  </steps>

  <p>이 방식은 선택한 파일만 대상이 아닌 동일한 형식의 모든 파일을 대상으로 기본 프로그램을 바꿉니다.</p>

<!-- TODO: mention resetting the open with list with the "Reset" button -->

</section>

</page>
