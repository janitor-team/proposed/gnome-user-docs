<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-autosuspend" xml:lang="ko">

  <info>
     <link type="guide" xref="power#saving"/>
     <link type="seealso" xref="power-suspend"/>
     <link type="seealso" xref="shell-exit#suspend"/>

    <revision version="gnome:3.38.3" date="2021-03-07" status="candidate"/>
    <revision pkgversion="gnome:41" date="2021-09-08" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>컴퓨터를 자동으로 대기 모드에 들어가도록 설정합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>자동 대기 모드 설정</title>

  <p>컴퓨터가 절전 상태일 때 자동으로 절전 상태로 들어가도록 설정할 수 있습니다. 배터리 또는 교류 전원에 연결할 경우 각각에 대해 다른 시간을 지정할 수 있습니다.</p>

  <steps>

    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>전원</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>전원</gui>을 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p><gui>절전 옵션</gui> 섹션에서 <gui>자동 대기 모드</gui>를 누릅니다.</p>
    </item>
    <item>
      <p><gui>배터리 전원</gui> 또는 <gui>전원 연결</gui>을 선택하고 스위치를 켠 다음 <gui>지연 시간</gui>을 선택합니다. 각 옵션을 설정할 수 있습니다.</p>

      <note style="tip">
        <p>데스크톱 컴퓨터에서는 <gui>입력이 없을 때</gui> 옵션이 있습니다.</p>
      </note>
    </item>

  </steps>

</page>
