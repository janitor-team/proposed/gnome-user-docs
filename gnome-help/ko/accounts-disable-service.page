<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-disable-service" xml:lang="ko">

  <info>
    <link type="guide" xref="accounts"/>

    <revision pkgversion="3.5.5" date="2012-08-14" status="review"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
<credit type="editor">
      <name>Klein Kravis</name>
      <email>kleinkravis44@outlook.com</email>
      <years>2020</years>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>일부 온라인 계정은 (달력 또는 전자메일 같은) 여러 서비스에 접근하는 용도로 활용할 수 있습니다. 프로그램에서 활용할 수 있는 서비스를 제어할 수 있습니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>계정에서 접근할 수 있는 온라인 서비스 제어하기</title>

  <p>일부 온라인 계정에서는 동일한 사용자 계정에서 여러 서비스에 접근할 수 있게 해줍니다. 예를 들면, 구글 계정은 달력, 전자메일, 연락처에 접근할 수 있게 해줍니다. 다른 서비스도 아니고 일부 서비스 사용 용도로 계정을 활용하고 싶을 때도 있습니다. 이를테면, 달력을 사용하는 다른 온라인 계정이 있다면 전자메일 용도로 구글 계정을 활용하지 달력 용도로 활용하려 하지는 않습니다.</p>

  <p>각 온라인 계정에서 제공하는 일부 서비스를 끌 수 있습니다:</p>

  <steps>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>온라인 계정</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>온라인 계정</gui>을 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p>우측 목록에서 바꾸고자 하는 계정을 선택합니다.</p>
    </item>
    <item>
      <p><gui>사용 목적</gui>에 이 계정에서 사용할 수 있는 서비스 목록이 뜹니다. 어떤 서비스에 어떤 프로그램이 접근할 수 있는지 알아보려면 <link xref="accounts-which-application"/>을 살펴보십시오.</p>
    </item>
    <item>
      <p>원하지 않는 서비스의 스위치를 끕니다.</p>
    </item>
  </steps>

  <p>계정 서비스를 끄고 나면, 컴퓨터의 프로그램에서 계정을 활용하여 더이상 서비스에 연결할 수 없습니다.</p>

  <p>서비스를 끄려면 <gui>온라인 계정</gui>창으로 돌아가서 스위치를 켭니다.</p>

</page>
