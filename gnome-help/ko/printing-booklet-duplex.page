<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-booklet-duplex" xml:lang="ko">

  <info>
    <link type="guide" xref="printing-booklet"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>일반 A4/레터 규격 용지로 PDF의 접이식 소책자(책 또는 팸플릿류)를 인쇄합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>양면 프린터에서 소책자 인쇄</title>

  <p>특별한 순서로 문서의 페이지를 인쇄하고 몇가지 인쇄 옵션 설정을 바꾸어 접는 소책자(작은 책 또는 팸플릿)를 만들 수 있습니다.</p>

  <p>이 순서는 PDF 문서로 소책자를 인쇄하는 방법입니다.</p>

  <p><app>리버오피스</app>문서의 소책자를 인쇄할 경우 우선 <guiseq><gui>파일</gui><gui>PDF로 내보내기…</gui></guiseq>를 선택하여 PDF로 내보냅니다. 문서는 4의 배수 페이지(4, 8, 12, 16,…)여야 합니다. 최대 빈 페이지 3쪽을 넣을 수 있습니다.</p>

  <p>소책자를 인쇄하려면:</p>

  <steps>
    <item>
      <p>인쇄 대화상자를 엽니다. 보통 메뉴에서 <gui style="menuitem">인쇄</gui>를 선택하거나 <keyseq><key>Ctrl</key><key>P</key></keyseq> 키보드 바로 가기 키를 누르면 됩니다.</p>
    </item>
    <item>
      <p><gui>속성…</gui> 단추를 누릅니다</p>
      <p><gui>방향</gui> 드롭다운 목록에서 <gui>가로</gui>를 선택했는지 확인합니다.</p>
      <p><gui>양면</gui> 드롭다운 목록에서, <gui>짧은 모서리 방향</gui>을 선택합니다.</p>
      <p><gui>확인</gui>을 눌러 대화 상자로 돌아갑니다.</p>
    </item>
    <item>
      <p><gui>범위 및 사본</gui>에서 <gui>페이지</gui>를 선택합니다.</p>
    </item>
    <item>
      <p>페이지 수를 다음 순서대로 입력합니다(n은 총 페이지 수, 그리고 4배수입니다):</p>
      <p>n, 1, 2, n-1, n-2, 3, 4, n-3, n-4, 5, 6, n-5, n-6, 7, 8, n-7, n-8, 9, 10, n-9, n-10, 11, 12, n-11…</p>
      <p>예제:</p>
      <list>
        <item><p>4 페이지 소책자: <input>4,1,2,3</input> 입력</p></item>
        <item><p>8 페이지 소책자: <input>8,1,2,7,6,3,4,5</input> 입력</p></item>
        <item><p>20 페이지 소책자: <input>20,1,2,19,18,3,4,17,16,5,6,15,14,7,8,13,12,9,10,11</input> 입력</p></item>
      </list>
    </item>
    <item>
      <p><gui>페이지 레이아웃</gui> 탭을 선택합니다.</p>
      <p><gui>레이아웃</gui>에서 <gui>브로셔</gui>를 선택합니다.</p>
      <p><gui>페이지 면</gui>의 <gui>포함</gui> 드롭다운 목록에서 <gui>모든 페이지</gui>를 선택합니다.</p>
    </item>
    <item>
      <p><gui>인쇄</gui>를 누릅니다.</p>
    </item>
  </steps>

</page>
