<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-bouncekeys" xml:lang="ko">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>동일 키를 빠르게 반복하는 키 누름을 무시합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>탄력키 켜기</title>

  <p>빨리 반복하는 키 누름을 무시하려면 <em>탄력 키</em>를 켭니다.</p>

  <steps>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>설정</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>설정</gui>을 누릅니다.</p>
    </item>
    <item>
      <p>가장자리 창에서 <gui>접근성</gui>을 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p><gui>자판 입력</gui> 섹션에서 <gui>타이핑 도움 (AccessX)</gui>을 누릅니다.</p>
    </item>
    <item>
      <p><gui>탄력 키</gui> 스위치를 켭니다.</p>
    </item>
  </steps>

  <note style="tip">
    <title>탄력 키 간단하게 켜고 끄기</title>
    <p>상단 표시줄의 <link xref="a11y-icon">접근성 아이콘</link>을 누른 후 <gui>탄력 키</gui>를 선택하여 탄력 키를 간단하게 켜고 끌 수 있습니다. 접근성 아이콘은 <gui>접근성</gui> 창에서 하나 이상의 설정을 켰을 경우 나타납니다.</p>
  </note>

  <p>처음 키를 누른 후 다른 키 누름을 확인하기 전 탄력 키 입력 대기 시간을 조절하려면 <gui>허용 지연 시간</gui> 슬라이더를 활용합니다. 앞선 키 누름 후 다음 키 누름을 빨리 진행했을 때 키 누름을 무시할 때마다 컴퓨터에서 소리를 내게 하려면 <gui>키가 거부되었을 때 삑소리</gui>를 선택합니다.</p>

</page>
