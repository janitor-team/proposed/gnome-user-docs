<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-workspaces-switch" xml:lang="ko">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces"/>
    <link type="seealso" xref="shell-workspaces"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.35.91" date="2020-02-27" status="candidate"/>

    <credit type="author">
      <name>그놈 문서 프로젝트</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>작업 공간 선택 상자를 활용합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

 <title>작업 공간 전환</title>

 <steps>  
 <title>마우스 활용:</title>
 <item>
  <p if:test="!platform:gnome-classic"><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 엽니다.</p>
  <p if:test="platform:gnome-classic">화면 우측 하단에서 작업 공간 넷 중 하나를 눌러 작업 공간을 띄웁니다.</p>
 </item>
 <item if:test="!platform:gnome-classic">
  <p>다음 작업 공간에 창을 열어 보려면 현재 작업 공간의 우측에 나타난 작업 공간을 누릅니다. 현재 선택한 작업 공간이 가장 우측에 있는게 아니라면, 좌측에 나타난 작업 공간을 눌러 이전 작업 공간을 나타냅니다.</p>
  <p>하나 이상의 작업 공간을 활용하는 경우, 검색 창과 창 목록 사이의 <link xref="shell-workspaces">작업 공간 선택</link> 상자를 눌러 다른 작업 공간에 직접 들어갈 수 있습니다.</p>
 </item>
 <item if:test="!platform:gnome-classic">
  <p>활성 상태로 띄울 작업 공간을 누릅니다.</p>
 </item>
 </steps>
 
 <list>
 <title>키보드 활용:</title>  
  <item>
    <p if:test="!platform:gnome-classic"><keyseq><key xref="keyboard-key-super">Super</key><key>Page Up</key></keyseq> 키 또는 <keyseq><key>Ctrl</key><key>Alt</key><key>Up</key></keyseq> 키를 눌러 좌측 작업 공간 선택 상자의 작업 공간을 움직입니다.</p>
    <p if:test="platform:gnome-classic"><keyseq><key>Ctrl</key> <key>Alt</key><key>←</key></keyseq> 키를 눌러 <em>작업 공간 선택 상자</em>에서 나타나는 현재 작업 공간을 왼쪽 방향으로 움직입니다.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic"><keyseq><key>Super</key><key>Page Down</key></keyseq> 키 또는 <keyseq><key>Ctrl</key><key>Alt</key><key>Down</key></keyseq> 키를 눌러 우측 작업 공간 선택 상자의 작업 공간을 움직입니다.</p>
    <p if:test="platform:gnome-classic"><keyseq><key>Ctrl</key> <key>Alt</key><key>→</key></keyseq> 키를 눌러 <em>작업 공간 선택 상자</em>에서 나타나는 현재 작업 공간을 오른쪽 방향으로 움직입니다.</p>
  </item>
 </list>

</page>
