<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="touchscreen-gestures" xml:lang="ko">

  <info>
    <link type="guide" xref="mouse" group="#last"/>

    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <!--
    For 41: https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/121
    -->
    <revision version="gnome:40" date="2021-03-18" status="candidate"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>터치패드 또는 터치스크린의 손가락 움직임으로 데스크톱을 다룹니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>터치패드와 터치스크린에서 손가락 움직임 사용</title>

  <p>시스템 탐색, 프로그램 탐색시 멀티터치 손가락 움직임을 터치패드 또는 터치스크린에서 활용할 수 있습니다.</p>

  <p>손가락 움직임을 사용할 수 있는 프로그램은 여러가지가 있습니다. <app>문서보기</app>에서는 손가락 움직임으로 문서를 확대하거나 넘길 수 있으며, <app>그림 보기</app>에서는 확대, 회전, 상하좌우 방향 이동이 가능합니다.</p>

<section id="system">
  <title>시스템 영역 손가락 움직임</title>

<table rules="rows" frame="bottom">
  <tr>
    <td><p><media type="image" src="figures/touch-overview.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>현재 활동 개요와 프로그램 목록 열기</em></p>
    <p>터치패드 또는 터치스크린에 세 손가락을 두고 위로 닦아내어 현재 활동 개요를 엽니다.</p>
    <p>프로그램 목록을 열려면, 세 손가락을 두고 한 번 더 위로 닦아냅니다.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-workspaces.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>작업 공간 전환</em></p>
    <p>터치패드 또는 터치 스크린에 세 손가락을 두고 왼쪽 또는 오른쪽으로 닦아냅니다.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-unfullscreen.svg" width="128" its:translate="no"/></p></td>
    <td><p><gui>최대 화면 나가기</gui></p>
    <p>터치스크린에서 가장자리 상단을 끌어내려 창의 전체 화면 모드에서 빠져나갑니다.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-osk.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>화면 키보드 띄우기</em></p>
    <p>화면 키보드 기능을 켰을 때 터치스크린에서 가장자리 하단을 끌어올려 <link xhref="keyboard-osk">화면 키보드</link>를 띄웁니다.</p></td>
  </tr>
</table>

</section>

<section id="apps">
  <title>프로그램 손가락 움직임</title>

<table rules="rows" frame="bottom">
  <tr>
    <td><p><media type="image" src="figures/touch-tap.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>항목 열기, 프로그램 실행, 노래 재생</em></p>
    <p>항목을 두드립니다.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-hold.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>항목을 선택하여 실행할 수 있는 동작 보기</em></p>
    <p>누른 채로 몇초간 기다립니다.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-scroll.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>화면 영역 스크롤</em></p>
    <p>끌기: 표면에 손가락을 댄 채로 미끄러지듯 움직입니다.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-pinch-to-zoom.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>확대 보기 수준 바꾸기 (<app>지도</app>, <app>사진</app>)</em></p>
    <p>두 손가락 오므리기 또는 간격 늘이기: 두 손가락을 표면에 댄 채로 가까이 모으거나 멀리 벌어지게 합니다.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-rotate.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>사진 회전</em></p>
    <p>두 손가락 회전: 표면에 두 손가락을 댄 후 회전합니다.</p></td>
  </tr>
</table>

</section>

</page>
