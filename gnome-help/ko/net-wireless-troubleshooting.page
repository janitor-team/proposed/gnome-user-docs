<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting" xml:lang="ko">

  <info>
    <link type="guide" xref="net-wireless" group="first"/>
    <link type="guide" xref="hardware#problems" group="first"/>
    <link type="next" xref="net-wireless-troubleshooting-initial-check"/>

    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>우분투 문서 위키 기여자</name>
    </credit>
    <credit type="author">
      <name>그놈 문서 프로젝트</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>무선 연결 문제를 식별하고 해결합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>무선 네트워크 문제 해결사</title>

  <p>이 부분에서는 무선 연결 문제를 인지하고 수정하도록 돕는 단계별 문제 해결 안내서입니다. 어떤 문제로 인해 무선 네트워크에 연결하지 못한다면, 다음 절차대로 진행해봅니다.</p>

  <p>다음 단계를 따라 컴퓨터를 인터넷에 연결할 수 있는 과정을 진행하도록 하겠습니다:</p>

  <list style="numbered compact">
    <item>
      <p>초기 확인 진행</p>
    </item>
    <item>
      <p>하드웨어 정보를 수집합니다</p>
    </item>
    <item>
      <p>하드웨어 확인</p>
    </item>
    <item>
      <p>무선 라우터에 연결 설정을 만들어봅니다</p>
    </item>
    <item>
      <p>모뎀과 라우터 검사를 진행합니다</p>
    </item>
  </list>

  <p>시작했다면 페이지의 우측 상단에 있는 <em>다음</em> 링크를 누릅니다. 이 안내서의 각 단계를 끝내면 다음 페이지에서도 마찬가지로 이 링크를 누르면 그 다음 단계로 진행하게 해줍니다.</p>

  <note>
    <title>명령행 사용</title>
    <p>이 안내서 일부 과정에선느 <em>명령행</em>(터미널)에 명령을 입력하라고 요청하기도 합니다. <gui>현재 활동</gui> 개요에서 <app>터미널</app> 프로그램을 찾을 수 있습니다.</p>
    <p>명령행 사용에 익숙하지 않다 하더라도 걱정하지 않으셔도 됩니다. 이 안내서에서 각 단계를 안내해드립니다. 기억해야 할 모든건 명령에서 대 소문자를 구분하며(따라서 나타난대로 <em>정확하게</em> 입력해야합니다), 각 명령 입력이 끝나면 <key>Enter</key>키를 눌러야 실행한다는 사실 뿐입니다.</p>
  </note>

</page>
