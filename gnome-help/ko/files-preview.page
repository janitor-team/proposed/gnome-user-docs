<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="files-preview" xml:lang="ko">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="nautilus-preview"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-10-06" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>문서, 그림, 영상 등의 미리보기를 간단하게 보이거나 숨깁니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

<title>파일 및 폴더 미리 보기</title>

<note if:test="platform:ubuntu" style="important">
  <p>이 단계를 수행하려면 <app>스시</app>를 컴퓨터에 설치해야 합니다.</p>
  <p its:locNote="Translators: This button is only shown on Ubuntu, where this   link format is preferred over 'install:gnome-sushi'."><link style="button" href="apt:gnome-sushi"><app>스시</app> 설치</link></p>
</note>

<p>완전한 기능을 갖춘 프로그램에서 파일을 열지 않고도 파일을 간단하게 미리 볼 수 있습니다. 임의의 파일을 선택하고 스페이스 바를 누릅니다. 파일을 간단한 미리 보기 창에 엽니다. 스페이스 바를 다시 누르면 미리 보기 창을 닫습니다.</p>

<p>내장 미리 보기 기능은 대부분의 문서, 사진, 비디오, 오디오 파일 형시을 지원합니다. 미리 보기에서 문서를 스크롤 해서 보거나, 비디오 또는 오디오를 탐색해볼 수 있습니다.</p>

<p>미리 보기 전체 화면을 보려면 <key>F</key>키 또는 <key>F11</key>키를 누릅니다. 전체 화면에서 나가려면 <key>F</key> 키 또는 <key>F11</key>키를 더 누르거나 스페이스 바를 눌러 미리 보기 화면을 완전히 빠져나갑니다.</p>

</page>
