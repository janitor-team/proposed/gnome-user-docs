<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="keyboard-cursor-blink" xml:lang="tr">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>
    <link type="guide" xref="keyboard"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-10-11" status="candidate"/>
    <revision pkgversion="3.13.92" date="2013-09-22" status="candidate"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
       <name>Natalia Ruz Leiva</name>
       <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="author">
       <name>Julita Inca</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Make the insertion point blink and control how quickly it
    blinks.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sabri Ünal</mal:name>
      <mal:email>libreajans@gmail.com</mal:email>
      <mal:years>2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Emin Tufan Çetin</mal:name>
      <mal:email>etcetin@gmail.com</mal:email>
      <mal:years>2021.</mal:years>
    </mal:credit>
  </info>

  <title>Make the keyboard cursor blink</title>

  <p>If you find it difficult to see the keyboard cursor in a text field, you
  can make it blink to make it easier to locate.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Accessibility</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Press <gui>Cursor Blinking</gui> in the <gui>Typing</gui> section.</p>
    </item>
    <item>
      <p>Use the <gui>Speed</gui> slider to adjust how quickly the cursor
      blinks.</p>
    </item>
  </steps>

</page>
