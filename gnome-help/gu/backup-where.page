<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-where" xml:lang="gu">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>GNOME દસ્તાવેજીકરણ પ્રોજેક્ટ</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>ટિફની એન્ટોપોલસ્કી</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>તમારો બેકઅપ ક્યાં સંગ્રહવો છે તેની પર સલાહ અને વાપરવા માટે ક્યો સંગ્રહ ઉપકરણનો પ્રકાર છે.</desc>
  </info>

<title>તમારા બેકઅપને ક્યાં સંગ્રહવાનો છે</title>

  <p>You should store backup copies of your files somewhere separate from your
 computer — on an external hard disk, for example. That way, if the computer
 breaks, or is lost or is stolen, the backup will still be intact. For maximum
 security, you shouldn’t keep the backup in the same building as your computer.
 If there is a fire or theft, both copies of the data could be lost if they are
 kept together.</p>

  <p>અનૂકુળ <em>બેકઅપ માધ્યમ</em> ને પસંદ કરવાનું મહત્વનું છે. તમારે ઉપકરણ પર તમારાં બેકઅપને સંગ્રહવાની જરૂર છે કે જેની પાસે બધી બેકઅપ ફાઇલો માટે પૂરતી ડિસ્ક ક્ષમતા છે.</p>

   <list style="compact">
    <title>સ્થાનિક અને દૂરસ્થ સંગ્રહ વિકલ્પો</title>
    <item>
      <p>USB મેમરી કી (ઓછી ક્ષમતા)</p>
    </item>
    <item>
      <p>આંતરિક ડિસ્ક ડ્રાઇવ (ઉચ્ચ ક્ષમતા)</p>
    </item>
    <item>
      <p>બહારની હાર્ડ ડિસ્ક (ઉચ્ચ ક્ષમતા)</p>
    </item>
    <item>
      <p>નેટવર્ક જોડાયેલ ડ્રાઇવ (ઉચ્ચ ક્ષમતા)</p>
    </item>
    <item>
      <p>ફાઇલ/બેકઅપ સર્વર (ઉચ્ચ ક્ષમતા)</p>
    </item>
    <item>
     <p>લખી શકાય તેવી CDs અથવા DVDs (ઓછી/મધ્યમ ક્ષમતા)</p>
    </item>
    <item>
     <p>Online backup service
     (<link href="http://aws.amazon.com/s3/">Amazon S3</link>, for example;
     capacity depends on price)</p>
    </item>
   </list>

  <p>અમુક આ વિકલ્પો પાસે તમારી સિસ્ટમ પર દરેક ફાઇલનાં બેકઅપને પરવાનગી આપવા પૂરતી ક્ષમતા છે, <em>સિસ્ટમ બેકઅપ સમાપ્ત કરો</em> તરીકે જાણીતુ છે.</p>
</page>
