<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-changepicture" xml:lang="gu">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.14.0" date="2014-10-08" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.32.0" date="2019-03-16" status="final"/>

    <credit type="author">
      <name>GNOME દસ્તાવેજીકરણ પ્રોજેક્ટ</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>ઍકાટેરીના ગેરાસીમોવા</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>પ્રવેશ અને વપરાશકર્તા સ્ક્રીનમાં તમારા ફોટાને ઉમેરો.</desc>
  </info>

  <title>તમારા સ્ક્રીન ફોટોને બદલો</title>

  <p>જ્યારે તમે પ્રવેશો અથવા વપરાશકર્તાઓને બદલો, તમે તેનાં પ્રવેશ ફોટો સાથે વપરાશકર્તાઓની યદીને જોશો. તમે તમારી પોતાની ઇમેજ અથવા સ્ટોક ઇમેજમાં તમારા ફોટાને બદલી શકો છો. તમે તમારા વેબકેમ સાથે નવાં પ્રવેશ ફોટાને લઇ શકો છો.</p>

  <p>You need <link xref="user-admin-explain">administrator privileges</link>
  to edit user accounts other than your own.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Users</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Users</gui> to open the panel.</p>
    </item>
    <item>
      <p>If you want to edit a user other than yourself, press
      <gui style="button">Unlock</gui> in the top right corner and type in your
      password when prompted.</p>
    </item>
    <item>
      <p>તમારાં નામ આગળ પિક્ચર પર ક્લિક કરો. ડ્રોપ-ડાઉન ગેલરિ એ અમુક સ્ટોક પ્રવેશ ફોટા સા થે બતાવેલ હશે. જો તમને તેઓમાંનુ એક ગમે તો, તમારી જાત માટે તેને વાપરવા તેની પર ક્લિક કરો.</p>
      <list>
        <item>
          <p>If you would rather use a picture you already have on your
          computer, click <gui>Select a file…</gui>.</p>
        </item>
        <item>
          <p>If you have a webcam, you can take a new login photo right now by
          clicking <gui>Take a picture…</gui>. Take your
          picture, then move and resize the square outline to crop out the
          parts you do not want. If you do not like the picture you took, click
          <gui style="button">Take Another Picture</gui> to try again, or
          <gui>Cancel</gui> to give up.</p>
        </item>
      </list>
    </item>
  </steps>

</page>
