<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-screen-lock" xml:lang="gu">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="seealso" xref="session-screenlocks"/>
    <link type="seealso" xref="shell-exit#lock-screen"/>

    <revision pkgversion="3.8" date="2013-05-21" status="candidate"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>
    <revision pkgversion="3.38.1" date="2020-11-22" status="final"/>
    <revision pkgversion="3.38.4" date="2020-03-07" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>માઇકલ હીલ</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit>
      <name>જીમ કેમ્પબેલ</name>
      <email>jwcampbell@gmail.com</email>
      <years>૨૦૧૩</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>તમારાં ડેસ્કટોપને વાપરવાથી બીજા લોકોને અટકાવો જ્યારે તમે તમારાં કમ્પ્યૂટરથી દૂર જાવ.</desc>
  </info>

  <title>આપમેળે તમારી સ્ક્રીનને તાળુ મારો</title>
  
  <p>When you leave your computer, you should
  <link xref="shell-exit#lock-screen">lock the screen</link> to prevent
  other people from using your desktop and accessing your files. If you
  sometimes forget to lock your screen, you may wish to have your computer’s
  screen lock automatically after a set period of time. This will help to
  secure your computer when you aren’t using it.</p>

  <note><p>જ્યારે તમારી સ્ક્રીનને તાળુ મારેલ હોય તો, તમારાં કાર્યક્રમો અને સિસ્ટમ પ્રક્રિયાઓ ચલાવવા ચાલુ કરશે, પરંતુ તમે ફરી તેઓને વાપરવાનું શરૂ કરવા માટે તમારા પાસવર્ડને દાખલ કરવાની જરૂર પડશે.</p></note>
  
  <steps>
    <title>આપમેળે તમારી સ્ક્રીનનું તાળુ મારતા પહેલાં લાંબા સમયગાળાને સુયોજિત કરવા માટે:</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Screen Lock</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Screen Lock</gui> to open the panel.</p>
    </item>
    <item>
      <p>Make sure <gui>Automatic Screen Lock</gui> is switched on, then select a
      length of time from the <gui>Automatic Screen Lock Delay</gui> drop-down list.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Applications can present notifications to you that are still displayed
    on your lock screen. This is convenient, for example, to see if you have
    any email without unlocking your screen. If you’re concerned about other
    people seeing these notifications, switch <gui>Show Notifications on Lock Screen</gui>
    off. For further notification settings, refer to <link xref="shell-notifications"/>.</p>
  </note>

  <p>When your screen is locked, and you want to unlock it, press
  <key>Esc</key>, or swipe up from the bottom of the screen with your mouse.
  Then enter your password, and press <key>Enter</key> or click
  <gui>Unlock</gui>. Alternatively, just start typing your password and the
  lock curtain will be automatically raised as you type.</p>

</page>
