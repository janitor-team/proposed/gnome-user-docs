<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-select" xml:lang="gu">

  <info>
    <link type="guide" xref="files#faq"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>ઘણી ફાઇલો કે જે તમારી પાસે સરખા નામો હોય તેને પસંદ કરવા માટે <keyseq><key>Ctrl</key><key>S</key></keyseq> દબાવો.</desc>
  </info>

  <title>ભાત દ્દારા ફાઇલો પસંદ કરો</title>

  <p>ફાઇલ નામ પર ભાતની મદદથી ફોલ્ડરમાં તમે ફાઇલોને પસંદ કરી શકો છો. <gui>બંધબેસતી વસ્તુઓને પસંદ કરો</gui> ને લાવવા માટે <keyseq><key>Ctrl</key><key>S</key></keyseq> ને દબાવો. ફાઇલ નામો વધુમાં વાઇલ્ડ કાર્ડ અક્ષરોનાં સામાન્ય ભાગોની મદદથી ભાતમાં લખો. ત્યાં બે વાઇલ્ડ કાર્ડ અક્ષરો ઉપલબ્ધ છે:</p>

  <list style="compact">
    <item><p><file>*</file> એ કોઇપણ અક્ષરોની કોઇપણ સંખ્યા સાથે બંધબેસે છે, કોઇપણ અક્ષરો સાથે નહિં.</p></item>
    <item><p><file>?</file> એ કોઇપણ અક્ષરનાં એક સાથે બરાબર રીતે બંધબેસે છે.</p></item>
  </list>

  <p>ઉદાહરણ તરીકે:</p>

  <list>
    <item><p>જો તમારી પાસે OpenDocument લખાણ ફાઇલ, PDF ફાઇલ, અને ઇમેજ હોય તો કે જે બધા પાસે એજ આધાર નામ <file>Invoice</file> હોય તો, ભાત સાથે બધા ત્રણને પસંદ કરો</p>
    <example><p><file>Invoice.*</file></p></example></item>

    <item><p>જો તમારી પાસે અમુક ફોટો હોય તો કે જે <file>Vacation-001.jpg</file>, <file>Vacation-002.jpg</file>, <file>Vacation-003.jpg</file> જેવા નામ થયેલ છે; તે ભાત સાથે તેઓને પસંદ કરો</p>
    <example><p><file>Vacation-???.jpg</file></p></example></item>

    <item><p>If you have photos as before, but you have edited some of them and
    added <file>-edited</file> to the end of the file name of the photos you
    have edited, select the edited photos with</p>
    <example><p><file>Vacation-???-edited.jpg</file></p></example></item>
  </list>

</page>
