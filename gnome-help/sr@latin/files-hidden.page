<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-hidden" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-29" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>Fil Bul</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Dejvid King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Učinite datoteku nevidljivom, tako da nećete moći da je vidite u upravniku datoteka.</desc>
  </info>

<title>Sakrijte datoteku</title>

  <p>Upravnik datoteka <app>Datoteke</app> vam pruža mogućnost da sakrijete i da prikažete datoteke prema vašoj želji. Kada je datoteka skrivena, nije prikazana u upravniku datoteka, ali se još uvek nalazi u fascikli.</p>

  <p>Da sakrijete datoteku <link xref="files-rename">preimenujte je</link> tačkom <file>.</file> na početku njenog naziva. Na primer, da sakrijete datoteku pod nazivom <file>primer.txt</file>, trebate da je preimenujete u <file>.primer.txt</file>.</p>

<note>
  <p>Možete da sakrijete fascikle na isti način na koji možete da sakrijete datoteke. Sakrijte fasciklu tako što ćete postaviti <file>.</file> na početak njenog naziva.</p>
</note>

<section id="show-hidden">
 <title>Prikažite sve skrivene datoteke</title>

  <p>Ako želite da vidite sve skrivene datoteke u fascikli, idite do te fascikle i kliknite na dugme opcija pregleda na traci alata i izaberite <gui>Prikaži skrivene datoteke</gui>, ili pritisnite <keyseq><key>Ktrl</key><key>H</key></keyseq>. Videćete sve skrivene datoteke, zajedno sa običnim datotekama koje nisu skrivene.</p>

  <p>Da ponovo sakrijete ove datoteke, kliknite na dugme opcija pregleda na traci alata i izaberite <gui>Prikaži skrivene datoteke</gui>, ili ponovo pritisnite <keyseq><key>Ktrl</key><key>H</key> </keyseq>.</p>

</section>

<section id="unhide">
 <title>Otkrijte datoteku</title>

  <p>Da otkrijete datoteku, idite u fasciklu koja sadrži skrivenu datoteku i pritisnite dugme na traci alata i izaberite <gui>Prikaži skrivene datoteke</gui>. Zatim, pronađite skrivenu datoteku i preimenujte je tako da ne sadrži <file>.</file> na početku njenog naziva. Na primer, da otkrijete datoteku pod nazivom <file>.primer.txt</file>, treba da je preimenujete u <file>primer.txt</file>.</p>

  <p>Kada ste preimenovali datoteku, ili kliknite na dugme na traci alata i izaberite <gui>Prikaži skrivene datoteke</gui>, ili ponovo pritisnite <keyseq><key>Ktrl</key><key>H</key></keyseq> da ponovo sakrijete sve ostale skrivene datoteke.</p>

  <note><p>Po osnovi, videćete samo skrivene datoteke u upravniku datoteka sve dok ga ne zatvorite. Da izmenite ovo podešavanje tako da upravnik datoteka uvek prikazuje skrivene datoteke, pogledajte <link xref="nautilus-views"/>.</p></note>

  <note><p>Većina skrivenih datoteka će imati <file>.</file> na početku njihovog naziva, ali neke druge mogu imati <file>~</file> na kraju njihovog naziva umesto ovoga. Te datoteke su datoteke rezerve. Pogledajte <link xref="files-tilde"/> za više podataka.</p></note>

</section>

</page>
