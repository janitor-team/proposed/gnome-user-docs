<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-where" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Gnomov projekat dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tifani Antopoloski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Savet o tome gde da sačuvate vaše rezerve i koju vrstu skladišnog uređaja da koristite.</desc>
  </info>

<title>Gde da sačuvate vašu rezervu</title>

  <p>You should store backup copies of your files somewhere separate from your
 computer — on an external hard disk, for example. That way, if the computer
 breaks, or is lost or is stolen, the backup will still be intact. For maximum
 security, you shouldn’t keep the backup in the same building as your computer.
 If there is a fire or theft, both copies of the data could be lost if they are
 kept together.</p>

  <p>Takođe je vrlo važno i da izaberete odgovarajući <em>medijum rezerve</em>. Treba da skladištite vaše rezerve na uređaju koji ima dovoljno prostora na disku za sve vaše datoteke rezerve.</p>

   <list style="compact">
    <title>Opcije lokalnog i udaljenog skladištenja</title>
    <item>
      <p>Memorijski USB ključ (nizak kapacitet)</p>
    </item>
    <item>
      <p>Unutrašnji uređaj diska (visoki kapacitet)</p>
    </item>
    <item>
      <p>Spoljni čvrsti disk (obično visoki kapacitet)</p>
    </item>
    <item>
      <p>Uređaj povezan na mrežu (visoki kapacitet)</p>
    </item>
    <item>
      <p>Server datoteka/rezerve (visoki kapacitet)</p>
    </item>
    <item>
     <p>Zapisivi CD ili DVD diskovi (nizak/srednji kapacitet)</p>
    </item>
    <item>
     <p>Usluge rezerve na mreži (<link href="http://aws.amazon.com/s3/">Amazon S3</link>, na primer; kapacitet zavisi od cene)</p>
    </item>
   </list>

  <p>Neke od ovih opcija imaju dovoljno kapaciteta da omoguće pravljenje rezerve svih datoteka na vašem sistemu, takođe poznatoj kao <em>rezerva čitavog sistema</em>.</p>
</page>
