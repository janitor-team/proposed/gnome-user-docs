<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-what" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Gnomov projekat dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tifani Antopoloski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Napravite rezervu svega onoga što nemožete dozvoliti da bude izgubljeno ako nešto pođe naopako.</desc>
  </info>

  <title>Šta da pričuvate u rezervi</title>

  <p>Vaš prioritet treba da bude da napravite rezervu vaših <link xref="backup-thinkabout">najvažnijih datoteka</link> kao i svega onoga čega bi ponovno stvaranje bilo teško. Na primer, od najvažnijih do manje važnih stvari:</p>

<terms>
 <item>
  <title>Vaše lične datoteke</title>
   <p>U ovo mogu da spadaju dokumenta, tablice, e-pošta, sastanci u kalendaru, finansijski podaci, porodične fotografije, ili sve druge lične datoteke koje smatrate nezamenjivim.</p>
 </item>

 <item>
  <title>Vaša lična podešavanja</title>
   <p>Ovde spadaju izmene koje ste možda načinili na bojama, pozadinama, rezoluciji ekrana i podešavanjima miša na vašoj radnoj površi. Ovde takođe spadaju postavke programa, kao podešavanja <app>Libre ofisa</app>, programa za muziku i e-poštu. Sva ova podešavanja se mogu zameniti, ali njihovo ponovno stvaranje može potrajati malo duže.</p>
 </item>

 <item>
  <title>Podešavanja sistema</title>
   <p>Većina ljudi nikada ne menja podešavanja sistema koja su stvorena za vreme instalacije. Ako iz nekog razloga prilagodite podešavanja sistema, ili ako koristite računar kao server, onda ćete možda želeti da napravite rezervu tih podešavanja.</p>
 </item>

 <item>
  <title>Instalirani programi</title>
   <p>Programi koje koristite obično mogu biti povraćeni vrlo brzo nakon ozbiljnih problema na računaru njihovim ponovnim instaliranjem.</p>
 </item>
</terms>

  <p>U opšte, želećete da napravite rezervu datoteka koje su nezamenjive i datoteka koje zahtevaju dosta uloženog vremena za njihovu zamenu bez rezerve. Ako su stvari lako zamenjive, s druge strane, možda nećete želeti da koristite prostor na disku njihovom rezervom.</p>

</page>
