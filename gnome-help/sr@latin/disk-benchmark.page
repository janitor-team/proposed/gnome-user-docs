<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="disk-benchmark" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="disk"/>

    <revision pkgversion="3.6.2" version="0.2" date="2012-11-16" status="review"/>
    <revision pkgversion="3.10" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Gnomov projekat dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalija Ruz Lejva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
   <credit type="editor">
     <name>Majkl Hil</name>
     <email>mdhillca@gmail.com</email>
   </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Izvršite probna ispitivanja na vašem čvrstom disku da proverite koliko je brz.</desc>
  </info>

<title>Isprobajte osobine vašeg čvrstog diska</title>

  <p>Da isprobate brzinu vašeg čvrstog diska:</p>

  <steps>
    <item>
      <p>Otvorite <app>Diskove</app> iz pregleda <gui xref="shell-introduction#activities">aktivnosti</gui>.</p>
    </item>
    <item>
      <p>Izaberite disk sa spiska u levoj površi.</p>
    </item>
    <item>
      <p>Kliknite na dugme izbornika i izaberite <gui>Oceni disk…</gui>.</p>
    </item>
    <item>
      <p>Kliknite <gui>Započni ocenjivanje…</gui> i doterajte parametre <gui>Stopa prenosa</gui> i <gui>Vreme pristupa</gui> kako želite.</p>
    </item>
    <item>
      <p>Click <gui>Start Benchmarking…</gui> to test how fast data can be read
      from the disk. <link xref="user-admin-explain">Administrative
      privileges</link> may be required. Enter your password, or the password
      for the requested administrator account.</p>
      <note>
        <p>Ako je izabrano <gui>Izvrši ocenjivanje pisanja</gui>, ocenjivanje će da isproba koliko brzo podaci mogu biti pročitani sa i zapisani na disk. Ovo će trajati duže.</p>
      </note>
    </item>
  </steps>

  <p>Nakon završenog testa, rezultati će se pojaviti na grafikonu. Zelene tačke i linije povezivanja ukazuju na iskorišćene uzorke; koji odgovaraju desnoj osi, prikazujući vreme pristupa, iscrtani naspram donje ose, predstavljaju procenat proteklog vremena za vreme isprobavanja. Plava linija predstavlja stopu čitanja, dok crvena linija predstavlja stopu pisanja; prikazane su kao stope pristupa podacima na levoj osi, iscrtani naspram procenta pređenog diska, od spolja do osovine, duž donje ose.</p>

  <p>Ispod grafika, vrednosti su prikazane za najmanju, najveću i prosečnu stopu čitanja i pisanja, prosečno vreme pristupa i vreme proteklo od poslednjeg testa isprobavanja.</p>

</page>
