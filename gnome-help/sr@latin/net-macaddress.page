<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="net-macaddress" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Šon Mek Kens</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Džim Kembel</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jedinstveni odrednik dodeljen mrežnom hardveru.</desc>
  </info>

  <title>Šta je to MAK adresa?</title>

  <p><em>MAK adresa</em> je jedinstveni odrednik koji je proizvođač dodelio komadu mrežne fizičke komponente (kao što je bežična kartica ili mrežna kartica). MAK znači <em>Kontrola pristupa medija</em>, i svaki odrednik je nameravan da bude jedinstven za poseban uređaj.</p>

  <p>MAK adresa se sastoji od šest skupa od po dva znaka, razdvojenih dvotačkom. <code>00:1B:44:11:3A:B7</code> je jedan primer MAK adrese.</p>

  <p>Da odredite MAK adresu vašeg mrežnog hardvera:</p>

  <steps>
    <item>
      <p>Otvorite pregled <gui xref="shell-introduction#activities">Aktivnosti</gui> i počnite da kucate <gui>Mreža</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Mreža</gui> da otvorite panel.</p>
    </item>
    <item>
      <p>Izaberite uređaj, <gui>bežični</gui> ili <gui>žičani</gui> sa spiska na levoj strani.</p>
      <p>MAK adresa za žičani uređaj biće prikazana kao <gui>Hardverska adresa</gui> sa desne strane.</p>
      
      <p>Pritisnite na dugme <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">podešavanja</span></media> da vidite MAK adresu bežičnog uređaja koja se prikazuje kao <gui>hardverska adresa</gui> u panelu <gui>detalja</gui>.</p>
    </item>
  </steps>

  <p>U praksi, možda ćete morati da izmenite ili da „prevarite“ MAK adresu. Na primer, neki dostavljači internet usluga mogu da zahtevaju da bude korišćena određena MAK adresa za pristup njihovoj usluzi. Ako mrežna kartica prestane da radi, i morate da je zamenite novom, usluga neće više raditi. U tom slučaju, moraćete da prevarite MAK adresu.</p>

</page>
