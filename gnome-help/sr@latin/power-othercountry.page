<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-othercountry" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="power#problems"/>
    <desc>Vaš računar će raditi, ali će vam možda zatrebati drugačiji kabal za napajanje ili prenosni prilagođivač.</desc>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Gnomov projekat dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Fil Bul</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Da li će moj računar raditi sa napajanjem u drugoj državi?</title>

<p>Neke države koriste napajanje drugačijeg napona (obično 110V ili 220-240V) i učestalosti naizmenične struje (obično 50 Hz ili 60 Hz). Vaš računar će raditi sa izvorom napajanja u drugoj državi samo ako imate odgovarajući prilagođivač napajanja. Morate takođe da preokrenete prekidač.</p>

<p>Ako imate prenosni računar, sve što treba da uradite je da nabavite odgovarajući utikač za prilagođivač napajanja. Neki prenosni računari dolaze zapakovani sa više od jednim utikačem za njihov prilagođivač, tako da ćete možda već i imati onaj pravi. Ako ne, priključivanje postojećeg u standardni prenosni prilagođivač će biti dovoljno.</p>

<p>Ako imate stoni računar, možete takođe da nabavite kabal sa drugačijim utikačem, ili da koristite prenosni prilagođivač. U ovom slučaju, međutim, moraćete da promenite naponski prekidač na izvoru napajanja računara, ako postoji. Mnogi računari nemaju ovakav prekidač, i dobro će raditi na svakom naponu. Pogledajte na poleđini računara i pronađe utičnicu u koju se uključuje kabal za napajanje. Negde u blizini, možda postoji mali prekidač sa oznakom „110V“ ili „230V“ (na primer). Prebacite ga ako je potrebno.</p>

<note style="warning">
  <p>Budite pažljivi prilikom menjanja kablova napajanja ili prilikom korišćenja prenosnih prilagođivača. Isključite sve ako možete.</p>
</note>

</page>
