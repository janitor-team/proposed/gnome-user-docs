<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="tip" version="1.0 if/1.0 ui/1.0" id="shell-keyboard-shortcuts" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="tips"/>
    <link type="guide" xref="keyboard"/>
    <link type="guide" xref="shell-overview#apps"/>
    <link type="seealso" xref="keyboard-key-super"/>

    <revision pkgversion="3.8.0" version="0.4" date="2013-04-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.20" date="2016-08-13" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author copyright">
      <name>Šon Mek Kens</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Krećite se po radnoj površi koristeći tastaturu.</desc>
  </info>

<title>Korisne prečice tastature</title>

<p>Ova stranica obezbeđuje pregled prečica tastature koje mogu da vam pomognu da uspešnije koristite vašu radnu površ i programe. Ako uopšte ne možete da koristite miša ili pokazivački uređaj, pogledajte <link xref="keyboard-nav"/> za više podataka o kretanju po korisničkom sučelju samo tastaturom.</p>

<table rules="rows" frame="top bottom" ui:expanded="true">
<title>Kretanje po radnoj površi</title>
  <tr xml:id="alt-f1">
    <td><p>
      <keyseq><key>Alt</key><key>F1</key></keyseq> or the</p>
      <p><key xref="keyboard-key-super">Super</key> key
    </p></td>
    <td><p>Prebacujte se između pregleda <gui>Aktivnosti</gui> i radne površi. U pregledu, počnite da kucate da odmah potražite vaše programe, kontakte i dokumenta.</p></td>
  </tr>
  <tr xml:id="alt-f2">
    <td><p><keyseq><key>Alt</key><key>F2</key></keyseq></p></td>
    <td><p>Pop up command window (for quickly running commands).</p>
    <p>Use the arrow keys to quickly access previously run commands.</p></td>
  </tr>
  <tr xml:id="super-tab">
    <td><p><keyseq><key>Super</key><key>Tab</key></keyseq></p></td>
    <td><p><link xref="shell-windows-switching">Brzo se prebacujte između prozora</link>. Držite pritisnutim <key>Šift</key> za preokrenuti poredak.</p></td>
  </tr>
  <tr xml:id="super-tick">
    <td><p><keyseq><key>Super</key><key>`</key></keyseq></p></td>
    <td>
      <p>Prebacujte se između prozora iz istog programa, ili iz izabranog programa posle <keyseq><key>Super</key><key>Tab</key></keyseq>.</p>
      <p>Ova prečica koristi <key>`</key> na SAD tastaturama, na kojima se taster <key>`</key> nalazi iznad tastera <key>Tab</key>. Na svim ostalim tastaturama, prečica je <key>Super</key> plus taster iznad tastera <key>Tab</key>.</p>
    </td>
  </tr>
  <tr xml:id="alt-escape">
    <td><p><keyseq><key>Alt</key><key>Izađi</key></keyseq></p></td>
    <td>
      <p>Switch between windows in the current workspace. Hold down
      <key>Shift</key> for reverse order.</p>
    </td>
  </tr>
  <tr xml:id="ctrl-alt-tab">
    <!-- To be updated to <key>Tab</key> in the future. -->
    <td><p><keyseq><key>Ktrl</key><key>Alt</key><key>Tab</key></keyseq></p></td>
    <td>
      <p>Give keyboard focus to the top bar. In the <gui>Activities</gui>
      overview, switch keyboard focus between the top bar, dash, windows
      overview, applications list, and search field. Use the arrow keys to
      navigate.</p>
    </td>
  </tr>
  <tr xml:id="super-a">
    <td><p><keyseq><key>Super</key><key>A</key></keyseq></p></td>
    <td><p>Prikažite spisak programa.</p></td>
  </tr>
  <tr xml:id="super-updown">
    <td>
      <p if:test="!platform:gnome-classic"><keyseq><key>Super</key><key>Stranica gore</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Ktrl</key><key>Alt</key><key>→</key></keyseq></p>
      <p>and</p>
      <p if:test="!platform:gnome-classic"><keyseq><key>Super</key><key>Stranica dole</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Ktrl</key><key>Alt</key><key>←</key></keyseq></p>
    </td>
    <td><p><link xref="shell-workspaces-switch">Prebacujte se između radnih prostora</link>.</p></td>
  </tr>
  <tr xml:id="shift-super-updown">
    <td>
      <p if:test="!platform:gnome-classic"><keyseq><key>Shift</key><key>Super</key><key>Page Up</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Šift</key><key>Ktrl</key><key>Alt</key><key>→</key></keyseq></p>
      <p>and</p>
      <p if:test="!platform:gnome-classic"><keyseq><key>Šift</key><key>Super</key><key>Stranica dole</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Šift</key><key>Ktrl</key><key>Alt</key><key>←</key></keyseq></p>
    </td>
    <td><p><link xref="shell-workspaces-movewindow">Premestite trenutni prozor na drugi radni prostor</link>.</p></td>
  </tr>
  <tr xml:id="shift-super-left">
    <td><p><keyseq><key>Shift</key><key>Super</key><key>←</key></keyseq></p></td>
    <td><p>Premešta tekući prozor jedan monitor na levo.</p></td>
  </tr>
  <tr xml:id="shift-super-right">
    <td><p><keyseq><key>Shift</key><key>Super</key><key>→</key></keyseq></p></td>
    <td><p>Premešta tekući prozor jedan monitor na desno.</p></td>
  </tr>
  <tr xml:id="ctrl-alt-Del">
    <td><p><keyseq><key>Ktrl</key><key>Alt</key><key>Obriši</key></keyseq></p></td>
    <td><p><link xref="shell-exit">Show the Power Off dialog</link>.</p></td>
  </tr>
  <tr xml:id="super-l">
    <td><p><keyseq><key>Super</key><key>L</key></keyseq></p></td>
    <td><p><link xref="shell-exit#lock-screen">Zaključajte ekran.</link></p></td>
  </tr>
  <tr xml:id="super-v">
    <td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
    <td><p>Show <link xref="shell-notifications#notificationlist">the notification
    list</link>. Press <keyseq><key>Super</key><key>V</key></keyseq> again or
    <key>Esc</key> to close.</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Opšte prečice za uređivanje</title>
  <tr>
    <td><p><keyseq><key>Ktrl</key><key>A</key></keyseq></p></td>
    <td><p>Izaberite sav tekst ili stavke na spisku.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ktrl</key><key>H</key></keyseq></p></td>
    <td><p>Isecite (uklonite) izabrani tekst ili stavke i postavite ih u ostavu.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ktrl</key><key>C</key></keyseq></p></td>
    <td><p>Umnožite izabrani tekst ili stavke u ostavu.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ktrl</key><key>V</key></keyseq></p></td>
    <td><p>Umetnite sadržaj ostave.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ktrl</key><key>Z</key></keyseq></p></td>
    <td><p>Opozovite poslednju radnju.</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Snimanje ekrana</title>
  <tr>
    <td><p><key>Print Screen</key></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Napravite sliku ekrana.</link></p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Print Screen</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Napravite sliku prozora.</link></p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key><key>Print Screen</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Napravite snimak oblasti ekrana</link>. Pokazivač se menja u nišan. Kliknite i prevucite da izaberete oblast.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ktrl</key><key>Alt</key><key>Šift</key><key>R</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screencast">Start and stop screencast
     recording.</link></p></td>
  </tr>
</table>

</page>
