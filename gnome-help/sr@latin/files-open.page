<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-open" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>Kristofer Tomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Šon Mek Kens</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Otvorite datoteke koristeći program koji nije osnovni za tu vrstu datoteke. Možete da izmenite i podrazumevani.</desc>
  </info>

<title>Otvorite datoteke drugim programima</title>

  <p>Kada dva puta kliknete na datoteku (ili pritisnete srednji taster miša) u upravniku datoteka, biće otvorena osnovnim programom za tu vrstu datoteke. Istu možete da otvorite drugim programom, da potražite programe na mreži, ili da postavite osnovni program za sve datoteke iste vrste.</p>

  <p>Da otvorite datoteku programom koji je drugačiji od osnovnog, kliknite desnim tasterom miša na datoteku i izaberite program koji želite iz vrha izbornika. Ako ne vidite program koji želite, izaberite <gui>Otvori drugim programom</gui>. Po osnovi, upravnik datoteka prikazuje samo programe za koje zna da mogu da rukuju datotekom. Da pregledate sve programe na vašem računaru, pritisnite <gui>Ostali programi…</gui>.</p>

<p>Ako još uvek ne možete da pronađete program koji želite, možete da potražite još programa klikom na <gui>Nađi nove programe</gui>. Upravnik datoteka će na mreži potražiti pakete koji sadrže programe za koje se zna da rukuju datotekama ove vrste.</p>

<section id="default">
  <title>Izmenite osnovni program</title>
  <p>Možete da izmenite osnovni program koji je korišćen za otvaranje datoteka date vrste. Ovo će vam omogućiti da otvorite vaš omiljeni program kada kliknete dva pua da biste otvorili datoteku. Na primer, možda ćete želeti da se otvori vaš omiljeni muzički program kada dva puta kliknete na MP3 datoteku.</p>

  <steps>
    <item><p>Izaberite datoteku vrste za koju želite da izmenite osnovni program. Na primer, da izmenite koji program se koristi za otvaranje MP3 datoteka, izaberite <file>.mp3</file> datoteku.</p></item>
    <item><p>Kliknite desnim tasterom miša na datoteku i izaberite <gui>Osobine</gui>.</p></item>
    <item><p>Izaberite jezičak <gui>Otvori pomoću</gui>.</p></item>
    <item><p>Izaberite program koji želite i kliknite <gui>Postavi kao osnovni</gui>.</p>
    <p>Ako <gui>Ostali programi</gui> sadrže program koji ćete poželeti da koristite ponekad, ali ne želite da ga postavite za osnovni, izaberite program i kliknite <gui>Dodaj</gui>. Ovo će ga dodati među <gui>Preporučene programe</gui>. Nakon toga ćete biti u mogućnosti da koristite ovaj program tako što ćete kliknuti desnim tasterom miša na datoteku i izabrati ga sa spiska.</p></item>
  </steps>

  <p>Ovo će izmeniti osnovni program ne samo za izabranu datoteku, već i za sve datoteke iste vrste.</p>

<!-- TODO: mention resetting the open with list with the "Reset" button -->

</section>

</page>
