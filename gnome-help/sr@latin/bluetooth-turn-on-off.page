<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="bluetooth-turn-on-off" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="bluetooth" group="#first"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-04" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-21" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Džim Kembel</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Šon Mek Kens</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Dejvid King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Uključite ili isključite blutut uređaj na vašem računaru.</desc>
  </info>

<title>Uključite ili isključite blutut</title>

  <p>Možete da uključite blutut da se povežete na druge blutut uređaje, ili da ga isključite da sačuvate energiju. Da uključite blutut:</p>

  <steps>
    <item>
      <p>Otvorite pregled <gui xref="shell-introduction#activities">Aktivnosti</gui> i počnite da kucate <gui>Blutut</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Blutut</gui> da otvorite panel.</p>
    </item>
    <item>
      <p>Set the switch at the top to on.</p>
    </item>
  </steps>

  <p>Mnogi prenosni računari imaju fizički prekidač ili kombinaciju tastera za uključivanje i isključivanje blututa. Potražite prekidač na vašem računaru ili taster na tastaturi. Tasteru na tastaturi se obično pristupa uz pomoć <key>Fn</key> tastera.</p>

  <p>Da isključite blutut:</p>
  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#systemmenu">system menu</gui> from the
      right side of the top bar.</p>
    </item>
    <item>
      <p>Izaberite <gui><media its:translate="no" type="image" mime="image/svg" src="figures/bluetooth-active-symbolic.svg"/> Nije u upotrebi</gui>. Odeljak blututa u izborniku će se raširiti.</p>
    </item>
    <item>
      <p>Izaberite <gui>Isključi</gui>.</p>
    </item>
  </steps>

  <note><p>Vaš računar je <link xref="bluetooth-visibility">vidljiv</link> sve dok je otvoren panel <gui>blututa</gui>.</p></note>

</page>
