<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-fixed-ip-address" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="net-wired"/>
    <link type="seealso" xref="net-findip"/>

    <revision pkgversion="3.4.0" date="2012-03-13" status="final"/>
    <revision pkgversion="3.15" date="2014-12-04" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.24" date="2017-03-26" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author">
      <name>Džim Kembel</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Korišćenje statičke IP adrese može da olakša obezbeđivanje nekih mrežnih usluga sa vašeg računara.</desc>
  </info>

  <title>Napravite vezu sa stalnom IP adresom</title>

  <p>Većina mreža će sama da dodeli <link xref="net-what-is-ip-address">IP adresu</link> i ostale detalje vašem računaru kada se priključite na mrežu. Ovi detalji mogu da se promene s vremena na vreme, ali vi ćete možda želeti da imate stalnu IP adresu za računar tako da znate uvek koja je njegova adresa (na primer, ako je to server datoteka).</p>

  <!-- TODO Update for Network/Wi-Fi split -->
  <steps>
    <title>Da date vašem računaru stalnu (statičku) IP adresu:</title>
    <item>
      <p>Otvorite pregled <gui xref="shell-introduction#activities">Aktivnosti</gui> i počnite da kucate <gui>Mreža</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Mreža</gui> da otvorite panel.</p>
    </item>
    <item>
      <p>Find the network connection that you want to have a fixed address.
      Click the
      <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">settings</span></media>
      button next to the network connection. For a <gui>Wi-Fi</gui> connection,
      the
      <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">settings</span></media>
      button will be located next to the active network.</p>
    </item>
    <item>
      <p>Select the <gui>IPv4</gui> or <gui>IPv6</gui> tab and change the
      <gui>Method</gui> to <gui>Manual</gui>.</p>
    </item>
    <item>
      <p>Ukucajte <gui xref="net-what-is-ip-address">Adresu</gui> i <gui>Mrežni prolaz</gui>, kao i odgovarajuću <gui>Mrežnu masku</gui>.</p>
    </item>
    <item>
      <p>In the <gui>DNS</gui> section, switch the <gui>Automatic</gui> switch
      to off. Enter the IP address of a DNS server you want to use. Enter
      additional DNS server addresses using the <gui>+</gui> button.</p>
    </item>
    <item>
      <p>In the <gui>Routes</gui> section, switch the <gui>Automatic</gui>
      switch to off. Enter the <gui>Address</gui>, <gui>Netmask</gui>,
      <gui>Gateway</gui> and <gui>Metric</gui> for a route you want to use.
      Enter additional routes using the <gui>+</gui> button.</p>
    </item>
    <item>
      <p>Pritisnite <gui>Primeni</gui>. Mrežna veza će sada imati stalnu IP adresu.</p>
    </item>
  </steps>

</page>
