<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-lowpower" xml:lang="el">

  <info>
    <link type="guide" xref="power#faq"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>

    <desc>Επιτρέποντας στην μπαταρία την πλήρη αποφόρτιση είναι κακό για τον υπολογιστή.</desc>
    <credit type="author">
      <name>Έργο Τεκμηρίωσης GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Γιατί ο υπολογιστής μου απενεργοποιήθηκε όταν η μπαταρία έφτασε στο 10%;</title>

<p>Όταν το επίπεδο φόρτισης της μπαταρίας σας φτάσει χαμηλά, τότε ο υπολογιστής σας θα απενεργοποιηθεί αυτόματα. Αυτό γίνεται για να σιγουρευτεί ότι η μπαταρία σας δεν θα αποφορτιστεί τελείως, καθώς δεν είναι καλό για την ίδια τη μπαταρία. Αν η μπαταρία αποφορτίστηκε πλήρως, ο υπολογιστής δεν θα έχει τον χρόνο να τερματιστεί σωστά.</p>

<p>Να έχετε κατά νου, πως όταν ο υπολογιστής σας απενεργοποιηθεί, οι εφαρμογές και τα έγγραφα σας <em>δεν</em> αποθηκεύονται. Για να αποφύγετε την απώλεια της εργασίας που έχετε κάνει, αποθηκεύστε πρωτού φτάσει η μπαταρία σε χαμηλά επίπεδα.</p>

</page>
