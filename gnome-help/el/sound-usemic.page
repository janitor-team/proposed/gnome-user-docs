<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-usemic" xml:lang="el">

  <info>
    <link type="guide" xref="media#sound"/>

    <revision version="gnome:40" date="2021-02-26" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Χρησιμοποιήστε ένα αναλογικό ή USB μικρόφωνο και επιλέξτε μια προεπιλεγμένη συσκευή εισόδου.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Χρήση διαφορετικού μικροφώνου</title>

  <p>Μπορείτε να χρησιμοποιήσετε ένα εξωτερικό μικρόφωνο για συνομιλία με φίλους, συζήτηση με συναδέλφους στη δουλειά, εγγραφή φωνών, ή για χρήση με άλλες εφαρμογές πολυμέσων. Ακόμα κι αν ο υπολογιστής σας έχει ένα ενσωματωμένο μικρόφωνο ή μια ιστοκάμερα με μικρόφωνο, ένα ξεχωριστό μικρόφωνο συνήθως παρέχει καλύτερη ποιότητα ήχου.</p>

  <p>Αν το μικρόφωνο σας έχει ένα κυκλικό βύσμα , απλά συνδέστε το στην κατάλληλη υποδοχή ήχου στον υπολογιστή σας. Οι περισσότεροι υπολογιστές έχουν δύο υποδοχές: μια για μικρόφωνα και μια για ηχεία. Αυτή η υποδοχή έχει συνήθως ένα απαλό κόκκινο χρώμα ή μια φωτογραφία ενός μικροφώνου. Τα μικρόφωνα που συνδέονται στην σωστή υποδοχή χρησιμοποιούνται από προεπιλογή. Αν όχι, δείτε τις οδηγίες παρακάτω για το πως να επιλέξετε μια προεπιλεγμένη συσκευή εισόδου.</p>

  <p>Εάν έχετε ένα μικρόφωνο USB, συνδέστε το σε οποιαδήποτε θύρα USB στον υπολογιστή σας. Τα μικρόφωνα USB δρουν ως ξεχωριστές συσκευές ήχου και μπορεί να πρέπει να καθορίσετε ποιο μικρόφωνο να χρησιμοποιήσετε από προεπιλογή.</p>

  <steps>
    <title>Επιλογή μιας προεπιλεγμένης συσκευής εισόδου ήχου</title>
    <item>
      <p>Ανοίξτε την επισκόπηση <gui xref="shell-introduction#activities">Δραστηριότητες</gui> και αρχίστε να πληκτρολογείτε <gui>Ήχος</gui>.</p>
    </item>
    <item>
      <p>Κάντε κλικ στο <gui>Ήχος</gui> για να ανοίξετε τον πίνακα.</p>
    </item>
    <item>
      <p>In the <gui>Input</gui> section, select the device that you want to
      use. The input level indicator should respond when you speak.</p>
    </item>
  </steps>

  <p>Μπορείτε να ρυθμίσετε την ένταση και να απενεργοποιήσετε το μικρόφωνο από αυτόν τον πίνακα.</p>

</page>
