<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting-hardware-info" xml:lang="el">

  <info>
    <link type="next" xref="net-wireless-troubleshooting-hardware-check"/>
    <link type="guide" xref="net-wireless-troubleshooting"/>

    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Συντελεστές στην τεκμηρίωση wiki του Ubuntu</name>
    </credit>
    <credit type="author">
      <name>Έργο Τεκμηρίωσης GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Μπορεί να χρειαστείτε λεπτομέρειες όπως τον αριθμό μοντέλου του ασύρματου προσαρμογέα σας στα επόμενα βήματα ανίχνευσης του προβλήματος.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Ανιχνευτής προβλημάτων ασύρματου δικτύου</title>
  <subtitle>Συλλέξτε πληροφορίες για το υλικό του δικτύου</subtitle>

  <p>Σε αυτό το βήμα, θα συλλέξετε πληροφορίες για τη συσκευή ασύρματου δικτύου. Ο τρόπος που διορθώνετε πολλά προβλήματα ασύρματου εξαρτάται από τον κατασκευαστή και τον αριθμό μοντέλου του ασύρματου προσαρμογέα, έτσι θα χρειαστείτε να σημειώσετε αυτές τις λεπτομέρειες. Μπορεί επίσης να βοηθήσει να έχετε κάποια στοιχεία που ήρθαν με τον υπολογιστή σας επίσης, όπως δίσκους εγκατάστασης οδηγού συσκευής. Ψάξτε για τα επόμενα στοιχεία, εάν τα έχετε ακόμα:</p>

  <list>
    <item>
      <p>Το πακετάρισμα και τις οδηγίες για τις ασύρματες συσκευές σας (ειδικά τον οδηγό χρήστη για τον δρομολογητή σας)</p>
    </item>
    <item>
      <p>Ο δίσκος περιέχει οδηγούς για τον ασύρματο προσαρμογέα σας (ακόμα κι αν περιέχει μόνο οδηγούς Windows)</p>
    </item>
    <item>
      <p>Οι κατασκευαστές και οι αριθμοί μοντέλου του υπολογιστή σας, του ασύρματου προσαρμογέα και του δρομολογητή. Αυτές οι πληροφορίες μπορούν συνήθως να βρεθούν στην κάτω πλευρά της συσκευής.</p>
    </item>
    <item>
      <p>Οποιοιδήποτε αριθμοί έκδοσης ή αναθεώρησης που μπορούν να είναι τυπωμένοι στις συσκευές ασύρματου δικτύου ή στη συσκευασία τους. Αυτά μπορούν να είναι χρήσιμα, γι' αυτό ελέγξτε τα προσεκτικά.</p>
    </item>
    <item>
      <p>Anything on the driver disc that identifies either the device itself,
      its “firmware” version, or the components (chipset) it uses.</p>
    </item>
  </list>

  <p>Εάν είναι δυνατό, δοκιμάστε να προσπελάσετε μια εναλλακτική λειτουργούσα σύνδεση διαδικτύου έτσι ώστε να μπορείτε να κάνετε λήψη λογισμικού και οδηγών, εάν είναι απαραίτητο. (η σύνδεση του υπολογιστή σας άμεσα στον δρομολογητή με ένα καλώδιο δικτύου Ethernet είναι ένας τρόπος παροχής του, αλλά συνδέστε το μόνο εάν χρειάζεται.)</p>

  <p>Μόλις έχετε όσα στοιχεία είναι δυνατό, κάντε κλικ στο <gui>Επόμενο</gui>.</p>

</page>
