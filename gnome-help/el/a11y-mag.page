<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-mag" xml:lang="el">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-10" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Μεγεθύνετε την οθόνη σας ώστε να διευκολυνθείτε να δείτε πράγματα.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Μεγέθυνση της περιοχής οθόνης</title>

  <p>Μεγεθύνοντας την οθόνη είναι διαφορετικό από την απλή μεγέθυνση του <link xref="a11y-font-size">μεγέθους κειμένου</link>. Αυτή η λειτουργία είναι σαν να έχουμε μεγεθυντικούς φακούς, που επιτρέπουν τη μετακίνηση ολόγυρα εστιάζοντας σε μέρη της οθόνης.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Accessibility</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Accessibility</gui> to open the panel.</p>
    </item>
    <item>
      <p>Πατήστε <gui>Εστίαση</gui> στην ενότητα <gui>Όραση</gui>.</p>
    </item>
    <item>
      <p>Switch the <gui>Zoom</gui> switch in the top-right corner of the
      <gui>Zoom Options</gui> window to on.</p>
      <!--<note>
        <p>The <gui>Zoom</gui> section lists the current settings for the
        shortcut keys, which can be set in the <gui>Accessibility</gui>
        section of the <link xref="keyboard-shortcuts-set">Shortcuts</link> tab
        on the <gui>Keyboard</gui> panel.</p>
      </note>-->
    </item>
  </steps>

  <p>Μπορείτε τώρα να μετακινηθείτε στην περιοχή της οθόνης. Μετακινώντας το ποντίκι σας στις άκρες της οθόνης, θα μετακινήσετε τη μεγεθυσμένη περιοχή σε διαφορετικές κατευθύνσεις, επιτρέποντας να δείτε την επιθυμητή περιοχή.</p>

  <note style="tip">
    <p>Μπορείτε να απ/ενεργοποιήσετε άμεσα κάνοντας κλικ στο <link xref="a11y-icon">εικονίδιο προσιτότητας</link> στην πάνω γραμμή και να επιλέξετε <gui>Εστίαση</gui>.</p>
  </note>

  <p>Μπορείτε να αλλάξετε τον συντελεστή μεγέθυνσης, την ανίχνευση ποντικιού και τη θέση της μεγεθυσμένης προβολής στην οθόνη. Ρυθμίστε τα στην καρτέλα <gui>Μεγεθυντής</gui> του παραθύρου ρυθμίσεων <gui>Εστίαση</gui>.</p>

  <p>Μπορείτε να ενεργοποιήσετε τους στόχους για να σας βοηθήσουν να βρείτε το ποντίκι ή τον δείκτη της πινακίδας αφής. Ενεργοποιήστε τους και να ρυθμίστε το μήκος, το χρώμα και το πάχος τους στην καρτέλα <gui>Στόχοι</gui> του παραθύρου ρυθμίσεων <gui>Εστίαση</gui>.</p>

  <p>Μπορείτε να αλλάξετε σε αντιστροφή βίντεο ή <gui>Λευκό σε μαύρο</gui> και να ρυθμίσετε τη φωτεινότητα, την αντίθεση και τις επιλογές γκρίζας κλίμακας για τον μεγεθυντή. Ο συνδυασμός αυτών των επιλογών είναι χρήσιμος για ανθρώπους με χαμηλή όραση, οποιουδήποτε βαθμού φωτοφοβίας, ή απλά για χρήση του υπολογιστή σε συνθήκες δυσμενούς φωτισμού. Επιλέξτε την καρτέλα <gui>Εφέ χρώματος</gui> στο παράθυρο ρυθμίσεων <gui>Εστίαση</gui> για να τις ενεργοποιήσετε και να αλλάξτε αυτές τις επιλογές.</p>

</page>
