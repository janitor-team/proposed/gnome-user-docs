<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="power-batteryestimate" xml:lang="el">

  <info>

    <link type="guide" xref="power#faq"/>
    <link type="guide" xref="status-icons#batteryicons"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>
    <revision version="gnome:40" date="2021-03-22" status="candidate"/>

    <desc>Η εμφανιζόμενη ζωή της μπαταρίας όταν κάνετε κλικ στο <gui>εικονίδιο μπαταρίας</gui> είναι μια εκτίμηση.</desc>

    <credit type="author">
      <name>Έργο Τεκμηρίωσης GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Η εκτιμώμενη ζωή της μπαταρίας είναι εσφαλμένη</title>

<p>Όταν ελέγχετε την υπολειπόμενη ζωή της μπαταρίας, ίσως βρείτε ότι ο υπολειπόμενος χρόνος που αναφέρει είναι διαφορετικός από τον πραγματικό χρόνο διάρκειας. Αυτό συμβαίνει επειδή η ποσότητα της υπολειπόμενης ζωής της μπαταρίας μπορεί μόνο να εκτιμηθεί. Κανονικά, οι εκτιμήσεις βελτιώνονται με τον χρόνο.</p>

<p>Για να υπολογίσετε την υπολειπόμενη διάρκεια της μπαταρίας, πρέπει να λάβετε υπόψη ορισμένους παράγοντες. Ένας είναι το ποσοστό της ενέργειας που χρησιμοποιείται από τον υπολογιστή: η κατανάλωση της ενέργειας διαφέρει ανάλογα με τον αριθμό των εφαρμογών που έχετε ανοιχτά, ποιες συσκευές είναι συνδεδεμένες και αν εκτελείτε εργασίες υψηλές σε απαιτήσεις (όπως για παράδειγμα, η προβολή ενός βίντεο υψηλής ευκρίνειας ή η μετατρποή μουσικών αρχείων). Αυτό αλλάζει συνεχώς και είναι δύσκολο να προβλεφθεί.</p>

<p>Ένας άλλος παράγοντας είναι πώς εκφορτίζεται η μπαταρία. Μερικές μπαταρίες χάνουν γρηγορότερα όσο φορτίο όσο αδειάζουν. Χωρίς ακριβή γνώση της εκφόρτωσης της μπαταρίας, μόνο μια χοντρική εκτίμηση της υπολειπόμενης ζωής της μπαταρίας μπορεί να γίνει.</p>

<p>Καθώς εκφορτίζεται η μπαταρία, ο διαχειριστής ισχύος θα υπολογίσει τις ιδιότητες εκφόρτισής του και θα μάθει να κάνει καλύτερες εκτιμήσεις για τη ζωή της μπαταρίας. Δεν θα είναι ποτέ πλήρως ακριβείς, όμως.</p>

<note>
  <p>Εάν παίρνετε μια πλήρως γελοία εκτίμηση της ζωής της μπαταρίας (ας πούμε εκατοντάδες ημέρες), ο διαχειριστής ισχύος δεν έχει προφανώς κάποια δεδομένα που χρειάζεται για να κάνει μια λογική εκτίμηση.</p>
  <p>Εάν αποσυνδέσετε την τροφοδοσία και εκτελέσετε τον φορητό υπολογιστή με μπαταρία για κάμποσο, έπειτα τον συνδέσετε και τον αφήσετε να επαναφορτιστεί ξανά, ο διαχειριστής ισχύος πρέπει να μπορεί να πάρει τα δεδομένα που χρειάζεται.</p>
</note>

</page>
