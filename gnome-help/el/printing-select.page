<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-select" xml:lang="el">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Εκτυπώστε συγκεκριμένες σελίδες, ή μόνο ένα εύρος σελίδων.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Εκτύπωση συγκεκριμένων σελίδων</title>

  <p>Για εκτύπωση ορισμένων σελίδων από το έγγραφο:</p>

  <steps>
    <item>
      <p>Άνοίξτε τον διάλόγο εκτύπωσης πατώντας <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Στην καρτέλα <gui>Γενικά</gui>, επιλέξτε <gui>Σελίδες</gui> από την ενότητα <gui>Εύρος</gui>.</p>
    </item>
    <item><p>Πληκτρολογήστε τους αριθμούς των σελίδων, που θέλετε να εκτυπώσετε στο πλαίσιο του κειμένου, διαχωρισμένους με κόμματα. Χρησιμοποιήστε μια παύλα για να δηλώσετε περιοχή σελίδων.</p></item>
  </steps>

  <note>
    <p>For example, if you enter “1,3,5-7” in the <gui>Pages</gui> text box,
    pages 1,3,5,6 and 7 will be printed.</p>
    <media type="image" src="figures/printing-select.png"/>
  </note>

</page>
