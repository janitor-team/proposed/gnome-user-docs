<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="nautilus-views" xml:lang="el">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-views"/>

    <revision pkgversion="3.8" version="0.2" date="2013-04-03" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <its:rules xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <desc>Ορίστε την προεπιλεγμένη προβολή ταξινόμησης και ομαδοποίησης του διαχειριστή αρχείων.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Προτιμήσεις προβολών στα <app>Αρχεία</app></title>

<p>You can change how files and folders are grouped and sorted by default.
Press the menu button in the top-right corner of the window, select
<gui style="menuitem">Preferences</gui>, and then select the
<gui style="tab">Views</gui> tab.</p>

<section id="default-view">
<title>Προεπιλεγμένη προβολή</title>
<terms>
  <item>
    <title><gui>Ταξινόμηση στοιχείων</gui></title>
    <p its:locNote="TRANSLATORS: use the translation of the tooltip for the     view selector button that opens the view popover in the main window for     'View options'">You can change how <link xref="files-sort">files are
    sorted</link> in an individual folder by clicking the view options
    <!-- FIXME: Get a tooltip added for "View options" -->
    menu button in the toolbar and choosing the desired option under
    <gui>Sort</gui>, or by clicking the list column headers in list view.</p>
  </item>
  <item>
    <title><gui>Ταξινόμηση φακέλων πριν από αρχεία</gui></title>
    <p>Από προεπιλογή, ο διαχειριστής αρχείων δεν εμφανίζει πια όλους τους φακέλους πριν από τα αρχεία. Για να δείτε όλους τους φακέλους καταχωρισμένους πριν τα αρχεία, ενεργοποιήστε αυτήν την επιλογή.</p>
  </item>
</terms>
</section>

</page>
