<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-delete" xml:lang="pl">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="user-add"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Projekt dokumentacji GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Usuwanie użytkowników, którzy nie używają już komputera.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

  <title>Usuwanie konta użytkownika</title>

  <p>Można <link xref="user-add">dodawać wiele kont użytkowników na komputerze</link>. Jeśli ktoś nie używa już komputera, to można usunąć jego konto.</p>

  <p>Do usuwania kont użytkowników wymagane są <link xref="user-admin-explain">uprawnienia administratora</link>.</p>

  <steps>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Użytkownicy</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Użytkownicy</gui>, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Kliknij przycisk <gui style="button">Odblokuj</gui> w górnym prawym rogu i wpisz swoje hasło.</p>
    </item>
    <item>
      <p>Zaznacz użytkownika do usunięcia i kliknij przycisk <gui style="button">-</gui> pod listą kont po lewej, aby go usunąć.</p>
    </item>
    <item>
      <p>Każdy użytkownik ma własny katalog domowy dla swoich plików i ustawień. Można wybrać jego zachowanie lub usunięcie. Kliknij przycisk <gui>Usuń pliki</gui>, jeśli ma się pewność, że nie będą one więcej używane i potrzeba zwolnić miejsce na dysku. Zostaną one trwale usunięte. Nie można ich przywrócić. Przed usunięciem można zachować ich kopię na zewnętrznym urządzeniu.</p>
    </item>
  </steps>

</page>
