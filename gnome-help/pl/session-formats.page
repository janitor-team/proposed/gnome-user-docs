<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-formats" xml:lang="pl">

  <info>
    <link type="guide" xref="prefs-language"/>
    <link type="seealso" xref="session-language"/>

    <revision pkgversion="3.10" version="0.4" date="2013-11-03" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.38.4" date="2021-03-11" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Wybór regionu używanego dla formatu daty i czasu, liczb, waluty i jednostek miary.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

  <title>Zmiana formatu daty i jednostek</title>

  <p>Można zmienić formaty używane do wyświetlania dat, czasu, liczb, waluty i jednostek miary, aby pasowały do zwyczajów danego regionu.</p>

  <steps>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Region i język</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Region i język</gui>, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Kliknij <gui>Formaty</gui>.</p>
    </item>
    <item>
      <p>W sekcji <gui>Często używane formaty</gui> wybierz region i język najbardziej pasujące do formatów, których chcesz używać.</p>
    </item>
    <item>
      <p>Kliknij przycisk <gui style="button">Gotowe</gui>, aby zapisać.</p>
    </item>
    <item>
      <p>Sesja musi zostać ponownie uruchomiona, aby zastosować zmiany. Kliknij przycisk <gui style="button">Uruchom ponownie…</gui> lub ręcznie zrób to później.</p>
    </item>
  </steps>

  <p>Po wybraniu regionu obszar na prawo od listy wyświetli różne przykłady tego, jak daty i inne wartości będą pokazywane. Chociaż nie ma tego w przykładach, region wpływa także na pierwszy dzień tygodnia w kalendarzach.</p>

  <note style="tip">
    <p>Jeśli na komputerze jest wiele kont użytkowników, to dostępny jest oddzielny panel <gui>Region i język</gui> dla ekranu logowania. Kliknij przycisk <gui>Ekran logowania</gui> w górnym prawym rogu, aby przełączyć między panelami.</p>
  </note>

</page>
