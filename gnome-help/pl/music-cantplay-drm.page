<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="music-cantplay-drm" xml:lang="pl">
  <info>
    <link type="guide" xref="media#music"/>


    <credit type="author">
      <name>Projekt dokumentacji GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <desc>Obsługa tego formatu pliku może nie być zainstalowana lub utwory mogą mieć ochronę przed kopiowaniem.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

<title>Nie mogę odtwarzać utworów kupionych w sklepie internetowym</title>

<p>Jeśli pobrano muzykę ze sklepu internetowego, to może się okazać, że nie da się jej odtwarzać na komputerze, zwłaszcza jeśli kupiono ją na komputerze z systemem Windows lub macOS i potem skopiowano na ten komputer.</p>

<p>Może to być spowodowane tym, że muzyka jest w formacie nierozpoznawanym przez komputer. Aby móc odtwarzać utwór, potrzebna jest obsługa właściwego formatu dźwięku — na przykład do odtwarzania plików MP3 należy zainstalować obsługę MP3. Jeśli nie ma obsługi danego formatu dźwięku, to powinien zostać wyświetlony komunikat na ten temat podczas próby odtworzenia utworu. Komunikat powinien także zawierać instrukcje instalacji obsługi tego formatu.</p>

<p>Jeśli zainstalowano obsługę formatu dźwięku utworu, ale nadal nie da się go odtworzyć, to może mieć <em>ochronę przed kopiowaniem</em> (znaną także jako <em>ograniczeniem DRM</em>). DRM to sposób ograniczania, kto może odtwarzać utwór i na jakich urządzeniach. Firma, która sprzedała utwór to kontroluje, nie użytkownik. Jeśli plik muzyczny ma ograniczenia DRM, to prawdopodobnie nie będzie można go odtworzyć — zwykle potrzeba specjalnego oprogramowania od dostawcy, aby odtwarzać pliki z ograniczeniami DRM, ale to oprogramowanie często nie obsługuje systemu Linux.</p>

<p>Więcej informacji o DRM można znaleźć na stronie <link href="https://www.eff.org/issues/drm">Electronic Frontier Foundation</link> (w języku angielskim).</p>

</page>
