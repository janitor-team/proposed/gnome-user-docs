<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="a11y task" version="1.0 if/1.0" id="a11y-screen-reader" xml:lang="pl">

  <info>
    <link type="guide" xref="a11y#vision" group="blind"/>

    <revision pkgversion="3.13.92" date="2014-09-20" status="incomplete"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Jana Heves</name>
      <email>jsvarova@gnome.org</email>
    </credit>

    <desc>Czytnik ekranowy <app>Orca</app> odczytuje interfejs użytkownika na głos.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

  <title>Czytanie ekranu na głos</title>

  <p>Czytnik ekranowy <app>Orca</app> może odczytywać interfejs użytkownika na głos. W zależności od sposobu instalacji systemu, Orca może być już zainstalowana. Jeśli nie, to najpierw ją zainstaluj.</p>

  <p if:test="action:install"><link style="button" action="install:orca">Zainstaluj Orcę</link></p>
  
  <p>Aby uruchomić <app>Orcę</app> za pomocą klawiatury:</p>
  
  <steps>
    <item>
    <p>Naciśnij klawisze <key>Super</key>+<key>Alt</key>+<key>S</key>.</p>
    </item>
  </steps>
  
  <p>Aby uruchomić <app>Orcę</app> za pomocą myszy i klawiatury:</p>

  <steps>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Ułatwienia dostępu</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Ułatwienia dostępu</gui>, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Kliknij <gui>Czytnik ekranowy</gui> w sekcji <gui>Wzrok</gui>, a następnie włącz <gui>Czytnik ekranowy</gui> w otwartym oknie.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Szybkie przełączanie czytnika ekranowego</title>
    <p>Można szybko włączać i wyłączać czytnik ekranowy klikając <link xref="a11y-icon">ikonę ułatwień dostępu</link> na górnym pasku i wybierając <gui>Czytnik ekranowy</gui>.</p>
  </note>

  <p><link href="help:orca">Pomoc czytnika Orca</link> zawiera więcej informacji.</p>
</page>
