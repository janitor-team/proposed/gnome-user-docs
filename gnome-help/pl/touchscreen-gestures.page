<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="touchscreen-gestures" xml:lang="pl">

  <info>
    <link type="guide" xref="mouse" group="#last"/>

    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <!--
    For 41: https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/121
    -->
    <revision version="gnome:40" date="2021-03-18" status="candidate"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Używanie komputera za pomocą gestów na panelu lub ekranie dotykowym.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

  <title>Używanie gestów na panelach i ekranach dotykowych</title>

  <p>Można używać gestów na panelu lub ekranie dotykowym do poruszania się po systemie, a także w programach.</p>

  <p>Wiele programów umożliwia korzystanie z gestów. W <app>Przeglądarce dokumentów</app> można przybliżać i przewijać dokumenty za pomocą gestów, a <app>Przeglądarka obrazów</app> umożliwia przybliżanie, obracanie i przesuwanie.</p>

<section id="system">
  <title>Gesty systemowe</title>

<table rules="rows" frame="bottom">
  <tr>
    <td><p><media type="image" src="figures/touch-overview.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Otwarcie ekranu podglądu i widoku programów</em></p>
    <p>Połóż trzy palce na panelu lub ekranie dotykowym i wykonaj gest w górę, aby otworzyć ekran podglądu.</p>
    <p>Aby otworzyć widok programów, połóż trzy palce i wykonaj gest w górę jeszcze raz.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-workspaces.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Przełączenie obszarów roboczych</em></p>
    <p>Połóż trzy palce na panelu lub ekranie dotykowym i wykonaj gest w lewo lub w prawo.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-unfullscreen.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Wyłączenie pełnego ekranu</em></p>
    <p>Na ekranie dotykowym przeciągnij w dół od górnej krawędzi ekranu, aby wyłączyć tryb pełnego ekranu dowolnego okna.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-osk.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Wyświetlenie klawiatury ekranowej</em></p>
    <p>Na ekranie dotykowym przeciągnij w górę od dolnej krawędzi ekranu, aby wysunąć <link xref="keyboard-osk">klawiaturę ekranową</link>, jeśli jest ona włączona.</p></td>
  </tr>
</table>

</section>

<section id="apps">
  <title>Gesty programów</title>

<table rules="rows" frame="bottom">
  <tr>
    <td><p><media type="image" src="figures/touch-tap.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Otwarcie elementu, uruchomienie programu, odtworzenie utworu</em></p>
    <p>Stuknij element.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-hold.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Zaznaczenie elementu i wyświetlenie działań, które można wykonać</em></p>
    <p>Stuknij i przytrzymaj przez sekundę lub dwie.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-scroll.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Przewinięcie obszaru na ekranie</em></p>
    <p>Przeciągnięcie: przesuń palec, dotykając powierzchni.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-pinch-to-zoom.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Zmiana poziomu powiększenia (<app>Mapy</app>, <app>Zdjęcia</app>)</em></p>
    <p>Uszczypnięcie lub rozciągnięcie dwoma palcami: dotknij powierzchnię dwoma palcami, przybliżając lub oddalając je od siebie.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-rotate.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Obrócenie zdjęcia</em></p>
    <p>Obrót dwoma palcami: dotknij powierzchnię dwoma palcami i obróć.</p></td>
  </tr>
</table>

</section>

</page>
