<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-changepassword" xml:lang="pl">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>
    <link type="seealso" xref="user-goodpassword"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Projekt dokumentacji GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Zabezpieczanie konta przez częstą zmianę hasła w ustawieniach.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

  <title>Zmiana hasła</title>

  <p>Zalecana jest zmiana hasła od czasu do czasu, zwłaszcza jeśli ktoś inny mógł je poznać.</p>

  <p>Do modyfikowania kont innych użytkowników wymagane są <link xref="user-admin-explain">uprawnienia administratora</link>.</p>

  <steps>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Użytkownicy</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Użytkownicy</gui>, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Kliknij pole <gui>·····</gui> obok napisu <gui>Hasło</gui>. Jeśli hasło jest zmieniane dla innego użytkownika, to najpierw należy kliknąć przycisk <gui>Odblokuj</gui>.</p>
    </item>
    <item>
      <p>Wpisz swoje obecne hasło, a następnie nowe. Wpisz je ponownie w polu <gui>Potwierdzenie nowego hasła</gui>.</p>
      <p>Można kliknąć ikonę <gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">wygeneruj hasło</span></media></gui>, aby automatycznie wygenerować losowe hasło.</p>
    </item>
    <item>
      <p>Kliknij przycisk <gui>Zmień</gui>.</p>
    </item>
  </steps>

  <p>Upewnij się, że <link xref="user-goodpassword">wybrano dobre hasło</link>. Pomoże to zabezpieczać konto użytkownika.</p>

  <note>
    <p>Podczas aktualizacji hasła logowania hasło domyślnej bazy kluczy zostanie automatycznie zaktualizowane w tym samym czasie, aby oba hasła były takie same.</p>
  </note>

  <p>Jeśli zapomniano hasła, to każdy użytkownik z uprawnieniami administratora może je zmienić za użytkownika.</p>

</page>
