<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="backup-check" xml:lang="pl">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Projekt dokumentacji GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>davidk@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Sprawdzanie poprawności kopii zapasowej.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

  <title>Sprawdzanie kopii zapasowej</title>

  <p>Po utworzeniu kopii zapasowej plików należy się upewnić, że kopia jest poprawna. Jeśli coś się nie powiodło i części plików nie ma w kopii, to można bezpowrotnie utracić ważne dane.</p>

   <p>Podczas używania programu <app>Pliki</app> (lub innego menedżera plików) do kopiowania lub przenoszenia plików komputer upewnia się, że wszystkie dane zostały poprawnie przesłane. Jednakże, jeśli przesyłane dane są bardzo ważne, to można dodatkowo sprawdzić, czy dane zostały właściwie przesłane.</p>

  <p>Można przejrzeć skopiowane pliki i katalogi na nośniku docelowym. Sprawdzenie, czy przesłane pliki i katalogi są w kopii zapasowej zwiększa pewność, że proces się powiódł.</p>

  <note style="tip"><p>Jeśli regularnie wykonywane są kopie zapasowej dużej ilości danych, to łatwiej jest używać wyspecjalizowanego programu, takiego jak <app>Déjà Dup</app>. Programy tego typu mają więcej możliwości i są bardziej niezawodne niż po prostu kopiowanie i wklejanie plików.</p></note>

</page>
