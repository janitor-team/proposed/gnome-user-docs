<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-adhoc" xml:lang="pl">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.32" date="2019-09-01" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <desc>Używanie sieci ad-hoc, aby udostępniać komputer i jego połączenia sieciowe innym urządzeniom.</desc>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

<title>Tworzenie hotspotu bezprzewodowego</title>

  <p>Można używać komputera jako hotspot bezprzewodowy. Umożliwia to innym urządzeniom łączenie się z komputerem bez oddzielnej sieci oraz udostępnianie połączenia internetowego używającego innego interfejsu, takiego jak sieć przewodowa lub komórkowa.</p>

<steps>
  <item>
    <p>Otwórz <gui xref="shell-introduction#systemmenu">menu systemowe</gui> po prawej stronie górnego paska.</p>
  </item>
  <item>
    <p>Wybierz <gui>Nie połączono z siecią Wi-Fi</gui> lub nazwę sieci bezprzewodowej, z którą już połączono. Sekcja menu <gui>Sieć Wi-Fi</gui> zostanie rozwinięta.</p>
  </item>
  <item>
    <p>Kliknij <gui>Ustawienia Wi-Fi</gui>.</p></item>
  <item><p>Kliknij przycisk menu w górnym prawym rogu okna i wybierz <gui>Włącz hotspot Wi-Fi…</gui>.</p></item>
  <item><p>Jeśli połączono już z siecią bezprzewodową, to użytkownik zostanie zapytany, czy z niej rozłączyć. Jeden adapter bezprzewodowy może łączyć się z lub tworzyć tylko jedną sieć jednocześnie. Kliknij przycisk <gui>Włącz</gui>, aby potwierdzić.</p></item>
</steps>

<p>Nazwa sieci (SSID) i klucz zabezpieczeń zostaną wygenerowane automatycznie. Nazwa sieci zostanie utworzona na podstawie nazwy komputera. Inne urządzenia potrzebują tych informacji do łączenia się z utworzonym hotspotem.</p>

<note style="tip">
  <p>Można także połączyć się z siecią bezprzewodową skanując kod QR na telefonie lub tablecie za pomocą wbudowanej aplikacji aparatu lub skanera kodów QR.</p>
</note>

</page>
