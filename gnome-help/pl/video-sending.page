<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="video-sending" xml:lang="pl">

  <info>
    <link type="guide" xref="media#videos"/>
    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.12.1" date="2014-03-30" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.17.90" date="2015-08-30" status="final"/>

    <credit type="author">
      <name>Projekt dokumentacji GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Sprawdź, czy mają zainstalowane właściwe kodeki wideo.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

  <title>Inni nie mogą odtwarzać filmów zrobionych na komputerze</title>

  <p>Jeśli na komputerze z systemem Linux zrobiono film i wysłano go do kogoś używającego systemu Windows lub macOS, to może się okazać, że ma on problemy z jego odtworzeniem.</p>

  <p>Aby móc odtwarzać ten film, właściwe <em>kodeki</em> muszą być zainstalowane na komputerze odbiorcy. Kodek to oprogramowanie potrafiące przetworzyć plik z filmem i wyświetlić go na ekranie. Dostępnych jest wiele różnych formatów wideo i każdy z nich do odtworzenia wymaga innego kodeka. Można sprawdzić, jakiego formatu jest plik z filmem:</p>

  <list>
    <item>
      <p>Otwórz program <app>Pliki</app> z <gui xref="shell-introduction#activities">ekranu podglądu</gui>.</p>
    </item>
    <item>
      <p>Kliknij plik wideo prawym przyciskiem myszy i wybierz <gui>Właściwości</gui>.</p>
    </item>
    <item>
      <p>Przejdź do karty <gui>Dźwięk/obraz</gui> lub <gui>Wideo</gui> i zobacz, jaki <gui>Kodek</gui> jest wyświetlany w sekcjach <gui>Wideo</gui> i <gui>Dźwięk</gui> (jeśli plik z filmem ma też dźwięk).</p>
    </item>
  </list>

  <p>Zapytaj osobę mającą problem z odtwarzaniem, czy ma zainstalowany właściwy kodek. Pomocne może być wyszukanie w Internecie nazwy kodeka plus nazwy odtwarzacza filmów. Na przykład, jeśli film używa formatu <em>Theora</em>, a znajoma używa programu Windows Media Player do jego odtworzenia, to należy wyszukać „theora windows media player”. Często można bezpłatnie pobrać właściwy kodek, jeśli nie jest zainstalowany.</p>

  <p>Jeśli nie można odnaleźć właściwego kodeku, to wypróbuj program <link href="http://www.videolan.org/vlc/">VLC</link>. Działa on w systemach Windows i macOS, a także Linux, i obsługuje wiele różnych formatów wideo. Jeśli to się nie powiedzie, to spróbuj konwertować film na inny format. Większość edytorów wideo może to zrobić, a dostępne są także specjalistyczne programy do konwersji filmów. Poszukaj dostępnych programów w instalatorze oprogramowania.</p>

  <note>
    <p>Kilka innych problemów może uniemożliwiać komuś odtwarzanie filmu. Mógł on zostać uszkodzony podczas wysyłania (czasami duże pliki nie są kopiowane idealnie), odtwarzacz filmów może działać nieprawidłowo, albo film nie został poprawnie utworzony (mogły wystąpić błędy podczas jego zapisywania).</p>
  </note>

</page>
