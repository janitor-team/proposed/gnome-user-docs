<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" version="1.0 if/1.0" id="shell-notifications" xml:lang="pl">

  <info>
    <link type="guide" xref="shell-overview#desktop"/>

    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>
    <revision pkgversion="40" date="2021-09-10" status="review"/>

    <credit type="author">
      <name>Marina Zhurakhinskaya</name>
      <email>marinaz@redhat.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Komunikaty o pewnych zdarzeniach są wyświetlane na górze ekranu.</desc> 
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

<title>Powiadomienia i lista powiadomień</title>

<section id="what">
  <title>Czym jest powiadomienie?</title>

  <p>Jeśli program lub składnik systemu chce zwrócić na siebie uwagę użytkownika, to na górze ekranu lub na ekranie blokady zostanie wyświetlone powiadomienie.</p>

  <p>Na przykład, po otrzymaniu nowej wiadomości z komunikatora lub wiadomości e-mail zostanie wyświetlone powiadomienie o tym informujące. Powiadomienia z komunikatora są wyświetlane inaczej: jako wiadomości od konkretnych kontaktów.</p>

<!--  <p>To minimize distraction, some notifications first appear as a single line.
  You can move your mouse over them to see their full content.</p>-->

  <p>Inne powiadomienia mają przyciski z opcjami. Aby zamknąć takie powiadomienie bez wybierania żadnej opcji, kliknij przycisk zamknięcia.</p>

  <p>Kliknięcie przycisku zamknięcia na niektórych powiadomieniach je odrzuca. Inne, takie jak powiadomienia odtwarzacza muzyki lub komunikatora, będą ukryte na liście powiadomień.</p>

</section>

<section id="notificationlist">

  <title>Lista powiadomień</title>

  <p>Lista powiadomień umożliwia powrót do powiadomień w dogodnej chwili. Jest wyświetlany po kliknięciu zegara lub naciśnięciu klawiszy <keyseq><key xref="keyboard-key-super">Super</key><key>V</key></keyseq>. Lista powiadomień zawiera wszystkie powiadomienia, na które nie zareagowano lub które są na niej na stałe.</p>

  <p>Można wyświetlić powiadomienie klikając je na liście. Można zamknąć listę powiadomień naciskając klawisze <keyseq><key>Super</key><key>V</key></keyseq> ponownie lub klawisz <key>Esc</key>.</p>

  <p>Kliknij przycisk <gui>Wyczyść wszystko</gui>, aby opróżnić listę powiadomień.</p>

</section>

<section id="hidenotifications">

  <title>Ukrywanie powiadomień</title>

  <p>Jeśli użytkownik jest w trakcie pracy i nie chce, żeby mu przeszkadzać, to można wyłączyć powiadomienia.</p>

  <p>Można ukryć wszystkie powiadomienia otwierając listę powiadomień i przełączając <gui>Nie przeszkadzać</gui> na dole lub zamiast tego:</p>

  <steps>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Ustawienia</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Ustawienia</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Powiadomienia</gui> na panelu bocznym, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Przełącz <gui>Nie przeszkadzać</gui> na <gui>◯</gui> (wyłączone).</p>
    </item>
  </steps>

  <p>Po wyłączeniu większość powiadomień nie będzie wyświetlanych na górze ekranu. Powiadomienia będą nadal dostępne na liście powiadomień po jej wyświetleniu (klikając zegar lub naciskając klawisze <keyseq><key>Super</key><key>V</key></keyseq>), i będą wyświetlane z powrotem po przełączeniu ich na <gui>|</gui> (włączone).</p>

  <p>Można także wyłączyć lub ponownie włączyć powiadomienia dla poszczególnych programów w panelu <gui>Powiadomienia</gui>.</p>

</section>

<section id="lock-screen-notifications">

  <title>Ukrywanie powiadomień na ekranie blokady</title>

  <p>Kiedy ekran jest zablokowany, powiadomienia są wyświetlane na ekranie blokady. Można skonfigurować ukrywanie tych powiadomień na ekranie blokady w celu ochrony prywatności.</p>

  <steps>
    <title>Aby wyłączyć powiadomienia, kiedy ekran jest zablokowany:</title>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Ustawienia</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Ustawienia</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Powiadomienia</gui> na panelu bocznym, aby otworzyć panel.</p>
    </item>
    <item><p>Przełącz <gui>Powiadomienia na ekranie blokady</gui> na <gui>◯</gui> (wyłączone).</p></item>
  </steps>

</section>

</page>
