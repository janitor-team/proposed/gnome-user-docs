<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-font-size" xml:lang="pl">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.26" date="2017-08-04" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Większe czcionki ułatwiają czytanie tekstu.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

  <title>Zmiana rozmiaru tekstu na ekranie</title>

  <p>Jeśli czytanie tekstu na ekranie sprawia trudności, to można zmienić rozmiar czcionki.</p>

  <steps>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Ułatwienia dostępu</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Ułatwienia dostępu</gui>, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Przełącz <gui>Duży tekst</gui> na <gui>|</gui> (włączone) w sekcji <gui>Wzrok</gui>.</p>
    </item>
  </steps>

  <p>Można także szybko zmieniać rozmiar tekstu klikając <link xref="a11y-icon">ikonę ułatwień dostępu</link> na górnym pasku i wybierając <gui>Duży tekst</gui>.</p>

  <note style="tip">
    <p>Wiele programów umożliwia zwiększanie rozmiaru tekstu w dowolnej chwili za pomocą klawiszy <keyseq><key>Ctrl</key><key>+</key></keyseq>. Aby zmniejszyć rozmiar tekstu, naciśnij klawisze <keyseq><key>Ctrl</key><key>-</key></keyseq>.</p>
  </note>

  <p>Funkcja <gui>Duży tekst</gui> powiększy tekst o 20%. Program <app>Dostrajanie</app> umożliwia dalsze powiększanie lub zmniejszanie rozmiaru tekstu.</p>

</page>
