<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="color-calibrationcharacterization" xml:lang="pl">

  <info>

    <link type="guide" xref="color#calibration"/>

    <desc>Kalibracja i charakterystyka to zupełnie różne rzeczy.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

  <title>Czym różni się kalibracja od charakterystyki?</title>
  <p>Różnica między kalibracją a charakteryzacją na początku jest myląca dla wielu osób. Kalibracja to proces modyfikowania zachowania kolorów urządzenia. Używa się do tego zwykle dwóch mechanizmów:</p>
  <list>
    <item><p>Zmiana wewnętrznych ustawień urządzenia</p></item>
    <item><p>Zastosowanie krzywych na kanałach kolorów urządzenia</p></item>
  </list>
  <p>Kalibracja polega na ustawieniu urządzenia w określonym stanie odpowiedzi kolorów. Często jest używana jako codzienny sposób na utrzymywanie powtarzalnego zachowania. Kalibracja zwykle jest przechowywana w formacie pliku dla konkretnego urządzenia lub systemu, który nagrywa ustawienia urządzenia i krzywe kalibracji dla każdego kanału.</p>
  <p>Charakteryzacja (lub profilowanie) to <em>nagrywanie</em> sposobu, w jaki urządzenie odtwarza lub odpowiada na kolory. Zwykle wynik jest przechowywany w profilu ICC urządzenia. Taki profil sam z siebie nie modyfikuje kolorów w żaden sposób. Umożliwia systemowi, takiemu jak CMM (moduł zarządzania kolorami), lub programowi rozumiejącemu kolory modyfikować kolory po połączeniu z profilem innego urządzenia. Tylko znając charakterystykę dwóch urządzeń można uzyskać sposób na przenoszenie kolorów z jednej reprezentacji urządzenia na drugą.</p>
  <note>
    <p>Zauważ, że charakteryzacja (profil) jest prawidłowa dla urządzenia tylko, jeśli jest w tym samym stanie kalibracji, co podczas procesu charakteryzacji.</p>
  </note>
  <p>Profile ekranów są dodatkowo mylące, ponieważ często informacje o kalibracji są dla wygody przechowywane w profilu. Zwyczajowo są one w znaczniku o nazwie <em>vcgt</em>. Chociaż te informacje są przechowywane w profilu, to żadne ze zwykłych narzędzi opartych na ICC ani programów o tym nie wiedzą, ani nic z nimi nie robią. Podobnie typowe narzędzia do kalibracji ekranów i programy nic nie wiedzą, ani nic nie robią z informacjami o charakteryzacji (profilu) ICC.</p>

</page>
