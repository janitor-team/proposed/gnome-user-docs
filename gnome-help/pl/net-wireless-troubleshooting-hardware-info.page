<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting-hardware-info" xml:lang="pl">

  <info>
    <link type="next" xref="net-wireless-troubleshooting-hardware-check"/>
    <link type="guide" xref="net-wireless-troubleshooting"/>

    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Współtwórcy wiki dokumentacji Ubuntu</name>
    </credit>
    <credit type="author">
      <name>Projekt dokumentacji GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>W kolejnych krokach rozwiązywania problemów mogą być potrzebne takie informacje, jak numer modelu adaptera bezprzewodowego.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

  <title>Rozwiązywanie problemów z siecią bezprzewodową</title>
  <subtitle>Zbierz informacje o sprzęcie sieciowym</subtitle>

  <p>W tym kroku zbierzemy informacje o używanym bezprzewodowym urządzeniu sieciowym. Sposób naprawiania wielu problemów zależy od producenta i numeru modelu adaptera bezprzewodowego, więc należy zapisać te informacje. Rzeczy dołączone do komputera, takie jak płyty instalacji sterowników, także mogą być przydatne. Poszukaj poniższych rzeczy, jeśli nadal je masz.</p>

  <list>
    <item>
      <p>Opakowania i instrukcje urządzeń bezprzewodowych (przede wszystkim podręcznik użytkownika routera).</p>
    </item>
    <item>
      <p>Płyta zawierająca sterowniki adaptera bezprzewodowego (nawet jeśli tylko dla systemu Windows).</p>
    </item>
    <item>
      <p>Producenci i numery modeli komputera, adaptera bezprzewodowego i routera. Zwykle można znaleźć te informacje na spodzie lub odwrocie urządzenia.</p>
    </item>
    <item>
      <p>Wszystkie numery wersji widoczne na urządzeniach bezprzewodowych lub ich opakowaniach. Są one bardzo przydatne, więc spróbuj je znaleźć.</p>
    </item>
    <item>
      <p>Wszystko na płycie ze sterownikami, co identyfikuje urządzenie, wersję „oprogramowania sprzętowego” urządzenia lub procesor urządzenia.</p>
    </item>
  </list>

  <p>Jeśli to możliwe, spróbuj uzyskać dostęp do innego, działającego połączenia internetowego, aby w razie potrzeby można było pobrać oprogramowanie i sterowniki. Można na przykład podłączyć komputer bezpośrednio do routera za pomocą sieciowego kabla ethernetowego, ale należy to robić tylko w razie potrzeby.</p>

  <p>Po zebraniu jak największej liczby informacji, kliknij przycisk <gui>Następna</gui>.</p>

</page>
