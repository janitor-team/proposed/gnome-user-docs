<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-assignprofiles" xml:lang="pl">

  <info>
    <link type="guide" xref="color"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-why-calibrate"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-04" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc><guiseq><gui>Ustawienia</gui><gui>Kolor</gui></guiseq> umożliwia dodawanie profilu kolorów dla ekranu.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

  <title>Jak przydzielać profile urządzeniom?</title>

  <p>Można przydzielić profil kolorów dla ekranu lub drukarki, aby wyświetlane i drukowane kolory były dokładniejsze.</p>

  <steps>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Ustawienia</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Ustawienia</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Kolor</gui> na panelu bocznym, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Wybierz urządzenie, dla którego dodać profil.</p>
    </item>
    <item>
      <p>Kliknij przycisk <gui>Dodaj profil</gui>, aby wybrać istniejący profil lub zaimportować nowy profil.</p>
    </item>
    <item>
      <p>Kliknij przycisk <gui>Dodaj</gui>, aby potwierdzić wybór.</p>
    </item>
    <item>
      <p>Aby zmienić używany profil, zaznacz profil do użycia i kliknij przycisk <gui>Włącz</gui>, aby potwierdzić wybór.</p>
    </item>
  </steps>

  <p>Każde urządzenie może mieć przydzielonych wiele profili, ale tylko jeden może być <em>domyślny</em>. Domyślny profil jest używany, kiedy nie ma dodatkowych informacji umożliwiających automatyczny wybór profilu. Przykładem automatycznego wyboru jest przypadek, gdy jeden profil został utworzony dla papieru błyszczącego, a drugi dla zwykłego.</p>

  <p>Jeśli sprzęt do kalibracji jest podłączony, to przycisk <gui>Skalibruj…</gui> utworzy nowy profil.</p>

</page>
