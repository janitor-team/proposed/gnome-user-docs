<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-lost" xml:lang="ta">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>GNOME ஆவணமாக்கத் திட்டப்பணி</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>ஷான் மெக்கேன்ஸ்</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Follow these tips if you can’t find a file you created or
    downloaded.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

<title>இழந்த கோப்பைக் கண்டுபிடித்தல்</title>

<p>If you created or downloaded a file, but now you cannot find it, follow
these tips.</p>

<list>
  <item><p>If you do not remember where you saved the file, but you have some
  idea of how you named it, you can <link xref="files-search">search for the
  file by name</link>.</p></item>

  <item><p>If you just downloaded the file, your web browser might
  have automatically saved it to a common folder. Check the
  <file>Desktop</file> and <file>Downloads</file> folders in your home
  folder.</p></item>

  <item><p>நீங்கள் எதிர்பாராமல் கோப்பை அழித்திருக்கலாம். நீங்கள் ஒரு கோப்பை அழிக்கும் போது, அது குப்பைத் தொட்டிக்கு நகர்த்தப்படும், நீங்கள் கைமுறையாக குப்பைத் தொட்டியை காலி செய்யும் வரை அது அங்கேயே இருக்கும். அழித்த கோப்பை எப்படி மீட்டெடுப்பது என அறிய  <link xref="files-recover"/> ஐப் பார்க்கவும்.</p></item>

  <item><p>You might have renamed the file in a way that made the file hidden.
  Files that start with a <file>.</file> or end with a <file>~</file> are
  hidden in the file manager. Click the
  <!-- FIXME: Get a tooltip added for "View options" -->
  view options button in the <app>Files</app> toolbar and enable <gui>Show
  Hidden Files</gui> to display them. See <link xref="files-hidden"/> to learn
  more.</p></item>
</list>

</page>
