<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="ui" version="1.0 if/1.0" id="shell-workspaces" xml:lang="ta">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces" group="#first"/>

    <revision pkgversion="3.8.0" date="2013-04-23" status="review"/>
    <revision pkgversion="3.10.3" date="2014-01-26" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.35.91" date="2020-02-27" status="candidate"/>

    <credit type="author">
      <name>GNOME ஆவணமாக்கத் திட்டப்பணி</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>ஆன்ட்ரி க்ளாப்பர்</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>பணியிடங்கள் உங்கள் பணிமேசையில் உள்ள சாளரங்களைக் குழுப்படுத்த உதவுகின்றன.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

<title>பணியிடம் என்றால் என்ன, அது எப்படி எனக்கு உதவும்?</title>

  <p if:test="!platform:gnome-classic">உங்கள் பணிமேசையில் உள்ள சாளரங்களின் குழுக்களே பணியிடங்கள் ஆகும். நீங்கள் பல பணியிடங்களை உருவாக்கலாம், அவை ஒவ்வொன்றும் ஒரு பணிமேசை போல செயல்படும். பணிமேசை நிரம்பியில்லாமல் இருக்கவும் பணிமேசையில் எளிதாக நகரவும் வழி செய்யவே பணியிடங்கள் உருவாக்கப்பட்டன.</p>

  <p if:test="platform:gnome-classic">உங்கள் பணிமேசையில் உள்ள சாளரங்களின் குழுக்களே பணியிடங்கள் ஆகும். நீங்கள் பல பணியிடங்களைப் பயன்படுத்தலாம், அவை ஒவ்வொன்றும் ஒரு பணிமேசை போல செயல்படும். பணிமேசை நிரம்பியில்லாமல் இருக்கவும் பணிமேசையில் எளிதாக நகரவும் வழி செய்யவே பணியிடங்கள் உருவாக்கப்பட்டன.</p>

  <p>பணியிடங்கள் உங்கள் பணியை ஒழுங்கமைக்கப் பயன்படும். உதாரனமாக நீங்கள் மின்னஞ்சல், அரட்டை பயன்பாடு போன்ற தகவல் தொடர்பு சாளரங்கள் அனைத்தையும் ஒரு பணியிடத்திலும், நீங்கள் வேலை செய்யும் சாளரத்தை வேறொரு பணியிடத்திலும் வைத்துக்கொள்ள முடியும். உங்கள் இசை நிர்வாகி மூன்றாவது பணியிடத்தில் வைக்கப்படலாம்.</p>

<p>பணியிடங்களைப் பயன்படுத்துதல்:</p>

<list>
  <item>
    <p if:test="!platform:gnome-classic">In the
    <gui xref="shell-introduction#activities">Activities</gui> overview, you can
    horizontally navigate between the workspaces.</p>
    <p if:test="platform:gnome-classic">Click the button at the bottom left of
    the screen in the window list, or press the
    <key xref="keyboard-key-super">Super</key> key to open the
    <gui>Activities</gui> overview.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">If more than one workspace is already
    in use, the <em>workspace selector</em> is shown between the search field and
    the window list. It will display currently used workspaces plus an empty workspace.</p>
    <p if:test="platform:gnome-classic">In the bottom right corner, you see four
    boxes. This is the workspace selector.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">To add a workspace, drag and drop a
    window from an existing workspace onto the empty workspace in the workspace
    selector. This workspace now contains the window you have dropped, and a new
    empty workspace will appear next to it.</p>
    <p if:test="platform:gnome-classic">Drag and drop a window from your current
    workspace onto an empty workspace in the workspace selector. This workspace
    now contains the window you have dropped.</p>
  </item>
  <item if:test="!platform:gnome-classic">
    <p>ஒரு பணியிடத்தை நீக்க, அனைத்து சாளரங்களையும் மூடலாம் அல்லது அவற்றை வேறு பணியிடங்களுக்கு நகர்த்தலாம்.</p>
  </item>
</list>

<p if:test="!platform:gnome-classic">எப்போதும் குறைந்தது ஒரு பணியிடம் இருக்கும்.</p>

    <media its:translate="yes" type="image" src="figures/shell-workspaces.png" width="940" height="291">
        <p>பணியிடப் தேர்வி</p>
    </media>

</page>
