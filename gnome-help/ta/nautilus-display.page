<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-display" xml:lang="ta">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-display"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>
    <revision pkgversion="40.2" date="2021-08-25" status="candidate"/>

    <credit type="author">
      <name>ஷான் மெக்கேன்ஸ்</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>கோப்பு மேலாளரில் பயன்படுத்தப்படும் கட்டுப்பாடு சின்னங்களின் தலைப்புகளைக் கட்டுப்படுத்துதல்.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

<title>கோப்பு மேலாளர் காட்சி முன்னுரிமைகள்</title>

<p>You can control how the file manager displays captions under icons. Click
the menu button in the top-right corner of the window, select
<gui>Preferences</gui>, then go to the <gui>Icon View Captions</gui> section.</p>

<section id="icon-captions">
  <title>சின்னத் தலைப்புகள்</title>
  <!-- TODO: update screenshot for 3.18 and above. -->
  <media type="image" src="figures/nautilus-icons.png" width="250" height="110" style="floatend floatright">
    <p>தலைப்புகள் கொண்ட கோப்பு மேலாளர் சின்னங்கள்</p>
  </media>
  <p>நீங்கள் சின்னங்கள் காட்சிவகையைப் பயன்படுத்தும் போது, ஒவ்வொரு சின்னத்தின் கீழும் தலைப்பில் கோப்புகள் மற்றும் கோப்புறைகள் பற்றிய கூடுதல் தகவல் காண்பிக்கும்படி அமைக்க முடியும். ஒரு கோப்புக்கு யார் உரிமையாளர் என்று அல்லது அது கடைசியாக யார் திருத்தினார்கள் என்பதை நீங்கள் அடிக்கடி பார்ப்பீர்களானால், இது பயனுள்ளதாக இருக்கும்.</p>
  <p>You can zoom in a folder by clicking the view options button in the
  <!-- FIXME: Get a tooltip added for "View options" -->
  toolbar and choosing a zoom level with the slider. As you zoom in, the
  file manager will display more and more information in captions. You can
  choose up to three things to show in captions. The first will be displayed at
  most zoom levels. The last will only be shown at very large sizes.</p>
  <p>நீங்கள் சின்னங்களின் தலைப்புகளில் காட்டும்படி அமைக்கின்ற தகவல்கள், பட்டியல் காட்சியில் நீங்கள் நெடுவரிசைகளாகக் காணும் அதே தகவல்களே. மேலும் தகவலுக்கு <link xref="nautilus-list"/> ஐப் பார்க்கவும்.</p>
</section>

<section id="list-view">

  <title>List View</title>

  <p>When viewing files as a list, you can display <gui>Expandable Folders in
  List View</gui>. This shows expanders on each directory in the file list, so
  that the contents of several folders can be shown at once. This is useful if
  the folder structure is relevant, such as if your music files are organized
  with a folder per artist, and a subfolder per album.</p>

</section>

</page>
