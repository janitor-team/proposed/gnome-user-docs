<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-usespeakers" xml:lang="ta">

  <info>
    <link type="guide" xref="media#sound"/>
    <link type="seealso" xref="sound-usemic"/>

    <revision version="gnome:40" date="2021-02-26" status="candidate"/>

    <credit type="author">
      <name>ஷான் மெக்கேன்ஸ்</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>எக்காட்டெரினா ஜெராசிமோவா</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>ஸ்பீக்கர்கள் அல்லது ஹெட்ஃபோன்களை இணைத்து ஒரு முன்னிருப்பு ஆடியோ வெளியீடு சாதனத்தைத் தேர்ந்தெடுக்கவும்.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>வேறு ஸ்பீக்கர்கள் அல்லது ஹெட்ஃபோன்களைப் பயன்படுத்துதல்</title>

  <p>You can use external speakers or headphones with your computer. Speakers
  usually either connect using a circular TRS (<em>tip, ring, sleeve</em>) plug
  or a USB.</p>

  <p>If your speakers or headphones have a TRS plug, plug it into the
  appropriate socket on your computer. Most computers have two sockets: one for
  microphones and one for speakers. This socket is usually light green in color
  or is accompanied by a picture of headphones. Speakers or headphones
  plugged into a TRS socket are usually used by default. If not, see the
  instructions below for selecting the default device.</p>

  <p>Some computers support multi-channel output for surround sound. This
  usually uses multiple TRS jacks, which are often color-coded. If you are
  unsure which plugs go in which sockets, you can test the sound output in the
  sound settings.</p>

  <p>உங்களிடம் USB ஸ்பீக்கர்கள் அல்லது ஹெட்ஃபோன்கள் இருந்தால் அல்லது அனலாக் ஹெட்ஃபோன்கள் USB சவுன்ட் கார்டில் செருகப்பட்டிருந்தால், அவற்றை ஒரு USB போர்ட்டில் செருகவும். USB ஸ்பீக்கர்கள் தனி ஆடியோ சாதனங்களாக செயல்படும், எந்த ஸ்பீக்கர்களை முன்னிருப்பாக பயன்படுத்த வேண்டும் என்பதை நீங்கள் குறிப்பிட வேண்டி இருக்கும்.</p>

  <steps>
    <title>Select a default audio output device</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>Sound</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Sound</gui> to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Output</gui> section, select the device that you want to
      use.</p>
    </item>
  </steps>

  <p>Use the <gui style="button">Test</gui> button to check that all
  speakers are working and are connected to the correct socket.</p>

</page>
