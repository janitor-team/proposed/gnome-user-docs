<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" version="1.0 if/1.0" id="shell-introduction" xml:lang="ta">

  <info>
    <link type="guide" xref="shell-overview" group="#first"/>
    <link type="guide" xref="index" group="intro"/>

    <revision pkgversion="3.6.0" date="2012-10-13" status="review"/>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-28" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="review"/>
    <revision pkgversion="3.35.91" date="2020-07-19" status="review"/>
    <revision version="gnome:40" date="2021-06-16" status="review"/>

    <credit type="author">
      <name>ஷான் மெக்கேன்ஸ்</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>எக்காட்டெரினா ஜெராசிமோவா</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>ஆன்ட்ரி க்ளாப்பர்</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>A visual overview of your desktop, the top bar, and the
    <gui>Activities</gui> overview.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>Visual overview of GNOME</title>

  <p>GNOME features a user interface designed to stay out of your way, minimize
  distractions, and help you get things done. When you first log in, you will
  see the <gui>Activities</gui> overview and the top bar.</p>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-top-bar.png" width="600" if:test="!target:mobile">
      <p>GNOME ஷெல் மேல் பட்டி</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-top-bar-classic.png" width="500" if:test="!target:mobile">
      <p>GNOME ஷெல் மேல் பட்டி</p>
    </media>
  </if:when>
</if:choose>

  <p>The top bar provides access to your windows and applications, your
  calendar and appointments, and
  <link xref="status-icons">system properties</link> like sound, networking,
  and power. In the system menu in the top bar, you can change the volume or
  screen brightness, edit your <gui>Wi-Fi</gui> connection details, check your
  battery status, log out or switch users, and turn off your computer.</p>

<links type="section"/>

<!-- TODO: Replace "Activities overview" title for classic mode with something
like "Application windows" by using if:when and if:else ? -->
<section id="activities">
  <title><gui>செயல்பாடுகள்</gui> மேலோட்டம்</title>

  <p if:test="!platform:gnome-classic">When you start GNOME, you automatically
  enter the <gui>Activities</gui> overview. The overview allows you to access
  your windows and applications. In the overview, you can also just start
  typing to search your applications, files, folders, and the web.</p>

  <p if:test="!platform:gnome-classic">To access the overview at any time,
  click the <gui>Activities</gui> button, or just move your mouse pointer to
  the top-left hot corner. You can also press the
  <key xref="keyboard-key-super">Super</key> key on your keyboard.</p>

  <p if:test="platform:gnome-classic">To access your windows and applications,
  click the button at the bottom left of the screen in the window list. You can
  also press the <key xref="keyboard-key-super">Super</key> key to see an
  overview with live thumbnails of all the windows on the current workspace.</p>

  <media type="image" its:translate="no" src="figures/shell-activities-dash.png" height="65" style="floatend floatright" if:test="!target:mobile, !platform:gnome-classic">
    <p>Activities button and Dash</p>
  </media>
  <p if:test="!platform:gnome-classic">At the bottom of the overview, you will find the <em>dash</em>. The dash
  shows you your favorite and running applications. Click any icon in the
  dash to open that application; if the application is already running, it will
  have a small dot below its icon. Clicking its icon will bring up the most
  recently used window. You can also drag the icon onto a workspace.</p>

  <p if:test="!platform:gnome-classic">சின்னத்தை வலது சொடுக்கினால் ஒரு மெனு காட்டப்படும், அதில் நீங்கள் இயங்கிக்கொண்டிருக்கும் பயன்பாட்டின் ஒரு சாளரத்தைத் தேர்வு செய்யலாம் அல்லது புதிய சாளரத்தைத் திறக்கலாம். ஒரு புதிய சாளரத்தைத் திறக்க நீங்கள் <key>Ctrl</key> ஐ அழுத்திக் கொண்டும் சின்னத்தை சொடுக்கலாம்.</p>

  <p if:test="!platform:gnome-classic">நீங்கள் மேலோட்டத்திற்குச் செல்லும் போது, நீங்கள் முதலில் சாளரங்கள் மேலோட்டத்தில் இருப்பீர்கள். இது நடப்பு பணியிடத்தின் அனைத்து சாளரங்களின் சிறுபடங்களைக் காண்பிக்கும்.</p>

  <p if:test="!platform:gnome-classic">Click the grid button (which has nine dots) in the dash to display the
  applications overview. This shows you all the applications installed on your
  computer. Click any application to run it, or drag an application to the
  onto a workspace shown above the installed applications. You can also drag an application onto
  the dash to make it a favorite. Your favorite applications stay in the dash
  even when they’re not running, so you can access them quickly.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-apps-open">பயன்பாடுகளைத் தொடங்குவது பற்றி மேலும் அறிக. </link></p>
    </item>
    <item>
      <p><link xref="shell-windows">சாளரங்கள் மற்றும் பணியிடங்கள் பற்றி மேலும் அறிக.</link></p>
    </item>
  </list>

</section>

<section id="appmenu">
  <title>பயன்பாடு மெனு</title>
  <if:choose>
    <if:when test="!platform:gnome-classic">
      <media type="image" src="figures/shell-appmenu-shell.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>App Menu of <app>Terminal</app></p>
      </media>
      <p>Application menu, located beside the <gui>Activities</gui> button,
      shows the name of the active application alongside with its icon and
      provides quick access to windows and details of the application, as well
      as a quit item.</p>
    </if:when>
    <!-- TODO: check how the app menu removal affects classic mode -->
    <if:when test="platform:gnome-classic">
      <media type="image" src="figures/shell-appmenu-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>App Menu of <app>Terminal</app></p>
      </media>
      <p>Application menu, located next to the <gui>Applications</gui> and
      <gui>Places</gui> menus, shows the name of the active application
      alongside with its icon and provides quick access to application
      preferences or help. The items that are available in the application menu
      vary depending on the application.</p>
    </if:when>
  </if:choose>

</section>

<section id="clock">
  <title>கடிகாரம், நாள்காட்டி &amp; சந்திப்புத்திட்டங்கள்</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-appts.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Clock, calendar, appointments and notifications</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-appts-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>கடிகாரம், நாள்காட்டி மற்றும் சந்திப்புத்திட்டங்கள்</p>
    </media>
  </if:when>
</if:choose>

  <p>Click the clock on the top bar to see the current date, a month-by-month
  calendar, a list of your upcoming appointments and new notifications. You can
  also open the calendar by pressing
  <keyseq><key>Super</key><key>M</key></keyseq>. You can access the date and
  time settings and open your full calendar application directly from
  the menu.</p>

  <list style="compact">
    <item>
      <p><link xref="clock-calendar">நாள்காட்டி மற்றும் சந்திப்புத் திட்டங்கள் பற்றி மேலும் அறிக.</link></p>
    </item>
    <item>
      <p><link xref="shell-notifications">Learn more about notifications and
      the notification list.</link></p>
    </item>
  </list>

</section>


<section id="systemmenu">
  <title>System menu</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-exit.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>பயனர் மெனு</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-exit-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>பயனர் மெனு</p>
    </media>
  </if:when>
</if:choose>

  <p>Click the system menu in the top-right corner to manage your system
  settings and your computer.</p>

<!-- TODO: Update for 3.36 UI option "Do Not Disturb" in calendar dropdown:

<p>If you set yourself to Unavailable, you won’t be bothered by message popups
at the bottom of your screen. Messages will still be available in the message
tray when you move your mouse to the bottom-right corner. But only urgent
messages will be presented, such as when your battery is critically low.</p>
-->

  <p>When you leave your computer, you can lock your screen to prevent other
  people from using it. You can also quickly switch users without logging out
  completely to give somebody else access to the computer, or you can
  suspend or power off the computer from the menu. If you have a screen 
  that supports vertical or horizontal rotation, you can quickly rotate the 
  screen from the system menu. If your screen does not support rotation, 
  you will not see the button.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-exit">பயனர்களை மாற்றுதல், விடுபதிகை மற்றும் கணினியை அணைத்தல் பற்றி மேலும் அறிக.</link></p>
    </item>
  </list>

</section>

<section id="lockscreen">
  <title>பூட்டுத் திரை</title>

  <p>When you lock your screen, or it locks automatically, the lock screen is
  displayed. In addition to protecting your desktop while you’re away from your
  computer, the lock screen displays the date and time. It also shows
  information about your battery and network status.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-lockscreen">பூட்டுத் திரை பற்றி மேலும் அறிக.</link></p>
    </item>
  </list>

</section>

<section id="window-list">
  <title>சாளர பட்டியல்</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <p>GNOME இல் மற்ற பணிமேசைகளில் இருப்பது போன்ற எப்போதும் தெரியும் சாளர பட்டையல் போன்று இல்லாமல் சாளரங்களிடையே மாறுவதற்கு புதிய அணுகுமுறையைக் கொண்டுள்ளது. இது கவனச்சிதறல்கள் இல்லாமல் உங்கள் வேலையில் கவனம் செலுத்த உதவும்.</p>
    <list style="compact">
      <item>
        <p><link xref="shell-windows-switching">சாளரங்களை மாற்றுதல் குறித்து மேலும் அறிக. </link></p>
      </item>
    </list>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-window-list-classic.png" width="800" if:test="!target:mobile">
      <p>சாளர பட்டியல்</p>
    </media>
    <p>திரையின் கீழே உள்ள சாளர பட்டியலில் திறந்துள்ள உங்கள் சாளரங்கள் மற்றும் பயன்பாடுகள் அனைத்துக்கும் அணுகலை வழங்கும், இதைக் கொண்டு நீங்கள் அவற்றை விரைவில் சிறிதாக்கவும் மீட்டமைக்கவும் முடியும்.</p>
    <p>At the right-hand side of the window list, GNOME displays the four
    workspaces. To switch to a different workspace, select the workspace you
    want to use.</p>
  </if:when>
</if:choose>

</section>

</page>
