<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:itst="http://itstool.org/extensions/" type="topic" style="task" id="nautilus-file-properties-permissions" xml:lang="ta">

  <info>
    <its:rules xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="files#faq"/>
    <link type="seealso" xref="nautilus-file-properties-basic"/>

    <desc>உங்கள் கோப்புகள் மற்றும் கோப்புறைகளை யாரெல்லாம் காண மற்றும் திருத்த முடியும் என்பதை கட்டுப்படுத்துதல்.</desc>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>

    <credit type="author">
      <name>டிஃபானி அன்ட்டொபோல்ஸ்கி</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>ஷான் மெக்கேன்ஸ்</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>
  <title>கோப்பு அனுமதிகளை அமைத்தல்</title>

  <p>நீங்கள் உங்களுக்கு சொந்தமான கோப்புகளை யாரெல்லாம் பார்க்க, திருத்த முடியும் என்பதைக் கட்டுப்படுத்த கோப்பு அனுமதிகளைப் பயன்படுத்தலாம். ஒரு கோப்பு அனுமதிகளைக் காண மற்றும் அமைக்க, அதை வலது சொடுக்கம் செய்து, <gui>பண்புகள்</gui> ஐ தேர்ந்தெடுத்து பிறகு <gui>அனுமதிகள்</gui> தாவலைத் தேர்ந்தெடுக்கவும்.</p>

  <p>நீங்கள் அமைக்கக்கூடியா அனுமதிகளின் வகைகளைப் பற்றிய விவரமறிய <link xref="#files"/> மற்றும் <link xref="#folders"/> ஐப் பார்க்கவும்.</p>

  <section id="files">
    <title>கோப்புகள்</title>

    <p>You can set the permissions for the file owner, the group owner,
    and all other users of the system. For your files, you are the owner,
    and you can give yourself read-only or read-and-write permission.
    Set a file to read-only if you don’t want to accidentally change it.</p>

    <p>Every user on your computer belongs to a group. On home computers,
    it is common for each user to have their own group, and group permissions
    are not often used. In corporate environments, groups are sometimes used
    for departments or projects. As well as having an owner, each file belongs
    to a group. You can set the file’s group and control the permissions for
    all users in that group. You can only set the file’s group to a group you
    belong to.</p>

    <p>You can also set the permissions for users other than the owner and
    those in the file’s group.</p>

    <p>If the file is a program, such as a script, you must select <gui>Allow
    executing file as program</gui> to run it. Even with this option selected,
    the file manager will still open the file in an application. See
    <link xref="nautilus-behavior#executable"/> for more information.</p>
  </section>

  <section id="folders">
    <title>கோப்புறைகள்</title>
    <p>நீங்கள் கோப்புறையில் உரிமையாளர், குழு மற்றும் பிற பயனர்களுக்கு அனுமதிகளை அமைக்க முடியும். உரிமையாளர்கள், குழுக்கள் மற்றும் பிற பயனர்களின் விளக்கத்திற்கு மேலே உள்ள கோப்பு அனுமதிகள் பற்றிய விவரங்களைப் பார்க்கவும்.</p>
    <p>நீங்கள் ஒரு கோப்புறைக்கு அமைக்கக்கூடிய அனுமதிகள் ஒரு கோப்புக்கு அமைக்கக்கூடிய அனுமதிகளில் இருந்து வேறுபட்டது.</p>
    <terms>
      <item>
        <title><gui itst:context="permission">None</gui></title>
        <p>கோப்புறையில் என்ன கோப்புகள் உள்ளது என்பதைக் கூட பயனரால் பார்க்க முடியாது.</p>
      </item>
      <item>
        <title><gui>கோப்புகளின் பட்டியல் மட்டும்</gui></title>
        <p>பயனர் கோப்புறையில் என்ன கோப்புகள் உள்ளன எனப் பார்க்க முடியும், ஆனால் கோப்புகளை திறக்க, உருவாக்க அல்லது அழிக்க முடியாது.</p>
      </item>
      <item>
        <title><gui>கோப்புகளை அணுக</gui></title>
        <p>பயனர் கோப்புறையில் உள்ள கோப்புகளைத் திறக்க முடியும் (குறிப்பிட்ட கோப்பில் அவர்கள் அவ்வாறு செய்ய அனுமதி வழங்கப்பட்டிருந்தால்) ஆனால் புதிய கோப்புகளை உருவாக்க அல்லது கோப்புகளை அழிக்க முடியாது.</p>
      </item>
      <item>
        <title><gui>கோப்புகளை உருவாக்க மற்றும் அழிக்க</gui></title>
        <p>பயனருக்கு கோப்புகளை திறத்தல், உருவாக்குதல் மற்றும் அழித்தல் உட்பட கோப்புறைக்கான முழு அணுகல் இருக்கும்.</p>
      </item>
    </terms>

    <p>நீங்கள் <gui>உள்ளே உள்ள கோப்புகளுக்கான அனுமதிகளை மாற்று</gui> என்பதை சொடுக்குவதன் மூலம் கோப்புறையில் உள்ள அனைத்து கோப்புகளுக்கும் விரைவாக கோப்பு அனுமதிகளை அமைக்க முடியும். அங்குள்ள கோப்புகள் அல்லது கோப்புறைகளின் அனுமதிகளை சரிசெய்ய கீழ் தோன்று பட்டியலைப் பயன்படுத்தி <gui>மாற்று</gui> என்பதை சொடுக்கவும். அனுமதிகள் கோப்புறைகளுக்கும் கோப்புகளுக்கும் கோப்புறைகளில் உள்ளே உள்ளே உள்ள அனைத்து உப கோப்புறைகளுக்கும் பயன்படுத்தப்படுகிறது.</p>
  </section>

</page>
