<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="tips-specialchars" xml:lang="ta">

  <info>
    <link type="guide" xref="tips"/>
    <link type="seealso" xref="keyboard-layouts"/>

    <revision pkgversion="3.8.2" version="0.3" date="2013-05-18" status="review"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.26" date="2017-11-27" status="review"/>
    <revision version="gnome:40" date="2021-03-02" status="review"/>

    <credit type="author">
      <name>ஷான் மெக்கேன்ஸ்</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>எக்காட்டெரினா ஜெராசிமோவா</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>ஆன்ட்ரி க்ளாப்பர்</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Type characters not found on your keyboard, including foreign alphabets, mathematical symbols, emoji, and dingbats.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>சிறப்பு எழுத்துக்களை உள்ளிடுதல்</title>

  <p>You can enter and view thousands of characters from most of the world’s
  writing systems, even those not found on your keyboard. This page lists
  some different ways you can enter special characters.</p>

  <links type="section">
    <title>எழுத்துக்களை உள்ளிடும் முறைகள்</title>
  </links>

  <section id="characters">
    <title>Characters</title>
    <p>The character map application allows you to find and insert unusual
    characters, including emoji, by browsing character categories or searching
    for keywords.</p>

    <p>You can launch <app>Characters</app> from the Activities overview.</p>

  </section>

  <section id="emoji">
    <title>Emoji</title>
    <steps>
      <title>Insert emoji</title>
      <item>
        <p>Press <keyseq><key>Ctrl</key><key>;</key></keyseq>.</p>
      </item>
      <item>
        <p>Browse the categories at the bottom of the dialog or start typing a
    description in the search field.</p>
      </item>
      <item>
        <p>Select an emoji to insert.</p>
      </item>
    </steps>
  </section>

  <section id="compose">
    <title>கம்போஸ் விசை</title>
    <p>கம்போஸ் விசை என்பது, ஒரு வரிசையிலுள்ள பல விசைகளை ஒன்றாக அழுத்தி ஒரு சிறப்பு எழுத்தை வரவழைக்க உதவும் விசையாகும். உதாரணமாக நீங்கள் உச்சரிப்பு அழுத்தம் கொண்ட<em>é</em> என்ற எழுத்தை தட்டச்சு செய்ய <key>கம்போஸ்</key> விசையை அழுத்தி பிறகு <key>'</key> பிறகு <key>e</key> ஐ அழுத்தலாம்.</p>
    <p>Keyboards don’t have specific compose keys. Instead, you can define
    one of the existing keys on your keyboard as a compose key.</p>

    <steps>
      <title>ஒரு கம்போஸ் விசையை வரையறுத்தல்</title>
      <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
      </item>
      <item>
        <p>Click on <gui>Settings</gui>.</p>
      </item>
      <item>
        <p>Click <gui>Keyboard</gui> in the sidebar to open the panel.</p>
      </item>
      <item>
        <p>In the <gui>Type Special Characters</gui> section, click <gui>Compose Key</gui>.</p>
      </item>
      <item>
        <p>Turn the switch on for the <gui>Compose Key</gui>.</p>
      </item>
      <item>
        <p>Tick the checkbox of the key that you want to set as the Compose
        key.</p>
      </item>
      <item>
        <p>Close the dialog.</p>
      </item>
    </steps>

    <p>நீங்கள் கம்போஸ் விசையைப் பயன்படுத்தி பல பொதுவான எழுத்துக்களைத் தட்டச்சு செய்யலாம், உதாரணமாக:</p>

    <list>
      <item><p><key>compose</key> ஐ அழுத்தி பிறகு <key>'</key> ஐ அழுத்தி பிறகு <em>é</em> போன்று அக்யூட் அழுத்தத்தைச் சேர்க்க வேண்டிய எழுத்தை அழுத்தவும்.</p></item>
      <item><p><key>compose</key> ஐ அழுத்தி பிறகு <key>`</key> ஐ (பேக் டிக்) அழுத்தி பிறகு <em>è</em> போன்று கிரேவ் அழுத்தத்தைச் சேர்க்க வேண்டிய எழுத்தை அழுத்தவும்.</p></item>
      <item><p><key>compose</key> ஐ அழுத்தி பிறகு <key>"</key> ஐ அழுத்தி பிறகு <em>ë</em> போன்று உம்லட் அழுத்தத்தைச் சேர்க்க வேண்டிய எழுத்தை அழுத்தவும்.</p></item>
      <item><p><key>compose</key> ஐ அழுத்தி பிறகு <key>-</key> ஐ அழுத்தி பிறகு <em>ē</em> போன்று மேக்ரன் அழுத்தத்தைச் சேர்க்க வேண்டிய எழுத்தை அழுத்தவும்.</p></item>
    </list>
    <p>For more compose key sequences, see <link href="https://en.wikipedia.org/wiki/Compose_key#Common_compose_combinations">the
    compose key page on Wikipedia</link>.</p>
  </section>

<section id="ctrlshiftu">
  <title>குறியீட்டுப் புள்ளிகள்</title>

  <p>You can enter any Unicode character using only your keyboard with the
  numeric code point of the character. Every character is identified by a
  four-character code point. To find the code point for a character, look it up
  in the <app>Characters</app> application. The code point is the four characters
  after <gui>U+</gui>.</p>

  <p>To enter a character by its code point, press
  <keyseq><key>Ctrl</key><key>Shift</key><key>U</key></keyseq>, then type the
  four-character code and press <key>Space</key> or <key>Enter</key>. If you often
  use characters that you can’t easily access with other methods, you might find
  it useful to memorize the code point for those characters so you can enter
  them quickly.</p>

</section>

  <section id="layout">
    <title>விசைப்பலகை லேயவுட்டுகள்</title>
    <p>நீங்கள் உங்கள் விசைப்பலகையை வேறு மொழி விசைப்பலகை போல செயல்படவைக்கவும் முடியும், உஙக்ள் விசைப்பலகையில் அச்சிடப்பட்டுள்ள எழுத்துகள் முக்கியமல்ல. மேல் பட்டியில் உள்ள சின்னத்தைப் பயன்படுத்தி வெவ்வேறு விசைப்பலகை லேயவுட்டுகளிடையே விரைவாகவும் மாறலாம். எப்படி என அறிய <link xref="keyboard-layouts"/> ஐப் பார்க்கவும்.</p>
  </section>

<section id="im">
  <title>உள்ளீட்டு முறைகள்</title>

  <p>உள்ளீட்டு முறை என்பது என்பது முந்தைய முறைகளின் விரிவாக்கமாகும், இது விசைப்பலகையை மட்டுமின்றி எந்த ஒரு உள்ளீட்டு சாதனங்களைக் கொண்டும் எழுத்துகளை உள்ளிடும் வசதியை அளிக்கிறது உதாரணமாக நீங்கள் சைகை முறையைப் பயன்படுத்தி சொடுக்கியைக் கொண்டு எழுத்துகளை உள்ளிட முடியும் அல்லது இலத்தீன் விசைப்பலகையைப் பயன்படுத்தி ஜாப்பனீஸ் எழுத்துகளை உள்ளிட முடியும்.</p>

  <p>ஒரு உள்ளீட்டு முறையைத் தேர்வு செய்ய, ஒரு உரை விட்ஜெட்டை வலது சொடுக்கி, <gui>உள்ளீட்டு முறைகள்</gui> மெனுவில் நீங்கள் பயன்படுத்த விரும்பும் உள்ளீட்டு முறையை தேர்வு செய்யவும். முன்னிருப்பு உள்ளீட்டு முறை என்று எதுவும் இல்லை, ஆகவே அவற்றை எப்படிப் பயன்படுத்துவது என அறிய உள்ளீட்டு முறைகள் ஆவணமாக்கத்தைப் பார்க்கவும்.</p>

</section>

</page>
