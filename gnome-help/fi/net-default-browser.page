<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-default-browser" xml:lang="fi">

  <info>
    <link type="guide" xref="net-browser"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.36" date="2020-06-24" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Vaihda oletusselainta <gui>Asetukset</gui>-ikkunan <gui>Tiedot</gui>-osiosta.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2021.</mal:years>
    </mal:credit>
  </info>

  <title>Vaihda verkkosivut oletuksena avaavaa verkkoselainta</title>

  <p>Kun napsautat jossakin sovelluksessa linkkiä verkkosivulle, se aukeaa automaattisesi selaimeen. Jos sinulla on enemmän kuin yksi selain, sivu ei ehkä aukea haluamaasi selaimeen. Voit korjata tämän vaihtamalla oletusselainta:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <input>Default Applications</input>.</p>
    </item>
    <item>
      <p>Click on <gui>Default Applications</gui> to open the panel.</p>
    </item>
    <item>
      <p>Choose which web browser you would like to open links by
      changing the <gui>Web</gui> option.</p>
    </item>
  </steps>

  <p>When you open up a different web browser, it might tell you that it’s not
  the default browser any more. If this happens, click the <gui>Cancel</gui>
  button (or similar) so that it does not try to set itself as the default
  browser again.</p>

</page>
