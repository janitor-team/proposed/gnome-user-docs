<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-remove" xml:lang="fi">

  <info>
    <link type="guide" xref="accounts" group="remove"/>
    <link type="seealso" xref="accounts-disable-service"/>

    <revision pkgversion="3.5.5" date="2012-08-15" status="draft"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="draft"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2015</years>
    </credit>
    <credit type="editor">
      <name>Klein Kravis</name>
      <email>kleinkravis44@outlook.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Poista sovellusten käyttöoikeus verkkotiliisi.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2021.</mal:years>
    </mal:credit>
  </info>

  <title>Poista tili</title>

  <p>Voit poistaa verkkotilin, jota et halua enää käyttää.</p>

  <note style="tip">
    <p>Many online services provide an authorization token which your desktop stores
    instead of your password. If you remove an account, you should also revoke
    that certificate in the online service. This will ensure that no other
    application or website can connect to that service using the authorization
    for your desktop.</p>

    <p>How to revoke the authorization depends on the service provider. Check
    your settings on the provider’s website for authorized or connected apps
    or sites. Look for an app called “GNOME” and remove it.</p>
  </note>

  <steps>
    <item>
      <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja kirjoita <gui>Verkkotilit</gui>.</p>
    </item>
    <item>
      <p>Napsauta <gui>Verkkotilit</gui>.</p>
    </item>
    <item>
      <p>Valitse tili, jonka haluat poistaa.</p>
    </item>
    <item>
      <p>Napsauta <gui>-</gui> ikkunan vasemmasta alareunasta.</p>
    </item>
    <item>
      <p>Napsauta <gui>Poista</gui> vahvistusikkunassa.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Instead of deleting the account completely, it is possible to
    <link xref="accounts-disable-service">restrict the services</link> accessed
    by your desktop.</p>
  </note>
</page>
