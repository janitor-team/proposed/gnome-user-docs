<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-percentage" xml:lang="fi">

  <info>

    <link type="guide" xref="power"/>
    <link type="guide" xref="status-icons"/>
    <link type="seealso" xref="power-status"/>

    <revision version="gnome:40" date="2021-03-21" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2021</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Näytä akun varaus prosentteina yläpalkissa.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2021.</mal:years>
    </mal:credit>
  </info>
  
  <title>Näytä akun tila prosentteina</title>

  <p>The <link xref="status-icons#batteryicons">status icon</link> in the top
    bar shows the charge level of the main internal battery, and whether it is
    currently charging or not. It can also display the charge as a
    <link xref="power-percentage">percentage</link>.</p>

  <steps>

    <title>Näytä akun varaus prosentteina yläpalkissa</title>

    <item>
      <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja ala kirjoittamaan <gui>Virransäästö</gui>.</p>
    </item>
    <item>
      <p>Napsauta <gui>Virranhallinta</gui> avataksesi paneelin.</p>
    </item>
    <item>
      <p>In the <gui>Suspend &amp; Power Button</gui> section, set <gui>Show
      Battery Percentage</gui> to on.</p>
    </item>

  </steps>

</page>
