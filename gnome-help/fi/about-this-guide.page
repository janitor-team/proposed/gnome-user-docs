<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="about-this-guide" xml:lang="fi">

  <info>
    <link type="guide" xref="more-help"/>
    <desc>Vinkkejä työpöytäoppaan käyttöön.</desc>
    <credit type="author">
      <name>Gnomen dokumentointiprojekti</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2021.</mal:years>
    </mal:credit>
  </info>

<title>Tietoja tästä oppaasta</title>
<p>Tämä opas on suunniteltu esittämään työpöydän ominaisuuksia, vastaamaan tietokoneisiin liittyviin kysymyksiin sekä antamaan vinkkejä tietokoneen tehokkaampaan käyttöön. Tässä muutama huomio liittyen ohjeeseen:</p>

<list>
  <item><p>Opas on lajiteltu pieniin, tiettyihin aiheisiin suuntautuneisiin alueisiin — eli ei siis kappaleisiin. Tämä tarkoittaa sitä, että sinun ei tarvitse selata koko käyttöohjetta läpi, jotta löytäisit vastauksen kysymykseesi.</p></item>
  <item><p>Samankaltaiset aiheet ovat linkitetty toisiinsa. "Katso myös"-linkit joidenkin ohjeiden alaosassa ohjaavat sinut samankaltaisiin aiheisiin. Tämä helpottaa samankaltaisten asioiden löytämistä, jotka voivat auttaa sinua saamaan vastauksen kysymykseesi.</p></item>
  <item><p>Ohjeselaimessa on sisäänrakennettu <em>hakupalkki</em>, ja se löytyy aina Ohjeselaimen yläosasta. Se toimii samalla tavalla kuin <gui>kojelaudan</gui> hakutoiminto, eli se aloittaa etsimisen välittömästi kun aloitat kirjoittamisen.</p></item>
  <item><p>Opasta parannellaan jatkuvasti. Vaikka yritämme tarjota sinulle kattavan valikoiman hyödyllistä tietoa, tiedämme, ettei opas välttämättä vastaa kaikkiin kysymyksiisi. Jos tarvitset enemmän tukea, tarkista miten valitsemasi jakelu tarjoaa eri tukivaihtoehtoja.</p></item>
</list>

</page>
