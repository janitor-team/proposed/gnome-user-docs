<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-autorun" xml:lang="fi">

  <info>
    <link type="guide" xref="media#photos"/>
    <link type="guide" xref="media#videos"/>
    <link type="guide" xref="media#music"/>
    <link type="guide" xref="files#removable"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-10-04" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>Gnomen dokumentointiprojekti</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Suorita jokin sovellus automaattisesti kun liität CD:n, ja DVD:n, kameran, musiikkisoittimen tai muun laitteen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2021.</mal:years>
    </mal:credit>
  </info>

  <!-- TODO: fix bad UI strings, then update help -->
  <title>Avaa sovelluksia laitteita tai levyjä liitettäessä</title>

  <p>Voit käynnistää automaattisesti sovelluksen, kun liität tietyn laitteen. Saatat esimerkiksi haluta valokuvien hallinnan avautuvan, kun liität tietokoneeseen digitaalisen kameran. Voit myös estää sovelluksien käynnistymisen laitteita liittäessä.</p>

  <p>Määritä seuraavasti, miten toimitaan kun tietty laite liitetään tietokoneeseen:</p>

<steps>
  <item>
    <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
    start typing <gui>Removable Media</gui>.</p>
  </item>
  <item>
    <p>Click <gui>Removable Media</gui>.</p>
  </item>
  <item>
    <p>Find your desired device or media type, and then choose an application
    or action for that media type. See below for a description of the different
    types of devices and media.</p>
    <p>Instead of starting an application, you can also set it so that the
    device will be shown in the file manager, with the <gui>Open folder</gui>
    option. When that happens, you will be asked what to do, or nothing will
    happen automatically.</p>
  </item>
  <item>
    <p>If you do not see the device or media type that you want to change in
    the list (such as Blu-ray discs or E-book readers), click <gui>Other
    Media…</gui> to see a more detailed list of devices. Select the type of
    device or media from the <gui>Type</gui> drop-down and the application or
    action from the <gui>Action</gui> drop-down.</p>
  </item>
</steps>

  <note style="tip">
    <p>If you do not want any applications to be opened automatically, whatever
    you plug in, select <gui>Never prompt or start programs on media
    insertion</gui> at the bottom of the <gui>Removable Media</gui> window.</p>
  </note>

<section id="files-types-of-devices">
  <title>Eri laite- ja mediatyyppejä</title>
<terms>
  <item>
    <title>Äänilevyt</title>
    <p>Choose your favorite music application or CD audio extractor to handle
    audio CDs. If you use audio DVDs (DVD-A), select how to open them under
    <gui>Other Media…</gui>. If you open an audio disc with the file manager,
    the tracks will appear as WAV files that you can play in any audio player
    application.</p>
  </item>
  <item>
    <title>Videolevyt</title>
    <p>Choose your favorite video application to handle video DVDs. Use the
    <gui>Other Media…</gui> button to set an application for Blu-ray, HD DVD,
    video CD (VCD), and super video CD (SVCD). If DVDs or other video discs
    do not work correctly when you insert them, see <link xref="video-dvd"/>.
    </p>
  </item>
  <item>
    <title>Tyhjät levyt</title>
    <p>Use the <gui>Other Media…</gui> button to select a disc-writing
    application for blank CDs, blank DVDs, blank Blu-ray discs, and blank HD
    DVDs.</p>
  </item>
  <item>
    <title>Kamerat ja valokuvat</title>
    <p>Use the <gui>Photos</gui> drop-down to choose a photo-management
    application to run when you plug in your digital camera, or when you insert
    a media card from a camera, such as a CF, SD, MMC, or MS card. You can also
    simply browse your photos using the file manager.</p>
    <p>Under <gui>Other Media…</gui>, you can select an application to open
    Kodak picture CDs, such as those you might have made in a store. These are
    regular data CDs with JPEG images in a folder called
    <file>Pictures</file>.</p>
  </item>
  <item>
    <title>Musiikkisoittimet</title>
    <p>Choose an application to manage the music library on your portable music
    player, or manage the files yourself using the file manager.</p>
    </item>
    <item>
      <title>E-kirjojen lukijat</title>
      <p>Use the <gui>Other Media…</gui> button to choose an application to
      manage the books on your e-book reader, or manage the files yourself
      using the file manager.</p>
    </item>
    <item>
      <title>Ohjelmisto</title>
      <p>Some discs and removable media contain software that is supposed to be
      run automatically when the media is inserted. Use the <gui>Software</gui>
      option to control what to do when media with autorun software is
      inserted. You will always be prompted for a confirmation before software
      is run.</p>
      <note style="warning">
        <p>Älä koskaan suorita tai liitä mediaa, johon et luota.</p>
      </note>
   </item>
</terms>

</section>

</page>
