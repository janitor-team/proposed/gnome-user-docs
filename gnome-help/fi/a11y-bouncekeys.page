<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-bouncekeys" xml:lang="fi">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Jätä huomioimatta saman näppäimen toistuvat painallukset.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2021.</mal:years>
    </mal:credit>
  </info>

  <title>Kimmonäppäimien päälle kytkeminen</title>

  <p>Käytä <em>kimmonäppäimiä</em>, jotta nopeasti toistuvat näppäinpainallukset jäävät huomiotta. Esimerkiksi vapisevat kädet voivat aiheuttaa toistuvia näppäinpainalluksia vaikka tarkoituksena olisi ollut kertapainallus. Tällöin kimmonäppäimet auttavat.</p>

  <steps>
    <item>
      <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja ala kirjoittamaan <gui>Asetukset</gui>.</p>
    </item>
    <item>
      <p>Napsauta <gui>Asetukset</gui>.</p>
    </item>
    <item>
      <p>Napsauta <gui>Esteettömyys</gui> sivupalkista avataksesi paneelin.</p>
    </item>
    <item>
      <p>Paina <gui>Kirjoitusapu (AccessX)</gui> <gui>Kirjoittaminen</gui>-osiossa.</p>
    </item>
    <item>
      <p>Switch the <gui>Bounce Keys</gui> switch to on.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Kimmokenäppäimien pikainen käyttöönotto ja käytöstä poistaminen</title>
    <p>You can turn bounce keys on and off by clicking the
    <link xref="a11y-icon">accessibility icon</link> on the top bar and
    selecting <gui>Bounce Keys</gui>. The accessibility icon is visible when
    one or more settings have been enabled from the <gui>Accessibility</gui>
    panel.</p>
  </note>

  <p>Käytä <gui>Viiveen kesto</gui> -liukusäädintä määrittääksesi, kuinka kauan kimmonäppäin odottaa, ennen kuin se rekisteröi uuden näppäinpainalluksen. Valitse <gui>Soita äänimerkki, kun näppäinpainallusta ei hyväksytty</gui>, jos haluat tietokoneen ilmoittavan äänimerkillä joka kerta, kun painat näppäimiä liian nopeasti ja tietokone hylkää näppäinpainalluksen.</p>

</page>
