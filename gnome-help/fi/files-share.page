<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-share" xml:lang="fi">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>
    <link type="guide" xref="sharing"/>
    <link type="seealso" xref="nautilus-connect"/>

    <revision pkgversion="3.8.2" version="0.3" date="2013-05-11" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Lähetä tiedostoja sähköpostitse tiedostonhallinnasta.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2021.</mal:years>
    </mal:credit>
  </info>

<title>Jaa tiedostoja sähköpostitse</title>

<p>Voit jakaa tiedostoja helposti tuttavillesi sähköpostitse suoraan tiedostonhallinnasta.</p>

  <note style="important">
    <p>Ennen kuin jatkat, varmista että <app>Evolution</app> tai <app>Geary</app> on asennettu tietokoneelle, ja että sähköpostitilisi asetukset on määritetty.</p>
  </note>

<steps>
  <title>Jaa tiedosto sähköpostitse:</title>
    <item>
      <p>Open the <app>Files</app> application from the
      <gui xref="shell-introduction#activities">Activities</gui> overview.</p>
    </item>
  <item><p>Paikallista tiedosto, jonka haluat lähettää.</p></item>
    <item>
      <p>Right-click the file and select <gui>Send to…</gui>. An email compose
      window will appear with the file attached.</p>
    </item>
  <item><p>Click <gui>To</gui> to choose a contact, or enter an email address
  where you want to send the file. Fill in the <gui>Subject</gui> and the body
  of the message as required and click <gui>Send</gui>.</p></item>
</steps>

<note style="tip">
  <p>You can send multiple files at once. Select multiple files by holding
  down <key>Ctrl</key> while clicking the files, then right-click any selected
  file.</p>
</note>

</page>
