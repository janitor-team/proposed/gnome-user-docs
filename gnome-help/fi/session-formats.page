<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-formats" xml:lang="fi">

  <info>
    <link type="guide" xref="prefs-language"/>
    <link type="seealso" xref="session-language"/>

    <revision pkgversion="3.10" version="0.4" date="2013-11-03" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.38.4" date="2021-03-11" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Päivämäärän, kellonajan, lukujen, valuutan ja mittojen esitysmuodon muuttaminen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2021.</mal:years>
    </mal:credit>
  </info>

  <title>Vaihda päiväyksen ja mittojen muotoja</title>

  <p>Ubuntu tarjoaa mahdollisuuden muuttaa päivämäärien, kellonaikojen, lukujen, valuutan ja mittojen esitysmuotoa paikallisia esitysmuotoja vastaaviksi.</p>

  <steps>
    <item>
      <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja ala kirjoittamaan <gui>Alue ja kielet</gui>.</p>
    </item>
    <item>
      <p>Napsauta <gui>Alue ja kielet</gui> avataksesi paneelin.</p>
    </item>
    <item>
      <p>Click <gui>Formats</gui>.</p>
    </item>
    <item>
      <p>Under <gui>Common Formats</gui>, select the region and language that most
      closely matches the formats you would like to use.</p>
    </item>
    <item>
      <p>Click <gui style="button">Done</gui> to save.</p>
    </item>
    <item>
      <p>Your session needs to be restarted for changes to take effect. Either click
      <gui style="button">Restart…</gui>, or manually log back in later.</p>
    </item>
  </steps>

  <p>After you have selected a region, the area to the right of the list shows
  various examples of how dates and other values are shown. Although not shown
  in the examples, your region also controls the starting day of the week in
  calendars.</p>

  <note style="tip">
    <p>If there are multiple user accounts on your system, there is a separate
    instance of the <gui>Region &amp; Language</gui> panel for the login screen.
    Click the <gui>Login Screen</gui> button at the top right to toggle between
    the two instances.</p>
  </note>

</page>
