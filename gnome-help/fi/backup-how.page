<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="backup-how" xml:lang="fi">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit>
      <name>Gnomen dokumentointiprojekti</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Klein Kravis</name>
      <email>kleinkravis44@outlook.com</email>
      <years>2020</years>
    </credit>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Use Déjà Dup (or another backup application) to make copies of
    your valuable files and settings to protect against loss.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2021.</mal:years>
    </mal:credit>
  </info>

<title>Miten varmuuskopioidaan?</title>

  <p>Helpoin tapa varmuuskopioida tiedostoja ja asetuksia on käyttää varmuuskopiointiin tarkoitettua sovellusta. Kyseisiä sovelluksia on lukuisia, esimerkiksi <app>Déjà Dup</app>.</p>

  <p>Valitsemasi varmuuskopiosovelluksen käyttöohje auttaa sinua asetusten luomisessa ja tietojen palauttamisessa.</p>

  <p>An alternative option is to <link xref="files-copy">copy your files</link>
 to a safe location, such as an external hard drive, an online storage service,
 or a USB drive. Your <link xref="backup-thinkabout">personal files</link>
 and settings are usually in your Home folder, so you can copy them from there.</p>

  <p>Varmuuskopioitavien tietojen määrää rajoittaa tallennuslaitteen levykapasiteetti. Jos tallennuslaitteellasi on reilusti tilaa, on viisainta kopioida koko kotikansion sisältö seuraavia poikkeuksia lukuun ottamatta:</p>

<list>
 <item><p>Files that are already backed up somewhere else, such as to a USB drive,
 or other removable media.</p></item>
 <item><p>Tiedostot, joiden uudelleenluonti on helppoa. Esimerkiksi ohjelmoijien ei tarvitse varmuuskopioida tiedostoja, jotka luodaan kääntämisen yhteydessä. Sen sijaan alkuperäisten lähdekoodien varmuuskopioinnista kannattaa huolehtia.</p></item>
 <item><p>Roskakorissa olevat tiedostot, jotka sijaitsevat polussa <file>~/.local/share/Trash</file>.</p></item>
</list>

</page>
