<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-vpn-connect" xml:lang="ru">

  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-wired"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.10" date="2013-12-05" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Настройка VPN-соединения до локальной сети через интернет.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Подключение к VPN</title>

<p>A VPN (or <em>Virtual Private Network</em>) is a way of connecting to a
 local network over the internet. For example, say you want to connect to the
 local network at your workplace while you’re on a business trip. You would
 find an internet connection somewhere (like at a hotel) and then connect to
 your workplace’s VPN. It would be as if you were directly connected to the
 network at work, but the actual network connection would be through the
 hotel’s internet connection. VPN connections are usually <em>encrypted</em>
 to prevent people from accessing the local network you’re connecting to
 without logging in.</p>

<p>There are a number of different types of VPN. You may have to install some
 extra software depending on what type of VPN you’re connecting to. Find out
 the connection details from whoever is in charge of the VPN and see which
 <em>VPN client</em> you need to use. Then, go to the software installer
 application and search for the <app>NetworkManager</app> package which works
 with your VPN (if there is one) and install it.</p>

<note>
 <p>If there isn’t a NetworkManager package for your type of VPN, you will
 probably have to download and install some client software from the company
 that provides the VPN software. You’ll probably have to follow some different
 instructions to get that working.</p>
</note>

<p>To set up the VPN connection:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Network</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Сеть</gui> чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>Чтобы добавить новое соединение, нажмите кнопку <key>+</key>, расположенную в нижней части списка с левой стороны.</p>
    </item>
    <item>
      <p>Выберите <gui>VPN</gui> в списке интерфейсов.</p>
    </item>
    <item>
      <p>Выберите используемый вами тип VPN-соединения.</p>
    </item>
    <item>
      <p>Введите параметры VPN-соединения. Закончив, нажмите <gui>Добавить</gui>.</p>
    </item>
    <item>
      <p>When you have finished setting up the VPN, open the
      <gui xref="shell-introduction#systemmenu">system menu</gui> from the right side of
      the top bar, click <gui>VPN off</gui> and select <gui>Connect</gui>. You
      may need to enter a password for the connection before it is established.
      Once the connection is made, you will see a lock shaped icon in the top
      bar.</p>
    </item>
    <item>
      <p>Hopefully you will successfully connect to the VPN. If not, you may
      need to double-check the VPN settings you entered. You can do this from
      the <gui>Network</gui> panel that you used to create the connection.
      Select the VPN connection from the list, then press the
<media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">settings</span></media> button to review the settings.</p>
    </item>
    <item>
      <p>To disconnect from the VPN, click the system menu on the top bar and
      click <gui>Turn Off</gui> under the name of your VPN connection.</p>
    </item>
  </steps>

</page>
