<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-bouncekeys" xml:lang="ru">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Шон МакКенс (Shaun McCance)</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Фил Булл (Phil Bull)</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Игнорировать быстро повторяющиеся нажатия одной и той же клавиши.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Включение «отскакивающих клавиш»</title>

  <p>Включите <em>«отскакивающие клавиши»</em>, чтобы игнорировать быстро повторяющиеся нажатия. Это может быть полезно, например, если из-за дрожания рук вместо однократного нажатия клавиши вы нажимаете её несколько раз.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Accessibility</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Нажмите <gui>Помощник ввода (AccessX)</gui> в разделе <gui>Ввод</gui>.</p>
    </item>
    <item>
      <p>Switch the <gui>Bounce Keys</gui> switch to on.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Быстрое включение и отключение «отскакивающих клавиш»</title>
    <p>You can turn bounce keys on and off by clicking the
    <link xref="a11y-icon">accessibility icon</link> on the top bar and
    selecting <gui>Bounce Keys</gui>. The accessibility icon is visible when
    one or more settings have been enabled from the <gui>Accessibility</gui>
    panel.</p>
  </note>

  <p>Используйте ползунок <gui>Задержка принятия</gui>, чтобы изменить время, в течение которого «отскакивающие клавиши» ожидают регистрации повторного нажатия после нажатия клавиши в первый раз. Включите <gui>Подавать сигнал при игнорировании клавиш</gui>, если хотите, чтобы компьютер подавал звуковой сигнал каждый раз, когда нажатие клавиши игнорируется из-за того, что она нажата слишком рано после предыдущего её нажатия.</p>

</page>
