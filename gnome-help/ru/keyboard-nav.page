<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="keyboard-nav" xml:lang="ru">
  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision pkgversion="3.7.5" version="0.2" date="2013-02-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.20" date="2016-08-13" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
       <name>Юлита Инка (Julita Inca)</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="author copyright">
      <name>Шон МакКенс (Shaun McCance)</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
       <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
       <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Работа с приложениями и рабочим столом без мыши.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Управление с помощью клавиатуры</title>

  <p>На этой странице приведены комбинации клавиш для людей, которые не могут использовать мышь или другое указательное устройство, или хотят как можно больше пользоваться клавиатурой. Комбинации клавиш, полезные для всех пользователей смотрите в разделе <link xref="shell-keyboard-shortcuts"/>.</p>

  <note style="tip">
    <p>Если вы не можете использовать указательное устройство типа мыши, можно управлять указателем мыши с помощью клавиш цифрового блока клавиатуры. Подробнее смотрите: <link xref="mouse-mousekeys"/>.</p>
  </note>

<table frame="top bottom" rules="rows">
  <title>Навигация по пользовательскому интерфейсу</title>
  <tr>
    <td><p><key>Tab</key> and</p>
    <p><keyseq><key>Ctrl</key><key>Tab</key></keyseq></p>
    </td>
    <td>
      <p>Перемещение фокуса клавиатуры между различными элементами управления. <keyseq><key>Ctrl</key> <key>Tab</key></keyseq> перемещает между группами элементов, например, с боковой панели в основную область окна. <keyseq><key>Ctrl</key><key>Tab</key></keyseq> также позволяет выйти из элемента управления, который сам использует <key>Tab</key>, такого как текстовая область.</p>
      <p>Удерживайте нажатой <key>Shift</key> для перемещения фокуса в обратном порядке.</p>
    </td>
  </tr>
  <tr>
    <td><p>Клавиши со стрелками</p></td>
    <td>
      <p>Перемещение выбора между частями одного элемента управления или группы взаимосвязанных элементов. Используйте клавиши со стрелками для выбора кнопки на панели инструментов, объектов при просмотре в режиме списка или значков, а также кнопки из группы радиокнопок.</p>
    </td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key>клавиши со стрелками</keyseq></p></td>
    <td><p>В режиме списка или значков — перемещение фокуса клавиатуры на другой объект, не снимая выделение с ранее выделенного объекта.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key>клавиши со стрелками</keyseq></p></td>
    <td><p>В режиме списка или значков — выделение группы объектов, начиная с текущего выделенного объекта, и заканчивая вновь выбранным объектом.</p>
    <p>In a tree view, items that have children can be expanded or collapsed,
    to show or hide their children: expand by pressing
    <keyseq><key>Shift</key><key>→</key></keyseq>, and collapse by
    pressing <keyseq><key>Shift</key><key>←</key></keyseq>.</p></td>
  </tr>
  <tr>
    <td><p><key>Пробел</key></p></td>
    <td><p>Активация обладающего фокусом объекта, такого как кнопка, флажок или элемент списка.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>пробел</key></keyseq></p></td>
    <td><p>В режиме списка или значков — выделение или снятие выделения с обладающего фокусом объекта, не снимая выделение с других объектов.</p></td>
  </tr>
  <tr>
    <td><p><key>Alt</key></p></td>
    <td><p>Удерживайте клавишу <key>Alt</key> для показа <em>горячих клавиш</em>: подчёркнутых букв в командах меню, на кнопках и других элементах управления. Нажмите <key>Alt</key> и подчёркнутую букву, чтобы активировать элемент управления, как если бы вы нажали на него мышью.</p></td>
  </tr>
  <tr>
    <td><p><key>Esc</key></p></td>
    <td><p>Выход из меню, всплывающего окна, переключателя или диалогового окна.</p></td>
  </tr>
  <tr>
    <td><p><key>F10</key></p></td>
    <td><p>Открывает первое меню в строке меню окна. Используйте клавиши со стрелками для перемещения по меню.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key xref="keyboard-key-super">Super</key> <key>F10</key></keyseq></p></td>
    <td><p>Открывает меню приложения в верхней панели.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key><key>F10</key></keyseq> or</p>
    <p><key xref="keyboard-key-menu">Menu</key></p></td>
    <td>
      <p>Всплывающее контекстное меню для выбранного объекта, как при нажатии правой кнопкой.</p>
    </td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>F10</key></keyseq></p></td>
    <td><p>В менеджере файлов всплывающее контекстное меню для текущей папки, как при нажатии правой кнопкой на пустом месте в папке.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>PageUp</key></keyseq></p>
    <p>and</p>
    <p><keyseq><key>Ctrl</key><key>PageDown</key></keyseq></p></td>
    <td><p>В интерфейсе со вкладками переключение на вкладку слева или справа.</p></td>
  </tr>
</table>

<table frame="top bottom" rules="rows">
  <title>Навигация по рабочему столу</title>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="alt-f1"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-tab"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-tick"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="ctrl-alt-tab"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-updown"/>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F6</key></keyseq></p></td>
    <td><p>Циклическое переключение между окнами одного приложения. Удерживайте <key>Alt</key> и нажимайте <key>F6</key>, пока не выделится подсветкой нужное окно, затем отпустите <key>Alt</key>. Это действие, подобное комбинации <keyseq><key>Alt</key><key>`</key></keyseq>.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
    <td><p>Циклическое переключение между всеми окнами на рабочем месте.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
    <td><p><link xref="shell-notifications#notificationlist">Open the
    notification list.</link> Press <key>Esc</key> to close.</p></td>
  </tr>
</table>

<table frame="top bottom" rules="rows">
  <title>Управление окнами</title>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
    <td><p>Закрывает текущее окно.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F5</key></keyseq> или <keyseq><key>Super</key><key>↓</key></keyseq></p></td>
    <td><p>Восстановление исходных размеров окна. Используйте <keyseq><key>Alt</key> <key>F10</key></keyseq> для разворачивания окна. Повторное нажатие <keyseq><key>Alt</key><key>F10</key></keyseq> восстановит предыдущие размеры окна.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
    <td><p>Перемещение текущего окна. Нажмите <keyseq><key>Alt</key><key>F7</key></keyseq>, затем используйте клавиши со стрелками для перемещения окна. Нажмите <key>Enter</key>, чтобы завершить перемещение окна или <key>Esc</key> для возврата окна в исходную позицию.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
    <td><p>Изменение размера текущего окна. Нажмите <keyseq><key>Alt</key><key>F8</key></keyseq>, затем используйте клавиши со стрелками для изменения размера окна. Нажмите <key>Enter</key>, чтобы завершить изменение размера, или <key>Esc</key>, чтобы вернуть окну исходный размер.</p></td>
  </tr>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-updown"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-left"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-right"/>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F10</key></keyseq> или <keyseq><key>Super</key><key>↑</key></keyseq></p>
    </td>
    <td><p><link xref="shell-windows-maximize">Разворачивание</link> окна. Чтобы восстановить размер окна до прежних размеров, нажмите <keyseq><key>Alt</key><key>F10</key></keyseq> или <keyseq><key>Super</key><key>↓</key></keyseq>.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>H</key></keyseq></p></td>
    <td><p>Свернуть окно</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>←</key></keyseq></p></td>
    <td><p>Разворачивание окна вертикально вдоль левого края экрана. Нажмите снова, чтобы восстановить предыдущий размер окна. Нажмите <keyseq><key>Super</key><key>→</key></keyseq> для смены краёв экрана.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>→</key></keyseq></p></td>
    <td><p>Разворачивание окна вертикально вдоль правого края экрана. Нажмите снова, чтобы восстановить предыдущий размер окна. Нажмите <keyseq><key>Super</key><key>←</key></keyseq> для смены краёв экрана.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>пробел</key></keyseq></p></td>
    <td><p>Открывает меню окна, как при нажатии правой кнопкой по заголовку окна.</p></td>
  </tr>
</table>

</page>
