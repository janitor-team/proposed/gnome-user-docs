<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" version="1.0 if/1.0" id="shell-introduction" xml:lang="ru">

  <info>
    <link type="guide" xref="shell-overview" group="#first"/>
    <link type="guide" xref="index" group="intro"/>

    <revision pkgversion="3.6.0" date="2012-10-13" status="review"/>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-28" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="review"/>
    <revision pkgversion="3.35.91" date="2020-07-19" status="review"/>
    <revision version="gnome:40" date="2021-06-16" status="review"/>

    <credit type="author">
      <name>Шон МакКенс (Shaun McCance)</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Андрэ Клаппер (Andre Klapper)</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>A visual overview of your desktop, the top bar, and the
    <gui>Activities</gui> overview.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Visual overview of GNOME</title>

  <p>GNOME features a user interface designed to stay out of your way, minimize
  distractions, and help you get things done. When you first log in, you will
  see the <gui>Activities</gui> overview and the top bar.</p>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-top-bar.png" width="600" if:test="!target:mobile">
      <p>Верхняя панель GNOME Shell</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-top-bar-classic.png" width="500" if:test="!target:mobile">
      <p>Верхняя панель GNOME Shell</p>
    </media>
  </if:when>
</if:choose>

  <p>The top bar provides access to your windows and applications, your
  calendar and appointments, and
  <link xref="status-icons">system properties</link> like sound, networking,
  and power. In the system menu in the top bar, you can change the volume or
  screen brightness, edit your <gui>Wi-Fi</gui> connection details, check your
  battery status, log out or switch users, and turn off your computer.</p>

<links type="section"/>

<!-- TODO: Replace "Activities overview" title for classic mode with something
like "Application windows" by using if:when and if:else ? -->
<section id="activities">
  <title>Режим <gui>обзора</gui></title>

  <p if:test="!platform:gnome-classic">When you start GNOME, you automatically
  enter the <gui>Activities</gui> overview. The overview allows you to access
  your windows and applications. In the overview, you can also just start
  typing to search your applications, files, folders, and the web.</p>

  <p if:test="!platform:gnome-classic">To access the overview at any time,
  click the <gui>Activities</gui> button, or just move your mouse pointer to
  the top-left hot corner. You can also press the
  <key xref="keyboard-key-super">Super</key> key on your keyboard.</p>

  <p if:test="platform:gnome-classic">To access your windows and applications,
  click the button at the bottom left of the screen in the window list. You can
  also press the <key xref="keyboard-key-super">Super</key> key to see an
  overview with live thumbnails of all the windows on the current workspace.</p>

  <media type="image" its:translate="no" src="figures/shell-activities-dash.png" height="65" style="floatend floatright" if:test="!target:mobile, !platform:gnome-classic">
    <p>Activities button and Dash</p>
  </media>
  <p if:test="!platform:gnome-classic">At the bottom of the overview, you will find the <em>dash</em>. The dash
  shows you your favorite and running applications. Click any icon in the
  dash to open that application; if the application is already running, it will
  have a small dot below its icon. Clicking its icon will bring up the most
  recently used window. You can also drag the icon onto a workspace.</p>

  <p if:test="!platform:gnome-classic">Нажатие правой кнопки мыши на значке открывает меню, в котором можно выбрать любое из уже открытых окон запущенного приложения или открыть новое окно. Новое окно также можно открыть просто нажав на значок, удерживая при этом клавишу <key>Ctrl</key>.</p>

  <p if:test="!platform:gnome-classic">Когда вы открываете обзор, то вы попадаете в режим обзора окон. В этом режиме показываются «живые» миниатюры всех окон, открытых на текущем рабочем месте.</p>

  <p if:test="!platform:gnome-classic">Click the grid button (which has nine dots) in the dash to display the
  applications overview. This shows you all the applications installed on your
  computer. Click any application to run it, or drag an application to the
  onto a workspace shown above the installed applications. You can also drag an application onto
  the dash to make it a favorite. Your favorite applications stay in the dash
  even when they’re not running, so you can access them quickly.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-apps-open">Подробнее о запуске приложений.</link></p>
    </item>
    <item>
      <p><link xref="shell-windows">Подробнее об окнах и рабочих местах.</link></p>
    </item>
  </list>

</section>

<section id="appmenu">
  <title>Меню приложения</title>
  <if:choose>
    <if:when test="!platform:gnome-classic">
      <media type="image" src="figures/shell-appmenu-shell.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>Меню приложения <app>Терминал</app></p>
      </media>
      <p>Application menu, located beside the <gui>Activities</gui> button,
      shows the name of the active application alongside with its icon and
      provides quick access to windows and details of the application, as well
      as a quit item.</p>
    </if:when>
    <!-- TODO: check how the app menu removal affects classic mode -->
    <if:when test="platform:gnome-classic">
      <media type="image" src="figures/shell-appmenu-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>Меню приложения <app>Терминал</app></p>
      </media>
      <p>Меню приложения, расположенное рядом с меню <gui>Обзор</gui> и <gui>Переход</gui>, показывает название активного приложения вместе со значком этого приложения, и предоставляет быстрый доступ к параметрам приложения или к его справке. Пункты, доступные в меню приложения, зависят от конкретного приложения.</p>
    </if:when>
  </if:choose>

</section>

<section id="clock">
  <title>Часы, календарь и события</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-appts.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Часы, календарь, события и уведомления</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-appts-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Часы, календарь и события</p>
    </media>
  </if:when>
</if:choose>

  <p>Click the clock on the top bar to see the current date, a month-by-month
  calendar, a list of your upcoming appointments and new notifications. You can
  also open the calendar by pressing
  <keyseq><key>Super</key><key>M</key></keyseq>. You can access the date and
  time settings and open your full calendar application directly from
  the menu.</p>

  <list style="compact">
    <item>
      <p><link xref="clock-calendar">Подробнее о календаре и событиях.</link></p>
    </item>
    <item>
      <p><link xref="shell-notifications">Learn more about notifications and
      the notification list.</link></p>
    </item>
  </list>

</section>


<section id="systemmenu">
  <title>Системное меню</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-exit.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Меню пользователя</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-exit-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Меню пользователя</p>
    </media>
  </if:when>
</if:choose>

  <p>Для управления системными настройками и работой компьютера нажмите на системное меню в верхнем правом углу экрана.</p>

<!-- TODO: Update for 3.36 UI option "Do Not Disturb" in calendar dropdown:

<p>If you set yourself to Unavailable, you won’t be bothered by message popups
at the bottom of your screen. Messages will still be available in the message
tray when you move your mouse to the bottom-right corner. But only urgent
messages will be presented, such as when your battery is critically low.</p>
-->

  <p>When you leave your computer, you can lock your screen to prevent other
  people from using it. You can also quickly switch users without logging out
  completely to give somebody else access to the computer, or you can
  suspend or power off the computer from the menu. If you have a screen 
  that supports vertical or horizontal rotation, you can quickly rotate the 
  screen from the system menu. If your screen does not support rotation, 
  you will not see the button.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-exit">Подробнее о смене пользователей, завершении сеанса и выключении компьютера.</link></p>
    </item>
  </list>

</section>

<section id="lockscreen">
  <title>Блокирование экрана</title>

  <p>When you lock your screen, or it locks automatically, the lock screen is
  displayed. In addition to protecting your desktop while you’re away from your
  computer, the lock screen displays the date and time. It also shows
  information about your battery and network status.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-lockscreen">Узнать больше об экране блокировки.</link></p>
    </item>
  </list>

</section>

<section id="window-list">
  <title>Список окон</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <p>GNOME представляет другой подход к переключению окон, отличный от постоянно видимого списка окон в других графических средах, и позволяет полностью сосредоточиться на текущих задачах.</p>
    <list style="compact">
      <item>
        <p><link xref="shell-windows-switching">Подробнее о переключении окон.</link></p>
      </item>
    </list>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-window-list-classic.png" width="800" if:test="!target:mobile">
      <p>Список окон</p>
    </media>
    <p>Список окон в нижней части экрана предоставляет доступ ко всем открытым окнам и приложениям и даёт возможность быстро сворачивать и восстанавливать их.</p>
    <p>At the right-hand side of the window list, GNOME displays the four
    workspaces. To switch to a different workspace, select the workspace you
    want to use.</p>
  </if:when>
</if:choose>

</section>

</page>
