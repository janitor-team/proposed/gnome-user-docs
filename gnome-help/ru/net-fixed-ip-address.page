<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-fixed-ip-address" xml:lang="ru">

  <info>
    <link type="guide" xref="net-wired"/>
    <link type="seealso" xref="net-findip"/>

    <revision pkgversion="3.4.0" date="2012-03-13" status="final"/>
    <revision pkgversion="3.15" date="2014-12-04" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.24" date="2017-03-26" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author">
      <name>Джим Кэмпбелл (Jim Campbell)</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Использование статического IP-адреса может упростить предоставление компьютером некоторых сетевых услуг.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Создание соединения с фиксированным IP-адресом</title>

  <p>Большинство сетей автоматически назначает <link xref="net-what-is-ip-address">IP-адрес</link> и другие настройки вашему компьютеру при подключении к сети. Эти настройки могут периодически изменяться, но в некоторых случаях вам может понадобиться постоянный IP-адрес компьютера, чтобы вы всегда его знали (например, если компьютер используется в качестве файлового сервера).</p>

  <!-- TODO Update for Network/Wi-Fi split -->
  <steps>
    <title>Чтобы присвоить компьютеру постоянный (статический) IP-адрес:</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Network</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Сеть</gui> чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>Find the network connection that you want to have a fixed address.
      Click the
      <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">settings</span></media>
      button next to the network connection. For a <gui>Wi-Fi</gui> connection,
      the
      <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">settings</span></media>
      button will be located next to the active network.</p>
    </item>
    <item>
      <p>Select the <gui>IPv4</gui> or <gui>IPv6</gui> tab and change the
      <gui>Method</gui> to <gui>Manual</gui>.</p>
    </item>
    <item>
      <p>Type in the <gui xref="net-what-is-ip-address">IP Address</gui> and
      <gui>Gateway</gui>, as well as the appropriate <gui>Netmask</gui>.</p>
    </item>
    <item>
      <p>In the <gui>DNS</gui> section, switch the <gui>Automatic</gui> switch
      to off. Enter the IP address of a DNS server you want to use. Enter
      additional DNS server addresses using the <gui>+</gui> button.</p>
    </item>
    <item>
      <p>In the <gui>Routes</gui> section, switch the <gui>Automatic</gui>
      switch to off. Enter the <gui>Address</gui>, <gui>Netmask</gui>,
      <gui>Gateway</gui> and <gui>Metric</gui> for a route you want to use.
      Enter additional routes using the <gui>+</gui> button.</p>
    </item>
    <item>
      <p>Click <gui>Apply</gui>. The network connection should now have a fixed
      IP address.</p>
    </item>
  </steps>

</page>
