<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="printing-streaks" xml:lang="ru">

  <info>
    <link type="guide" xref="printing#problems"/>
    <link type="seealso" xref="printing-inklevel"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Если на распечатках видны полосы, непропечатки или отсутствуют некоторые цвета, проверьте уровень чернил или выполните очистку печатающей головки.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Почему на распечатках присутствуют полосы, посторонние линии или неправильные цвета?</title>

  <p>If your print-outs are streaky, faded, have lines on them that should not be
  there, or are otherwise poor in quality, this may be due to a problem with the
  printer or a low ink or toner supply.</p>

  <terms>
     <item>
       <title>Блёклый текст или изображения</title>
       <p>You may be running out of ink or toner. Check your ink or toner supply
       and buy a new cartridge if necessary.</p>
     </item>
     <item>
       <title>Полосы и линии</title>
       <p>If you have an inkjet printer, the print head may be dirty or
       partially blocked. Try cleaning the print head. See the printer’s manual
       for instructions.</p>
     </item>
     <item>
       <title>Неправильные цвета</title>
       <p>The printer may have run out of one color of ink or toner. Check your
       ink or toner supply and buy a new cartridge if necessary.</p>
     </item>
     <item>
       <title>Jagged lines, or lines are not straight</title>
       <p>If lines on your print-out that should be straight turn out jagged,
       you may need to align the print head. See the printer’s instruction
       manual for details on how to do this.</p>
     </item>
  </terms>

</page>
