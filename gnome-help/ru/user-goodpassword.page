<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="user-goodpassword" xml:lang="ru">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Фил Булл (Phil Bull)</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Используйте длинные, более сложные пароли.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Выбор надёжного пароля</title>

  <note style="important">
    <p>Выбирайте пароли, которые вам достаточно легко запомнить, а другим (включая компьютерные программы) — сложно подобрать.</p>
  </note>

  <p>Выбор хорошего пароля поможет содержать компьютер в безопасности. Если пароль легко угадать, кто-нибудь может сделать это и получить доступ к вашей личной информации.</p>

  <p>Для систематического подбора паролей могут быть использованы компьютеры. Пароль, который человеку угадать сложно, взломать с помощью компьютерной программы может оказаться очень легко. Вот несколько советов по выбору надёжного пароля:</p>
  
  <list>
    <item>
      <p>Используйте в пароле смесь заглавных и строчных букв, цифр, символов и пробелов. Тогда их будет сложнее угадать: чем больше разнообразие символов, тем больше возможных паролей придётся проверить взломщику.</p>
      <note>
        <p>A good method for choosing a password is to take the first letter of
        each word in a phrase that you can remember. The phrase could be the
        name of a movie, a book, a song or an album. For example, “Flatland: A
        Romance of Many Dimensions” would become F:ARoMD or faromd or f:
        aromd.</p>
      </note>
    </item>
    <item>
      <p>Сделайте пароль настолько длинным, насколько это возможно. Чем больше символов он содержит, тем больше времени уйдёт у взломщика на перебор возможных паролей.</p>
    </item>
    <item>
      <p>Do not use any words that appear in a standard dictionary in any
      language. Password crackers will try these first. The most common
      password is “password” — people can guess passwords like this very
      quickly!</p>
    </item>
    <item>
      <p>Do not use any personal information such as a date, license plate
      number, or any family member’s name.</p>
    </item>
    <item>
      <p>Не используйте имена существительные.</p>
    </item>
    <item>
      <p>Выбирайте пароли, которые можно набрать быстро, чтобы уменьшить вероятность, что кто-то подсмотрит пароль во время набора.</p>
      <note style="tip">
        <p>Никогда нигде не записывайте ваши пароли. Их могут найти!</p>
      </note>
    </item>
    <item>
      <p>Используйте разные пароли для различных задач.</p>
    </item>
    <item>
      <p>Используйте разные пароли для каждой из учётных записей.</p>
      <p>Если использовать одинаковые пароли для всех учётных записей, то любой, кто узнает пароль, получит доступ сразу ко всем учётным записям.</p>
      <p>It can be difficult to remember lots of passwords, however. Though not
      as secure as using a different passwords for everything, it may be easier
      to use the same one for things that do not matter (like websites), and
      different ones for important things (like your online banking account and
      your email).</p>
   </item>
   <item>
     <p>Регулярно меняйте пароли.</p>
   </item>
  </list>

</page>
