<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="net-what-is-ip-address" xml:lang="it">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Un indirizzo IP è come il numero di telefono del proprio computer.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Cos'è un indirizzo IP?</title>

  <p>“IP address” stands for <em>Internet Protocol address</em>, and each
  device that is connected to a network (like the internet) has one.</p>

  <p>Un indirizzo IP è simile a un numero di telefono. Il numero di telefono è un gruppo di cifre che identifica univocamente il telefono in maniera tale da consentire ad altri di chiamarlo. Analogamente, un indirizzo IP è un gruppo univoco di numeri che identifica il computer, rendendo così possibile lo scambio di dati con altri computer.</p>

  <p>Attualmente, la maggior parte degli indirizzi IP consiste di quattro gruppi di numeri, ciascuno separato da un punto: <code>192.168.1.42</code> è un esempio di indirizzo IP.</p>

  <note style="tip">
    <p>An IP address can either be <em>dynamic</em> or <em>static</em>. Dynamic
    IP addresses are temporarily assigned each time your computer connects to a
    network. Static IP addresses are fixed, and do not change. Dynamic IP
    addresses are more common that static addresses — static addresses are
    typically only used when there is a special need for them, such as in the
    administration of a server.</p>
  </note>

</page>
