<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="backup-how" xml:lang="it">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit>
      <name>Progetto documentazione di GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Klein Kravis</name>
      <email>kleinkravis44@outlook.com</email>
      <years>2020</years>
    </credit>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Use Déjà Dup (or another backup application) to make copies of
    your valuable files and settings to protect against loss.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>Come eseguire un backup</title>

  <p>Il modo più semplice per eseguire il backup di file e impostazioni è lasciare a un'applicazione dedicata la gestione di tale processo. Esistono diverse applicazioni atte a tale compito, una tra queste è <app>Déjà Dup</app>.</p>

  <p>La guida dell'applicazione scelta sarà di aiuto nell'impostazione delle preferenze per il backup così come per ripristinare i dati.</p>

  <p>An alternative option is to <link xref="files-copy">copy your files</link>
 to a safe location, such as an external hard drive, an online storage service,
 or a USB drive. Your <link xref="backup-thinkabout">personal files</link>
 and settings are usually in your Home folder, so you can copy them from there.</p>

  <p>La quantità di dati di cui è possibile effettuare il backup è limitata dalla capienza del dispositivo di destinazione. Avendo spazio a sufficienza, sarebbe ideale effettuare il backup dell'intera cartella Home con le seguenti eccezioni:</p>

<list>
 <item><p>Files that are already backed up somewhere else, such as to a USB drive,
 or other removable media.</p></item>
 <item><p>Files that you can recreate easily. For example, if you are a
 programmer, you do not have to back up the files that get produced when you
 compile your programs. Instead, just make sure that you back up the original
 source files.</p></item>
 <item><p>Qualsiasi file nel Cestino, il quale può essere trovato in <cmd>~/.local/share/Trash</cmd>.</p></item>
</list>

</page>
