<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-usemic" xml:lang="it">

  <info>
    <link type="guide" xref="media#sound"/>

    <revision version="gnome:40" date="2021-02-26" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Usare un microfono analogico o USB e selezionare il dispositivo di ingresso predefinito.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Usare un altro microfono</title>

  <p>Un microfono esterno può essere utilizzato per chiacchierare con gli amici, con i colleghi di lavoro, per fare registrazioni audio o con applicazioni multimediali. Anche se il computer dispone di un microfono incorporato o una webcam dotata di microfono, generalmente l'utilizzo di un microfono separato consente di ottenere risultati audio di migliore qualità.</p>

  <p>If your microphone has a circular plug, just plug it into the appropriate
  audio socket on your computer. Most computers have two sockets: one for
  microphones and one for speakers. This socket is usually light red in color
  or is accompanied by a picture of a microphone. Microphones plugged
  into the appropriate socket are usually used by default. If not, see the
  instructions below for selecting a default input device.</p>

  <p>Se è disponibile un microfono USB, collegarlo a una porta corrispondente del computer. I microfoni USB funzionano come dei dispositivi audio a sé e potrebbe essere necessario specificare quale microfono utilizzare come dispositivo predefinito.</p>

  <steps>
    <title>Selezionare il dispositivo audio d'ingresso</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>Sound</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Sound</gui> to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Input</gui> section, select the device that you want to
      use. The input level indicator should respond when you speak.</p>
    </item>
  </steps>

  <p>You can adjust the volume and switch the microphone off from this
  panel.</p>

</page>
