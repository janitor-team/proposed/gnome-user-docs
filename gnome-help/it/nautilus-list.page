<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-list" xml:lang="it">

  <info>
    <its:rules xmlns:its="http://www.w3.org/2005/11/its" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="nautilus-prefs" group="nautilus-list"/>

    <revision pkgversion="3.5.92" date="2012-09-19" status="review"/>
    <revision pkgversion="3.14.0" date="2014-09-23" status="review"/>
    <revision pkgversion="3.18" date="2014-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>
    <revision pkgversion="41.0" date="2021-10-15" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Controllare le informazioni visualizzate nelle colonne della vista a elenco.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Files list columns preferences</title>

  <p>There are several columns of information that you can display in the
  <gui>Files</gui> list view. Right-click a column header and select or
  deselect which columns should be visible.</p>

  <terms>
    <item>
      <title><gui>Nome</gui></title>
      <p>The name of folders and files.</p>
      <note style="tip">
        <p>The <gui>Name</gui> column cannot be hidden.</p>
      </note>
    </item>
    <item>
      <title><gui>Dimensione</gui></title>
      <p>La dimensione di una cartella è intesa come il numero di file che contiene. La dimensione di un file è data in byte, KB o MB.</p>
    </item>
    <item>
      <title><gui>Tipo</gui></title>
      <p>Mostra «Cartella» oppure il tipo di file come «Documento PDF», «Immagine JPEG», «Audio MP3», ecc...</p>
    </item>
    <item>
      <title><gui>Ultima modifica</gui></title>
      <p>Gives the date of the last time the file was modified.</p>
    </item>
    <item>
      <title><gui>Proprietario</gui></title>
      <p>Il nome dell'utente a cui appartiene il file o la cartella.</p>
    </item>
    <item>
      <title><gui>Gruppo</gui></title>
      <p>The group the file is owned by. Each user is normally in their own
      group, but it is possible to have many users in one group. For example, a
      department may have their own group in a work environment.</p>
    </item>
    <item>
      <title><gui>Permessi</gui></title>
      <p>Displays the file access permissions. For example,
      <gui>drwxrw-r--</gui></p>
      <list>
        <item>
          <p>The first character is the file type. <gui>-</gui> means regular
          file and <gui>d</gui> means directory (folder). In rare cases, other
          characters can also be shown.</p>
        </item>
        <item>
          <p>I successivi tre caratteri, <gui>rwx</gui>, indicano i permessi per l'utente che possiede il file.</p>
        </item>
        <item>
          <p>I successivi tre, <gui>rw-</gui>, indicano i permessi per tutti i membri del gruppo che possiede il file.</p>
        </item>
        <item>
          <p>Gli ultimi tre caratteri nella colonna, <gui>r--</gui>, indicano i permessi per tutti gli altri utenti nel sistema.</p>
        </item>
      </list>
      <p>Each permission has the following meanings:</p>
      <list>
        <item>
          <p><gui>r</gui>: readable, meaning that you can open the file or
          folder</p>
        </item>
        <item>
          <p><gui>w</gui>: writable, meaning that you can save changes to it</p>
        </item>
        <item>
          <p><gui>x</gui>: executable, meaning that you can run it if it is a
          program or script file, or you can access subfolders and files if it
          is a folder</p>
        </item>
        <item>
          <p><gui>-</gui>: permission not set</p>
        </item>
      </list>
    </item>
    <item>
      <title><gui>Tipo MIME</gui></title>
      <p>Visualizza il tipo MIME del file.</p>
    </item>
    <item>
      <title><gui>Posizione</gui></title>
      <p>Il percorso alla posizione del file.</p>
    </item>
    <item>
      <title><gui>Modified — Time</gui></title>
      <p>Mostra la data e l'orario dell'ultima modifica al file.</p>
    </item>
    <item>
      <title><gui>Ultimo accesso</gui></title>
      <p>Gives the date or time of the last time the file was modified.</p>
    </item>
  </terms>

</page>
