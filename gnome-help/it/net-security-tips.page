<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-security-tips" xml:lang="it">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.4.0" date="2012-02-21" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Steven Richards</name>
      <email>steven.richardspc@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>General tips to keep in mind when using the internet.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Sicurezza su Internet</title>

  <p>A possible reason for why you are using Linux is the robust security that
  it is known for. One reason that Linux is relatively safe from malware and
  viruses is due to the lower number of people who use it. Viruses are targeted
  at popular operating systems, like Windows, that have an extremely large user
  base. Linux is also very secure due to its open source nature, which allows
  experts to modify and enhance the security features included with each
  distribution.</p>

  <p>Nonostante le misure adottate per garantire la sicurezza dell'installazione del sistema operativo, ci sono sempre delle vulnerabilità. L'utente medio di Internet è sempre esposto a:</p>

  <list>
    <item>
      <p>Phishing (siti web ed email che cercano di ottenere dati sensibili attraverso l'inganno)</p>
    </item>
    <item>
      <p><link xref="net-email-virus">Inoltro di email dannose</link></p>
    </item>
    <item>
      <p><link xref="net-antivirus">Applicazioni con scopi dannosi (virus)</link></p>
    </item>
    <item>
      <p><link xref="net-wireless-wepwpa">Unauthorized remote/local network
      access</link></p>
    </item>
  </list>

  <p>Per collegarsi in sicurezza, ricordare questi suggerimenti:</p>

  <list>
    <item>
      <p>Diffidare di email, allegati o collegamenti inviati da sconosciuti.</p>
    </item>
    <item>
      <p>If a website’s offer is too good to be true, or asks for sensitive
      information that seems unnecessary, then think twice about what
      information you are submitting and the potential consequences if that
      information is compromised by identity thieves or other criminals.</p>
    </item>
    <item>
      <p>Be careful in providing
      <link xref="user-admin-explain">root level permissions</link> to any
      application, especially ones that you have not used before or which are
      not well-known. Providing anyone or anything with root level permissions
      puts your computer at high risk to exploitation.</p>
    </item>
    <item>
      <p>Assicurarsi che siano in esecuzione unicamente i servizi ad accesso remoto necessari. Può essere utile mantenere i servizi SSH o VNC in esecuzione, ma questo potrebbe rendere il computer sensibile a intrusioni, se non adeguatamente protetto. Valutare l'opportunità di usare un <link xref="net-firewall-on-off">firewall</link> per aumentare la protezione del computer da intrusioni.</p>
    </item>
  </list>

</page>
