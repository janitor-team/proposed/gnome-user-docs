<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-remove" xml:lang="fa">

  <info>
    <link type="guide" xref="accounts" group="remove"/>
    <link type="seealso" xref="accounts-disable-service"/>

    <revision pkgversion="3.5.5" date="2012-08-15" status="draft"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="draft"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>مایکل هیل</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>اکاترینا گراسیموفا</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>شون مک‌کین</name>
      <email>shaunm@gnome.org</email>
      <years>۲۰۱۵</years>
    </credit>
    <credit type="editor">
      <name>کلایم کرویس</name>
      <email>kleinkravis44@outlook.com</email>
      <years>۲۰۲۰</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Remove access to an online service provider from your
    applications.</desc>
  </info>

  <title>برداشتن یک حساب</title>

  <p>می توانید حساب برخطی را که دیگر نمی‌خواهید استفاده کنید بردارید.</p>

  <note style="tip">
    <p>Many online services provide an authorization token which your desktop stores
    instead of your password. If you remove an account, you should also revoke
    that certificate in the online service. This will ensure that no other
    application or website can connect to that service using the authorization
    for your desktop.</p>

    <p>How to revoke the authorization depends on the service provider. Check
    your settings on the provider’s website for authorized or connected apps
    or sites. Look for an app called “GNOME” and remove it.</p>
  </note>

  <steps>
    <item>
      <p>نمای کلی <gui xref="shell-introduction#activities">فعالیت‌ها</gui> را باز کرده و تایپ <gui>حسابهای برخط</gui> را شروع کنید.</p>
    </item>
    <item>
      <p>برای گشودن تابلو، روی <gui>حسابهای برخط</gui> کلیک کنید.</p>
    </item>
    <item>
      <p>حسابی را که می خواهید بزدایید را برگزینید.</p>
    </item>
    <item>
      <p>روی دکمه <gui>-</gui> در گوشه پایین سمت چپ پنجره کلیک کنید.</p>
    </item>
    <item>
      <p>در گفتگوی تأیید <gui>حذف</gui> را کلیک کنید.</p>
    </item>
  </steps>

  <note style="tip">
    <p>به جای حذف کامل حساب، می توان <link xref="accounts-disable-service">خدمات را محدود کنید</link> که توسط دسک‌تاپ شما قابل دسترسی است.</p>
  </note>
</page>
