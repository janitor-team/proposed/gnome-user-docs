<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="tip" version="1.0 if/1.0 ui/1.0" id="shell-keyboard-shortcuts" xml:lang="fa">

  <info>
    <link type="guide" xref="tips"/>
    <link type="guide" xref="keyboard"/>
    <link type="guide" xref="shell-overview#apps"/>
    <link type="seealso" xref="keyboard-key-super"/>

    <revision pkgversion="3.8.0" version="0.4" date="2013-04-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.20" date="2016-08-13" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author copyright">
      <name>شون مک‌کین</name>
      <email>shaunm@gnome.org</email>
      <years>۲۰۱۲</years>
    </credit>
    <credit type="editor">
      <name>مایکل هیل</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>اکاترینا گراسیموفا</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>کار با میزکار با استفاده از صفحه‌کلید.</desc>
  </info>

<title>میان‌برهای مفید صفحه‌کلید</title>

<p>This page provides an overview of keyboard shortcuts that can help you
use your desktop and applications more efficiently. If you cannot use a
mouse or pointing device at all, see <link xref="keyboard-nav"/> for more
information on navigating user interfaces with only the keyboard.</p>

<table rules="rows" frame="top bottom" ui:expanded="true">
<title>کار با میزکار</title>
  <tr xml:id="alt-f1">
    <td><p><keyseq><key>دگرساز</key><key>F1</key></keyseq> یا</p>
      <p>کلید <key xref="keyboard-key-super">سوپر</key></p></td>
    <td><p>Switch between the <gui>Activities</gui> overview and desktop. In
    the overview, start typing to instantly search your applications, contacts,
    and documents.</p></td>
  </tr>
  <tr xml:id="alt-f2">
    <td><p><keyseq><key>دگرساز</key><key>F2</key></keyseq></p></td>
    <td><p>Pop up command window (for quickly running commands).</p>
    <p>برای دسترسی سریع به دستورهای اجرا شدهٔ پیشین، از کلیدهای جهت استفاده کنید.</p></td>
  </tr>
  <tr xml:id="super-tab">
    <td><p><keyseq><key>سوپر</key><key>جهش</key></keyseq></p></td>
    <td><p><link xref="shell-windows-switching">تعویض سریع بین پنجره‌ها</link>. برای ترتیب معکوس<key>تبدیل</key> را نگه دارید.</p></td>
  </tr>
  <tr xml:id="super-tick">
    <td><p><keyseq><key>سوپر</key><key>`</key></keyseq></p></td>
    <td>
      <p>Switch between windows from the same application, or from the selected
      application after <keyseq><key>Super</key><key>Tab</key></keyseq>.</p>
      <p>This shortcut uses <key>`</key> on US keyboards, where the <key>`</key>
      key is above <key>Tab</key>. On all other keyboards, the shortcut is
      <key>Super</key> plus the key above <key>Tab</key>.</p>
    </td>
  </tr>
  <tr xml:id="alt-escape">
    <td><p><keyseq><key>دگرساز</key><key>گریز</key></keyseq></p></td>
    <td>
      <p>Switch between windows in the current workspace. Hold down
      <key>Shift</key> for reverse order.</p>
    </td>
  </tr>
  <tr xml:id="ctrl-alt-tab">
    <!-- To be updated to <key>Tab</key> in the future. -->
    <td><p><keyseq><key>مهار</key><key>دگرساز</key><key>جهش</key></keyseq></p></td>
    <td>
      <p>Give keyboard focus to the top bar. In the <gui>Activities</gui>
      overview, switch keyboard focus between the top bar, dash, windows
      overview, applications list, and search field. Use the arrow keys to
      navigate.</p>
    </td>
  </tr>
  <tr xml:id="super-a">
    <td><p><keyseq><key>سوپر</key><key>A</key></keyseq></p></td>
    <td><p>Show the list of applications.</p></td>
  </tr>
  <tr xml:id="super-updown">
    <td>
      <p if:test="!platform:gnome-classic"><keyseq><key>سوپر</key><key>صفحه بالا</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>مهار</key><key>دگرساز</key><key>→</key></keyseq></p>
      <p>و</p>
      <p if:test="!platform:gnome-classic"><keyseq><key>سوپر</key><key>صفحه پایین</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>مهار</key><key>دگرساز</key><key>←</key></keyseq></p>
    </td>
    <td><p><link xref="shell-workspaces-switch">تعویض بین فضاهای کاری</link>.</p></td>
  </tr>
  <tr xml:id="shift-super-updown">
    <td>
      <p if:test="!platform:gnome-classic"><keyseq><key>تبدیل</key><key>سوپر</key><key>صفحه بالا</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>تبدیل</key><key>مهار</key><key>دگرساز</key><key>→</key></keyseq></p>
      <p>و</p>
      <p if:test="!platform:gnome-classic"><keyseq><key>تبدیل</key><key>سوپر</key><key>صفحه پایین</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>تبدیل</key><key>مهار</key><key>دگرساز</key><key>←</key></keyseq></p>
    </td>
    <td><p><link xref="shell-workspaces-movewindow">جابه‌جایی پنجرهٔ جاری به یک فضای کاری دیگر</link>.</p></td>
  </tr>
  <tr xml:id="shift-super-left">
    <td><p><keyseq><key>تبدیل</key><key>سوپر</key><key>←</key></keyseq></p></td>
    <td><p>Move the current window one monitor to the left.</p></td>
  </tr>
  <tr xml:id="shift-super-right">
    <td><p><keyseq><key>تبدیل</key><key>سوپر</key><key>→</key></keyseq></p></td>
    <td><p>Move the current window one monitor to the right.</p></td>
  </tr>
  <tr xml:id="ctrl-alt-Del">
    <td><p><keyseq><key>مهار</key><key>دگرساز</key><key>حذف</key></keyseq></p></td>
    <td><p><link xref="shell-exit#logout">نمایش گفت‌وگوی خاموش کردن</link>.</p></td>
  </tr>
  <tr xml:id="super-l">
    <td><p><keyseq><key>سوپر</key><key>L</key></keyseq></p></td>
    <td><p><link xref="shell-exit#lock-screen">قفل کردن صفحه.</link></p></td>
  </tr>
  <tr xml:id="super-v">
    <td><p><keyseq><key>سوپر</key><key>V</key></keyseq></p></td>
    <td><p>Show <link xref="shell-notifications#notificationlist">the notification
    list</link>. Press <keyseq><key>Super</key><key>V</key></keyseq> again or
    <key>Esc</key> to close.</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>میان‌برهای ویرایشی معمول</title>
  <tr>
    <td><p><keyseq><key>مهار</key><key>A</key></keyseq></p></td>
    <td><p>Select all text or items in a list.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>مهار</key><key>X</key></keyseq></p></td>
    <td><p>Cut (remove) selected text or items and place it on the clipboard.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>مهار</key><key>C</key></keyseq></p></td>
    <td><p>Copy selected text or items to the clipboard.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>مهار</key><key>V</key></keyseq></p></td>
    <td><p>Paste the contents of the clipboard.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>مهار</key><key>Z</key></keyseq></p></td>
    <td><p>Undo the last action.</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>ضبط از صفجه</title>
  <tr>
    <td><p><key>نماگرفت</key></p></td>
    <td><p><link xref="screen-shot-record#screenshot">نماگرفت از صفحه.</link></p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>دگرساز</key><key>نماگرفت</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screenshot">نماگرفت از پنجره.</link></p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>تبدیل</key><key>نماگرفت</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Take a screenshot of an
    area of the screen.</link> The pointer changes to a crosshair. Click and
    drag to select an area.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>مهار</key><key>دگرساز</key><key>تبدیل</key><key>R</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screencast">آغاز و پایان ضبط صفحه.</link></p></td>
  </tr>
</table>

</page>
