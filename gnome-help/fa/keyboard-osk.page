<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="keyboard-osk" xml:lang="fa">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>

    <credit type="author">
      <name>جرمی بیچا</name>
      <email>jbicha@ubuntu.com</email>
    </credit>
    <credit type="author">
      <name>جولیتا اینکا</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>مایکل هیل</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>اکاترینا گراسیموفا</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Use an on-screen keyboard to enter text by clicking buttons with the
    mouse or a touchscreen.</desc>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>استفاده از صفحه‌کلید برصفحه</title>

  <p>If you do not have a keyboard attached to your computer or prefer not to
  use it, you can turn on the <em>on-screen keyboard</em> to enter text.</p>

  <note>
    <p>The on-screen keyboard is automatically enabled if you use a
    touchscreen</p>
  </note>

  <steps>
    <item>
      <p>نمای کلی <gui xref="shell-introduction#activities">فعالیت‌ها</gui> را بگشایید و شروع به نوشتن <gui> تنظیمات </gui> کنید.</p>
    </item>
    <item>
      <p>روی <gui> تنظیمات </gui> کلیک کنید.</p>
    </item>
    <item>
      <p>برای گشودن تابلو، روی <gui>دسترسی‌پذیری</gui> در نوار کناری کلیک کنید.</p>
    </item>
    <item>
      <p>Switch on <gui>Screen Keyboard</gui> in the <gui>Typing</gui>
      section.</p>
    </item>
  </steps>

  <p>When you next have the opportunity to type, the on-screen keyboard will
  open at the bottom of the screen.</p>

  <p>Press the <gui style="button">?123</gui> button to enter numbers and
  symbols. More symbols are available if you then press the
  <gui style="button">=/&lt;</gui> button. To return to the alphabet keyboard,
  press the <gui style="button">ABC</gui> button.</p>

  <p>You can press the
  <gui style="button"><media its:translate="no" type="image" src="figures/go-down-symbolic.svg" width="16" height="16"><span its:translate="yes">down</span></media></gui>
  button to hide the keyboard temporarily. The keyboard will show again
  automatically when you next press on something where you can use it.
  On a touchscreen, you can also pull up the keyboard by
  <link xref="touchscreen-gestures">dragging up from the bottom edge</link>.</p>
  <p>Press the
  <gui style="button"><media its:translate="no" type="image" src="figures/emoji-flags-symbolic.svg" width="16" height="16"><span its:translate="yes">flag</span></media></gui>
  button to change your settings for
  <link xref="session-language">Language</link> or
  <link xref="keyboard-layouts">Input Sources</link>.</p>

</page>
