<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" version="1.0 if/1.0" id="shell-notifications" xml:lang="fa">

  <info>
    <link type="guide" xref="shell-overview#desktop"/>

    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>
    <revision pkgversion="40" date="2021-09-10" status="review"/>

    <credit type="author">
      <name>مارینا ژوراخینسکایا</name>
      <email>marinaz@redhat.com</email>
    </credit>
    <credit type="editor">
      <name>مایکل هیل</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>اکاترینا گراسیموفا</name>
      <email>kittykat3756@gmail.com</email>
      <years>۲۰۱۳۷ ۲۰۱۵</years>
    </credit>
    <credit type="editor">
      <name>پیتر کوار</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>پیام‌هایی از بالای صفحه به پایین کشیده می‌شوند، زمان رخ دادن رویداهای خاص را به شما می‌گویند.</desc> </info>

<title>آگاهی‌ها و فهرست آگاهی</title>

<section id="what">
  <title>آگاهی چیست؟</title>

  <p>If an application or a system component wants to get your attention, a
  notification will be shown at the top of the screen, or on your lock screen.</p>

  <p>For example, if you get a new chat message or a new email, you will get a
  notification informing you. Chat notifications are given special treatment,
  and are represented by the individual contacts who sent you the chat
  messages.</p>

<!--  <p>To minimize distraction, some notifications first appear as a single line.
  You can move your mouse over them to see their full content.</p>-->

  <p>Other notifications have selectable option buttons. To close one of these
  notifications without selecting one of its options, click the close
  button.</p>

  <p>Clicking the close button on some notifications dismisses them. Others,
  like Rhythmbox or your chat application, will stay hidden in the notification 
  list.</p>

</section>

<section id="notificationlist">

  <title>فهرست آگاهی</title>

  <p>The notification list gives you a way to
  get back to your notifications when it is convenient for you. It appears when
  you click on the clock, or press
  <keyseq><key xref="keyboard-key-super">Super</key><key>V</key></keyseq>. The
  notification list contains all the notifications that you have not acted upon
  or that permanently reside in it.</p>

  <p>You can view a notification by clicking on it in the list. You can close
  the notification list by pressing
  <keyseq><key>Super</key><key>V</key></keyseq> again or <key>Esc</key>.</p>

  <p>Click the <gui>Clear List</gui> button to empty the list of
  notifications.</p>

</section>

<section id="hidenotifications">

  <title>نهفتن آگاهی‌ها</title>

  <p>If you are working on something and do not want to be bothered, you can
  switch off notifications.</p>

  <p>You can hide all notifications by opening the notification list and
  switching <gui>Do Not Disturb</gui> to on at the bottom. Alternatively:</p>

  <steps>
    <item>
      <p>نمای کلی <gui xref="shell-introduction#activities">فعالیت‌ها</gui> را بگشایید و شروع به نوشتن <gui> تنظیمات </gui> کنید.</p>
    </item>
    <item>
      <p>روی <gui> تنظیمات </gui> کلیک کنید.</p>
    </item>
    <item>
      <p>Click on <gui>Notifications</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p><gui>مزاحم نشوید</gui> را خاموش کنید.</p>
    </item>
  </steps>

  <p>When switched off, most notifications will not pop up at the top of the
  screen. <!--Very important notifications, such as when your battery is
  critically low, will still pop up.--> Notifications will still be available
  in the notification list when you display it (by clicking on the clock, or by
  pressing <keyseq><key>Super</key><key>V</key></keyseq>), and they will start
  popping up again when you switch the switch back to on.</p>

  <p>You can also disable or re-enable notifications for individual
  applications from the <gui>Notifications</gui> panel.</p>

</section>

<section id="lock-screen-notifications">

  <title>نهفتن آگاهی‌های صفحهٔ قفل</title>

  <p>When your screen is locked, notifications appear on the lock screen. You
  can configure the lock screen to hide these notifications for privacy reasons.</p>

  <steps>
    <title>To switch off notifications when your screen is locked:</title>
    <item>
      <p>نمای کلی <gui xref="shell-introduction#activities">فعالیت‌ها</gui> را بگشایید و شروع به نوشتن <gui> تنظیمات </gui> کنید.</p>
    </item>
    <item>
      <p>روی <gui> تنظیمات </gui> کلیک کنید.</p>
    </item>
    <item>
      <p>Click on <gui>Notifications</gui> in the sidebar to open the panel.</p>
    </item>
    <item><p>Switch <gui>Lock Screen Notifications</gui> to off.</p></item>
  </steps>

</section>

</page>
