<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-autosuspend" xml:lang="fa">

  <info>
     <link type="guide" xref="power#saving"/>
     <link type="seealso" xref="power-suspend"/>
     <link type="seealso" xref="shell-exit#suspend"/>

    <revision version="gnome:3.38.3" date="2021-03-07" status="candidate"/>
    <revision pkgversion="gnome:41" date="2021-09-08" status="candidate"/>

    <credit type="author copyright">
      <name>مایکل هیل</name>
      <email>mdhillca@gmail.com</email>
      <years>۲۰۱۶</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>پیکربندی رایانه‌تان برای تعلیق خودکار.</desc>
  </info>

  <title>برپایی تعلیق خودکار</title>

  <p>You can configure your computer to automatically suspend when idle.
  Different intervals can be specified for running on battery or plugged in.</p>

  <steps>

    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>Power</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Power</gui> to open the panel.</p>
    </item>
    <item>
      <p>در بخش <gui>گزینه‌های ذخیرهٔ نیرو</gui>، روی <gui>تعلیق خودکار</gui> بزنید.</p>
    </item>
    <item>
      <p>Choose <gui>On Battery Power</gui> or <gui>Plugged In</gui>, set the
      switch to on, and select a <gui>Delay</gui>. Both options can
      be configured.</p>

      <note style="tip">
        <p>On a desktop computer, there is one option labeled
        <gui>When Idle</gui>.</p>
      </note>
    </item>

  </steps>

</page>
