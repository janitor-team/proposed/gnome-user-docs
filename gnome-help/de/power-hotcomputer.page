<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-hotcomputer" xml:lang="de">

  <info>
    <link type="guide" xref="power#problems"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Es ist normal, dass Rechner warm werden, aber wenn sie zu warm werden, können sie überhitzen und dadurch beschädigt werden.</desc>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021.</mal:years>
    </mal:credit>
  </info>

<title>Mein Rechner wird sehr heiß</title>

<p>Die meisten Rechner werden nach einiger Zeit warm und einige werden ziemlich heiß. Das ist normal: Es gehört einfach zur Kühlung des Rechners. Nichts desto trotz kann es ein Zeichen sein, dass Ihr Rechner überhitzt, wenn er sehr warm wird, was Schäden verursachen kann.</p>

<p>Die meisten Laptops werden nach längerer Nutzung warm. Darüber brauchen Sie sich normalerweise keine Sorgen zu machen – Rechner produzieren viel Wärme und Laptops sind sehr kompakt, was bedeutet, dass sie die Hitze schnell abgeben müssen und sich dadurch das Gehäuse erhitzt. Einige Laptops werden jedoch zu heiß, wodurch eine angenehme Nutzung nicht möglich ist. Dies hängt oft mit einem schlecht durchdachten Kühlungssystem zusammen. Manchmal ist zusätzliches Kühlzubehör erhältlich, das Sie auf der Unterseite des Laptops befestigen und das die Kühlung verbessert.</p>

<p>Wenn Sie einen Arbeitsplatzrechner verwenden, der beim Anfassen heiß ist, könnte die Kühlung nicht ausreichen. Bereitet dies Ihnen Sorgen, können Sie zusätzliche Lüfter kaufen oder die Lüfter von Staub und anderen Blockaden befreien. Sie können auch in Betracht ziehen, den Rechner an einem besser durchlüfteten Ort zu platzieren – in geschlossenen Räumen (z.B. in einem Schrank) kann das Kühlsystem des Rechners die Hitze eventuell nicht schnell genug ableiten und kalte Luft kann nicht schnell genug zirkulieren.</p>

<p>Einige Leute schrecken vor den Gesundheitsrisiken heißer Laptops zurück. Es gibt Bedenken, dass der andauernde Einsatz von heißen Laptops auf den Oberschenkeln möglicherweise die Zeugungsfähigkeit vermindert und es gibt Berichte über kleinere Verbrennungen (in Extremfällen). Wenn Sie hinsichtlich dieser Risiken Bedenken hegen, sollten Sie einen Arzt zu Rate ziehen. Natürlich können Sie den Laptop auch einfach nicht auf Ihren Oberschenkeln abstellen.</p>

<p>Die meisten neueren Rechner schalten sich aus, wenn Sie zu heiß werden, um Schäden zu verhindern. Wenn sich Ihr Rechner ständig ausschaltet, könnte dies der Grund dafür sein. Falls Ihr Rechner überhitzt, müssen Sie ihn wahrscheinlich zur Reparatur bringen.</p>

</page>
