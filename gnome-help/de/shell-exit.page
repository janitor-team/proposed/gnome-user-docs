<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="shell-exit" xml:lang="de">

  <info>
    <link type="guide" xref="shell-overview"/>
    <link type="guide" xref="power"/>
    <link type="guide" xref="index" group="#first"/>

    <revision pkgversion="3.6.0" date="2012-09-15" status="review"/>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.24.2" date="2017-06-11" status="candidate"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>
    <revision pkgversion="3.36.1" date="2020-04-18" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="author">
      <name>Alexandre Franke</name>
      <email>afranke@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David Faour</name>
      <email>dfaour.gnome@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Erfahren Sie, wie Sie Ihr Benutzerkonto verlassen können, indem Sie sich abmelden, den Benutzer wechseln und so weiter.</desc>
    <!-- Should this be a guide which links to other topics? -->
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021.</mal:years>
    </mal:credit>
  </info>

  <title>Abmelden, ausschalten oder Benutzer wechseln</title>

  <p>Wenn Sie Ihren Rechner nicht weiter benutzen wollen, so können Sie ihn ausschalten, in Bereitschaft versetzen (um Strom zu sparen) oder eingeschaltet lassen und sich einfach nur abmelden.</p>

<section id="logout">
  <title>Abmelden oder Benutzer wechseln</title>

  <p>Um andere Benutzer Ihren Rechner benutzen zu lassen, können Sie sich entweder abmelden oder angemeldet bleiben und einfach nur den Benutzer wechseln. Wenn Sie den Benutzer wechseln, laufen alle Ihre Anwendungen weiter, und wenn Sie sich wieder anmelden, finden Sie alles wieder so vor, wie Sie es verlassen haben.</p>

  <p>Um sich <gui>abzumelden</gui> oder den <gui>Benutzer zu wechseln</gui>, klicken Sie auf das <link xref="shell-introduction#systemmenu">Systemmenü</link> rechts in der oberen Leiste, klappen Sie <gui>Ausschalten / Abmelden</gui> aus und wählen Sie die entsprechende Option.</p>

  <note if:test="!platform:gnome-classic">
    <p>Die Einträge <gui>Abmelden</gui> und <gui>Benutzer wechseln</gui> sind nur dann im Menü, wenn mehr als ein Benutzerkonto auf Ihrem System vorhanden ist.</p>
  </note>

  <note if:test="platform:gnome-classic">
    <p>Der Eintrag <gui>Benutzer wechseln</gui> erscheint nur dann im Menü, wenn mehr als ein Benutzerkonto auf Ihrem System vorhanden ist.</p>
  </note>

</section>

<section id="lock-screen">
  <info>
    <link type="seealso" xref="session-screenlocks"/>
  </info>

  <title>Bildschirm sperren</title>

  <p>Wenn Sie Ihren Rechner für eine kurze Zeit verlassen, sollten Sie den Bildschirm sperren, damit andere Personen nicht auf Ihre Dateien zugreifen und Anwendungen starten können. Wenn Sie zurückkehren, sehen Sie den <link xref="shell-lockscreen">Sperrbildschirm</link> und geben Sie einfach Ihr Passwort ein, um sich wieder anzumelden. Wenn Sie den Bildschirm nicht sperren, wird er nach einer gewissen Zeit automatisch gesperrt.</p>

  <p>Um Ihren Bildschirm zu sperren, klicken Sie auf das Systemmenü an der rechten Seite der oberen Leiste und wählen Sie <gui>Sperren</gui> unten im Menü.</p>

  <p>Wenn der Bildschirm gesperrt ist, können sich andere Benutzer mit dem jeweils eigenen Benutzerkonto anmelden, indem sie auf <gui>Benutzer wechseln</gui> unten rechts auf dem Anmeldebildschirm klicken. Sie selbst können zu Ihrer Arbeitsumgebung zurück wechseln, wenn der andere Benutzer seine Arbeit beendet hat.</p>

</section>

<section id="suspend">
  <info>
    <link type="seealso" xref="power-suspend"/>
  </info>

  <title>Bereitschaft</title>

  <p>Setzen Sie Ihren Rechner in Bereitschaft, um Strom zu sparen, solange Sie Ihren Rechner nicht benutzen. Sollten Sie einen Laptop verwenden, versetzt das System Ihren Rechner automatisch in Bereitschaft, wenn Sie den Deckel schließen. Dabei wird der aktuelle Zustand in den Arbeitsspeicher gespeichert und die meisten Funktionen Ihres Rechners werden abgeschaltet. Während der Bereitschaft wird dennoch ein sehr geringer Strom verbraucht.</p>

  <p>Wenn Sie Ihren Rechner manuell in Bereitschaft versetzen wollen, klicken Sie auf das Systemmenü rechts in der oberen Leiste, klappen Sie <gui>Ausschalten / Abmelden</gui> aus und wählen Sie <gui>Bereitschaft</gui>.</p>

</section>

<section id="shutdown">
<!--<info>
  <link type="seealso" xref="power-off"/>
</info>-->

  <title>Ausschalten oder neu starten</title>

  <p>Wenn Sie Ihren Rechner vollständig ausschalten oder neu starten wollen, klicken Sie auf das Systemmenü rechts in der oberen Leiste, klappen Sie <gui>Ausschalten / Abmelden</gui> aus und wählen dann entweder <gui>Neu starten …</gui> oder <gui>Ausschalten …</gui> aus.</p>

  <p>Wenn andere Benutzer angemeldet sind, ist es Ihnen möglicherweise nicht erlaubt, den Rechner auszuschalten oder neu zu starten, weil dies deren Sitzung beenden würde. Wenn Sie über die Rechte des Systemverwalters verfügen, werden Sie eventuell nach Ihrem Passwort gefragt, um den Rechner auszuschalten.</p>

  <note style="tip">
    <p>Sie können Ihren Rechner ausschalten, falls Sie ihn bewegen wollen und er über keinen Akku verfügt, der Akku wenig geladen ist oder die Ladung unzureichend ist. Ein ausgeschalteter Rechner verbraucht auch <link xref="power-batterylife">weniger Energie</link> als einer, der sich lediglich in einem Energiesparzustand befindet.</p>
  </note>

</section>

</page>
