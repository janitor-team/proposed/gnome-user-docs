<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-suspend" xml:lang="de">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspendfail"/>

    <desc>Bereitschaft versetzt Ihren Rechner in einen Energiesparmodus, so dass er weniger Strom verbraucht.</desc>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021.</mal:years>
    </mal:credit>
  </info>

<title>Was passiert, wenn ich den Rechner in Bereitschaft versetze?</title>

<p>Wenn Sie den Rechner in <em>Bereitschaft</em> versetzen, schicken Sie ihn in einen Schlafmodus. Alle Ihre Anwendungen und Dokumente bleiben geöffnet, aber der Bildschirm und andere Teile des Rechners werden ausgeschaltet, um Strom zu sparen. Der Rechner ist aber immer noch eingeschaltet und verbraucht ein klein wenig Strom. Sie können ihn durch einen Tastendruck oder Mausklick aufwecken. Falls das nicht funktioniert, versuchen Sie die Ein-/Ausschalttaste zu betätigen.</p>

<p>Manche Rechner haben mangelhafte Hardwareunterstützung, was dazu führt, dass sie unter Umständen <link xref="power-suspendfail">nicht fehlerfrei in Bereitschaft 
versetzt</link> werden können. Es ist ratsam die Bereitschaft auf Ihrem Rechner auf ihre Funktionsfähigkeit hin zu überprüfen, bevor Sie diese einsetzen.</p>

<note style="important">
  <title>Speichern Sie immer Ihre Arbeit, bevor Sie den Rechner in Bereitschaft versetzen</title>
  <p>Sie sollten Ihre Arbeit speichern, bevor Sie den Rechner in Bereitschaft versetzen, für den Fall, dass etwas schief geht und Ihre offenen Anwendungen und Dokumente nicht wiederhergestellt werden können, wenn Sie den Rechner wieder einschalten.</p>
</note>

</page>
