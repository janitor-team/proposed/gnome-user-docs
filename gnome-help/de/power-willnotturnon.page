<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-willnotturnon" xml:lang="de">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="guide" xref="hardware-problems-graphics" group="#last"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Lose Kabel und Hardwareprobleme sind häufig mögliche Gründe dafür.</desc>
    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021.</mal:years>
    </mal:credit>
  </info>

<title>Mein Rechner lässt sich nicht einschalten</title>

<p>Es gibt eine Reihe von Gründen, weshalb sich Ihr Rechner nicht einschalten lässt. Dieses Thema gibt einen kurzen Überblick über mögliche Gründe.</p>
	
<section id="nopower">
  <title>Rechner nicht eingeschaltet, leerer Akku oder nicht eingesteckte Kabel</title>
  <p>Überprüfen Sie, ob das Stromkabel des Rechners richtig angeschlossen und Mehrfachsteckdose bzw. Netzteil eingeschaltet sind. Stellen Sie sicher, dass der Bildschirm ebenfalls richtig angeschlossen und eingeschaltet ist. Falls Sie einen Laptop verwenden, schließen Sie das Ladekabel an (falls der Akku leer ist). Sie können auch überprüfen, ob der Akku richtig sitzt (sehen Sie auf der Unterseite des Laptops nach), falls er herausnehmbar ist.</p>
</section>

<section id="hardwareproblem">
  <title>Problem mit der Hardware des Rechners</title>
  <p>Eine Komponente Ihres Rechners könnte defekt oder fehlerhaft sein. In diesem Fall müssen Sie Ihren Rechner zur Reparatur bringen. Häufige Fehlerursachen sind u.a. ein defektes Netzteil, ungeeignete Komponenten (wie Arbeitsspeicher/RAM) und ein Versagen der Hauptplatine.</p>
</section>

<section id="beeps">
  <title>Der Rechner gibt einen Warnton aus und schaltet sich dann ab</title>
  <p>Wenn der Rechner nach dem Einschalten mehrmals piept und sich dann wieder abschaltet (oder nicht korrekt startet), kann dies anzeigen, dass ein Problem festgestellt wurde. Dieses Piepen wird auch als <em>Beep-Codes</em> bezeichnet und der Verlauf des Piepens soll das Problem mit dem Rechner angeben. Verschiedene Hersteller nutzen verschiedene Beep-Codes, Sie müssen also das Handbuch der Hauptplatine Ihres Rechners zu Hilfe ziehen oder den Rechner zur Reparatur bringen.</p>
</section>

<section id="fans">
  <title>Die Lüfter des Rechners drehen sich, aber auf dem Bildschirm ist nichts zu sehen</title>
  <p>Überprüfen Sie zuerst, ob Ihr Bildschirm korrekt angeschlossen und eingeschaltet ist.</p>
  <p>Dieses Problem könnte auch durch ein Geräteversagen ausgelöst werden. Selbst wenn die Lüfter beim Drücken des Startknopfes anspringen, könnten andere erforderliche Teile Ihres Rechners dies vielleicht nicht tun. Bringen Sie Ihren Rechner in diesem Fall zur Reparatur.</p>
</section>

</page>
