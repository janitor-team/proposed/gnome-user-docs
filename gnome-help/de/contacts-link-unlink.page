<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="contacts-link-unlink" xml:lang="de">

  <info>
    <link type="guide" xref="contacts"/>
    <revision pkgversion="3.5.5" date="2012-02-19" status="review"/>
    <revision pkgversion="3.8" date="2013-04-27" status="review"/>
    <revision pkgversion="3.12" date="2014-02-27" status="final"/>
    <revision pkgversion="3.15" date="2015-01-28" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.38.0" date="2020-11-02" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <credit type="editor">
      <name>Pranali Deshmukh</name>
      <email>pranali21293@gmail.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Informationen aus mehreren Quellen für einen Kontakt kombinieren.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021.</mal:years>
    </mal:credit>
  </info>

<title>Kontakte verknüpfen oder deren Verknüpfung lösen</title>

<section id="link-contacts">
  <title>Kontakte verknüpfen</title>

  <p>Das Verknüpfen von mehrfachen Kontakten in Ihrem lokalen Adressbuch und Online-Konten legt diese in einen einzigen <app>Kontakt</app>eintrag zusammen. Das erleichtert die Verwaltung Ihres Adressbuchs, da alle Details zu einem Kontakt an einem zentralen Ort verfügbar sind.</p>

  <steps>
    <item>
      <p>Aktivieren Sie den <em>Auswahlmodus</em>, indem Sie den Haken oberhalb der Kontaktliste setzen.</p>
    </item>
    <item>
      <p>Eine Auswahlbox erscheint neben jedem Kontakt. Haken Sie die Auswahlboxen der Kontakte an, die Sie zusammenführen wollen.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui style="button">Verknüpfen</gui>, um die gewählten Kontakte zu verknüpfen.</p>
    </item>
  </steps>

</section>

<section id="unlink-contacts">
  <title>Kontaktverknüpfungen lösen</title>

  <p>Sie können die Verknüpfung von Kontakten lösen, falls Sie versehentlich Kontakte verknüpft haben, die nicht zusammen gehören.</p>

  <steps>
    <item>
      <p>Wählen Sie den Kontakt aus, dessen Verknüpfung Sie lösen wollen.</p>
    </item>
    <item>
      <p>Klicken Sie auf <media its:translate="no" type="image" src="figures/view-more-symbolic.svg">
      <span its:translate="yes">Mehr anzeigen</span></media> in der oberen rechten Ecke von <app>Kontakte</app>.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui style="button">Trennen</gui>, um die Verknüpfung des Eintrags von dem Kontakt zu lösen.</p>
    </item>
  </steps>

</section>

</page>
