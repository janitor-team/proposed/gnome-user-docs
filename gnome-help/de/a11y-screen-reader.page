<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="a11y task" version="1.0 if/1.0" id="a11y-screen-reader" xml:lang="de">

  <info>
    <link type="guide" xref="a11y#vision" group="blind"/>

    <revision pkgversion="3.13.92" date="2014-09-20" status="incomplete"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Jana Heves</name>
      <email>jsvarova@gnome.org</email>
    </credit>

    <desc>Den <app>Orca</app>-Bildschirmleser verwenden, damit er die Benutzerschnittstelle vorliest.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021.</mal:years>
    </mal:credit>
  </info>

  <title>Den Bildschirm laut vorlesen</title>

  <p>Der Bildschirmleser <app>Orca</app> steht zum Vorlesen der Benutzeroberfläche zur Verfügung. Abhängig davon, wie Sie Ihr System installiert haben, ist Orca bei Ihnen möglicherweise nicht installiert. Installieren Sie in diesem Fall zuerst Orca.</p>

  <p if:test="action:install"><link style="button" action="install:orca">Orca installieren</link></p>
  
  <p>So starten Sie <app>Orca</app> mit der Tastatur:</p>
  
  <steps>
    <item>
    <p>Drücken Sie <key>Super</key>+<key>Alt</key>+<key>S</key>.</p>
    </item>
  </steps>
  
  <p>So starten Sie <app>Orca</app> mit Maus und Tastatur:</p>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Barrierefreiheit</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Barrierefreiheit</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Stellen Sie im Abschnitt <gui>Sehen</gui> auf <gui>Bildschirmleser</gui>. Schalten Sie dann den <gui>Bildschirmleser</gui> ein.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Den Bildschirmleser schnell ein- und ausschalten</title>
    <p>Sie können den Bildschirmleser ein- und ausschalten, indem Sie auf das <link xref="a11y-icon">Symbol für Barrierefreiheit</link> in der oberen Leiste klicken und <gui>Bildschirmleser</gui> wählen.</p>
  </note>

  <p>Schauen Sie im Handbuch von <link href="help:orca">Orca</link> für weitere Informationen.</p>
</page>
