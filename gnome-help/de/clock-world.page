<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="clock-world" xml:lang="de">

  <info>
    <link type="guide" xref="clock" group="#last"/>
    <link type="seealso" href="help:gnome-clocks/index"/>

    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.28" date="2018-07-30" status="review"/>
    <revision pkgversion="3.37" date="2020-08-06" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhill@gnome.org</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Zeiten anderer Orte unter dem Kalender anzeigen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021.</mal:years>
    </mal:credit>
  </info>

  <title>Eine Weltuhr hinzufügen</title>

  <p>Verwenden Sie <app>Uhren</app>, um Zeiten anderer Orte hinzuzufügen.</p>

  <note>
    <p>Dafür muss <app>Uhren</app> auf Ihrem Rechner installiert sein.</p>
    <p>In den meisten Distributionen wird <app>Uhren</app> standardmäßig installiert. Falls dies bei Ihnen nicht der Fall ist, müssen Sie es über die Paketverwaltung Ihrer Distribution installieren.</p>
  </note>

  <p>So fügen Sie eine Weltuhr hinzu:</p>

  <steps>
    <item>
      <p>Klicken Sie auf die Uhr in der oberen Leiste.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Weltuhren hinzufügen …</gui> im Kalender, um <app>Uhren</app> zu starten.</p>

    <note>
       <p>Wenn Sie bereits eine oder mehrere Weltuhren haben, klicken Sie auf eine davon und <app>Uhren</app> wird geöffnet.</p>
    </note>

    </item>
    <item>
      <p>Klicken Sie Im Fenster von <app>Uhren</app> auf den Knopf <gui style="button">+</gui> oder drücken Sie <keyseq><key>Strg</key><key>N</key></keyseq>, um eine neue Stadt hinzuzufügen.</p>
    </item>
    <item>
      <p>Beginnen Sie damit, den Namen der Stadt in die suche einzugeben.</p>
    </item>
    <item>
      <p>Wählen Sie die Stadt oder die nächstgelegene aus der Liste aus.</p>
    </item>
    <item>
      <p>Klicken Sie auf den Knopf <gui style="button">Hinzufügen</gui>, um die Stadt hinzuzufügen.</p>
    </item>
  </steps>

  <p>In <link href="help:gnome-clocks">Hilfe zu Uhren</link> sind weitere Fähigkeiten von <app>Uhren</app> beschrieben.</p>

</page>
