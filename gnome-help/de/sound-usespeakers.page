<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-usespeakers" xml:lang="de">

  <info>
    <link type="guide" xref="media#sound"/>
    <link type="seealso" xref="sound-usemic"/>

    <revision version="gnome:40" date="2021-02-26" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Lautsprecher und Kopfhörer anschließen und ein Standardgerät für die Audioausgabe wählen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021.</mal:years>
    </mal:credit>
  </info>

  <title>Verschiedene Lautsprecher oder Kopfhörer verwenden</title>

  <p>Sie können externe Lautsprecher oder Kopfhörer an Ihrem Rechner verwenden. Lautsprecher werden üblicherweise mit einem Klinkenstecker oder über USB angeschlossen.</p>

  <p>Falls Ihre Lautsprecher oder Kopfhörer einen Klinkenstecker haben, stecken Sie diesen in die richtige Buchse ein. Die meisten Rechner haben zwei Buchsen: eine für Lautsprecher und eine für Mikrofone. Schauen Sie, ob sich neben der Buchse die Abbildung eines Kopfhörers befindet oder ob diese einen hellgrünen Ring hat. In den Klinkenbuchsen befindliche Lautsprecher oder Kopfhörer werden üblicherweise standardmäßig verwendet. Falls nicht, folgen Sie den Anweisungen zur Auswahl eines Standardgeräts.</p>

  <p>Einige Rechner unterstützen die mehrkanalige Ausgabe für Raumklang. Dafür werden im Allgemeinen mehrere Klinkenbuchsen mit unterschiedlichen Farbcodes verwendet. Wenn Sie nicht sicher sind, welche Stecker in welche Buchsen gehören, können Sie die Klangausgabe in den Klangeinstellungen testen.</p>

  <p>Falls Sie über USB-Lautsprecher oder -Kopfhörer verfügen, oder einen analogen Kopfhörer, der über eine USB-Soundkarte angeschlossen wird, dann schließen Sie ihn an einen beliebigen USB-Port an. USB-Lautsprecher funktionieren wie separate Audiogeräte. Sie haben dann zu entscheiden, welche Lautsprecher standardmäßig verwendet werden sollen.</p>

  <steps>
    <title>Vorgegebenes Audio-Ausgabegerät wählen</title>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Audio</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Audio</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Wählen Sie im Abschnitt <gui>Ausgang</gui> das Gerät, welches Sie verwenden wollen.</p>
    </item>
  </steps>

  <p>Klicken Sie auf <gui style="button">Test</gui>, um zu prüfen, ob alle Lautsprecher funktionieren und richtig angeschlossen sind.</p>

</page>
