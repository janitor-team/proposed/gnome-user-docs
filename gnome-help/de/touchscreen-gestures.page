<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="touchscreen-gestures" xml:lang="de">

  <info>
    <link type="guide" xref="mouse" group="#last"/>

    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <!--
    For 41: https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/121
    -->
    <revision version="gnome:40" date="2021-03-18" status="candidate"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Steuern Sie die Arbeitsumgebung mittels Gesten auf Ihrem Tastfeld oder Tastbildschirm.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021.</mal:years>
    </mal:credit>
  </info>

  <title>Gesten für Tastfelder und Tastbildschirme</title>

  <p>Mehrfinger-Gesten können jetzt auf Tastfeldern und Tastbildschirmen zur Bedienung des Systems und in Anwendungen verwendet werden.</p>

  <p>Einige Anwendungen setzen Gesten ein. Im <app>Dokumentenbetrachter</app> können Sie Dokumente vergrößern/verkleinern und verschieben. Im <app>Bildbetrachter</app> können Sie die Größe anpassen, drehen und verschieben.</p>

<section id="system">
  <title>Systemweite Gesten</title>

<table rules="rows" frame="bottom">
  <tr>
    <td><p><media type="image" src="figures/touch-overview.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Die Aktivitäten-Übersicht und Anwendungen-Ansicht öffnen</em></p>
    <p>Legen Sie drei Finger auf das Tastfeld oder den Tastbildschirm und wischen Sie nach oben, um die Aktivitäten-Übersicht zu öffnen.</p>
    <p>Um die Anwendungen-Ansicht zu öffnen, legen Sie drei Finger auf und wischen erneut nach oben.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-workspaces.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Arbeitsfläche wechseln</em></p>
    <p>Legen Sie drei Finger auf das Tastfeld oder den Tastbildschirm und wischen Sie nach links oder rechts.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-unfullscreen.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Vollbildmodus verlassen</em></p>
    <p>Wischen Sie auf dem Tastbildschirm von der oberen Kante aus nach unten, um den Vollbildmodus eines Fensters zu verlassen.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-osk.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Die Bildschirmtastatur einblenden</em></p>
    <p>Wischen Sie auf dem Tastbildschirm von der unteren Kante aus nach oben, um die <link xref="keyboard-osk">Bildschirmtastatur</link> hervorzuholen, sofern diese aktiviert ist.</p></td>
  </tr>
</table>

</section>

<section id="apps">
  <title>Anwendungsgesten</title>

<table rules="rows" frame="bottom">
  <tr>
    <td><p><media type="image" src="figures/touch-tap.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Ein Objekt öffnen, eine Anwendung starten, einen Musiktitel wiedergeben</em></p>
    <p>Tippen Sie auf ein Objekt.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-hold.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Ein Objekt wählen und Aktionen auflisten, die ausgeführt werden können</em></p>
    <p>Drücken und halten für wenige Sekunden.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-scroll.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Den Bildschirmbereich verschieben</em></p>
    <p>Ziehen: Mit einem Finger über den Bildschirm streichen.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-pinch-to-zoom.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Die Vergrößerungsstufe einer Ansicht ändern (<app>Karten</app>, <app>Fotos</app>)</em></p>
    <p>Zwei-Finger-Griff: Berühren Sie den Bildschirm mit zwei Fingern und führen Sie diese zueinander oder voneinander weg.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-rotate.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Ein Foto drehen</em></p>
    <p>Zwei-Finger-Drehung: Berühren Sie den Bildschirm mit zwei Fingern und drehen Sie sie.</p></td>
  </tr>
</table>

</section>

</page>
