<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="bluetooth-send-file" xml:lang="de">

  <info>
    <link type="guide" xref="bluetooth"/>
    <link type="guide" xref="sharing"/>
    <link type="seealso" xref="files-share"/>

    <revision pkgversion="3.8" date="2013-05-16" status="review"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Austausch von Dateien zwischen Bluetooth-Geräten, wie beispielsweise Ihrem Rechner und Ihrem Mobiltelefon.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021.</mal:years>
    </mal:credit>
  </info>

  <title>Dateien an ein Bluetooth-Gerät senden</title>

  <p>Sie können Dateien an verbundene Bluetooth-Geräte senden, zum Beispiel Mobiltelefone oder andere Rechner. Einige Gerätetypen ermöglichen keine Übertragungen von Dateien oder von bestimmten Dateitypen. Sie können Dateien mit dem Bluetooth-Einstellungsfenster verschicken.</p>

  <note style="important">
    <p><gui>Dateien senden</gui> wird nicht von allen Geräten unterstützt, wie beispielsweise iPhones.</p>
  </note>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Bluetooth</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Bluetooth</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Stellen Sie sicher, dass Bluetooth eingeschaltet ist: der Schalter in der Titelleiste muss angestellt sein.</p>
    </item>
    <item>
      <p>Wählen Sie das Zielgerät zum Senden der Dateien in der Liste <gui>Geräte</gui>. Wenn das gewünschte Gerät noch nicht in der Liste als <gui>verbunden</gui> angezeigt wird, müssen Sie eine <link xref="bluetooth-connect-device">Verbindung herstellen</link>.</p>
      <p>Ein an das externe Gerät angepasstes Panel erscheint.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Dateien Senden …</gui>. Daraufhin erscheint das Dateiauswahlfenster.</p>
    </item>
    <item>
      <p>Wählen Sie die gewünschte Datei aus und klicken Sie auf <gui>Auswählen</gui>.</p>
      <p>Um mehr als eine Datei aus einem Ordner zu versenden, halten Sie beim Markieren der Dateien die <key>Strg</key>-Taste gedrückt.</p>
    </item>
    <item>
      <p>Der Besitzer des empfangenden Gerätes muss üblicherweise eine Taste drücken, um eine Datei anzunehmen. Die Fortschrittsanzeige <gui>Bluetooth-Dateiübertragung</gui> wird daraufhin auf Ihrem Bildschirm angezeigt. Klicken Sie auf <gui>Schließen</gui>, sobald die Übertragung abgeschlossen ist.</p>
    </item>
  </steps>

</page>
