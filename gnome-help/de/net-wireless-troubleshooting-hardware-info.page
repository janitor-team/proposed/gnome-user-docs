<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting-hardware-info" xml:lang="de">

  <info>
    <link type="next" xref="net-wireless-troubleshooting-hardware-check"/>
    <link type="guide" xref="net-wireless-troubleshooting"/>

    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Mitwirkende im Dokumentations-Wiki von Ubuntu</name>
    </credit>
    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Für die folgenden Schritte zur Fehlerbeseitigung benötigen Sie eventuell Informationen wie die Modellnummer Ihres Funknetzwerk-Adapters.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021.</mal:years>
    </mal:credit>
  </info>

  <title>Problemlösungen für Funknetzwerke</title>
  <subtitle>Ermitteln von Informationen über Ihre Netzwerkgeräte</subtitle>

  <p>In diesem Schritt sammeln Sie Informationen über Ihr Funknetzwerkgerät. Wie man viele Probleme mit Funknetzwerken lösen kann, hängt nämlich von Typ und Ausführung des Funknetzwerkadapters ab; deswegen müssen Sie sich diese Details notieren. Darüber hinaus können auch die Begleitunterlagen hilfreich sein, die bei Ihrem Rechner dabei waren, etwa Treiber-CDs. Suchen Sie nach den folgenden Dingen, falls Sie sie noch haben:</p>

  <list>
    <item>
      <p>Die Verpackung und Anleitung für Ihre Funknetzwerkgeräte (insbesondere das Benutzerhandbuch Ihres Routers)</p>
    </item>
    <item>
      <p>Ein Treibermedium für Ihren Funknetzwerkadapter – auch wenn es nur Windows-Treiber enthält</p>
    </item>
    <item>
      <p>Die Angaben zu Hersteller und Modellnummer Ihres Rechners, Funknetzwerkadapters und Routers. Diese Informationen finden Sie üblicherweise auf der Unterseite oder Rückseite des Gerätes.</p>
    </item>
    <item>
      <p>Jedwede Versions-/Revisionsnummer, die auf Ihren Funknetzwerkgeräten oder deren Verpackung angebracht ist. Diese Informationen können besonders hilfreich sein, deswegen ist eine gründliche Suche empfehlenswert.</p>
    </item>
    <item>
      <p>Alles auf der Treiber-CD, wodurch das Gerät selbst identifiziert wird, seine »Firmware«-Version oder die verwendeten Bauteile (Chipset).</p>
    </item>
  </list>

  <p>Wenn möglich, versuchen Sie sich ersatzweise eine andere funktionierende Internetverbindung zu organisieren, um Software und Treiber herunterladen zu können, falls das erforderlich ist. (Eine Möglichkeit ist es, Ihren Rechner mit einem Netzwerkkabel direkt an den Router anzuschließen, aber tun Sie das nur, wenn es sein muss.)</p>

  <p>Wenn Sie so viele dieser Einzelheiten wie möglich gesammelt haben, klicken Sie auf <gui>Weiter</gui>.</p>

</page>
