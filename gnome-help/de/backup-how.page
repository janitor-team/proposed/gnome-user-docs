<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="backup-how" xml:lang="de">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit>
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Klein Kravis</name>
      <email>kleinkravis44@outlook.com</email>
      <years>2020</years>
    </credit>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Verwenden Sie Déjà Dup (oder eine andere Anwendung zur Datensicherung), um Kopien Ihrer wertvollen Dateien und Einstellungen zu erstellen und vor Verlust zu schützen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021.</mal:years>
    </mal:credit>
  </info>

<title>Eine Sicherung erstellen</title>

  <p>Der einfachste Weg zum Sichern Ihrer Daten und Einstellungen ist es, eine spezielle Anwendung zur Datensicherung dafür einzusetzen. Eine Reihe solcher Anwendungen ist verfügbar, wie beispielsweise <app>Déjà Dup</app>.</p>

  <p>Die Hilfe für Ihren gewählten Sicherungsort führt Sie durch die Einstellungen für die Sicherung und die Wiederherstellung der Daten.</p>

  <p>Eine alternative Option ist das <link xref="files-copy">Kopieren Ihrer Dateien</link> an einen sicheren Ort, beispielsweise eine externe Festplatte, einen Onlinespeicher-Dienst oder ein USB-Speichermedium. Ihre <link xref="backup-thinkabout">persönlichen Dateien</link> und Einstellungen finden Sie für gewöhnlich in Ihrem persönlichen Ordner, so dass Sie sie von dort kopieren können.</p>

  <p>Die mögliche zu sichernde Datenmenge wird durch die Größe des Speichergerätes beschränkt. Falls genügend Platz zur Verfügung steht, ist es am besten, den gesamten persönlichen Ordner zu sichern, mit folgenden Ausnahmen:</p>

<list>
 <item><p>Dateien, die bereits an einem anderen Ort gesichert sind, zum Beispiel auf einem USB-Speichermedium oder anderen Wechselmedien.</p></item>
 <item><p>Dateien, die sich einfach neu erstellen lassen. Sollten Sie z.B. Programmierer sein, brauchen Sie keine Dateien zu sichern, die beim Kompilieren von Programmen erstellt werden. Sorgen Sie aber dafür, dass Sie die originalen Quelldateien sichern.</p></item>
 <item><p>Alle Dateien im Papierkorb-Ordner. Sie finden den Papierkorb-Ordner unter <file>~/.local/share/Trash</file>.</p></item>
</list>

</page>
