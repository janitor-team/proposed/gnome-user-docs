<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-stickykeys" xml:lang="sv">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Mata in snabbtangenter en tangent i taget istället för att hålla ner alla tangenterna på en gång.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Aktivera klistriga tangenter</title>

  <p><em>Klistriga tangenter</em> låter dig skriva in snabbtangenter en tangent i taget istället för att hålla ner alla tangenterna på en gång. Till exempel växlar snabbtangenten <keyseq><key xref="keyboard-key-super">Super</key><key>Tabb</key></keyseq> mellan fönster. Utan att ha klistriga tangenter aktiverat så skulle du behöva hålla ner båda tangenterna samtidigt, med klistriga tangenter aktiverat skulle du behöva trycka på <key>Super</key> och sedan <key>Tabb</key> för att göra samma sak.</p>

  <p>Du kanske vill aktivera klistriga tangenter om du har svårt att hålla ner flera tangenter samtidigt.</p>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Inställningar</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Inställningar</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Hjälpmedel</gui> i sidopanelen för att öppna panelen.</p>
    </item>
    <item>
      <p>Tryck på <gui>Skrivassistent (AccessX)</gui> i avsnittet <gui>Skriva</gui>.</p>
    </item>
    <item>
      <p>Slå på <gui>Klistriga tangenter</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Aktivera och inaktivera klistriga tangenter snabbt</title>
    <p>Växla värdet för brytaren <gui>Aktivera med tangentbord</gui> för att kunna aktivera eller inaktivera klistriga tangenter från tangentbordet. När denna inställning är vald kan du trycka på <key>Skift</key> fem gånger i rad för att aktivera eller inaktivera klistriga tangenter.</p>
    <p>Du kan också aktivera eller inaktivera klistriga tangenter genom att klicka på <link xref="a11y-icon">hjälpmedelsikonen</link> i systemraden och välja <gui>Klistriga tangenter</gui>. Hjälpmedelsikonen är synlig när en eller flera inställningar har aktiverats från panelen <gui>Hjälpmedel</gui>.</p>
  </note>

  <p>Om du trycker på två tangenter samtidigt stänger klistriga tangenter tillfälligt av sig själv för att låta dig mata in snabbtangenter på det vanliga sättet.</p>

  <p>Till exempel om du har klistriga tangenter aktiverat men trycker ner <key>Super</key> och <key>Tabb</key> samtidigt så kommer klistriga tangenter inte att vänta på att du ska trycka ner ytterligare en tangent om du har denna inställning aktiverad. Den <em>hade</em> väntat om du bara hade tryckt ner en tangent dock. Detta är användbart om du kan trycka ner vissa snabbtangenter samtidigt (till exempel tangenter som sitter nära varandra), men inte andra.</p>

  <p>Välj <gui>Inaktivera om två tangenter trycks samtidigt</gui> för att aktivera detta.</p>

  <p>Du kan få datorn att spela upp ett ”pip”-ljud när du börjar mata in en snabbtangent när klistriga tangenter är aktiverat. Detta är användbart om du vill veta att klistriga tangenter förväntar sig att en snabbtangent håller på att matas in, så att nästa tangent som trycks ner kommer att tolkas som en del av snabbtangenten. Välj <gui>Pip när en modifierartangent trycks ned</gui> för att aktivera detta.</p>

</page>
