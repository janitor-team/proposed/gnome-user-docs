<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="bluetooth-visibility" xml:lang="sv">

  <info>
    <link type="guide" xref="bluetooth" group="#last"/>
    <link type="seealso" xref="bluetooth-device-specific-pairing"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-04" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Huruvida andra enheter kan detektera din dator.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Vad är Bluetooth-synlighet?</title>

  <p>Bluetooth-synlighet refererar till huruvida andra enheter kan detektera din dator när de söker efter Bluetooth-enheter. När Bluetooth är aktiverat och <gui>Bluetooth</gui>-panelen är öppen kommer din dator att utannonsera sig själv till alla enheter inom räckhåll vilket låter dem försöka att ansluta till dig.</p>

  <note style="tip">
    <p>Du kan <link xref="sharing-displayname">ändra</link>namnet som din dator visar för andra enheter.</p>
  </note>

  <p>Efter att du har <link xref="bluetooth-connect-device">anslutit till en enhet</link> behöver varken din dator eller enheten vara synliga för att kunna kommunicera med varandra.</p>

  <p>Enheter utan en skärm har vanligtvis ett ihopparningsläge som kan initieras genom att trycka på en knapp, eller en kombination av knappar under en viss tid, antingen när de redan är igång eller precis när de slås på.</p>

  <p>Det bästa sättet att ta reda på hur du initierar det läget är att referera till enhetens handbok. För vissa enheter kan proceduren vara <link xref="bluetooth-device-specific-pairing">lite annorlunda än vanligt</link>.</p>

</page>
