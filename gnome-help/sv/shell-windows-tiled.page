<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="shell-windows-tiled" xml:lang="sv">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.4.0" date="2012-03-14" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Maximera två fönster sida-vid-sida.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>


<title>Placera fönster sida-vid-sida</title>

  <p>Du kan maximera ett fönster enbart längs vänster eller höger sida av skärmen, vilket låter dig placera två fönster sida-vid-sida för att snabbt kunna växla mellan dem.</p>

  <p>För att maximera ett fönster längs en sida av skärmen, ta tag i namnlisten och dra den till vänster eller höger sida av skärmen tills halva skärmen markeras. Via tangentbordet, håll ner <key xref="keyboard-key-super">Super</key> och tryck på <key>Vänster</key>- eller <key>Höger</key>-tangenten.</p>

  <p>För att återställa ett fönster till dess normala storlek, dra det bort från sidan av skärmen eller använd samma tangentbordsgenväg du använde för att maximera.</p>

  <note style="tip">
    <p>Håll ner <key>Super</key>-tangenten och dra i ett fönster var som helst för att flytta det.</p>
  </note>

</page>
