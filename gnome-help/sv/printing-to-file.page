<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="task" id="printing-to-file" xml:lang="sv">

  <info>
    <link type="guide" xref="printing" group="#last"/>

    <revision pkgversion="3.8" date="2013-03-29" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Spara ett dokument som en PDF-, Postscript- eller SVG-fil istället för att skicka den till en skrivare.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Skriv ut till fil</title>

  <p>Du kan välja att skriva ut ett dokument till en fil istället för att skicka det för utskrift till en skrivare. Att skriva ut till en fil kommer att skapa en <sys>PDF-</sys>, <sys>Postscript-</sys> eller <sys>SVG</sys>-fil som innehåller dokumentet. Detta kan vara användbart om du vill överföra dokumentet till en annan maskin eller dela det med någon.</p>

  <steps>
    <title>För att skriva ut till fil:</title>
    <item>
      <p>Öppna utskriftsdialogen genom att trycka på <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Välj <gui>Skriv ut till fil</gui> under <gui>Skrivare</gui> i fliken <gui style="tab">Allmänt</gui>.</p>
    </item>
    <item>
      <p>För att ändra standardfilnamn och var filen sparas, klicka på filnamnet under skrivarvalet. Klicka på <gui style="button">Välj</gui> när du har valt klart.</p>
    </item>
    <item>
      <p><sys>PDF</sys> är standardfiltypen för dokumentet. Om du vill använda ett annat <gui>Utskriftsformat</gui>, välj antingen <sys>Postscript</sys> eller <sys>SVG</sys>.</p>
    </item>
    <item>
      <p>Välj dina övriga sidinställningar.</p>
    </item>
    <item>
      <p>Tryck på <gui style="button">Skriv ut</gui> för att spara filen.</p>
    </item>
  </steps>

</page>
