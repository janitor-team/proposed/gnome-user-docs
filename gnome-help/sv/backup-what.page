<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-what" xml:lang="sv">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Säkerhetskopiera allting som du inte kan stå ut med att förlora om något går fel.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Vad bör säkerhetskopieras</title>

  <p>Din högsta prioritet bör vara att säkerhetskopiera dina <link xref="backup-thinkabout">viktigaste filer</link> samt de som är svåra att återställa. Till exempel, rankade från allra viktigast till minst viktiga:</p>

<terms>
 <item>
  <title>Dina personliga filer</title>
   <p>Detta kan inkludera dokument, kalkylblad, e-post, kalenderbokningar, finansiell data, familjefoton eller andra personliga filer som du anser vara oersättliga.</p>
 </item>

 <item>
  <title>Dina personliga inställningar</title>
   <p>Detta kan omfatta ändrar du har gjort vad det gäller färger, bakgrunder, skärmupplösningar och musinställningar i din skrivbordsmiljö. Detta inkluderar programinställningar, till exempel inställningar för <app>LibreOffice</app>, din musikspelare och ditt e-postprogram. Dessa är ersättliga men tar ett tag att återställa.</p>
 </item>

 <item>
  <title>Systeminställningar</title>
   <p>De flesta personer ändrar aldrig på systeminställningar som skapats vid installationen. Om du brukar anpassa dina systeminställningar av någon anledning eller om du använder datorn som en server, då kanske du vill säkerhetskopiera dessa inställningarna.</p>
 </item>

 <item>
  <title>Installerad programvara</title>
   <p>Programvaran du använder kan normalt sett återställas ganska snabbt efter ett allvarligt datorproblem genom att ominstallera den.</p>
 </item>
</terms>

  <p>Generellt sett så vill du säkerhetskopiera filer som är oersättliga och filer som kräver en stor tidsinvestering för att ersätta dem utan en säkerhetskopia. Om saker, å andra sidan, är enkla att ersätta så kanske du inte vill använda upp diskutrymmet genom att ha säkerhetskopior av dem.</p>

</page>
