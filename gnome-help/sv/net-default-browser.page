<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-default-browser" xml:lang="sv">

  <info>
    <link type="guide" xref="net-browser"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.36" date="2020-06-24" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ändra standardwebbläsaren genom att gå till <gui>Detaljer</gui> i <gui>Inställningar</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Ändra vilken webbläsare som öppnar webbplatser som standard</title>

  <p>När du klickar på en länk till en webbsida i något program, kommer en webbläsare automatiskt att öppna upp den sidan. Om du däremot har mer än en webbläsare installerad kanske den inte öppnas i den webbläsare som du önskar att den ska öppnas i. För att åtgärda detta ändra standardwebbläsaren:</p>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <input>Standardprogram</input>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Standardprogram</gui> för att öppna panelen.</p>
    </item>
    <item>
      <p>Välj vilken webbläsare du önskar att öppna länkar i genom att ändra alternativet <gui>Webb</gui>.</p>
    </item>
  </steps>

  <p>När du öppnar en annan webbläsare kan den komma att berätta för dig att den inte är standardwebbläsaren längre. Om detta händer, klicka på knappen <gui>Avbryt</gui> (eller liknande) så att den inte försöker ställa in sig själv som standardwebbläsare igen.</p>

</page>
