<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-calibrate-printer" xml:lang="sv">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrate-scanner"/>
    <link type="seealso" xref="color-calibrate-screen"/>
    <link type="seealso" xref="color-calibrate-camera"/>
    <desc>Det är viktigt att kalibrera din skrivare för att utskriften ska ge rätt färger.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Hur kalibrerar jag min skrivare?</title>

  <p>Det finns två sätt att skapa en profil för en skrivare:</p>

  <list>
    <item><p>Använd en fotospektrometerenhet, som Pantone ColorMunki</p></item>
    <item><p>Hämta en utskriftsreferensfil från ett färgföretag</p></item>
  </list>

  <p>Att använda ett färgföretag för att generera en skrivarprofil är vanligtvis det billigaste alternativet om du bara har en eller två olika papperstyper. Genom att hämta referenskartan från företagens webbplatser kan du skicka utskriften tillbaka till dem i ett vadderat kuvert som de kommer att skanna, generera en färgprofil och e-posta dig en korrekt ICC-profil.</p>
  <p>Att använda en dyr enhet som ColorMunki blir endast billigare om du profilerar ett stort antal bläck eller papperstyper.</p>

  <note style="tip">
    <p>Om du ändrar din bläckleverantör se till att du omkalibrerar skrivaren!</p>
  </note>

</page>
