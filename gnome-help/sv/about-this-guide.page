<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="about-this-guide" xml:lang="sv">

  <info>
    <link type="guide" xref="more-help"/>
    <desc>Några tips om hur man använder hjälpguiden för skrivbordet.</desc>
    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

<title>Om denna handbok</title>
<p>Denna guide är designad för att ge dig en visning av funktionerna i din skrivbordsmiljö, svara på datorrelaterade frågor och ge tips om hur datorn används mer effektivt. Här är ett par saker att tänka på kring hjälpguiden:</p>

<list>
  <item><p>Guiden är uppdelad i små, uppgiftsorienterade ämnen — inte kapitel. Detta innebär att du inte behöver ögna igenom hela manualen för att hitta svaret på dina frågor.</p></item>
  <item><p>Relaterade saker länkas ihop. ”Se även”-länkar längst ner på vissa sidor kan också dirigera dig vidare till relaterade ämnen. Detta gör det enkelt att hitta liknande ämnen som kan hjälpa dig att utföra en viss uppgift.</p></item>
  <item><p>Den inkluderar inbyggd sökning. Raden överst i hjälpläsaren är en <em>sökrad</em> och relevanta sökresultat kommer att visas så snart du börjat skriva.</p></item>
  <item><p>Guiden förbättras hela tiden. Även om vi har försökt att tillhandahålla en omfattande samling med hjälpinformation så vet vi att vi inte kan svara på alla dina frågor här. Om du behöver mer assistans kan du höra med din distributions supportgrupp.</p></item>
</list>

</page>
