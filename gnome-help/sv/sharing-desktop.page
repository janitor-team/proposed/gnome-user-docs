<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="sharing-desktop" xml:lang="sv">

  <info>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.14" date="2015-01-25" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.29" date="2018-08-28" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Låt andra personer se och interagera med ditt skrivbord via VNC.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Dela ditt skrivbord</title>

  <p>Du kan låta andra personer se och styra ditt skrivbord från en annan dator med ett skrivbordsvisningsprogram. Konfigurera <gui>Skärmdelning</gui> till att låta andra personer få åtkomst till ditt skrivbord och ställ in säkerhetsinställningarna.</p>

  <note style="info package">
    <p>Du måste ha paketet <app>GNOME Fjärrskrivbord</app> installerat för att <gui>Skärmdelning</gui> ska visas.</p>

    <if:choose xmlns:if="http://projectmallard.org/if/1.0/">
      <if:when test="action:install">
        <p><link action="install:gnome-remote-desktop" style="button">Installera GNOME Fjärrskrivbord</link></p>
      </if:when>
    </if:choose>
  </note>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Inställningar</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Inställningar</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Delning</gui> i sidopanelen för att öppna panelen.</p>
    </item>
    <item>
      <p>Om <gui>Delning</gui> längst upp till höger i fönstret är av, slå på det.</p>

      <note style="info"><p>Om texten nedanför <gui>Datornamn</gui> låter dig redigera den, kan du <link xref="sharing-displayname">ändra</link> namnet din dator visar på nätverket.</p></note>
    </item>
    <item>
      <p>Välj <gui>Skärmdelning</gui>.</p>
    </item>
    <item>
      <p>För att låta andra se ditt skrivbord, slå på <gui>Skärmdelning</gui>. Detta innebär att andra personer kan försöka att ansluta till din dator och se vad som finns på din skärm.</p>
    </item>
    <item>
      <p>För att låta andra interagera med ditt skrivbord, säkerställ att <gui>Tillåt anslutningar att styra skärmen</gui> är ikryssat. Detta kan låta den andra personen flytta din mus, köra program och bläddra bland filer på din dator, beroende på säkerhetsinställningarna som du använder för närvarande.</p>
    </item>
  </steps>

  <section id="security">
  <title>Säkerhet</title>

  <p>Det är viktigt att du funderar på den fulla vidden av vad varje säkerhetsalternativ innebär innan du ändrar det.</p>

  <terms>
    <item>
      <title>Nya anslutningar måste be om åtkomst</title>
      <p>Om du vill kunna välja huruvida någon ska beviljas åtkomst till ditt skrivbord, aktivera <gui>Nya anslutningar måste fråga efter åtkomst</gui>. Om du inaktiverar detta alternativ kommer du inte att blir tillfrågad huruvida du vill tillåta någon att ansluta till din dator.</p>
      <note style="tip">
        <p>Detta alternativ är aktiverat som standard.</p>
      </note>
    </item>
    <item>
      <title>Kräv ett lösenord</title>
      <p>För att kräva att andra personer använder ett lösenord när de ansluter till ditt skrivbord, aktivera <gui>Begär lösenord</gui>. Om du inte använder detta alternativ kan vem som helst försöka se ditt skrivbord.</p>
      <note style="tip">
        <p>Detta alternativ är inaktiverat som standard, men du bör aktivera det och ställa in ett säkert lösenord.</p>
      </note>
    </item>
<!-- TODO: check whether this option exists.
    <item>
      <title>Allow access to your desktop over the Internet</title>
      <p>If your router supports UPnP Internet Gateway Device Protocol and it
      is enabled, you can allow other people who are not on your local network
      to view your desktop. To allow this, select <gui>Automatically configure
      UPnP router to open and forward ports</gui>. Alternatively, you can
      configure your router manually.</p>
      <note style="tip">
        <p>This option is disabled by default.</p>
      </note>
    </item>
-->
  </terms>
  </section>

  <section id="networks">
  <title>Nätverk</title>

  <p>Avsnittet <gui>Nätverk</gui> listar nätverken som du för närvarande är ansluten till. Använd brytaren intill respektive nätverk för att välja var ditt skrivbord kan delas.</p>
  </section>

  <section id="disconnect">
  <title>Sluta dela ditt skrivbord</title>

  <p>För att koppla från någon som tittar på ditt skrivbord:</p>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Inställningar</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Inställningar</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Delning</gui> i sidopanelen för att öppna panelen.</p>
    </item>
    <item>
      <p><gui>Skärmdelning</gui> kommer att visas som <gui>Aktiv</gui>. Klicka på det.</p>
    </item>
    <item>
      <p>Slå av inställningen längst upp.</p>
    </item>
  </steps>

  </section>


</page>
