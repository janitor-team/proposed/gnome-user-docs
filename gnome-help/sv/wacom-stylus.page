<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-stylus" xml:lang="sv">

  <info>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Definiera knappfunktioner och tryckkänsligheten för Wacom-pennan.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Konfigurera pennan</title>

<steps>
  <item>
    <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Inställningar</gui>.</p>
  </item>
  <item>
    <p>Klicka på <gui>Inställningar</gui>.</p>
  </item>
  <item>
    <p>Klicka på <gui>Wacom-ritplatta</gui> i sidopanelen för att öppna panelen.</p>
  </item>
  <item>
    <p>Klicka på knappen <gui>Styluspenna</gui> i rubrikraden.</p>
    <note style="tip"><p>Om ingen styluspenna detekteras kommer du att bli ombedd att <gui>Förflytta din styluspenna nära ritplattan för att konfigurera den</gui>.</p></note>
  </item>
  <item><p>Panelen innehåller detaljer och inställningar som är specifika för din penna, med enhetsnamnet (pennklassen) och diagram till vänster. Dessa inställningar kan justeras:</p>
    <list>
      <item><p><gui>Tryckkänsla för raderare:</gui> använd skjutreglaget för att justera ”känslan” (hur fysiskt tryck översätts till digitala värden) mellan <gui>Mjuk</gui> och <gui>Fast</gui>.</p></item>
      <item><p><gui>Knapp/Rullhjul</gui>-konfiguration (dessa ändras för att återspegla pennan). Klicka på menyn intill varje etikett för att välja en av dessa funktioner: ingen åtgärd, vänsterklick med musknapp, mittenklick med musknapp, högerklick med musknapp, rullning uppåt, rullning nedåt, rullning åt vänster, rullning åt höger, bakåt eller framåt.</p></item>
      <item><p><gui>Tryckkänsla för spets</gui> använd skjutreglaget för att justera ”känslan” mellan <gui>Mjuk</gui> och <gui>Fast</gui>.</p></item>
    </list>
  </item>
</steps>

<note style="info">
  <p>Om du har mer än en penna, använd växlaren bredvid pennans enhetsnamn för att välja vilken penna som ska konfigureras.</p>
</note>

</page>
