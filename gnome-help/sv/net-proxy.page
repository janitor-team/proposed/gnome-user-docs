<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:itst="http://itstool.org/extensions/" type="topic" style="task" id="net-proxy" xml:lang="sv">

  <info>
    <its:rules xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Baptiste Mille-Mathias</name>
      <email>baptistem@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>En proxy är en mellanliggande server för nättrafik, du kan användas för att komma åt webbtjänster anonymt, för att styrning eller av säkerhetsskäl.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Definiera proxyinställningar</title>

<section id="what">
  <title>Vad är en proxyserver?</title>

  <p>En <em>webbproxy</em> filtrerar webbplatser som du tittar på, den erhåller förfrågningar från din webbläsare för att hämta webbsidor och deras delar och genom att följa en policy kommer den att avgöra huruvida de ska skickas till dig. De används vanligtvis på företag och på publika trådlösa surfzoner för att styra vilka webbplatser du kan titta på, förhindra dig från att nå internet utan att logga in, eller göra säkerhetskontroll på webbplatser.</p>

</section>

<section id="change">
  <title>Ändra proxymetod</title>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Nätverk</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Nätverk</gui> för att öppna panelen.</p>
    </item>
    <item>
      <p>Välj <gui>Nätverksproxy</gui> från listan till vänster.</p>
    </item>
    <item>
      <p>Välj vilken proxymetod du vill använda bland:</p>
      <terms>
        <item>
          <title><gui itst:context="proxy">Ingen</gui></title>
          <p>Programmen kommer att använda en direktanslutning för att hämta innehållet från nätet.</p>
        </item>
        <item>
          <title><gui>Manuell</gui></title>
          <p>För varje proxy:at protokoll, definiera adressen för en proxy och port för protokollen. Protokollen är <gui>HTTP</gui>, <gui>HTTPS</gui>, <gui>FTP</gui> och <gui>SOCKS</gui>.</p>
        </item>
        <item>
          <title><gui>Automatisk</gui></title>
          <p>En URL som pekar på en resurs som innehåller en lämplig konfiguration för ditt system.</p>
        </item>
      </terms>
    </item>
  </steps>

  <p>Program som använder nätverksanslutningen kommer att använda dina angivna proxyinställningar.</p>

</section>

</page>
