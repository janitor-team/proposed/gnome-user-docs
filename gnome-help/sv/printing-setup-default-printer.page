<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-setup-default-printer" xml:lang="sv">

  <info>
    <link type="guide" xref="printing#setup"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.7.1" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision version="gnome:40" date="2021-03-05" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Välj skrivaren som du använder oftast.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Ställ in standardskrivaren</title>

  <p>Om du har mer än en skrivare tillgänglig kan du välja vilken som kommer att vara din standardskrivare. Du bör välja skrivaren som du använder oftast.</p>

  <note>
    <p>Du behöver <link xref="user-admin-explain">administrationsbehörighet</link> på systemet för att ställa in standardskrivaren.</p>
  </note>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Skrivare</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Skrivare</gui>.</p>
    </item>
    <item>
      <p>Beroende på ditt system kan du behöva trycka på <gui style="button">Lås upp</gui> i övre högra hörnet och skriva in ditt lösenord när du blir tillfrågad.</p>
    </item>
    <item>
      <p>Klicka på knappen <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">inställningar</span></media> intill skrivaren.</p>
    </item>
    <item>
      <p>Välj kryssrutan <gui style="menuitem">Använd skrivare som standard</gui>.</p>
    </item>
  </steps>

  <p>När du skriver ut i ett program kommer standardskrivaren automatiskt att användas, om du inte väljer en annan skrivare.</p>

</page>
