<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-inklevel" xml:lang="sv">

  <info>
    <link type="guide" xref="printing"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Anita Reitere</name>
      <email>nitalynx@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Kontrollera mängden bläck eller toner som finns kvar i skrivarpatroner.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Hur kan jag kontrollera min skrivares bläck- eller tonernivå?</title>

  <p>Hur du kontrollerar hur mycket bläck eller toner som finns kvar i din skrivare beror på din skrivares modell och tillverkare och på drivrutinerna och programmen som är installerade på din dator.</p>

  <p>Vissa skrivare har en inbyggd skärm som visar bläcknivåer och annan information.</p>

  <p>Vissa skrivare rapporterar toner- eller bläcknivåer till datorn, dessa kan hittas i panelen <gui>Skrivare</gui> i <app>Inställningar</app>. Bläcknivåerna kommer att visas tillsammans med skrivardetaljerna om de är tillgängliga.</p>

  <p>Drivrutiner och statusverktyg för de flesta HP-skrivare tillhandahålls av projektet HP Linux Imaging and Printing (HPLIP). Andra tillverkare kan leverera proprietära drivrutiner med liknande funktioner.</p>

  <p>Alternativt kan du installera ett program som kontrollerar eller övervakar bläcknivåer. <app>Inkblot</app> visar bläcknivåer för många HP-, Epson- och Canon-skrivare. Se om din skrivare finns på <link href="http://libinklevel.sourceforge.net/#supported">listan över modeller som stöds</link>. Ett annat program för bläcknivåer för Epson och några andra skrivare är <app>mtink</app>.</p>

  <p>Vissa skrivare saknar fortfarande bra stöd under Linux och andra skrivare är inte designade för att rapportera bläcknivåer.</p>

</page>
