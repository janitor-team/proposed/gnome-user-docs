<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="touchscreen-gestures" xml:lang="sv">

  <info>
    <link type="guide" xref="mouse" group="#last"/>

    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <!--
    For 41: https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/121
    -->
    <revision version="gnome:40" date="2021-03-18" status="candidate"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Manipulera ditt skrivbord via gester på din styrplatta eller pekskärm.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Använd gester på styrplattor och pekskärmar</title>

  <p>Flerfingersgester kan användas på styrplattor och pekskärmar för systemnavigering, så väl som i program.</p>

  <p>Ett antal program kan använda gester. I <app>Dokumentvisare</app> kan dokument zoomas och svepas undan med gester, och <app>Bildvisare</app> låter dig zooma, rotera och panorera.</p>

<section id="system">
  <title>Systemomfattande gester</title>

<table rules="rows" frame="bottom">
  <tr>
    <td><p><media type="image" src="figures/touch-overview.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Öppna översiktsvyn Aktiviteter och programvyn</em></p>
    <p>Placera tre fingrar på styrplattan eller pekskärmen och gör en gest uppåt för att öppna översiktsvyn Aktiviteter.</p>
    <p>För att öppna programvyn, placera tre fingrar och gör en gest uppåt igen.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-workspaces.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Växla arbetsyta</em></p>
    <p>Placera tre fingrar på styrplattan eller pekskärmen och gör en gest åt vänster eller höger.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-unfullscreen.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Lämna helskärmsläge</em></p>
    <p>På en pekskärm, dra neråt från den övre kanten för att lämna helskärmsläget för alla fönster.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-osk.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Ta fram skärmtangentbordet</em></p>
    <p>På en pekskärm, dra uppåt från den nedre kanten för att få upp <link xref="keyboard-osk">skärmtangentbordet</link>, om skärmtangentbordet är aktiverat.</p></td>
  </tr>
</table>

</section>

<section id="apps">
  <title>Programgester</title>

<table rules="rows" frame="bottom">
  <tr>
    <td><p><media type="image" src="figures/touch-tap.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Öppna ett objekt, starta ett program, spela en sång</em></p>
    <p>Tryck på ett objekt.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-hold.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Välj ett objekt och liståtgärder som kan utföras</em></p>
    <p>Tryck och håll under en eller två sekunder.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-scroll.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Rulla ytan på skärmen</em></p>
    <p>Dra: svep med ett finger som rör ytan.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-pinch-to-zoom.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Ändra zoomnivå för en vy (<app>Kartor</app>, <app>Foton</app>)</em></p>
    <p>Tvåfingernypning eller utsträckning: Rör vid ytan med två fingrar medan du rör dem närmre mot eller längre ifrån varandra.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-rotate.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Rotera ett foto</em></p>
    <p>Tvåfingersrotation: Rör vid ytan med två fingrar och rotera.</p></td>
  </tr>
</table>

</section>

</page>
