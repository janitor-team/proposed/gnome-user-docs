<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-resize" xml:lang="hu">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <revision pkgversion="3.25.90" date="2017-08-17" status="review"/>

    <desc>Fájlrendszer és partíciójának növelése vagy zsugorítása.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

<title>Fájlrendszer méretének beállítása</title>

  <p>A fájlrendszer növelésével kihasználható a meglévő partíció utáni üres hely. Ez gyakran akkor is lehetséges, ha a fájlrendszer csatolva van.</p>
  <p>A fájlrendszer a benne található üres hely erejéig zsugorítható a hely felszabadításához egy új, mögé helyezett partíciónak.</p>
  <p>Nem minden fájlrendszer támogatja az átméretezést.</p>
  <p>A partíció mérete a fájlrendszer méretével együtt lesz módosítva. Ugyanígy lehetséges a fájlrendszer nélküli partíciók átméretezése is.</p>

<steps>
  <title>Fájlrendszer/partíció átméretezése</title>
  <item>
    <p>Nyissa meg a <app>Lemezeket</app> a <gui>Tevékenységek</gui> áttekintésből.</p>
  </item>
  <item>
    <p>Válassza ki a fájlrendszert tartalmazó lemezt a tárolóeszközök bal oldali listájából. Ha több kötet is van a lemezen, akkor válassza ki a fájlrendszert tartalmazó kötetet.</p>
  </item>
  <item>
    <p>A <gui>Kötetek</gui> szakasz alatti eszköztáron kattintson a menü gombra, majd a <gui>Fájlrendszer átméretezése</gui> vagy ha nincs fájlrendszer, akkor a <gui>Formázás…</gui> menüpontra.</p>
  </item>
  <item>
    <p>Megjelenik egy ablak, ahol kiválasztható az új méret. A fájlrendszer csatolásra kerül a minimális méret kiszámításához az aktuális tartalom alapján. Ha a zsugorítás nem támogatott, akkor a minimális méret az aktuális méret. Zsugorításkor hagyjon elegendő helyet a fájlrendszeren, hogy az gyorsan és megbízhatóan működhessen.</p>
    <p>A zsugorított részről áthelyezendő adatok mennyiségétől függően a fájlrendszer átméretezése hosszabb ideig is eltarthat.</p>
    <note style="warning">
      <p>A fájlrendszer átméretezése automatikusan magába foglalja a fájlrendszer <link xref="disk-repair">javítását</link>. Emiatt a kezdés előtt javasolt biztonsági mentést készíteni a fontos adatokról. Ezt a műveletet nem szabad leállítani, mert az sérült fájlrendszert hagyna maga után.</p>
    </note>
  </item>
  <item>
      <p>Erősítse meg a művelet elkezdését az <gui style="button">Átméretezés</gui> gombra kattintással.</p>
   <p>A művelet leválasztja a fájlrendszert, ha a csatolt fájlrendszer átméretezése nem támogatott. Legyen türelmes a fájlrendszer átméretezéséig.</p>
  </item>
  <item>
    <p>A szükséges átméretezési és javítási műveletek befejezése után a fájlrendszer készen áll a használatra.</p>
  </item>
</steps>

</page>
