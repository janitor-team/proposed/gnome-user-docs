<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="printing-streaks" xml:lang="hu">

  <info>
    <link type="guide" xref="printing#problems"/>
    <link type="seealso" xref="printing-inklevel"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ha a nyomatok csíkosak, halványak vagy egyes színek hiányoznak, ellenőrizze a tintaszinteket, vagy tisztítsa meg a nyomtatófejet.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Miért csíkosak vagy hibás színűek a nyomataim?</title>

  <p>Ha a nyomatai csíkosak, halványak, váratlan helyeken tartalmaznak vonalakat vagy más módon rossz minőségűek, akkor az a nyomtatóval kapcsolatos hibát jelezhet, illetve alacsony tinta- vagy tonerszintet.</p>

  <terms>
     <item>
       <title>Halvány szöveg vagy képek</title>
       <p>Lehet, hogy a tinta vagy toner kifogyóban van. Ellenőrizze a tinta vagy toner szintjét, és szükség esetén vásároljon új patront.</p>
     </item>
     <item>
       <title>Csíkok és vonalak</title>
       <p>Ha tintasugaras nyomtatója van, akkor a nyomtatófej piszkos lehet, vagy részlegesen eltömődött. Próbálja megtisztítani a nyomtatófejet, utasításokért lásd a nyomtató kézikönyvét.</p>
     </item>
     <item>
       <title>Hibás színek</title>
       <p>Lehet, hogy az egyik színű tinta vagy toner kifogyóban van. Ellenőrizze a tinta vagy toner szintjét, és szükség esetén vásároljon új patront.</p>
     </item>
     <item>
       <title>Cakkos vagy nem egyenes vonalak</title>
       <p>Ha az egyenesnek szánt vonalak a nyomaton cakkosak, akkor igazítani kell a nyomtatófejen. A részletekért nézze meg a nyomtató kézikönyvét.</p>
     </item>
  </terms>

</page>
