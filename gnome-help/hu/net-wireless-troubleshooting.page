<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting" xml:lang="hu">

  <info>
    <link type="guide" xref="net-wireless" group="first"/>
    <link type="guide" xref="hardware#problems" group="first"/>
    <link type="next" xref="net-wireless-troubleshooting-initial-check"/>

    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Az Ubuntu dokumentációs wiki közreműködői</name>
    </credit>
    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Vezeték nélküli kapcsolatok problémáinak azonosítása és megoldása.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Vezeték nélküli hálózatok hibáinak elhárítása</title>

  <p>Ez a hibaelhárítási útmutató lépésről lépésre végigvezeti a vezeték nélküli hálózattal kapcsolatos problémák azonosításán és megoldásán. Ha nem tud csatlakozni a vezeték nélküli hálózathoz, akkor próbálja meg ezeket az utasításokat követni.</p>

  <p>A következő lépéseken végighaladva elérhető a számítógépe csatlakoztatása az internetre:</p>

  <list style="numbered compact">
    <item>
      <p>Kiinduló ellenőrzés végrehajtása</p>
    </item>
    <item>
      <p>Információk gyűjtése a hardveréről</p>
    </item>
    <item>
      <p>A hardver ellenőrzése</p>
    </item>
    <item>
      <p>Kísérlet kapcsolat létrehozására a vezeték nélküli routerhez</p>
    </item>
    <item>
      <p>A modem és router ellenőrzése</p>
    </item>
  </list>

  <p>A kezdéshez kattintson a <em>Következő</em> hivatkozásra az oldal jobb felső sarkában. Ez a hivatkozás, és a hasonló továbbiak végigvezetik az útmutató egyes lépésein.</p>

  <note>
    <title>A parancssor használata</title>
    <p>Ezen útmutató néhány utasítása parancsok beírására kéri a <em>parancssorba</em> (Terminál). A <app>Terminál</app> alkalmazást a <gui>Tevékenységek</gui> áttekintésben találhatja meg.</p>
    <p>Ha nem szokta használni a parancssort, ne aggódjon: az útmutató minden lépésnél segítséget ad. Csak arra kell figyelni, hogy a parancsok megkülönböztetik a kis- és nagybetűket (emiatt <em>pontosan</em> úgy kell beírnia azokat, ahogy itt megjelennek), és a parancsok beírása után meg kell nyomni az <key>Enter</key> billentyűt a futtatásukhoz.</p>
  </note>

</page>
