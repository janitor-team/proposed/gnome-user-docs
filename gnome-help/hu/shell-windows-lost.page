<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-lost" xml:lang="hu">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>

    <revision pkgversion="3.8.0" date="2013-04-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision version="gnome:40" date="2021-02-24" status="review"/>

    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Nézze meg a <gui>Tevékenységek</gui> áttekintést, vagy a többi munkaterületet.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Elveszett ablak megtalálása</title>

  <p>A másik munkaterületen lévő vagy egy másik ablak alatt lévő ablak egyszerűen megtalálható a <gui xref="shell-introduction#activities">tevékenységek áttekintés</gui> segítségével:</p>

  <list>
    <item>
      <p>Nyissa meg a <gui>Tevékenységek</gui> áttekintést. Ha a keresett ablak az éppen használt <link xref="shell-windows#working-with-workspaces">munkaterületen</link> van, akkor itt meg fog jelenni. Egyszerűen csak kattintson a bélyegképre az ismételt megjelenítéséhez, vagy</p>
    </item>
    <item>
      <p>Click different workspaces in the
      <link xref="shell-workspaces">workspace selector</link>
      to try to find your window, or</p>
    </item>
    <item>
      <p>kattintson a jobb egérgombbal az alkalmazásra az indítópanelen, és minden nyitott ablaka felsorolásra kerül. Ebben a listában kattintson arra az ablakra, amelyre át kíván váltani.</p>
    </item>
  </list>

  <p>Az ablakváltó használata:</p>

  <list>
    <item>
      <p>Nyomja meg a <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq> kombinációt az <link xref="shell-windows-switching">ablakváltó</link> megjelenítéséhez. Tartsa lenyomva a <key>Super</key> billentyűt, majd nyomja meg a <key>Tab</key> billentyűt a megnyitott ablakok listáján való végiglépkedéshez, vagy a <keyseq><key>Shift</key><key>Tab</key></keyseq> kombinációt a visszafelé lépkedéshez.</p>
    </item>
    <item if:test="!platform:gnome-classic">
      <p>Ha egy alkalmazásnak több ablaka is meg van nyitva, akkor az azokon való végiglépkedéshez nyomja le a <key>Super</key> billentyűt, és nyomja meg a <key>`</key> (magyar billentyűzeteken: a <key>Tab</key> fölötti <key>0</key>) billentyűt.</p>
    </item>
  </list>

</page>
