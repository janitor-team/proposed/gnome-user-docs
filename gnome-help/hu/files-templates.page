<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-templates" xml:lang="hu">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Anita Reitere</name>
      <email>nitalynx@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Új dokumentumok gyors létrehozása egyéni fájlsablonokból.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Sablonok gyakran használt dokumentumtípusokhoz</title>

  <p>Ha gyakran hoz létre azonos tartalmon alapuló dokumentumokat, akkor hasznos lehet fájlsablonokat használni. A fájlsablon tetszőleges típusú dokumentum lehet, amelyben az újra felhasználni kívánt formázás vagy tartalom található. Létrehozhat például egy sablon dokumentumot a céges levélfejléccel.</p>

  <steps>
    <title>Új sablon készítése</title>
    <item>
      <p>Hozza létre a sablonként használandó dokumentumot. Létrehozhat például egy céges levélfejlécet a szövegszerkesztőben.</p>
    </item>
    <item>
      <p>Mentse a sablont tartalmazó fájlt a <file>saját</file> mappájának <file>Sablonok</file> mappájába. Ha a <file>Sablonok</file> mappa nem létezik, akkor előbb hozza létre.</p>
    </item>
  </steps>

  <steps>
    <title>Sablon használata dokumentum létrehozására</title>
    <item>
      <p>Nyissa meg a mappát, amelybe el kívánja helyezni az új dokumentumot.</p>
    </item>
    <item>
      <p>Kattintson a jobb egérgombbal egy üres területre a mappában, majd válassza az <gui style="menuitem">Új dokumentum</gui> menüpontot. Az elérhető sablonok nevei az almenüben kerülnek felsorolásra.</p>
    </item>
    <item>
      <p>Válassza ki a kívánt sablont a listából.</p>
    </item>
    <item>
      <p>Kattintson duplán a fájlra a megnyitásához, és a szerkesztés elkezdéséhez. A szerkesztés befejezése után érdemes lehet <link xref="files-rename">átnevezni a fájlt</link>.</p>
    </item>
  </steps>

</page>
