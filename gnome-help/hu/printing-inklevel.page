<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-inklevel" xml:lang="hu">

  <info>
    <link type="guide" xref="printing"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Anita Reitere</name>
      <email>nitalynx@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ellenőrizze a nyomtatópatronokban lévő tinta vagy toner mennyiségét.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Hogyan ellenőrizhetem a nyomtatóm tinta- vagy tonerszintjeit?</title>

  <p>A nyomtatóban maradt tinta vagy toner szintjének ellenőrzési módja a nyomtató típusától és gyártójától függ, valamint a számítógépére telepített illesztőprogramoktól és alkalmazásoktól.</p>

  <p>Egyes nyomtatók beépített kijelzőn jelenítik meg a tintaszinteket és más információkat.</p>

  <p>Some printers report toner or ink levels to the computer, which can be
  found in the <gui>Printers</gui> panel in <app>Settings</app>. The ink
  level will be shown with the printer details if it is available.</p>

  <p>A legtöbb HP nyomtató illesztőprogramját és állapotfigyelő eszközét a HP Linux Imaging and Printing (HPLIP) projekt biztosítja. Más gyártók biztosíthatnak hasonló szolgáltatásokkal rendelkező, zárt forrású illesztőprogramokat.</p>

  <p>Ennek alternatívájaként telepíthet egy alkalmazást a tintaszintek ellenőrzésére vagy figyelésére. Az <app>Inkblot</app> képes számos HP, Epson és Canon nyomtató tintaszintjét kijelezni. Nézze meg, hogy nyomtatója szerepel-e a <link href="http://libinklevel.sourceforge.net/#supported">támogatott típusok listáján</link>. Az Epson és néhány más nyomtatókhoz használható másik tintaszint-figyelő alkalmazás az <app>mtink</app>.</p>

  <p>Néhány nyomtató még nem igazán jól támogatott Linux alatt, míg másokat nem terveztek a tintaszintek kijelzésére.</p>

</page>
