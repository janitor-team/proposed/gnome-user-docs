<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-envelopes" xml:lang="hu">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Győződjön meg róla, hogy a boríték megfelelő oldala van felfelé, és a megfelelő papírméretet választotta ki.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Borítékok nyomtatása</title>

  <p>A legtöbb nyomtató lehetővé teszi a borítékra való nyomtatást. Ez például akkor nagyon hasznos, ha sok levelet kell elküldenie.</p>

  <section id="envelope">
    <title>Nyomtatás borítékokra</title>

  <p>Borítékra való nyomtatás előtt két dolgot kell ellenőrizni.</p>
  <p>Az első, hogy a nyomtató ismerje a boríték méretét. Nyomja meg a <keyseq><key>Ctrl</key><key>P</key></keyseq> kombinációt a Nyomtatás ablak megnyitásához, lépjen az <gui>Oldalbeállítás</gui> lapra, és válassza a <gui>Papír típusa</gui> listából a „Boríték” értéket. Ha ez nem lehetséges, akkor próbálja meg a <gui>Papírméretet</gui> módosítani egy borítékméretre (például: <gui>C5</gui>). A borítékcsomagon fel van tüntetve a méret, a legtöbb boríték szabványos méretű.</p>

  <p>Másodszor meg kell győződnie, hogy a borítékok a megfelelő oldalukkal felfelé vannak a nyomtató papíradagolójába helyezve. Ehhez nézze meg a nyomtató kézikönyvét, vagy próbáljon meg egyetlen borítékra nyomtatni, hogy kiderüljön, melyik oldalával kell behelyezni.</p>

  <note style="warning">
    <p>Egyes nyomtatókkal, például bizonyos lézernyomtatókkal nem lehet borítékokra nyomtatni. Nézze meg a nyomtató kézikönyvében, hogy a nyomtató képes-e borítékra nyomtatni. Ellenkező esetben a nyomtató károsodhat, amikor mégis megpróbál borítékra nyomtatni.</p>
  </note>

  </section>

<!--
TODO: Please write this section!

<section id="labels">
 <title>Printing labels</title>

</section>
-->

</page>
