<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-fixed-ip-address" xml:lang="hu">

  <info>
    <link type="guide" xref="net-wired"/>
    <link type="seealso" xref="net-findip"/>

    <revision pkgversion="3.4.0" date="2012-03-13" status="final"/>
    <revision pkgversion="3.15" date="2014-12-04" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.24" date="2017-03-26" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Statikus IP-cím használatával egyszerűsíthető néhány hálózati szolgáltatás biztosítása a számítógépéről.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Statikus IP-címet használó kapcsolat létrehozása</title>

  <p>A legtöbb hálózat automatikusan társít <link xref="net-what-is-ip-address">IP-címet</link> és más adatokat a számítógépéhez, amikor az csatlakozik. Ezek az adatok rendszeresen változhatnak, de néha szüksége lehet rögzített IP-cím használatára, például ha fájlkiszolgálóként használja azt.</p>

  <!-- TODO Update for Network/Wi-Fi split -->
  <steps>
    <title>Rögzített IP-cím társítása a számítógéphez:</title>
    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni a <gui>Hálózat</gui> szót.</p>
    </item>
    <item>
      <p>Kattintson a <gui>Hálózat</gui> elemre a panel megnyitásához.</p>
    </item>
    <item>
      <p>Find the network connection that you want to have a fixed address.
      Click the
      <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">settings</span></media>
      button next to the network connection. For a <gui>Wi-Fi</gui> connection,
      the
      <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">settings</span></media>
      button will be located next to the active network.</p>
    </item>
    <item>
      <p>Select the <gui>IPv4</gui> or <gui>IPv6</gui> tab and change the
      <gui>Method</gui> to <gui>Manual</gui>.</p>
    </item>
    <item>
      <p>Írja be az <gui xref="net-what-is-ip-address">IP-címet</gui> és az <gui>Átjárót</gui>, valamint a megfelelő <gui>Hálózati maszkot</gui>.</p>
    </item>
    <item>
      <p>In the <gui>DNS</gui> section, switch the <gui>Automatic</gui> switch
      to off. Enter the IP address of a DNS server you want to use. Enter
      additional DNS server addresses using the <gui>+</gui> button.</p>
    </item>
    <item>
      <p>In the <gui>Routes</gui> section, switch the <gui>Automatic</gui>
      switch to off. Enter the <gui>Address</gui>, <gui>Netmask</gui>,
      <gui>Gateway</gui> and <gui>Metric</gui> for a route you want to use.
      Enter additional routes using the <gui>+</gui> button.</p>
    </item>
    <item>
      <p>Nyomja meg az <gui>Alkalmaz</gui> gombot. A hálózati kapcsolat mostantól rögzített IP-címmel rendelkezik.</p>
    </item>
  </steps>

</page>
