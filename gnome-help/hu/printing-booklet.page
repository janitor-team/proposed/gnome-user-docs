<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-booklet" xml:lang="hu">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Hogyan nyomtathat összehajtott, többlapos füzetet A4-es papír használatával.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Füzet nyomtatása</title>

  <p>Füzetet nyomtathat egy PDF fájlból.</p>

  <p>Ha egy <app>LibreOffice</app> dokumentumból szeretne füzetet nyomtatni, akkor előbb exportálja azt PDF-be a <guiseq><gui>Fájl</gui><gui>Exportálás PDF-be</gui></guiseq> menüpont kiválasztásával. A dokumentum oldalszámának 4 többszörösének kell lennie (4, 8, 12, 16, …). Szükség lehet akár 3 üres oldal beszúrására is.</p>

  <p>Ha PDF-dokumentumának oldalszáma nem 4 többszöröse, akkor megfelelő számú üres oldalt (1, 2 vagy 3) kell hozzáadnia, hogy 4 többszöröse legyen. Ehhez:</p>

  <steps>
    <item>
      <p>Hozzon létre egy <app>LibreOffice</app> dokumentumot, amely a szükséges számú (1-3) üres oldalt tartalmaz.</p>
    </item>
    <item>
      <p>Exportálja az üres oldalakat PDF-be a <guiseq><gui>Fájl</gui><gui>Exportálás PDF-be</gui></guiseq> menüpont kiválasztásával.</p>
    </item>
    <item>
      <p>Fésülje össze az üres oldalakat a PDF-dokumentummal a <app>PDF-Shuffler</app> vagy a <app>PDF Mod</app> segítségével, és helyezze az üres oldalakat a végére.</p>
    </item>
  </steps>

  <p>Válassza ki a használandó nyomtató típusát az alábbi listából:</p>

</page>
