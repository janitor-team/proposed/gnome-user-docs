<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="user-goodpassword" xml:lang="hu">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Használjon hosszabb és bonyolultabb jelszavakat.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Válasszon biztonságos jelszót</title>

  <note style="important">
    <p>Válasszon olyan jelszavakat, amelyeket könnyen megjegyez, de mások (beleértve a számítógépes programokat is) nehezen találnák ki.</p>
  </note>

  <p>A jó jelszóválasztás segít biztonságban tartani a számítógépén lévő adatait. Ha könnyen kitalálható jelszót használ, akkor valaki azt kitalálva hozzáférést szerezhet személyes információihoz.</p>

  <p>A jelszó szisztematikus kitalálásához a támadók számítógépet is használhatnak, így egy ember által nehezen kitalálható jelszót a számítógép rendkívül könnyen feltörhet. Néhány ötlet a jó jelszóválasztáshoz:</p>
  
  <list>
    <item>
      <p>Használjon kis- és nagybetűket, számokat, szimbólumokat és szóközöket is a jelszóban. Ettől nehezebb lesz kitalálni; ha több szimbólumból kell választani, akkor több lehetséges jelszót kell leellenőriznie annak, aki megpróbálja a jelszavát kitalálni.</p>
      <note>
        <p>Jó módszer jelszóválasztásra, ha egy könnyen megjegyezhető mondat szavainak első betűit veszi. A mondat lehet egy film, könyv, dal vagy album címe. Például a „Nekem lámpást adott kezembe az Úr, Pesten” kifejezésből előállítható a „NlakaÚP” vagy a „nlakaú,p” jelszó.</p>
      </note>
    </item>
    <item>
      <p>A jelszava legyen a lehető leghosszabb. Minél több karaktert tartalmaz, annál nehezebb egy személynek vagy számítógépnek kitalálnia.</p>
    </item>
    <item>
      <p>Ne használjon olyan szavakat, amelyek akármelyik nyelv alapszótárában szerepelnek. A jelszótörők ezeket próbálják elsőként. A leggyakoribb jelszó a „jelszo” – az ehhez hasonlókat bárki gyorsan kitalálja!</p>
    </item>
    <item>
      <p>Soha ne használjon személyes információkat, mint például egy dátum, rendszám vagy családtag neve.</p>
    </item>
    <item>
      <p>Ne használjon főnevet.</p>
    </item>
    <item>
      <p>Válasszon gyorsan begépelhető jelszót, ezzel csökkentve az esélyét annak, hogy gépelés közben valaki leolvashassa azt.</p>
      <note style="tip">
        <p>Soha ne írja fel a jelszavát. Az ilyen cetlik könnyen megtalálhatók!</p>
      </note>
    </item>
    <item>
      <p>Használjon különböző jelszavakat különböző dolgokhoz.</p>
    </item>
    <item>
      <p>Használjon különböző jelszavakat különböző fiókokhoz.</p>
      <p>Ha minden esetben ugyanazt a jelszót használja, bárki aki kitalálja a jelszavát az összes fiókjához azonnal hozzáfér.</p>
      <p>Ugyanakkor nehéz lehet sok jelszót megjegyezni. Bár nem olyan biztonságos, mintha mindenre különböző jelszót használna, egyszerűbb lehet ugyanazt a jelszót használni kevésbé fontos dolgokra (mint például a webhelyek) és ettől különbözőket a fontosakra (mint például az online bankfiókja és e-mail fiókja).</p>
   </item>
   <item>
     <p>Módosítsa rendszeresen a jelszavait.</p>
   </item>
  </list>

</page>
