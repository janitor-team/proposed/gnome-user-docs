<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-manual" xml:lang="hu">

  <info>
    <link type="guide" xref="net-wired"/>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.10" date="2013-11-11" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.28" date="2018-03-28" status="review"/>
    <revision pkgversion="3.33.3" date="2018-03-28" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <desc>Ha a hálózati beállításokat nem kapja meg automatikusan, akkor azokat saját kezűleg kell beírnia.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Hálózati beállítások kézi megadása</title>

  <p>Ha a hálózat nem rendel automatikusan hálózati beállításokat számítógépéhez, akkor szükség lehet a beállítások kézi megadására. Ez a leírás feltételezi, hogy már ismeri a megfelelő beállításokat. Ellenkező esetben kérdezze meg a hálózati rendszergazdát, vagy nézze meg a router vagy switch beállításait.</p>

  <steps>
    <title>Hálózati beállítások megadása saját kezűleg:</title>
    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni a <gui>Beállítások</gui> szót</p>
    </item>
    <item>
      <p>Válassza a <gui>Beállítások</gui> lehetőséget.</p>
    </item>
    <item>
      <p>If you plug in to the network with a cable, click <gui>Network</gui>.
      Otherwise click <gui>Wi-Fi</gui>.</p>
      <p>Győződjön meg róla, hogy a vezeték nélküli kártya be van kapcsolva, vagy a hálózati kábel be van dugva.</p>
    </item>
    <item>      
      <p>Kattintson a <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">beállítások</span></media> gombra.</p>
      <note>
        <p><gui>Wi-Fi</gui> kapcsolat esetén a <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">beállítások</span></media> gomb az aktív hálózat mellett található.</p>
      </note>
    </item>
    <item>
      <p>Select the <gui>IPv4</gui> or <gui>IPv6</gui> tab and change the
      <gui>Method</gui> to <gui>Manual</gui>.</p>
    </item>
    <item>
      <p>Írja be az <gui xref="net-what-is-ip-address">IP-címet</gui> és az <gui>Átjárót</gui>, valamint a megfelelő <gui>Hálózati maszkot</gui>.</p>
    </item>
    <item>
      <p>In the <gui>DNS</gui> section, switch the <gui>Automatic</gui> switch
      to off. Enter the IP address of a DNS server you want to use. Enter
      additional DNS server addresses using the <gui>+</gui> button.</p>
    </item>
    <item>
      <p>In the <gui>Routes</gui> section, switch the <gui>Automatic</gui>
      switch to off. Enter the <gui>Address</gui>, <gui>Netmask</gui>,
      <gui>Gateway</gui> and <gui>Metric</gui> for a route you want to use.
      Enter additional routes using the <gui>+</gui> button.</p>
    </item>
    <item>
      <p>Click <gui>Apply</gui>. If you are not connected to the network, open the
      <gui xref="shell-introduction#systemmenu">system menu</gui> from the right side of
      the top bar and connect. Test the network
      settings by trying to visit a website or look at shared files on the
      network, for example.</p>
    </item>
  </steps>

</page>
