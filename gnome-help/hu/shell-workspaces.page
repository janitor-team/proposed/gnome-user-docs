<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="ui" version="1.0 if/1.0" id="shell-workspaces" xml:lang="hu">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces" group="#first"/>

    <revision pkgversion="3.8.0" date="2013-04-23" status="review"/>
    <revision pkgversion="3.10.3" date="2014-01-26" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.35.91" date="2020-02-27" status="candidate"/>

    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>A munkaterületek az asztalán lévő ablakok csoportosítására szolgálnak.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

<title>Mi az a munkaterület, és mire használható?</title>

  <p if:test="!platform:gnome-classic">A munkaterületek az asztalán lévő ablakok csoportosítására szolgálnak. Több munkaterületet hozhat létre, amelyek virtuális asztalokként használhatók. A munkaterületek célja a zsúfoltság csökkentése, és az ablakok közti navigáció megkönnyítése.</p>

  <p if:test="platform:gnome-classic">A munkaterületek az asztalán lévő ablakok csoportosítására szolgálnak. Több munkaterületet használhat, amelyek virtuális asztalokként használhatók. A munkaterületek célja a zsúfoltság csökkentése, és az ablakok közti navigáció megkönnyítése.</p>

  <p>A munkaterületeket munkája rendszerezésére használhatja. Például az egyik munkaterületre elhelyezheti a kommunikációval kapcsolatos ablakokat, mint az e-mail és csevegés, a másikra pedig a munkájával kapcsolatos ablakokat. A zenelejátszó pedig a harmadik munkaterületre kerülhet.</p>

<p>Munkaterületek használata:</p>

<list>
  <item>
    <p if:test="!platform:gnome-classic">In the
    <gui xref="shell-introduction#activities">Activities</gui> overview, you can
    horizontally navigate between the workspaces.</p>
    <p if:test="platform:gnome-classic">Click the button at the bottom left of
    the screen in the window list, or press the
    <key xref="keyboard-key-super">Super</key> key to open the
    <gui>Activities</gui> overview.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">If more than one workspace is already
    in use, the <em>workspace selector</em> is shown between the search field and
    the window list. It will display currently used workspaces plus an empty workspace.</p>
    <p if:test="platform:gnome-classic">In the bottom right corner, you see four
    boxes. This is the workspace selector.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">To add a workspace, drag and drop a
    window from an existing workspace onto the empty workspace in the workspace
    selector. This workspace now contains the window you have dropped, and a new
    empty workspace will appear next to it.</p>
    <p if:test="platform:gnome-classic">Drag and drop a window from your current
    workspace onto an empty workspace in the workspace selector. This workspace
    now contains the window you have dropped.</p>
  </item>
  <item if:test="!platform:gnome-classic">
    <p>Egy munkaterület megszüntetéséhez zárja be a rajta lévő összes ablakot, vagy húzza át azokat más munkaterületekre.</p>
  </item>
</list>

<p if:test="!platform:gnome-classic">Mindig van legalább egy munkaterület.</p>

    <media its:translate="yes" type="image" src="figures/shell-workspaces.png" width="940" height="291">
        <p>Munkaterület-választó</p>
    </media>

</page>
