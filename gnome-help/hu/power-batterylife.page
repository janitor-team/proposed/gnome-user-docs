<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="power-batterylife" xml:lang="hu">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspend"/>
    <link type="seealso" xref="shell-exit#suspend"/>
    <link type="seealso" xref="shell-exit#shutdown"/>
    <link type="seealso" xref="display-brightness"/>
    <link type="seealso" xref="power-whydim"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>
    <revision pkgversion="41" date="2021-09-08" status="final"/>

    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Tippek a számítógép áramfogyasztásának csökkentéséhez.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Kevesebb energia használata és az akkumulátoros üzemidő javítása</title>

  <p>A számítógépek rengeteg energiát fogyaszthatnak. Az alábbi egyszerű energiatakarékossági stratégiák használatával csökkentheti villanyszámláját, és óvhatja a környezetet.</p>

<section id="general">
  <title>Általános tippek</title>

<list>
  <item>
    <p><link xref="shell-exit#suspend">Függessze fel számítógépét</link>, amikor nem használja. Ez jelentősen csökkenti a felhasznált energiamennyiséget, és a számítógép ebből az állapotból nagyon gyorsan felébreszthető.</p>
  </item>
  <item>
    <p><link xref="shell-exit#shutdown">Kapcsolja ki</link> a számítógépet, ha hosszabb ideig nem használja. Egyesek amiatt aggódnak, hogy a számítógép rendszeres kikapcsolása gyorsabb elhasználódást eredményez, de ez nem így van.</p>
  </item>
  <item>
    <p>Use the <gui>Power</gui> panel in <app>Settings</app> to change your
    power settings. There are a number of options that will help to save power:
    you can <link xref="display-blank">automatically blank the screen</link>
    after a certain time, enable the <gui>Automatic <link xref="power-profile">Power
    Saver</link></gui> mode when the battery is low, and have the computer
    <link xref="power-autosuspend">automatically suspend</link> if you have not
    used it for a certain period of time.</p>
  </item>
  <item>
    <p>Reduce the <link xref="display-brightness">screen brightness</link>.</p>
  </item>
  <item>
    <p>Kapcsolja ki a külső eszközöket (például nyomtatókat és szkennereket), amikor nem használja azokat.</p>
  </item>
</list>

</section>

<section id="laptop">
  <title>Laptopok, netbookok és más akkumulátoros eszközök</title>

 <list>
   <item>
     <p>Csökkentse a <link xref="display-brightness">kijelző fényerejét</link>. A kijelző a laptopok energiafelhasználásának jelentős részéért felelős.</p>
     <p>A laptopokon rendszerint találhatók a fényerő beállítására szolgáló gombok (vagy gyorsbillentyűk).</p>
   </item>
   <item>
     <p>If you do not need an Internet connection for a little while, <link xref="power-wireless">turn off
     the wireless or Bluetooth cards</link>. These devices work by broadcasting radio
     waves, which takes quite a bit of power.</p>
     <p>Egyes számítógépeken ez egy fizikai kapcsolóval, másokon billentyűkombinációval oldható meg. Ha később újra szüksége lesz az internetkapcsolatra, ismét bekapcsolhatja.</p>
   </item>
 </list>

</section>

<section id="advanced">
  <title>További tippek</title>

 <list>
   <item>
     <p>Csökkentse a háttérben futó feladatok számát. A számítógépek több energiát használnak, ha több dolguk van.</p>
     <p>A legtöbb futó alkalmazás nem használ sok erőforrást, amikor nem használja azokat. Azonban az internetről gyakran adatokat lekérő, illetve zenét vagy videókat lejátszó programok befolyásolhatják energiafelhasználását.</p>
   </item>
 </list>

</section>

</page>
