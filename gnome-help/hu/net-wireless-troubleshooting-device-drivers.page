<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting-device-drivers" xml:lang="hu">

  <info>
    <link type="guide" xref="net-wireless-troubleshooting"/>

    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Az Ubuntu dokumentációs wiki közreműködői</name>
    </credit>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Egyes illesztőprogramok nem működnek megfelelően bizonyos vezeték nélküli csatolókkal, emiatt szükség lehet másik illesztőprogram használatára.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Vezeték nélküli hálózatok hibáinak elhárítása</title>

  <subtitle>Győződjön meg róla, hogy működő illesztőprogramok vannak telepítve</subtitle>

<!-- Needs links (see below) -->

  <p>Ebben a lépésben ellenőrizheti, hogy a vezeték nélküli csatolóhoz telepítve van-e működő illesztőprogram. Az <em>illesztőprogram</em> olyan szoftver, amely megadja a számítógépnek, hogy hogyan használhat megfelelően egy hardvereszközt. Még ha a vezeték nélküli csatolót a számítógép fel is ismeri, nem biztos, hogy az illesztőprogramjai megfelelően működnek. Ilyenkor szükség lehet a csatolóhoz másik, jobban működő illesztőprogram keresésére. Próbálkozzon az alábbi lehetőségekkel:</p>

  <list>
    <item>
      <p>Ellenőrizze, hogy vezeték nélküli csatolója szerepel-e a támogatott eszközök listáján.</p>
      <p>Most Linux distributions keep a list of wireless devices that they
      have support for. Sometimes, these lists provide extra information on how
      to get the drivers for certain adapters working properly. Go to the list
      for your distribution (for example,
      <link href="https://help.ubuntu.com/community/WifiDocs/WirelessCardsSupported">Ubuntu</link>,
      <link href="https://wiki.archlinux.org/index.php/Wireless_network_configuration">Arch</link>,
      <link href="https://wireless.wiki.kernel.org/en/users/Drivers">Fedora</link> or
      <link href="http://en.opensuse.org/HCL:Network_(Wireless)">openSUSE</link>)
      and see if your make and model of wireless adapter is listed. You may be
      able to use some of the information there to get your wireless drivers
      working.</p>
    </item>
    <item>
      <p>Keressen korlátozott (bináris) illesztőprogramokat.</p>
      <p>Számos Linux disztribúció csak <em>szabad</em> és <em>nyílt forrású</em> illesztőprogramokat tartalmaz. Ennek oka, hogy nem terjeszthetnek szabadalomvédett vagy zárt forrású illesztőprogramokat. Ha a vezeték nélküli csatolóhoz megfelelő illesztőprogram csak nem szabad vagy „csak bináris” változatban érhető el, akkor alapesetben nem kerül telepítésre. Ebben az esetben keresse fel a vezeték nélküli csatoló gyártójának weboldalát, és keressen azon linuxos illesztőprogramokat.</p>
      <p>Egyes Linux disztribúciók rendelkeznek a korlátozott illesztőprogramok letöltésére szolgáló segédprogrammal. Ha Ön ilyen disztribúciót használ, akkor próbáljon meg azzal az eszközzel keresni illesztőprogramot a vezeték nélküli csatolóhoz.</p>
    </item>
    <item>
      <p>A kártya windowsos illesztőprogramjainak használata.</p>
      <p>In general, you cannot use a device driver designed for one operating
      system (like Windows) on another operating system (like Linux). This is
      because they have different ways of handling devices. For wireless
      adapters, however, you can install a compatibility layer called
      <em>NDISwrapper</em> which lets you use some Windows wireless drivers on
      Linux. This is useful because wireless adapters almost always have
      Windows drivers available for them, whereas Linux drivers are sometimes
      not available. You can learn more about how to use NDISwrapper
      <link href="https://sourceforge.net/p/ndiswrapper/ndiswrapper/Main_Page/">here</link>.
      Note that not all wireless drivers can be used through NDISwrapper.</p>
    </item>
  </list>

  <p>Ha egyik lehetőség sem válik be, akkor próbáljon egy másik vezeték nélküli csatolót használni. Az USB vezeték nélküli csatolók elég olcsók, és bármely számítógéphez csatlakoztathatók. Vásárlás előtt azonban ellenőrizze, hogy a csatoló kompatibilis-e az Ön által használt disztribúcióval.</p>

</page>
