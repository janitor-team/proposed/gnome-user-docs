<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="backup-how" xml:lang="hu">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit>
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Klein Kravis</name>
      <email>kleinkravis44@outlook.com</email>
      <years>2020</years>
    </credit>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Use Déjà Dup (or another backup application) to make copies of
    your valuable files and settings to protect against loss.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

<title>Biztonsági mentés</title>

  <p>A fájlok és beállítások mentésének legegyszerűbb módja egy biztonsági mentő alkalmazásra bízni a folyamatot. Számos ilyen alkalmazás érhető el, például a <app>Déjà Dup</app>.</p>

  <p>A kiválasztott biztonsági mentő alkalmazás súgója végigvezeti a biztonsági mentés beállításainak megadásán, valamint az adatok visszaállításán.</p>

  <p>An alternative option is to <link xref="files-copy">copy your files</link>
 to a safe location, such as an external hard drive, an online storage service,
 or a USB drive. Your <link xref="backup-thinkabout">personal files</link>
 and settings are usually in your Home folder, so you can copy them from there.</p>

  <p>Az elmenthető adatok mennyiségét csak a tárolóeszköz mérete korlátozza. Ha van elég hely az eszközön, a legjobb módszer a teljes saját mappájának mentése, a következő kivételekkel:</p>

<list>
 <item><p>Files that are already backed up somewhere else, such as to a USB drive,
 or other removable media.</p></item>
 <item><p>Egyszerűen újra létrehozható fájlok. Ha például Ön programozó, akkor nem kell a programok fordítása során keletkező fájlokat mentenie. Ehelyett győződjön meg róla, hogy az eredeti forrásfájlokat menti.</p></item>
 <item><p>A Kukába dobott fájlok, amelyek a <file>~/.local/share/Trash</file> mappában találhatók.</p></item>
</list>

</page>
