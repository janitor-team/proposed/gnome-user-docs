<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="sound-volume" xml:lang="pt">

  <info>
    <link type="guide" xref="media#sound"/>

    <revision version="gnome:40" date="2021-02-26" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Configure o volume sonoro para o computador e controle a sonoridade de cada aplicativo.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

<title>Alterando o volume do som</title>

  <p>Para alterar o volume do som, abra o <gui xref="shell-introduction#systemmenu">menu do sistema</gui> no lado direito na barra superior e mova o controle deslizante de volume para esquerda ou direita. Pode desligar completamente arrastando-o completamente para a esquerda.</p>

  <p>Alguns teclados têm teclas que lhe permitem controlar o volume. Normalmente elas se parecem com alto-falantes estilizados com ondas saindo deles. Elas costumam ficar próximas às teclas “F” (teclas de função) na parte superior. Em teclados de notebooks, ficam frequentemente nas teclas “F”. Mantenha pressionada a tecla <key>Fn</key> do teclado para usá-las.</p>

  <p>Se tem alto-falantes externos, também pode alterar o volume usando o controle de volume dos alto-falantes. Alguns fones de ouvido têm um controle de volume próprio também.</p>

<section id="apps">
 <title>Alterando o volume sonoro para aplicações individuais</title>

  <p>Pode alterar o volume de um aplicativo e deixar o volume inalterado para outros. Isso é útil se estiver ouvindo música e navegando pela web, por exemplo. Pode desligar o áudio no navegador de forma que os sons de sites não interrompam a música.</p>

  <p>Alguns aplicações possuem controles de volume nas janelas principais. Se seu aplicativo possui seu controle de volume, use aquele para alterar o volume. Se não:</p>

    <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Som</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Som</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Under <gui>Volume Levels</gui>, change the volume of the applications
      listed there. The button at the end of the volume slider toggles
      <gui>Mute</gui> on and off.</p>

  <note style="tip">
    <p>Somente aplicações que estejam reproduzindo sons serão listados. Se um aplicativo está reproduzindo sons e não está listado, pode não haver suporte para ele que lhe permita controlar seu volume desta maneira. Nesse caso, não pode alterar seu volume.</p>
  </note>
    </item>

  </steps>

</section>

</page>
