<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-disable-service" xml:lang="pt">

  <info>
    <link type="guide" xref="accounts"/>

    <revision pkgversion="3.5.5" date="2012-08-14" status="review"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
<credit type="editor">
      <name>Klein Kravis</name>
      <email>kleinkravis44@outlook.com</email>
      <years>2020</years>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Algumas contas on-line podem ser usadas para aceder a vários serviços (como agenda e e-mail). Pode controlar quais desses serviços podem ser usados por aplicações.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Controlando quais serviços on-line uma conta pode ser usada para acessar</title>

  <p>Some types of online account providers allow you to access several services
  with the same user account. For example, Google accounts provide access to
  calendar, email, and contacts. You may want to use your account for some
  services, but not others. For example, you may want to use your Google account
  for email but not calendar if you have a different online account that you use
  for calendar.</p>

  <p>Pode desabilitar alguns dos serviços que são fornecidos por cada conta on-line:</p>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Contas on-line</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Contas on-line</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Selecione a conta que deseja alterar na lista à direita.</p>
    </item>
    <item>
      <p>Uma lista de serviços que estão disponíveis para esta conta será mostrada sob <gui>Usar para</gui>. Veja <link xref="accounts-which-application"/> para ver que aplicações acessam quais serviços.</p>
    </item>
    <item>
      <p>Alterne para off quaisquer serviços que não deseja usar.</p>
    </item>
  </steps>

  <p>Uma vez que um serviço tenha sido desabilitado para uma conta, aplicações no seu computador não mais poderão usar a conta para ligar àquele serviço.</p>

  <p>Para habilitar o serviço que desativou, basta voltar na caixa de diálogo de <gui>Contas on-line</gui> e alterne-o para ON.</p>

</page>
