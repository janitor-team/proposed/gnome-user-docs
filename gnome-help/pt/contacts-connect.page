<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="contacts-connect" xml:lang="pt">

  <info>
    <link type="guide" xref="contacts"/>

    <revision pkgversion="3.5.5" date="2012-08-14" status="draft"/>
    <revision pkgversion="3.8" date="2013-04-27" status="draft"/>
    <revision pkgversion="3.12" date="2014-02-26" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.36.2" date="2020-08-11" status="review"/>
    <revision pkgversion="3.38.0" date="2020-11-02" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013-2014</years>
    </credit>
    
    <credit type="editor">
      <name>Pranali Deshmukh</name>
      <email>pranali21293@gmail.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Envie e-mail para, bata papo com ou ligue para um contato.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Conectando a seu contato</title>

  <p>Para enviar um e-mail, bater-papo com ou ligar para alguém nos <app>Contatos</app>:</p>

  <steps>
    <item>
      <p>Selecione o contato da sua lista de contactos.</p>
    </item>
    <item>
      <p>Press the button corresponding to the <em>detail</em> that you want to use.
      For example, to email your contact, press the 
      <media its:translate="no" type="image" src="figures/keyboard-key-mail.svg">
      <span its:translate="yes">mail</span></media> button next to the contact’s 
      email address.</p>
    </item>
    <item>
      <p>O aplicativo correspondente será iniciado usando os detalhes do contato.</p>
    </item>
  </steps>

  <note>
    <p>Se não houver qualquer aplicativo disponível para o detalhe que deseja usar, não poderá selecioná-lo.</p>
  </note>

</page>
