<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="touchscreen-gestures" xml:lang="pt">

  <info>
    <link type="guide" xref="mouse" group="#last"/>

    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <!--
    For 41: https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/121
    -->
    <revision version="gnome:40" date="2021-03-18" status="candidate"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Manipulate your desktop using gestures on your touchpad or touchscreen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Use gestures on touchpads and touchscreens</title>

  <p>Multitouch gestures can be used on touchpads and touchscreens for system navigation,
  as well as in applications.</p>

  <p>Várias aplicações fazem uso de gestos. No <app>Visualizador de documentos</app>, os documentos podem ser ampliados e deslizados com gestos, e o <app>Visualizador de imagens</app> permite ampliar, girar e deslocar.</p>

<section id="system">
  <title>Gestos em todo o sistema</title>

<table rules="rows" frame="bottom">
  <tr>
    <td><p><media type="image" src="figures/touch-overview.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Open the Activities Overview and Applications View</em></p>
    <p>Place three fingers on the touchpad or touchscreen and gesture upwards to open the Activities Overview.</p>
    <p>To open the Applications View, place three fingers and gesture upwards again.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-workspaces.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Alternar espaço de trabalho</em></p>
    <p>Place three fingers on the touchpad or touchscreen and gesture left or right.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-unfullscreen.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Exit from fullscreen</em></p>
    <p>On a touchscreen, drag down from the top edge to exit from the fullscreen mode of any window.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-osk.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Exibir o teclado virtual</em></p>
    <p>On a touchscreen, drag up from the bottom edge to bring up the
    <link xref="keyboard-osk">on-screen keyboard</link>, if the on-screen keyboard is enabled.</p></td>
  </tr>
</table>

</section>

<section id="apps">
  <title>Gestos de aplicações</title>

<table rules="rows" frame="bottom">
  <tr>
    <td><p><media type="image" src="figures/touch-tap.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Abrir um item, iniciar um aplicativo, reproduzir uma música</em></p>
    <p>Toque em um item.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-hold.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Selecionar um item e listar ações que podem ser realizadas</em></p>
    <p>Pressione e segure por um segundo ou dois.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-scroll.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Rolar a área no ecrã</em></p>
    <p>Arrastar: deslize um dedo tocando a superfície.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-pinch-to-zoom.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Alterar o nível de ampliação de uma visão (<app>Mapas</app>, <app>Fotos</app>)</em></p>
    <p>Pinça ou alongamento com dois dedos: toque a superfície com dois dedos enquanto os aproxima ou afasta.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-rotate.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Girar uma foto</em></p>
    <p>Rotação com dois dedos: toque a superfície com dois dedos e gire.</p></td>
  </tr>
</table>

</section>

</page>
