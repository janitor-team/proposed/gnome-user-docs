<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-open" xml:lang="pt">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Abra ficheiros usando um aplicativo que não é o padrão para aquele tipo. Pode alterar o padrão também.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

<title>Abrindo ficheiros com outras aplicações</title>

  <p>Quando dá clique duplo (ou clique no botão do meio) em um ficheiro no gestor de ficheiros, ele será aberto com o aplicativo padrão para aquele tipo de ficheiro. Pode abri-lo em um programa diferente, pesquisar aplicações na Internet ou configurar o aplicativo padrão para todos ficheiros do mesmo tipo.</p>

  <p>Para abrir um ficheiro com um aplicativo que não seja o padrão, clique com o botão direito no ficheiro e selecione o aplicativo desejado na parte superior do menu. Se não vir o aplicativo desejado, selecione <gui>Abrir com outro aplicativo</gui>. Por padrão, o gestor de ficheiros só mostra aplicações que ele sabe que conseguem lidar com o ficheiro. Para olhar todos as aplicações no seu computador, clique em <gui>Mostrar outras aplicações</gui>.</p>

<p>Se ainda não encontrar o aplicativo que deseja, pode buscar por mais deles clicando em <gui>Localizar novas aplicações</gui>. O gestor de ficheiros irá buscar na Internet pacotes contendo aplicações que sejam conhecidos por manipularem ficheiros desse tipo.</p>

<section id="default">
  <title>Alterar o aplicativo padrão</title>
  <p>Pode alterar o aplicativo padrão que é usado para abrir ficheiros de um certo tipo. Isso lhe permitirá abrir seu programa favorito quando der um clique duplo para abrir um ficheiro. Por exemplo, pode querer que seu reprodutor de música favorito abra quando der um clique duplo em um ficheiro MP3.</p>

  <steps>
    <item><p>Selecione um ficheiro de tipo referente ao aplicativo padrão que deseja alterar. Por exemplo, para alterar qual aplicativo é usado para abrir ficheiros MP3, selecione um ficheiro <file>.mp3</file>.</p></item>
    <item><p>Clique com o botão direito no ficheiro e selecione <gui>Propriedades</gui>.</p></item>
    <item><p>Selecione a aba <gui>Abrir com</gui>.</p></item>
    <item><p>Selecione o ficheiro que deseja e clique em <gui>Definir como padrão</gui>.</p>
    <p>Se <gui>Outras aplicações</gui> contiver um aplicativo que quer usar às vezes, mas não quer torná-lo o padrão, selecione esse aplicativo e clique em <gui>Adicionar</gui>. Isso fará com que ele seja adicionado aos <gui>Aplicativos recomendados</gui>. Você então será capaz de usá-lo pelo clique com botão direito no ficheiro e selecionando-o na lista.</p></item>
  </steps>

  <p>Isso altera o aplicativo padrão não apenas para o ficheiro selecionado, mas para todos aqueles que sejam do mesmo tipo.</p>

<!-- TODO: mention resetting the open with list with the "Reset" button -->

</section>

</page>
