<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="music-cantplay-drm" xml:lang="pt">
  <info>
    <link type="guide" xref="media#music"/>


    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <desc>Suporte àquele formato de ficheiro pode não estar instalado ou as músicas podem estar “protegidas contra cópia”.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

<title>Não consigo reproduzir aquelas músicas que eu comprei de uma loja de músicas on-line</title>

<p>Se baixou alguma música de uma loja on-line, pode descobrir que ela é reproduzida em seu computador, especialmente se comprou-a em um computador com Windows ou Mac OS e, então, copiou-o para Linux.</p>

<p>Isso pode estar acontecendo, pois a música está em um formato que não é reconhecido pelo seu computador. Para ser capaz de reproduzir uma música, precisa ter suporte aos formatos de áudio certos instalados — por exemplo, se deseja reproduzir ficheiros MP3, precisa ter suporte a MP3 instalado. Se não tem suporte a um determinado formato de áudio, deve ver uma mensagem dizendo isso a quando tentar reproduzir uma música. A mensagem deve fornecer instruções de como instalar suporte para aquele formato, de forma que possa reproduzi-la.</p>

<p>Se tiver instalado suporte ao formato do áudio, mas ainda não conseguir reproduzi-lo, a música pode estar <em>protegida contra cópia</em> (também conhecida como estando <em>restrito por DRM</em>). DRM é uma forma de restringir quem pode reproduzir uma música e em quais dispositivos ela pode ser reproduzida. A empresa que vendeu aquela música a está no controle dela, e não. Se um ficheiro de música possuir restrições de DRM, provavelmente não será capaz de reproduzi-la — geralmente precisará de um software especial do fornecedor para reproduzir ficheiros restringidos por DRM, mas este software geralmente não tem suporte no Linux.</p>

<p>You can learn more about DRM from the <link href="https://www.eff.org/issues/drm">Electronic Frontier Foundation</link>.</p>

</page>
