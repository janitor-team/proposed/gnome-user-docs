<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="music-player-ipodtransfer" xml:lang="pt">
  <info>
    <link type="guide" xref="media#music"/>


    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <desc>Use um reprodutor de mídia para copiar as músicas e remover com segurança o iPod posteriormente.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

<title>As músicas não aparecem no meu iPod quando eu as copio para ele</title>

<p>Quando conecta um iPod no seu computador, ele vai aparecer no seu aplicativo reprodutor de música e também no gestor de ficheiros (o aplicativo <app>Ficheiros</app> no panorama de <gui>Atividades</gui>). Deve copiar as músicas para o iPod usando o reprodutor de música — se copiá-las usando o gestor de ficheiros, ele não vai funcionar porque as músicas não serão colocadas na localização correta. iPods possuem uma localização especial para armazenamento de músicas as quais aplicações reprodutor de música sabem como obter, mas o gestor de ficheiros não.</p>

<p>Também precisa esperar as músicas acabarem de copiar para o iPod antes de desconectá-lo. Antes de desconectar o iPod, certifique-se de que <link xref="files-removedrive">removeu com segurança</link>. Isso garantirá que todas as músicas sejam copiadas apropriadamente.</p>

<p>Um outro motivo pelo qual as músicas podem não estar aparecendo no seu iPod é que o aplicativo reprodutor de música que está usando não tem suporte a conversão das músicas de um formato de áudio para outro. Se copiar uma música que está armazenada em um formato de áudio que não tem suporte no seu iPod (por exemplo, um ficheiro Ogg Vorbis – .oga), o reprodutor de música vai tentar convertê-lo para um formato que o iPod não entende, como o MP3. Se o software de conversão adequado (também chamado de codec ou codificador) não estiver instalado, o reprodutor de música não será capaz de fazer a conversão e, então, não vai copiar a música. Procure no instalador de software por um codec adequado.</p>

</page>
