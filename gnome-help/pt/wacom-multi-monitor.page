<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-multi-monitor" xml:lang="pt">

  <info>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>
    <revision pkgversion="3.33" date="2019-07-21" status="candidate"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Associe o tablet Wacom para um monitor específico.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Escolhendo um monitor</title>

<steps>
  <item>
    <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Tablet Wacom</gui>.</p>
  </item>
  <item>
    <p>Clique em <gui>Tablet Wacom</gui> para abrir o painel.</p>
  </item>
  <item>
    <p>Clique no botão <gui>Tablet</gui> na barra de cabeçalho.</p>
    <note style="tip"><p>Se nenhum tablet for detectado, será solicitado a <gui>Por favor conectar ou ligar seu tablet Wacom</gui>. Clique no link <gui>Configurações de Bluetooth</gui> para conectar um tablet sem fio.</p></note>
  </item>
  <item><p>Clique em <gui>Associar a um monitor…</gui></p></item>
  <item><p>Marque <gui>Associar a monitor singular</gui>.</p></item>
  <item><p>Próximo a <gui>Saída</gui>, selecione o monitor que deseja receber a entrada para seu tablet gráfico.</p>
     <note style="tip"><p>Apenas os monitores que estão configurados serão selecionáveis.</p></note>
  </item>
  <item>
    <p>Alterne <gui>Manter taxa de proporção (caixa de letras)</gui> para ligado para corresponder a área de desenho do tablet às proporções do monitor. Também chamado de <em>forçar proporções</em>, esta configuração deixa em formato “caixa de letras” a área de desenho em um tablet para corresponder mais diretamente à ecrã. Por exemplo, um tablet 4∶3 seria mapeado de forma que sua área de desenho corresponderia a umo ecrã widescreen.</p>
  </item>
  <item><p>Clique em <gui>Fechar</gui>.</p></item>
</steps>

</page>
