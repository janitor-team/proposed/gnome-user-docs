<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:itst="http://itstool.org/extensions/" type="topic" style="task" id="net-proxy" xml:lang="pt">

  <info>
    <its:rules xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Baptiste Mille-Mathias</name>
      <email>baptistem@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Um proxy é um intermediário para o tráfego web, podendo ser usado para acessar serviços web anonimamente, para ter mais controle ou segurança.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Definindo definições de proxy</title>

<section id="what">
  <title>O que é um proxy?</title>

  <p>Um <em>web proxy</em> filtra sites que acessa, ele recebe requisições do seu navegador web para obter páginas web e seus elementos e, seguindo uma política, vai decidir passá-las para de volta. Eles são normalmente usados em empresas e em pontos de acesso sem fio públicos para controlar quais sites pode acessar, prevenir de acessar a Internet sem se autenticar ou fazer verificações de segurança em sites.</p>

</section>

<section id="change">
  <title>Alterar o método de proxy</title>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Rede</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Rede</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Selecione <gui>proxy de rede</gui> na lista à esquerda.</p>
    </item>
    <item>
      <p>Escolha qual método de proxy deseja dentre:</p>
      <terms>
        <item>
          <title><gui itst:context="proxy">Nenhum</gui></title>
          <p>Os aplicações vão usar uma conexão direta para obter o conteúdo da web.</p>
        </item>
        <item>
          <title><gui>Manual</gui></title>
          <p>Para cada protocolo, defina o endereço de um proxy e sua porta para os protocolos. Os protocolos são <gui>HTTP</gui>, <gui>HTTPS</gui>, <gui>FTP</gui> e <gui>SOCKS</gui>.</p>
        </item>
        <item>
          <title><gui>Automático</gui></title>
          <p>Uma URL apontando para um recurso, o qual contém a configuração apropriada do seu sistema.</p>
        </item>
      </terms>
    </item>
  </steps>

  <p>Aplicativos que usem a conexão de rede vão usar as suas definições de proxy especificadas.</p>

</section>

</page>
