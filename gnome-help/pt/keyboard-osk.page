<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="keyboard-osk" xml:lang="pt">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>

    <credit type="author">
      <name>Jeremy Bicha</name>
      <email>jbicha@ubuntu.com</email>
    </credit>
    <credit type="author">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Use um teclado em ecrã para digitar um texto clicando nos botões com o rato ou um touchscreen.</desc>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Usando um teclado em ecrã</title>

  <p>Se não tem um teclado conectado a seu computador ou prefere não utilizá-lo, pode ativar o <em>teclado em ecrã</em> para digitar um texto.</p>

  <note>
    <p>O teclado em ecrã é habilitado automaticamente se usa um touchscreen</p>
  </note>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a escrever <gui>Definições</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Definições</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Acessibilidade</gui> na barra lateral para abrir o painel.</p>
    </item>
    <item>
      <p>Pressione <gui>Teclado virtual</gui> na seção <gui>Visão</gui>.</p>
    </item>
  </steps>

  <p>Quando tiver outra oportunidade de digitar, o teclado virtual vai abrir embaixo no ecrã.</p>

  <p>Pressione o botão <gui style="button">123</gui> para digitar números e símbolos. Mais símbolos ficam disponíveis se, então, pressionar o botão <gui style="button">=/&lt;</gui>. Para voltar ao teclado alfabético, pressione o botão <gui style="button">ABC</gui>.</p>

  <p>You can press the
  <gui style="button"><media its:translate="no" type="image" src="figures/go-down-symbolic.svg" width="16" height="16"><span its:translate="yes">down</span></media></gui>
  button to hide the keyboard temporarily. The keyboard will show again
  automatically when you next press on something where you can use it.
  On a touchscreen, you can also pull up the keyboard by
  <link xref="touchscreen-gestures">dragging up from the bottom edge</link>.</p>
  <p>Pressione o botão <gui style="button"><media its:translate="no" type="image" src="figures/emoji-flags-symbolic.svg" width="16" height="16"><span its:translate="yes">bandeira</span></media></gui> para alterar suas definições para <link xref="session-language">Idioma</link> ou <link xref="keyboard-layouts">Fontes de entrada</link>.</p>

</page>
