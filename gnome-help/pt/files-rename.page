<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-rename" xml:lang="pt">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Altere o nome do ficheiro ou da pasta.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Renomeando um ficheiro ou uma pasta</title>

  <p>Assim como em outros gestores de ficheiros, pode usar o <app>Ficheiros</app> para alterar o nome de um ficheiro ou pasta.</p>

  <steps>
    <title>Para renomear um ficheiro ou pasta:</title>
    <item><p>Clique com o botão direito em um item e selecione <gui>Renomear</gui> ou selecione o ficheiro e pressione <key>F2</key>.</p></item>
    <item><p>Digite o novo nome e pressione <key>Enter</key> ou clique em <gui>Renomear</gui>.</p></item>
  </steps>

  <p>Você também pode renomear um ficheiro pela janela de <link xref="nautilus-file-properties-basic">propriedades</link>.</p>

  <p>Quando renomeia um ficheiro, apenas a primeira parte do nome do ficheiro é selecionada; a sua extensão não (a parte depois do <file>.</file>). A extensão normalmente identifica que é o tipo de ficheiro (exemplo: <file>ficheiro.pdf</file> é um documento PDF), e normalmente não quer alterar isso. Se precisar alterar a extensão também, selecione o nome completo do ficheiro e mude-o.</p>

  <note style="tip">
    <p>Se renomeou o ficheiro incorreto ou renomeou incorretamente o ficheiro certo, pode desfazer esta ação. Para revertê-la, clique imediatamente no botão de menu na barra de ferramentas e selecione <gui>Desfazer renomeação</gui> ou <keyseq><key>Ctrl</key><key>Z</key></keyseq> para restaurar o nome anterior.</p>
  </note>

  <section id="valid-chars">
    <title>Caracteres válidos para nomes de ficheiros</title>

    <p>Pode usar qualquer caractere, exceto <file>/</file> (barra), nos nomes de ficheiros. Contudo, alguns dispositivos usam um <em>sistema de ficheiros</em> que tem mais restrições nos nomes de ficheiros. Portanto, a melhor prática para evitar os seguintes caracteres nos nomes dos ficheiros:<file>|</file>, <file>\</file>, <file>?</file>, <file>*</file>, <file>&lt;</file>, <file>"</file>, <file>:</file>, <file>&gt;</file>, <file>/</file>.</p>

    <note style="warning">
    <p>Se nomear um ficheiro com um <file>.</file> sendo o primeiro caractere, o ficheiro ficará <link xref="files-hidden">oculto</link> quando tentar visualizá-lo no gestor de ficheiros.</p>
    </note>

  </section>

  <section id="common-probs">
    <title>Problemas comuns</title>

    <terms>
      <item>
        <title>O nome de ficheiro já está em uso</title>
        <p>Você não pode ter dois ficheiros ou pastas com o mesmo nome em uma mesma pasta. Se tentar renomear um ficheiro com um nome que já exista na pasta em que está trabalhando, o gestor de ficheiros não permitirá.</p>
        <p>Nomes de ficheiros e pastas são sensíveis maiúsculas. Por exemplo, <file>Ficheiro.txt</file> não é a mesma coisa que <file>FICHEIRO.txt</file>. Usar nome de ficheiro assim é permitido, embora não seja recomendável.</p>
      </item>
      <item>
        <title>O nome do ficheiro é muito longo</title>
        <p>Em alguns sistemas de ficheiros, nomes de ficheiros não podem ter mais do que 255 caracteres. Essa limitação de 255 caracteres inclui tanto o nome do ficheiro quanto o caminho para o ficheiro (ex.: <file>/home/wanda/Documentos/trabalho/propostas-comerciais/…</file>), motivo pelo qual deve-se evitar ficheiros e pastas com nomes extensos, na medida do possível.</p>
      </item>
      <item>
        <title>A opção para renomear está acinzentada</title>
        <p>Se <gui>Renomear</gui> estiver acinzentado, não tem permissão para renomear o ficheiro. Você deveria ter cuidado ao renomear tais ficheiros, pois renomear alguns ficheiros protegidos pode causar instabilidade no seu sistema. Veja <link xref="nautilus-file-properties-permissions"/> para mais informações.</p>
      </item>
    </terms>

  </section>

</page>
