<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="contacts-setup" xml:lang="pt">

  <info>
    <link type="guide" xref="contacts" group="#first"/>

    <revision pkgversion="3.5.5" date="2012-08-13" status="draft"/>
    <revision pkgversion="3.12" date="2014-02-26" status="final"/>
    <revision pkgversion="3.13.91" date="2014-09-05" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.22" date="2017-03-19" status="draft"/>
    <revision pkgversion="3.38.0" date="2020-11-02" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    
    <credit type="author copyright">
      <name>Paul Cutler</name>
      <email>pcutler@gnome.org</email>
      <years>2017</years>
    </credit>

    <credit type="editor">
      <name>Pranali Deshmukh</name>
      <email>pranali21293@gmail.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Armazene seus contactos em um catálogo de endereços local ou em uma conta online.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Iniciar o Contactos pela primeira vez</title>

  <p>Quando executar <app>Contatos</app> pela primeira vez, a janela <gui>Selecionar catálogo de endereços</gui> abrirá.</p>
   
  <p>If you have <link xref="accounts">online accounts</link> configured, they
  are listed with <gui>Local Address Book</gui>. Select an item from the
  list and press <gui style="button">Done</gui>.</p>
  
  <p>All new contacts you create will be saved to the address book you choose. 
  You are also able to view, edit and delete contacts in other address books.</p>
  
  <p>If you have no online accounts configured, press
  <gui style="button">Online Accounts</gui> to begin the setup. If you do not
  wish to set up online accounts at this time, press <gui style="button">Local
  Address Book</gui>.</p>

</page>
