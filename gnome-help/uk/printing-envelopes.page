<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-envelopes" xml:lang="uk">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>

    <credit type="author">
      <name>Філ Булл (Phil Bull)</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Переконайтеся, що конверт вставлено так, як слід, і що вибрано належний розмір паперу.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Друк на конвертах</title>

  <p>На більшості принтерів передбачено можливість безпосереднього друку на конверті. Це особливо корисно, якщо вам, наприклад, потрібно надіслати багато листів.</p>

  <section id="envelope">
    <title>Друк на конвертах</title>

  <p>При друці на конвертах слід перевірити дві речі.</p>
  <p>По-перше, ваш принтер знає, якими є розміри конверта. Натисніть комбінацію клавіш <keyseq><key>Ctrl</key><key>P</key></keyseq>, щоб відкрити діалогове вікно друку, перейдіть на вкладку <gui>Параметри сторінки</gui> і виберіть у полі <gui>Тип паперу</gui> «Envelope», якщо такий пункт є у списку. Якщо такого пункту немає, спробуйте змінити значення параметра <gui>Розмір паперу</gui> до розмірів конверта (наприклад, <gui>C5</gui>). На пакунку з конвертами має бути написано розмір; більшість конвертів мають стандартні розміри.</p>

  <p>По-друге, вам слід переконатися, що конверти вкладено до вхідного лотка принтера правильно. Ознайомтеся із відповідними настановами з підручника до принтера або просто спробуйте надрукувати щось на папірці відповідного розміру, щоб визначити бік, на якому відбуватиметься друк, і вкласти конверти належним чином.</p>

  <note style="warning">
    <p>Деякі принтери не розраховано на друк конвертів, особливо деякі лазерні принтери. Ознайомтеся із підручником до принтера, щоб визначити, чи можна на ньому друкувати на конвертах. Якщо друк на конвертах не передбачено, спроба друку може призвести до пошкодження принтера.</p>
  </note>

  </section>

<!--
TODO: Please write this section!

<section id="labels">
 <title>Printing labels</title>

</section>
-->

</page>
