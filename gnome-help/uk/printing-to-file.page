<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="task" id="printing-to-file" xml:lang="uk">

  <info>
    <link type="guide" xref="printing" group="#last"/>

    <revision pkgversion="3.8" date="2013-03-29" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author copyright">
      <name>Єкатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Зберігання документа до файла PDF, PostScript або SVG замість надсилання його на принтер.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Друкувати у файл</title>

  <p>Ви можете надрукувати документ до файла замість надсилання його на друк на принтері. Результатом друку до файла буде файл <sys>PDF</sys>, <sys>PostScript</sys> або <sys>SVG</sys>, який міститиме дані документа. Це може бути корисним, якщо ви хочете перенести документ на інший комп'ютер або поділитися ним із кимось.</p>

  <steps>
    <title>Для друку до файла, виконайте такі дії:</title>
    <item>
      <p>Відкрийте діалогове вікно друку натисканням комбінації клавіш <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Виберіть <gui>Друкувати у файл</gui> у розділі <gui>Принтер</gui> на вкладці <gui style="tab">Загальне</gui>.</p>
    </item>
    <item>
      <p>Щоб змінити типову назву файла і місце зберігання файла, клацніть на назві файла під панеллю вибору принтера. Натисніть кнопку <gui style="button">Вибрати</gui>, щойно вибір буде здійснено.</p>
    </item>
    <item>
      <p><sys>PDF</sys> є типовим типом файлів для документів. Якщо ви хочете скористатися іншим <gui>Форматом виводу</gui>, виберіть <sys>PostScript</sys> або <sys>SVG</sys>.</p>
    </item>
    <item>
      <p>Виберіть параметри сторінки.</p>
    </item>
    <item>
      <p>Натисніть кнопку <gui style="button">Друкувати</gui>, щоб зберегти дані до файла.</p>
    </item>
  </steps>

</page>
