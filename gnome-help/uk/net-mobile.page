<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-mobile" xml:lang="uk">

  <info>
    <link type="guide" xref="hardware-phone#setup"/>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.14" date="2014-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.24" date="2017-03-26" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Проєкт з документування GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Девід Кінг (David King)</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <desc>Використання вашого телефону або інтернет-модему для встановлення з'єднання із мобільною широкосмуговою мережею.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>З'єднання із мобільною широкосмуговою мережею</title>

  <p>Ви можете налаштувати з'єднання зі стільниковою (3G) мережею за допомогою вбудованого до вашого комп'ютера 3G-модему, вашого мобільного телефону або інтернет-флешки.</p>

  <note style="tip">
  <p>У більшості телефонів передбачено параметр, який має назву <link xref="net-tethering">USB-прив'язування</link>, використання якого не потребує додаткового налаштовування на комп'ютері. Використання цього параметра, зазвичай, є кращим способом встановлення з'єднання із стільниковою мережею.</p>
  </note>

  <steps>
    <item><p>Якщо на вашому комп'ютері немає вбудовано 3G-модему, з'єднайте ваш телефон або інтернет-флешку із USB-портом на вашому комп'ютері.</p>
    </item>
    <item>
    <p>Відкрийте <gui xref="shell-introduction#systemmenu">меню системи</gui> у правій частині верхньої панелі.</p>
  </item>
  <item>
    <p>Виберіть <gui>Мобільну широкосмугову вимкнено</gui>. У відповідь буде відкрито розділ <gui>Мобільна широкосмугова</gui> меню.</p>
      <note>
        <p>Якщо у меню системи немає пункту <gui>Мобільна широкосмугова мережа</gui>, переконайтеся, що пристрій не з'єднано у режимі сховища даних.</p>
      </note>
    </item>
    <item><p>Натисніть кнопку <gui>З'єднатись</gui>. Якщо з'єднання відбувається уперше, буде запущено майстер <gui>Налаштування з'єднання з мобільною радіомережею</gui>. На першому екрані буде показано список потрібних відомостей. Натисніть кнопку <gui style="button">Далі</gui>.</p></item>
    <item><p>Виберіть країну і область вашого надавача послуг зі списку. Натисніть кнопку <gui style="button">Далі</gui>.</p></item>
    <item><p>Виберіть вашого надавача послуг зі списку. Натисніть кнопку <gui style="button">Далі</gui>.</p></item>
    <item><p>Виберіть тарифний план відповідно до типу пристрою, з яким встановлюється з'єднання. Це визначить назву точки доступу. Натисніть кнопку <gui style="button">Далі</gui>.</p></item>
    <item><p>Підтвердьте вказані вами параметри натисканням кнопки <gui style="button">Застосувати</gui>. Вікно майстра буде закрито, а на панелі <gui>Мережа</gui> буде показано властивості вашого з'єднання.</p></item>
  </steps>

</page>
