<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="sharing-desktop" xml:lang="uk">

  <info>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.14" date="2015-01-25" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.29" date="2018-08-28" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <credit type="author">
      <name>Єкатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Надання іншим користувачам доступу до перегляду і взаємодії із вашою стільницею за допомогою VNC.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Оприлюднення вашої стільниці</title>

  <p>Ви можете надати стороннім користувачам можливість переглядати і керувати вашою стільницею з іншого комп'ютера за допомогою програми для перегляду стільниці. Налаштуйте <gui>Оприлюднення екрана</gui>, щоб дозволити іншим користувачам отримувати доступ до вашої стільниці і встановити параметри захисту.</p>

  <note style="info package">
    <p>Вам слід встановити пакунок <app>Віддалена стільниця GNOME</app>, щоб став видимим пункт <gui>Оприлюднення екрана</gui>.</p>

    <if:choose xmlns:if="http://projectmallard.org/if/1.0/">
      <if:when test="action:install">
        <p><link action="install:gnome-remote-desktop" style="button">Встановити «Віддалену стільницю GNOME»</link></p>
      </if:when>
    </if:choose>
  </note>

  <steps>
    <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть вводити слово <gui>Параметри</gui>.</p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Параметри</gui>.</p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Оприлюднення</gui> на бічній панелі, щоб відкрити панель.</p>
    </item>
    <item>
      <p>Якщо перемикач <gui>Оприлюднення</gui> у верхній правій частині вікна встановлено у стан «вимкнено», перемкніть його у стан «увімкнено».</p>

      <note style="info"><p>Якщо для тексту під міткою <gui>Назва комп'ютера</gui> передбачено можливість редагування, ви можете <link xref="sharing-displayname">змінити</link> назву комп'ютера, яку буде показано у мережі.</p></note>
    </item>
    <item>
      <p>Виберіть <gui>Оприлюднення екрана</gui>.</p>
    </item>
    <item>
      <p>Щоб надати іншим користувачам змогу переглядати зображення з вашої стільниці, встановіть для перемикача <gui>Оприлюднення екрана</gui> стан «увімкнено». Це означатиме, що інші користувачі зможуть здійснювати спроби з'єднатися із вашим комп'ютером і переглянути те, що відбувається на його екрані.</p>
    </item>
    <item>
      <p>Щоб дозволити іншим користувачам взаємодіяти із вашою стільницею, позначте пункт <gui>Дозволити під'єднуватися і керувати екраном</gui>. Це може надати змогу стороннім користувачам рухати вказівник миші, запускати програми і здійснювати навігацію файлами на вашому комп'ютері, залежно від використаних параметрів захисту.</p>
    </item>
  </steps>

  <section id="security">
  <title>Безпека</title>

  <p>Важливо, щоб ви повністю розуміли призначення кожного з параметрів захисту, перш ніж його змінювати.</p>

  <terms>
    <item>
      <title>Нові з'єднання мають вимагати доступ</title>
      <p>Якщо ви хочете мати змогу вирішувати, чи слід дозволяти комусь отримувати доступ до вашої стільниці, позначте пункт <gui>Нові з'єднання мають вимагати доступ</gui>. Якщо цей пункт не буде позначено, середовище не питатиме вас про те, чи хочете ви комусь дозволити встановити з'єднання із вашим комп'ютером.</p>
      <note style="tip">
        <p>Цей параметр типово увімкнено.</p>
      </note>
    </item>
    <item>
      <title>Потрібен пароль</title>
      <p>Щоб вимагати у сторонніх користувачів введення пароля для встановлення з'єднання із вашою стільницею, позначте пункт <gui>Вимагати пароль</gui>. Якщо ви не позначите цей пункт, здійснювати спроби переглянути зображення на вашій стільниці зможе будь-хто.</p>
      <note style="tip">
        <p>Типово, цей параметри вимкнено, але вам слід увімкнути його і встановити безпечний пароль.</p>
      </note>
    </item>
<!-- TODO: check whether this option exists.
    <item>
      <title>Allow access to your desktop over the Internet</title>
      <p>If your router supports UPnP Internet Gateway Device Protocol and it
      is enabled, you can allow other people who are not on your local network
      to view your desktop. To allow this, select <gui>Automatically configure
      UPnP router to open and forward ports</gui>. Alternatively, you can
      configure your router manually.</p>
      <note style="tip">
        <p>This option is disabled by default.</p>
      </note>
    </item>
-->
  </terms>
  </section>

  <section id="networks">
  <title>Мережі</title>

  <p>У розділі <gui>Мережі</gui> буде показано список мереж, з якими з'єднано ваш комп'ютер. Скористайтеся перемикачем поряд із кожним з пунктів, щоб визначити, де буде оприлюднено вашу стільницю.</p>
  </section>

  <section id="disconnect">
  <title>Припинення спільного використання вашої стільниці</title>

  <p>Щоб від'єднати когось, хто переглядає зображення на вашій стільниці, виконайте такі дії:</p>

  <steps>
    <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть вводити слово <gui>Параметри</gui>.</p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Параметри</gui>.</p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Оприлюднення</gui> на бічній панелі, щоб відкрити панель.</p>
    </item>
    <item>
      <p><gui>Оприлюднення екрана</gui> буде показано як <gui>Активне</gui>. Клацніть на написі.</p>
    </item>
    <item>
      <p>Перемкніть перемикач згори у стан «вимкнено».</p>
    </item>
  </steps>

  </section>


</page>
