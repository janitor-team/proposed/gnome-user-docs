<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-suspend" xml:lang="uk">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspendfail"/>

    <desc>Присипляння призводить до призупинення роботи комп'ютера, отже він споживає менше енергії.</desc>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Проєкт з документування GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Єкатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>Що трапляється, коли комп'ютер присипляють?</title>

<p>Коли ви <em>присипляєте</em> комп'ютер, ви переводите його у стан сну. Усі ваші програми і документи лишаються відкритими, але екран та інші частини комп'ютера вимикаються для заощадження енергії. Втім, комп'ютер лишається увімкненим і споживає невеличку частку енергії. Ви можете пробудити комп'ютер натисканням клавіші або клацанням кнопкою миші. Якщо це не спрацює, спробуйте натиснути кнопку живлення.</p>

<p>На деяких комп'ютерах є проблеми із підтримкою обладнання, які призводять до того, що <link xref="power-suspendfail">комп'ютер не здатен належними чином присиплятися</link>. Варто перевірити, чи працює присипляння на вашому комп'ютері, перш ніж покладатися на нього.</p>

<note style="important">
  <title>Завжди зберігайте результати вашої роботи до присипляння</title>
  <p>Перш ніж присипляти комп'ютер, вам слід зберегти усі результати вашої роботи на випадок, якщо щось піде не так, і ваші програми і документи не вдасться відновити при спробі відновлення роботи комп'ютера.</p>
</note>

</page>
