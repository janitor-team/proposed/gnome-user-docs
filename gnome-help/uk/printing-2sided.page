<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-2sided" xml:lang="uk">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Філ Булл (Phil Bull)</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Друк на обох боках аркуша паперу або друк декількох сторінок на одному аркуші.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Двобічний друк та друк декількох сторінок на аркуші</title>

  <p>Щоб надрукувати документ на обох боках аркушів паперу, виконайте такі дії:</p>

  <steps>
    <item>
      <p>Відкрийте діалогове вікно друку натисканням комбінації клавіш <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Перейдіть на вкладку <gui>Параметри сторінки</gui> у вікні друку і виберіть пункт зі спадного списку <gui>З двох сторін</gui>. Якщо пункт вимкнено, на вашому принтері двобічний друк неможливий.</p>
      <p>Принтери виконують двобічний друк у декілька різних способів. Варто поекспериментувати з вашими принтером, щоб визначити спосіб, у який він працює.</p>
    </item>
    <item>
      <p>Ви можете також друкувати декілька сторінок документа на одному <em>боці</em> аркуша паперу. Для цього скористайтеся пунктом <gui>Сторінок на сторону</gui>.</p>
    </item>
  </steps>

  <note>
    <p>Доступність цих параметрів може залежати від типу принтера, а також використаної вами програми. Цей параметр може бути недоступним.</p>
  </note>

</page>
