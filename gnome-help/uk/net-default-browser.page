<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-default-browser" xml:lang="uk">

  <info>
    <link type="guide" xref="net-browser"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.36" date="2020-06-24" status="review"/>

    <credit type="author">
      <name>Філ Булл (Phil Bull)</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Єкатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Зміна типового браузера за допомогою розділу <gui>Подробиці</gui> у <gui>Параметрах</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Зміна типового браузера, за допомогою якого відкриватимуться сайти</title>

  <p>Коли ви натискаєте посилання на вебсторінці у будь-якій програмі, автоматично буде відкрито вікно браузера із відповідною сторінкою. Втім, якщо у системі встановлено декілька браузерів, сторінку може бути відкрито не у тому з них, який ви вважаєте улюбленим. Щоб усунути цю проблему, змініть типовий браузер:</p>

  <steps>
    <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть вводити слова <input>Типові програми</input>.</p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Типові програми</gui> на бічній панелі, щоб відкрити панель.</p>
    </item>
    <item>
      <p>Виберіть браузер, у якому ви хочете відкривати посилання, змінивши значення у полі <gui>Тенета</gui>.</p>
    </item>
  </steps>

  <p>Коли ви відкриваєте інший браузер, він може повідомити вам, що він вже не є типовим браузером. Якщо таке трапилося, натисніть кнопку <gui>Скасувати</gui> (або подібну), щоб браузер більше не намагався зробити себе типовим.</p>

</page>
