<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="contacts-edit-details" xml:lang="uk">

  <info>
    <link type="guide" xref="contacts"/>

    <revision pkgversion="3.5.5" date="2012-08-13" status="review"/>
    <revision pkgversion="3.8" date="2013-04-27" status="review"/>
    <revision pkgversion="3.12" date="2014-02-26" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.36.2" date="2020-08-11" status="review"/>
    <revision pkgversion="3.38.0" date="2020-11-02" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Єкатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <credit type="editor">
      <name>Pranali Deshmukh</name>
      <email>pranali21293@gmail.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Редагування відомостей для запису контакту.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>Редагування параметрів запису контакту</title>

  <p>Редагування параметрів записів контактів допоможе вам підтримувати актуальність і повноту даних у вашій адресній книзі.</p>

  <steps>
    <item>
      <p>Виберіть запис контакту з вашого списку контактів.</p>
    </item>
    <item>
      <p>Натисніть кнопку <media its:translate="no" type="image" src="figures/view-more-symbolic.svg">
      <span its:translate="yes">Докладніше</span></media> у верхньому правому куті вікна програми і виберіть пункт <gui style="menuitem">Редагувати</gui>.</p>
    </item>
    <item>
      <p>Змініть параметри запису контакту.</p>
      <p>Щоб додати <em>подробиці</em>, зокрема новий номер телефону або електронну адресу, просто заповніть наступне порожнє поле відповідного типу (номер телефону, адреса електронної пошти тощо).</p>
      <note style="tip">
        <p>Натисніть кнопку <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">Докладніше</span></media> у нижній частині вікна, щоб розгорнути список доступних пунктів — відкрити поля, подібні до полів <gui>Вебсайт</gui> та <gui>День народження</gui>.</p>
      </note>
    </item>
    <item>
      <p>Натисніть кнопку <gui style="button">Завершено</gui>, щоб завершити редагування запису контакту.</p>
    </item>
  </steps>

</page>
