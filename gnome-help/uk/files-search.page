<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:ui="http://projectmallard.org/ui/1.0/" type="guide" style="task" id="files-search" xml:lang="uk">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-25" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.34" date="2019-07-20" status="draft"/>
    <revision pkgversion="3.36" date="2020-04-18" status="draft"/>

    <credit type="author">
      <name>Проєкт з документування GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Девід Кінг (David King)</name>
      <email>amigadave@amigadave.com</email>
    </credit>
    <credit type="editor">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <title type="link" role="trail">Пошук</title>
    <desc>Пошук файлів на основі назви і типу.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Пошук файлів</title>

  <p>Ви можете виконати пошук файлів на основі назви або типу безпосередньо з вікна програми для керування файлами.</p>

  <links type="topic" style="linklist">
    <title>Інші програми для пошуку</title>
    <!-- This is an extension point where search apps can add
    their own topics. It's empty by default. -->
  </links>

  <steps>
    <title>Пошук</title>
    <item>
      <p>Відкрийте вікно програми <app>Файли</app> з панелі огляду <gui xref="shell-introduction#activities">Діяльності</gui>.</p>
    </item>
    <item>
      <p>Якщо вам відомо, що потрібні вам файли зберігаються у певній теці, перейдіть до цієї теки.</p>
    </item>
    <item>
      <p>Введіть слово або слова, які, як вам відомо, є частиною назви файла, і програма покаже відповідні файли на панелі пошуку. Наприклад, якщо у назві усіх ваших рахунків є слово «Рахунок», введіть <input>рахунок</input>. Відповідність слів встановлюватиметься без врахування регістру символів.</p>
      <note>
        <p>Замість введення слів безпосередньо для виклику панелі пошуку, ви можете натиснути кнопку <media its:translate="no" type="image" mime="image/svg" src="figures/edit-find-symbolic.svg"> <key>Search</key> key symbol
        </media> на панелі інструментів або натиснути комбінацію клавіш <keyseq><key>Ctrl</key><key>F</key></keyseq>.</p>
      </note>
    </item>
    <item>
      <p>Ви можете звузити перелік результатів за датою, типом файла або текстовим вмістом файла або наказати виконати пошук лише за назвою файла.</p>
      <p>Щоб застосувати фільтри, натисніть кнопку спадного меню ліворуч від піктограми <media its:translate="no" type="image" mime="image/svg" src="figures/edit-find-symbolic.svg"> <key>Search</key> key symbol</media> програми для керування файлами і виберіть якісь із доступних фільтрів:</p>
      <list>
        <item>
          <p><gui>Коли</gui>: наскільки далеко у минуле має бути спрямовано пошук?</p>
        </item>
        <item>
          <p><gui>Що</gui>: яким є тип файла?</p>
        </item>
        <item>
          <p>Має ваш пошук бути повнотекстовим чи слід виконати пошук лише за назвою файла?</p>
        </item>
      </list>
    </item>
    <item>
      <p>Щоб вилучити фільтр, натисніть <gui>X</gui> збоку від мітки фільтра, який ви хочете вилучити.</p>
    </item>
    <item>
      <p>Ви можете відкривати, копіювати, вилучати або виконувати інші дії з файлами у списку результатів пошуку так само, як ви виконуєте ці дії із будь-якими іншими файлами у теках у програмі для керування файлами.</p>
    </item>
    <item>
      <p>Натисніть кнопку <media its:translate="no" type="image" mime="image/svg" src="figures/edit-find-symbolic.svg"> <key>Search</key> key symbol
      </media> на панелі інструментів, щоб вийти з режиму пошуку і повернутися до початкової теки.</p>
    </item>
  </steps>

<section id="customize-files-search">
  <title>Налаштовування пошуку файлів</title>

<p>Ви можете включити або виключити певні каталоги із пошуку у програмі <app>Файли</app>. Щоб налаштувати каталоги для пошуку, виконайте такі дії:</p>

  <steps>
    <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть введення слова <gui>Пошук</gui>.</p>
    </item>
    <item>
      <p>Виберіть <guiseq><gui>Параметри</gui><gui>Пошук</gui></guiseq> у списку результатів. У відповідь буде відкрито панель <gui>Параметри пошуку</gui>.</p>
    </item>
    <item>
      <p>Натисніть кнопку <gui>Місця пошуку</gui> на смужці заголовка.</p>
    </item>
  </steps>

<p>У відповідь буде відкрито окрему панель параметрів, за допомогою якої ви зможете увімкнути або вимкнути пошук у каталогах. Перемкнути параметри пошуку можна на кожній з таких трьох вкладок:</p>

  <list>
    <item>
      <p><gui>Місця</gui>: показує список типових місць у домашньому каталозі</p>
    </item>
    <item>
      <p><gui>Закладки</gui>: показує список місць-каталогів, для яких було визначено закладки у програмі <app>Файли</app></p>
    </item>
    <item>
      <p><gui>Інше</gui>: показує список місць-каталогів, які ви включаєте за допомогою кнопки <gui>+</gui>.</p>
    </item>
  </list>

</section>

</page>
