<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-share" xml:lang="uk">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>
    <link type="guide" xref="sharing"/>
    <link type="seealso" xref="nautilus-connect"/>

    <revision pkgversion="3.8.2" version="0.3" date="2013-05-11" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Просте передавання файлів контактам електронною поштою з вікна програми для керування файлами.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>Надсилання файлів електронною поштою</title>

<p>Ви можете дуже просто поділитися файлами з вашими контактами за допомогою електронної пошти безпосередньо з вікна програми для керування файлами.</p>

  <note style="important">
    <p>Перш ніж почати, переконайтеся, що на комп'ютері встановлено <app>Evolution</app> або <app>Geary</app> і налаштовано обліковий запис електронної пошти.</p>
  </note>

<steps>
  <title>Щоб поділитися файлом електронною поштою, виконайте такі дії:</title>
    <item>
      <p>Відкрийте вікно програми <app>Файли</app> з панелі огляду <gui xref="shell-introduction#activities">Діяльності</gui>.</p>
    </item>
  <item><p>Перейдіть до теки файла, який ви хочете передати.</p></item>
    <item>
      <p>Клацніть правою кнопкою миші на пункті файла і виберіть у контекстному меню пункт <gui>Надіслати…</gui>. У відповідь буде відкрито вікно редактора повідомлень із вже долученим до повідомлення файлом.</p>
    </item>
  <item><p>Клацніть на пункті <gui>Кому</gui>, що вибрати контакт, або введіть адресу електронної пошти, на яку ви хочете надіслати файл. Заповніть поле <gui>Тема</gui> і поле тексту повідомлення, а потім натисніть кнопку <gui>Надіслати</gui>.</p></item>
</steps>

<note style="tip">
  <p>Ви можете надіслати одразу декілька файлів. Позначте декілька файлів, утримуючи натиснутою клавішу <key>Ctrl</key> під час клацання на пунктах файлів, потім клацніть правою кнопкою миші на пункті будь-якого з позначених файлів.</p>
</note>

</page>
