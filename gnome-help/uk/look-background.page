<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="look-background" xml:lang="uk">

  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-09" status="review"/>
    <revision pkgversion="3.34" date="2019-11-12" status="review"/>
    <revision pkgversion="3.38.1" date="2020-11-04" status="review"/>

    <credit type="author">
      <name>Проєкт з документування GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Ейпріл Гонсалес (April Gonzales)</name>
      <email>loonycookie@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Наталія Рус Лейва (Natalia Ruz Leiva)</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Андре Клаппер (Andre Klapper)</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Єкатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Айвен Стентон</name>
      <email>northivanastan@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Встановлення зображення як тла стільниці.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Зміна фонового зображення стільниці</title>

  <p>Щоб змінити зображення, яке використовуватиметься як фонове на стільниці, виконайте такі дії:</p>

  <steps>
    <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть введення слова <gui>Тло</gui></p>
    </item>
    <item>
      <p>Натисніть <gui>Тло</gui>, щоб відкрити панель. На початку списку буде показано поточне вибране фонове зображення.</p>
    </item>
    <item>
      <p>Передбачено два способи зміни зображення, яке буде використано як фонове на стільниці:</p>
      <list>
        <item>
          <p>Клацніть на одному із фонових зображень, які постачаються разом із системою.</p>
        <note style="info">
          <p>Деякі фонові зображення змінюються протягом дня. На пунктах таких зображень показано піктограму годинника у нижньому правому куті.</p>
        </note>
        </item>
        <item>
          <p>Натисніть кнопку <gui>Додати зображення…</gui>, щоб скористатися однією із ваших власних фотографій. Типово, буде відкрито <file>Картинки</file>, оскільки там зберігають фотографії більшість програм для керування фотографіями.</p>
        </item>
      </list>
    </item>
    <item>
      <p>Параметри буде застосовано негайно.</p>
        <note style="tip">
          <p>Іншим способом встановлення однієї із ваших фотографій як фонового зображення є клацання правою кнопкою миші на файлі зображення у програмі <app>Файли</app> і вибір пункту <gui>Встановити як шпалери</gui> або відкриття файла зображення у програмі <app>Переглядач зображень</app>, натискання кнопки меню на смужці заголовка і виберіть пункт <gui>Встановити як шпалери</gui>.</p>
        </note>
    </item>
    <item>
      <p><link xref="shell-workspaces-switch">Перемкніться на порожній робочий простір</link>, щоб переглянути усю вашу стільницю.</p>
    </item>
  </steps>

</page>
