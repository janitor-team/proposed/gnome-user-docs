<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="sharing-media" xml:lang="uk">

  <info>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.10" version="0.2" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <credit type="author">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Оприлюднення мультимедійних даних у локальній мережі за допомогою UPnP.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Оприлюднення вашої музики, фотографій та відео</title>

  <p>Ви можете здійснювати навігацію, шукати і відтворювати мультимедійні дані на вашому комп'ютері за допомогою пристроїв із можливостями <sys>UPnP</sys> або <sys>DLNA</sys>, зокрема телефонів, телевізорів та ігрових консолей. Налаштуйте <gui>Оприлюднення матеріалів</gui>, щоб дозволити цим пристроям отримувати доступ до тек, які містять музичні дані, фотографії та відео.</p>

  <note style="info package">
    <p>Вам слід встановити пакунок <app>Rygel</app>, щоб став видимим пункт <gui>Оприлюднення матеріалів</gui>.</p>

    <if:choose xmlns:if="http://projectmallard.org/if/1.0/">
      <if:when test="action:install">
        <p><link action="install:rygel" style="button">Встановити Rygel</link></p>
      </if:when>
    </if:choose>
  </note>

  <steps>
    <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть введення слова <gui>Оприлюднення</gui>.</p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Оприлюднення</gui>, щоб відкрити панель.</p>
    </item>
    <item>
      <p>Якщо перемикач <gui>Оприлюднення</gui> у верхній правій частині вікна встановлено у стан «вимкнено», перемкніть його у стан «увімкнено».</p>

      <note style="info"><p>Якщо для тексту під міткою <gui>Назва комп'ютера</gui> передбачено можливість редагування, ви можете <link xref="sharing-displayname">змінити</link> назву комп'ютера, яку буде показано у мережі.</p></note>
    </item>
    <item>
      <p>Виберіть пункт <gui>Оприлюднення матеріалів</gui>.</p>
    </item>
    <item>
      <p>Перемкніть перемикач <gui>Оприлюднення матеріалів</gui> у стан «увімкнено».</p>
    </item>
    <item>
      <p>Типово, оприлюднюються <file>Музика</file>, <file>Картинки</file> і <file>Відео</file>. Щоб вилучити одну з цих тек, натисніть кнопку <gui>×</gui> поряд із назвою теки.</p>
    </item>
    <item>
      <p>Щоб додати якусь іншу теку, натисніть <gui style="button">+</gui>, щоб відкрити вікно <gui>Виберіть теку</gui>. Перейдіть <em>до</em> бажаної теки і натисніть кнопку <gui style="button">Відкрити</gui>.</p>
    </item>
    <item>
      <p>Натисніть кнопку <gui style="button">×</gui>. Після цього у вас має з'явитися можливість навігації або відтворення мультимедійних даних у теках, які ви вибрали, за допомогою зовнішнього пристрою.</p>
    </item>
  </steps>

  <section id="networks">
  <title>Мережі</title>

  <p>У розділі <gui>Мережі</gui> буде показано список мереж, з якими з'єднано ваш комп'ютер. Скористайтеся перемикачем поряд із кожним з пунктів, щоб визначити, де буде оприлюднено ваші мультимедійні дані.</p>

  </section>

</page>
