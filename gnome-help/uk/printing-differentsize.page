<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-differentsize" xml:lang="uk">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Філ Булл (Phil Bull)</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <its:rules xmlns:its="http://www.w3.org/2005/11/its" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <desc>Друк документа на папері іншого розміру або в іншій орієнтації.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Зміна розміру паперу при друці</title>

  <p>Якщо ви хочете змінити розмір паперу для вашого документа (наприклад, надрукувати PDF із розміром сторінки «US Letter» на папері A4), ви можете змінити формат друку для документа.</p>

  <steps>
    <item>
      <p>Відкрийте діалогове вікно друку натисканням комбінації клавіш <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Перейдіть до вкладки <gui>Параметри сторінки</gui>.</p>
    </item>
    <item>
      <p>У стовпчику <gui>Папір</gui> виберіть зі спадного списку <gui>Розмір паперу</gui> потрібний вам варіант.</p>
    </item>
    <item>
      <p>Натисніть кнопку <gui>Надрукувати</gui>, щоб надрукувати документ.</p>
    </item>
  </steps>

  <p>Ви також можете скористатися спадним списком <gui>Орієнтація</gui> для вибору іншої орієнтації:</p>

  <list>
    <item><p><gui>Книжкова</gui></p></item>
    <item><p><gui>Альбомна</gui></p></item>
    <item><p><gui>Обернена книжкова</gui></p></item>
    <item><p><gui>Обернена альбомна</gui></p></item>
  </list>

</page>
