<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="touchscreen-gestures" xml:lang="uk">

  <info>
    <link type="guide" xref="mouse" group="#last"/>

    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <!--
    For 41: https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/121
    -->
    <revision version="gnome:40" date="2021-03-18" status="candidate"/>

    <credit type="author">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Керування вашою стільницею за допомогою жестів на сенсорній панелі або сенсорному екрані.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Використання жестів на сенсорних панелях та сенсорних екранах</title>

  <p>Жестами декількома пальцями можна скористатися на сенсорних панелях та сенсорних екранах для навігації системою, також керування програмами.</p>

  <p>Використовувати жести можуть декілька програм. У <app>Переглядачі документів</app> можна виконувати масштабування та пересування документом за допомогою жестів, а у <app>Переглядачі зображень</app> можна масштабувати, обертати та панорамувати зображення.</p>

<section id="system">
  <title>Загальносистемні жести</title>

<table rules="rows" frame="bottom">
  <tr>
    <td><p><media type="image" src="figures/touch-overview.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Відкрити огляд «Діяльності» і огляд «Програми»</em></p>
    <p>Розташуйте три пальці на сенсорній панелі або сенсорному екрані і виконайте рух вгору, щоб відкрити огляд «Діяльності».</p>
    <p>Щоб відкрити панель списку програм, розташуйте три пальці на сенсорному пристрої і виконайте рух вгору ще раз.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-workspaces.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Перемкнути робочий простір</em></p>
    <p>Розташуйте три пальці на сенсорній панелі або сенсорному екрані і виконайте рух ліворуч або праворуч.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-unfullscreen.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Вийти з повноекранного режиму</em></p>
    <p>На сенсорному екрані виконайте перетягування від верхнього краю екрана, щоб вийти з повноекранного режиму для будь-якого вікна.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-osk.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Розгортання екранної клавіатури</em></p>
    <p>На сенсорному екрані проведіть від нижнього краю вгору, щоб відкрити <link xref="keyboard-osk">екранну клавіатуру</link>, якщо екранну клавіатуру увімкнено.</p></td>
  </tr>
</table>

</section>

<section id="apps">
  <title>Жести програм</title>

<table rules="rows" frame="bottom">
  <tr>
    <td><p><media type="image" src="figures/touch-tap.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Відкрити запис, запустити програму, відтворити звук</em></p>
    <p>Торкання запису.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-hold.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Позначити запис і відкрити список дій, які можна виконати</em></p>
    <p>Натисніть і потримайте секунду або дві.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-scroll.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Гортання області на екрані</em></p>
    <p>Перетягування: проведення пальцем поверхнею екрана.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-pinch-to-zoom.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Зміна масштабу перегляду (<app>Карти</app>, <app>Фотографії</app>)</em></p>
    <p>Зведення або розведення двох пальців: торкніться сенсорного пристрою двома пальцями і зведіть або розведіть їх.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-rotate.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Обертати фотографію</em></p>
    <p>Обертання двома пальцями: торкніться поверхні двома пальцями і виконайте обертання пальцями.</p></td>
  </tr>
</table>

</section>

</page>
