<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-mousekeys" xml:lang="uk">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="pointing"/>

    <revision pkgversion="3.8" date="2013-03-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>Філ Булл (Phil Bull)</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Єкатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    
    <desc>Вмикання клавіш миші для керування мишею за допомогою числового блоку клавіатури.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Клацання і пересування вказівника миші за допомогою клавіатури</title>

  <p>Якщо у вас виникають проблеми із користуванням мишею або іншим координатним пристроєм, ви можете керувати вказівником миші за допомогою цифрового блоку на вашій клавіатурі. Ця можливість називається <em>клавішами миші</em>.</p>

  <steps>
    <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть вводити слово <gui>Доступність</gui>.</p>
      <p>Отримати доступ до панелі огляду <gui>Діяльності</gui> можна натисканням відповідного пункту, пересуванням вказівника миші у верхній лівий кут екрана, за допомогою натискання комбінації клавіш <keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq> із наступним натисканням клавіші <key>Enter</key> або за допомогою натискання клавіші <key xref="keyboard-key-super">Super</key>.</p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Доступність</gui>, щоб відкрити панель.</p>
    </item>
    <item>
      <!-- TODO investigate keyboard navigation, because the arrow keys show no
           visual feedback, and do not seem to work. -->
      <p>Скористайтеся клавішами зі стрілками вгору і вниз для вибору пункту <gui>Кнопки миші</gui> у розділі <gui>Вказування та натискання</gui>, потім натисніть клавішу <key>Enter</key>, щоб встановити для перемикача <gui>Кнопки миші</gui> значення «увімкнено».</p>
    </item>
    <item>
      <p>Переконайтеся, що <key>Num Lock</key> вимкнено. Після вимикання ви зможете пересувати вказівник миші за допомогою цифрового блоку клавіатури.</p>
    </item>
  </steps>

  <p>Цифрова панель клавіатури є набором клавіш із цифрами на вашій клавіатурі. Клавіші на цій панелі зібрано у прямокутний блок. Якщо ви користуєтеся клавіатурою без цифрової панелі (зокрема клавіатурою на ноутбуці), вам, ймовірно, доведеться утримувати натиснутою функціональну клавішу (<key>Fn</key>) і користуватися натисканням певних інших клавіш на клавіатурі для імітації цифрової панелі. Якщо ви часто користуєтеся цією можливістю на ноутбуці, вам варто придбати зовнішню цифрову панель зі з'єднанням за допомогою USB або Bluetooth.</p>

  <p>Кожній клавіші із цифрою на цифровій панелі відповідає напрямок. Наприклад, натискання клавіші <key>8</key> пересуватиме вказівник вгору, а натискання клавіші <key>2</key> пересуватиме його вниз. Натисканням клавіші <key>5</key> можна імітувати одинарне клацання миші, а швидким подвійним натисканням — подвійне клацання.</p>

  <p>На більшості клавіатур є спеціальна клавіша, за допомогою якої можна імітувати клацання правою кнопкою миші. Іноді цю клавішу називають клавішею <key xref="keyboard-key-menu">Menu</key>. Втім, зауважте, що ця клавіша спрацьовує за місцем перебування фокусу клавіатури, а не за місцем розташування вказівника миші. Ознайомтеся із розділом <link xref="a11y-right-click"/>, щоб дізнатися, як можна імітувати клацання правою кнопкою миші утриманням натиснутою клавіші <key>5</key> або лівою кнопкою миші.</p>

  <p>Якщо ви хочете скористатися цифровою панеллю клавіатури для введення цифр у режимі імітації миші, увімкніть <key>Num Lock</key>. Втім, доки <key>Num Lock</key> буде увімкнено, ви не зможете керувати вказівником миші за допомогою цифрової панелі клавіатури.</p>

  <note>
    <p>Звичайні цифрові клавіш у рядку верхньої частини клавіатури не керуватимуть вказівником миші. Використовуватимуться лише клавіші цифр на цифровому блоці клавіатури.</p>
  </note>

</page>
