<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-behavior" xml:lang="uk">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-behavior"/>

    <desc>Одинарне клацання для відкриття файлів, запуску або перегляду виконуваних текстових файлів та визначення поведінки смітника.</desc>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Sindhu S</name>
      <email>sindhus@live.in</email>
    </credit>
    <credit type="editor">
      <name>Девід Кінг (David King)</name>
      <email>amigadave@amigadave.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>Налаштування поведінки програми для керування файлами</title>
<p>Ви можете керувати тим, відкриватимуться файли у відповідь на одинарне чи подвійне клацання, як оброблятимуться виконувані текстові файли та керувати поведінкою смітника. Натисніть кнопку меню у верхній лівій частині вікна, виберіть <gui>Параметри</gui>, потім перейдіть до розділу <gui>Загальне</gui>.</p>

<section id="behavior">
<title>Поведінка</title>
<terms>
 <item>
  <title><gui>Дія для відкриття записів</gui></title>
  <p>Типово, клацання позначає файли, а подвійне клацання відкриває їх. Втім, ви можете наказати системі відкривати файли і теки одинарним клацанням. Коли ви користуєтеся режимом одинарного клацання, ви можете утримувати натиснутою клавішу <key>Ctrl</key> при клацанні, щоб позначити один або декілька файлів.</p>
 </item>
</terms>

</section>
<section id="executable">
<title>Виконувані текстові файли</title>
 <p>Виконуваний текстовий файл — файл, який містить програму, яку можна запустити (виконати). Крім того, для запуску програми запуск має бути уможливлено <link xref="nautilus-file-properties-permissions">правами доступу до файла</link>. Найпоширенішими виконуваними текстовими файлами є скрипти <sys>командної оболонки</sys>, <sys>Python</sys> і <sys>Perl</sys>. Назви відповідних файлів мають суфікси <file>.sh</file>, <file>.py</file> і <file>.pl</file>, відповідно.</p>

 <p>Виконувані текстові файли називають також <em>скриптами</em>. Усі скрипти у теці <file>~/.local/share/nautilus/scripts</file> буде показано у контекстному меню файла, а саме, у підменю <gui style="menuitem">Скрипти</gui>. Якщо скрипти виконують з локальної теки, як параметри, йому буде передано усі позначені файли. Щоб скрипт обробив певний файл, виконайте такі дії:</p>

<steps>
  <item>
    <p>Перейдіть до бажаної теки.</p>
  </item>
  <item>
    <p>Виберіть бажаний файл.</p>
  </item>
  <item>
    <p>Клацніть правою кнопкою миші на файлі, щоб відкрити контекстне меню і виберіть бажаний скрипт з підменю <gui style="menuitem">Скрипти</gui>.</p>
  </item>
</steps>

 <note style="important">
  <p>Скрипту не передаватимуться жодні параметри, якщо його буде виконано з віддаленої теки, зокрема теки, у якій показано інтернет-сторінку або вміст теки <sys>ftp</sys>.</p>
 </note>

</section>

</page>
