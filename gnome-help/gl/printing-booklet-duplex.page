<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-booklet-duplex" xml:lang="gl">

  <info>
    <link type="guide" xref="printing-booklet"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Imprimir pasquíns pregados (tipo libro ou panfleto) desde un PDF usando un tamaño de papel normal A4/Carta.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2021</mal:years>
    </mal:credit>
  </info>

  <title>Imprimir un pasquín nunha impresora de dobre cara</title>

  <p>Pode facer folletos pregados (como un pequeno libro ou pasquín) mediante a impresión de páxinas dun documento nunha orde especial e cambiando un par de opcións de impresión.</p>

  <p>Estas instrucións son para imprimir un pasquín a partires dun documento PDF.</p>

  <p>Se ten que imprimir un pasquín desde un documento de <app>LibreOffice</app>, primeiro expórteo a PDF escollendo <guiseq><gui>Ficheiro</gui><gui>Exportar como PDF…</gui></guiseq>. As páxinas do seu documento precisan ser un múltiplo de 4 (4, 8, 12, 16,…). Polo que podería ter que engadir 3 páxinas en branco.</p>

  <p>Para imprimir un pasquín:</p>

  <steps>
    <item>
      <p>Abra o diálogo de impresora. Isto pode facerse normalmente mediante <gui style="menuitem">Imprimir</gui> no menú ou usando o atallo de teclado <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Prema sobre o botón <gui>Propiedades…</gui></p>
      <p>Na lista despregábel <gui>Orientación</gui>, asegúrese que <gui>Apaisado</gui> está seleccionado.</p>
      <p>Na lista despregábel<gui>Duplex</gui>, seleccione <gui>Bordo curto</gui>.</p>
      <p>Prema <gui>OK</gui> para volver ao diálogo de impresión.</p>
    </item>
    <item>
      <p>Baixo <gui>Rango e copias</gui>, seleccione <gui>Páxina</gui>.</p>
    </item>
    <item>
      <p>Escriba os números das páxinas neste orde (n é o número total de páxinas, múltiplo de 4):</p>
      <p>n, 1, 2, n-1, n-2, 3, 4, n-3, n-4, 5, 6, n-5, n-6, 7, 8, n-7, n-8, 9, 10, n-9, n-10, 11, 12, n-11…</p>
      <p>Exemplos:</p>
      <list>
        <item><p>Pasquín de 4 páxinas: escriba <input>4,1,2,3</input></p></item>
        <item><p>Pasquín de 8 páxinas: escriba <input>8,1,2,7,6,3,4,5</input></p></item>
        <item><p>Pasquín de 20 páxinas: escriba <input>20,1,2,19,18,3,4,17,16,5,6,15,14,7,8,13,12,9,10,11</input></p></item>
      </list>
    </item>
    <item>
      <p>Seleccione a lapela <gui>Disposición de páxina</gui>.</p>
      <p>No menú <gui>Disposición</gui>, elixa <gui>Pasquín</gui>.</p>
      <p>Baixo <em>Lados da páxina</em>, no lista despregábel <gui>Incluír</gui>, seleccione <gui>Todas as páxinas</gui>.</p>
    </item>
    <item>
      <p>Prema <gui>Imprimir</gui>.</p>
    </item>
  </steps>

</page>
