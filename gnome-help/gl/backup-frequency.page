<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-frequency" xml:lang="gl">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Proxecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Aprenda con que frecuencia debe crear unha copia de seguranza dos seus datos importantes e asegúrese de que están a bo recaudo.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2021</mal:years>
    </mal:credit>
  </info>

<title>Frecuencia das copias de seguranza</title>

  <p>Cada canto tempo debe facer os respaldos dependerá do tipo de datos a respaldar. Por exemplo, se está executando un contorno de rede con datos críticos almacenados nos seus servidores, incluso as copias de seguranza cada noite poden non ser suficientes.</p>

  <p>Por outra banda, se está respaldando datos no seu computador peraoal entón os respaldos cada hora son innecesarios. Pode que considere de axuda os seguintes puntos ao planificar un respaldo:</p>

<list style="compact">
  <item><p>A cantidade de tempo que vostede pasa no seu computador.</p></item>
  <item><p>Con que frecuencia e en que medida cambian os datos do seu equipo.</p></item>
</list>

  <p>Se os datos que quere respaldar son de baixa prioridade ou suxeitos a poucos cambios, como música, correos electrónicos e fotos familiares, entón as copias de seguranza semanais ou incluso mensuais deberían ser suficientes. Porén, se pode ter unha auditoría de impostos, poden ser necesarias copias de seguranza máis frecuentes.</p>

  <p>Como regra xeral, a cantidade de tempo entre cada respaldo non debería ser maior que a cantidade de tempo que vostede pode tardar en refacer calquera traballo perdido. Por exemplo, se tardar unha semana en reescribir os documentos perdidos é demasiado para vostede, debería facer unha copia de seguranza unha vez á semana.</p>

</page>
