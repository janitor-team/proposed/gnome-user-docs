<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-slow" xml:lang="gl">

  <info>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-21" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Outras cousas poderían estar descargándose, ten unha conexión mala ou é unha hora moi ocupada do día.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2021</mal:years>
    </mal:credit>
  </info>

  <title>Parece que Internet vai lento</title>

  <p>Se está usando internet e semella lenta, hai moitas razóns que poderían causar esta ralentización.</p>

  <p>Tente pechar o seu navegador web e volte a abrilo, e desceonéctese de internet e logo reconéctese de novo. (Ao facer isto reinicianse moitas das cousas que poderían estar causando que internet vaia lento).</p>

  <list>
    <item>
      <p><em style="strong">Hora ocupada do día</em></p>
      <p>Internet service providers commonly setup internet connections so that
      they are shared between several households. Even though you connect
      separately, through your own phone line or cable connection, the
      connection to the rest of the internet at the telephone exchange might
      actually be shared. If this is the case and lots of your neighbors are
      using the internet at the same time as you, you might notice a slow-down.
      You’re most likely to experience this at times when your neighbors are
      probably on the internet (in the evenings, for example).</p>
    </item>
    <item>
      <p><em style="strong">Descargando moitas cousas á vez</em></p>
      <p>Se vostede ou outra persoa usando a súa conexión a internet está descargando varios ficheiros á vez, ou vendo vídeos, a conexión podería non ser rápida dabondo para manter a demanda. Neste caso, será máis lenta.</p>
    </item>
    <item>
      <p><em style="strong">Conexións non dispoñíbeis</em></p>
      <p>Algunhas conexións a internet non son fiábeis, especialmente as temporais ou aquelas en áreas de alta demanda. Se está nunha cafetaría ou centro de conferencias, a conexión a internet podería estar saturada.</p>
    </item>
    <item>
      <p><em style="strong">Sinal de conexión sen fíos baixa</em></p>
      <p>If you are connected to the internet by wireless (Wi-Fi), check the
      network icon on the top bar to see if you have good wireless signal. If
      not, the internet may be slow because you don’t have a very strong
      signal.</p>
    </item>
    <item>
      <p><em style="strong">Usar unha conexión móbil a Internet</em></p>
      <p>If you have a mobile internet connection and notice that it is slow,
      you may have moved into an area where signal reception is poor. When this
      happens, the internet connection will automatically switch from a fast
      “mobile broadband” connection like 3G to a more reliable, but slower,
      connection like GPRS.</p>
    </item>
    <item>
      <p><em style="strong">O navegador web ten un problema</em></p>
      <p>Sometimes web browsers encounter a problem that makes them run slow.
      This could be for any number of reasons — you could have visited a
      website that the browser struggled to load, or you might have had the
      browser open for a long time, for example. Try closing all of the
      browser’s windows and then opening the browser again to see if this makes
      a difference.</p>
    </item>
  </list>

</page>
