<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-switching" xml:lang="gl">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="guide" xref="shell-overview#apps"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.12" date="2014-03-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Proxecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>


    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Prema <keyseq><key>Super</key><key>Tab</key></keyseq>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2021</mal:years>
    </mal:credit>
  </info>

<title>Trocar entre as xanelas</title>

  <p>You can see all the running applications that have a graphical user
  interface in the <em>window switcher</em>. This makes
  switching between tasks a single-step process and provides a full picture of
  which applications are running.</p>

  <p>Desde un espazo de traballo:</p>

  <steps>
    <item>
      <p>Prema <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq> para mostrar o <gui>trocador de xanelas</gui>.</p>
    </item>
    <item>
      <p>Solte a tecla <key xref="keyboard-key-super">Super</key> para seleccionar a seguinte xanela (realzada) no selector.</p>
    </item>
    <item>
      <p>Otherwise, still holding down the <key xref="keyboard-key-super">Super</key>
      key, press <key>Tab</key> to cycle through the list of open
      windows, or <keyseq><key>Shift</key><key>Tab</key></keyseq> to cycle
      backwards.</p>
    </item>
  </steps>

  <p if:test="platform:gnome-classic">Tamén pode usar a lista de xanelas na barra inferior para acceder a todas as xanelas abertas e cambiar entre elas.</p>

  <note style="tip" if:test="!platform:gnome-classic">
    <p>As xanelas no selector de xanelas agrúpanse por aplicacións. As vistas previas das aplicacións con múltiples xanelas despréganse cando as preme. Prema <key xref="keyboard-key-super">Super</key> e prema <key>`</key> (ou a tecla <key>Tab</key>) para recorrer a lista.</p>
  </note>

  <p>Tamén pode moverse entre as iconas dos aplicativso no intecambiador de xanelas con <key>→</key> ou <key>←</key> ou seleccionar unha premendo sobre ela co punteiro do rato.</p>

  <p>As vistas previas das aplicacións cunha soa xanela, só están dispoñíbeis cando se preme expresamente a frecha <key>↓</key>.</p>

  <p>From the <gui>Activities</gui> overview, click on a
  <link xref="shell-windows">window</link> to switch to it and leave the
  overview. If you have multiple
  <link xref="shell-windows#working-with-workspaces">workspaces</link> open,
  you can click on each workspace to view the open windows on each
  workspace.</p>

</page>
