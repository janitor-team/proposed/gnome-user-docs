<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="disk-benchmark" xml:lang="gl">

  <info>
    <link type="guide" xref="disk"/>

    <revision pkgversion="3.6.2" version="0.2" date="2012-11-16" status="review"/>
    <revision pkgversion="3.10" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Proxecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
   <credit type="editor">
     <name>Michael Hill</name>
     <email>mdhillca@gmail.com</email>
   </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Execute executar probas de rendemento sobre o seu disco duro para comprobar como é de rápido.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2021</mal:years>
    </mal:credit>
  </info>

<title>Comprobar o rendemento do seu disco duro</title>

  <p>Para probar a velocidade do seu disco duro:</p>

  <steps>
    <item>
      <p>Abra <app>Discos</app> desde a vista de <gui xref="shell-introduction#activities">Actividades</gui>.</p>
    </item>
    <item>
      <p>Seleccione o disco desde a lista do panel esquerdo.</p>
    </item>
    <item>
      <p>Prema o botón de menú e seleccione <gui>Probar rendemento do unidade…</gui> desde o menú.</p>
    </item>
    <item>
      <p>Prema <gui>Iniciar proba de rendemento</gui> e axuste os parámetros <gui>Taxa de transferencia</gui> e <gui>Tempo de acceso</gui> como desexe.</p>
    </item>
    <item>
      <p>Click <gui>Start Benchmarking…</gui> to test how fast data can be read
      from the disk. <link xref="user-admin-explain">Administrative
      privileges</link> may be required. Enter your password, or the password
      for the requested administrator account.</p>
      <note>
        <p>Se está marcado <gui>Levar a cabo unha proba de rendemento</gui>, a proba de rendemento tamén probará a rapidez de lectura e escritura do disco . Aínda que tamén levará moito máis tempo rematala.</p>
      </note>
    </item>
  </steps>

  <p>Cando a proba remate, os resultados aparecerán no gráfico. Os puntos verdes e as liñas que se conectan indican os exemplos tomados; isto corresponde aos eixos vermellos, mostrando o tempo de acceso, mostrados fronte ao eixo inferior, que representa a porcentaxe de tempo gastada. A liña azul representa as taxas de lectura, mentres que a liña vermella representa as taxas de escritura; estas móstranse como taxas de acceso de datos no eixo esquerdo, fronte á porcentaxe de disco navegado, desde fóra até o eixo, máis aló do eixo inferior.</p>

  <p>Embaixo do gráfico, móstranse os valores de taxa de lectura e escritura mínima, máxima e media, o tempo de acceso medio e o tempo que pasou desde a última proba de rendemento.</p>

</page>
