<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-autosuspend" xml:lang="gl">

  <info>
     <link type="guide" xref="power#saving"/>
     <link type="seealso" xref="power-suspend"/>
     <link type="seealso" xref="shell-exit#suspend"/>

    <revision version="gnome:3.38.3" date="2021-03-07" status="candidate"/>
    <revision pkgversion="gnome:41" date="2021-09-08" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Configure o seu computador para suspendelo automaticamente.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2021</mal:years>
    </mal:credit>
  </info>

  <title>Configurar a suspensión automática</title>

  <p>Pode configurar o seu ordenador para que se suspenda automaticamente cando está inactivo. Pódense especificar diferentes intervalos para funcionar con batería ou conectarse. </p>

  <steps>

    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Enerxía</gui>.</p>
    </item>
    <item>
      <p>Prema <gui>Enerxía</gui> para abrir o panel.</p>
    </item>
    <item>
      <p>In the <gui>Power Saving Options</gui> section, click
      <gui>Automatic Suspend</gui>.</p>
    </item>
    <item>
      <p>Choose <gui>On Battery Power</gui> or <gui>Plugged In</gui>, set the
      switch to on, and select a <gui>Delay</gui>. Both options can
      be configured.</p>

      <note style="tip">
        <p>Nun computador de escritorio, hai unha opción coa etiqueta <gui>Cando estea en repouso</gui>.</p>
      </note>
    </item>

  </steps>

</page>
