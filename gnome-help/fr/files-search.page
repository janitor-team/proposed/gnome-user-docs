<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:ui="http://projectmallard.org/ui/1.0/" type="guide" style="task" id="files-search" xml:lang="fr">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-25" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.34" date="2019-07-20" status="draft"/>
    <revision pkgversion="3.36" date="2020-04-18" status="draft"/>

    <credit type="author">
      <name>Le projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>
    <credit type="editor">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <title type="link" role="trail">Search</title>
    <desc>Trouver des fichiers par leur nom et leur type.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hydroxyp</mal:name>
      <mal:email>hydroxyp@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Charles Monzat</mal:name>
      <mal:email>charles.monzat@free.fr</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Recherche de fichiers</title>

  <p>Vous pouvez rechercher des fichiers par nom ou par type directement dans le gestionnaire de fichiers.</p>

  <links type="topic" style="linklist">
    <title>Autres applications de recherche</title>
    <!-- This is an extension point where search apps can add
    their own topics. It's empty by default. -->
  </links>

  <steps>
    <title>Recherche</title>
    <item>
      <p>Ouvrez l’application <app>Fichiers</app> à partir de la vue d’ensemble des <gui xref="shell-introduction#activities">Activités</gui>.</p>
    </item>
    <item>
      <p>Si vous connaissez le nom du dossier contenant les fichiers que vous recherchez, allez à ce dossier.</p>
    </item>
    <item>
      <p>Saisissez un ou plusieurs mots qui devraient faire partie du nom du fichier et ils s’affichent dans le champ de recherche. Par exemple, si vous nommez toutes vos factures avec le mot « Facture », saisissez <input>facture</input>. Les mots sont recherchés sans tenir compte de la casse.</p>
      <note>
        <p>Plutôt que saisir les mots pour faire apparaître le champ de recherche, cliquez sur <media its:translate="no" type="image" mime="image/svg" src="figures/edit-find-symbolic.svg"> <key>Search</key> key symbol
        </media> dans la barre d’outils, ou appuyez sur <keyseq><key>Ctrl</key><key>F</key></keyseq>.</p>
      </note>
    </item>
    <item>
      <p>You can narrow your results by date, by file type, and by whether to
      search a file’s full text, or to only search for file names.</p>
      <p>To apply filters, select the drop-down menu button to the left of the
      file manager’s <media its:translate="no" type="image" mime="image/svg" src="figures/edit-find-symbolic.svg"> <key>Search</key> key symbol</media>
      icon, and choose from the available filters:</p>
      <list>
        <item>
          <p><gui>Quand</gui> : recherche à partir d’une date déterminée.</p>
        </item>
        <item>
          <p><gui>Quoi</gui> : recherche à partir d’un type d’élément.</p>
        </item>
        <item>
          <p>Recherche incluant le texte intégral ou le nom de fichier uniquement.</p>
        </item>
      </list>
    </item>
    <item>
      <p>Pour supprimer un filtre, sélectionnez le <gui>X</gui> situé à côté de l’étiquette de filtre que vous souhaitez supprimer.</p>
    </item>
    <item>
      <p>Vous pouvez ouvrir, copier, supprimer ou même travailler avec les fichiers à partir des résultats de la recherche, comme si vous étiez dans n’importe quel dossier du gestionnaire de fichiers.</p>
    </item>
    <item>
      <p>Cliquez une nouvelle fois sur <media its:translate="no" type="image" mime="image/svg" src="figures/edit-find-symbolic.svg"> <key>Search</key> key symbol
      </media> dans la barre d’outils pour quitter la recherche et retourner dans le répertoire.</p>
    </item>
  </steps>

<section id="customize-files-search">
  <title>Personnalisation de la recherche de fichiers</title>

<p>Vous pouvez souhaiter que certains répertoires soient inclus ou exclus des recherches dans l’application <app>Fichiers</app>. Pour personnaliser les répertoires recherchés :</p>

  <steps>
    <item>
      <p>Ouvrez la vue d’ensemble des <gui xref="shell-introduction#activities">Activités</gui> et commencez à saisir <gui>Recherche</gui>.</p>
    </item>
    <item>
      <p>Sélectionnez <guiseq><gui>Paramètres</gui><gui>Recherche</gui></guiseq> dans les résultats. Ceci ouvre le panneau des <gui>paramètres de recherche</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur le bouton <gui>Emplacements de la recherche</gui> situé dans la barre d’en-tête.</p>
    </item>
  </steps>

<p>Cette action ouvre un panneau de paramètres séparé qui vous permet d’activer ou de désactiver les recherches de répertoire. Vous pouvez basculer les recherches sur chacun des trois onglets :</p>

  <list>
    <item>
      <p><gui>Emplacements</gui> : répertorie les emplacements courants du répertoire personnel.</p>
    </item>
    <item>
      <p><gui>Signets</gui> : répertorie les emplacements de répertoire que vous avez mis en signet dans l’application <app>Fichiers</app>.</p>
    </item>
    <item>
      <p><gui>Autre</gui> : répertorie les emplacements de répertoire que vous ajoutez via le bouton <gui>+</gui>.</p>
    </item>
  </list>

</section>

</page>
