<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="keyboard-shortcuts-set" xml:lang="fr">

  <info>
    <link type="guide" xref="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-23" status="review"/>
    <revision pkgversion="3.9.92" date="2013-10-11" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.17.90" date="2015-08-30" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>
    <revision version="gnome:40" date="2021-03-02" status="review"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Juanjo Marín</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Définir ou modifier les raccourcis clavier dans les paramètres <gui>Clavier</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hydroxyp</mal:name>
      <mal:email>hydroxyp@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Charles Monzat</mal:name>
      <mal:email>charles.monzat@free.fr</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Définition de raccourcis clavier</title>

<p>Pour modifier la ou les touches d’un raccourci clavier :</p>

  <steps>
    <item>
      <p>Ouvrez la vue d’ensemble des <gui xref="shell-introduction#activities">Activités</gui> et commencez à saisir <gui>Paramètres</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Paramètres</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Keyboard</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Keyboard Shortcuts</gui> section, select <gui>Customize Shortcuts</gui>.</p>
    </item>
    <item>
      <p>Select the desired category, or enter a search term.</p>
    </item>
    <item>
      <p>Cliquez sur la ligne comportant l’action souhaitée. La fenêtre <gui>Définir un raccourci</gui> s’ouvre alors.</p>
    </item>
    <item>
      <p>Effectuez la combinaison de touches désirée ou appuyez sur <key>Retour arrière</key> pour l’effacer, ou sur <key>Échap</key> pour annuler.</p>
    </item>
  </steps>


<section id="defined">
<title>Raccourcis courants prédéfinis</title>
  <p>Un certain nombre de raccourcis pré-configurés et qui peuvent être modifiés sont regroupés dans ces catégories :</p>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Accessibilité</title>
  <tr>
	<td><p>Diminuer la taille du texte</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Activer ou désactiver le contraste élevé</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Augmenter la taille du texte</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Activer ou désactiver le clavier visuel</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Activer ou désactiver le lecteur d’écran</p></td>
	<td><p><keyseq><key>Alt</key><key>Super</key><key>S</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Activer ou désactiver le zoom</p></td>
	<td><p><keyseq><key>Alt</key><key>Super</key><key>8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Zoom avant</p></td>
  <td><p><keyseq><key>Alt</key><key>Super</key><key>=</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Zoom arrière</p></td>
  <td><p><keyseq><key>Alt</key><key>Super</key><key>-</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Les lanceurs</title>
  <tr>
	<td><p>Dossier personnel</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-folder.svg"> <key>Explorer</key> key symbol</media> ou <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-computer.svg"> <key>Explorer</key> key symbol</media> ou <key>Explorateur</key></p></td>
  </tr>
  <tr>
	<td><p>Lancer la calculatrice</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-calculator.svg"> <key>Calculator</key> key symbol</media> ou <key>Calculatrice</key></p></td>
  </tr>
  <tr>
	<td><p>Démarrer le client de messagerie</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mail.svg"> <key>Mail</key> key symbol</media> ou <key>Messagerie</key></p></td>
  </tr>
  <tr>
	<td><p>Démarrer le navigateur d’aide</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Démarrer le navigateur Web</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-world.svg"> <key>WWW</key> key symbol</media> ou <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-home.svg"> <key>WWW</key> key symbol</media> ou <key>WWW</key></p></td>
  </tr>
  <tr>
	<td><p>Recherche</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-search.svg"> <key>Search</key> key symbol</media> ou <key>Recherche</key></p></td>
  </tr>
  <tr>
	<td><p>Paramètres</p></td>
	<td><p><key>Outils</key></p></td>
  </tr>

</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Navigation</title>
  <tr>
	<td><p>Masquer toutes les fenêtres normales</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Move to workspace on the left</p></td>
	<td><p><keyseq><key>Super</key><key>Page haut</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move to workspace on the right</p></td>
	<td><p><keyseq><key>Super</key><key>Page bas</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Déplacer la fenêtre d’un écran vers le bas</p></td>
	<td><p><keyseq><key>Maj</key><key xref="keyboard-key-super">Super</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Déplacer la fenêtre d’un écran vers la gauche</p></td>
	<td><p><keyseq><key>Maj</key><key>Super</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Déplacer la fenêtre d’un écran vers la droite</p></td>
	<td><p><keyseq><key>Maj</key><key>Super</key><key>→</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Déplacer la fenêtre d’un écran vers le haut</p></td>
	<td><p><keyseq><key>Maj</key><key>Super</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window one workspace to the left</p></td>
	<td><p><keyseq><key>Maj</key><key>Super</key><key>page haut</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window one workspace to the right</p></td>
	<td><p><keyseq><key>Maj</key><key>Super</key><key>page bas</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Déplacer la fenêtre vers le dernier espace de travail</p></td>
	<td><p><keyseq><key>Maj</key><key>Super</key><key>Fin</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Déplacer la fenêtre vers l’espace de travail 1</p></td>
	<td><p><keyseq><key>Maj</key><key>Super</key><key>Origine</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Déplacer la fenêtre vers l’espace de travail 2</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Déplacer la fenêtre vers l’espace de travail 3</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Déplacer la fenêtre vers l’espace de travail 4</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Basculer entre les applications</p></td>
	<td><p><keyseq><key>Super</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Basculer les contrôles système</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Basculer les contrôles système directement</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Échap</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Basculer vers le dernier espace de travail</p></td>
	<td><p><keyseq><key>Super</key><key>Fin</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Basculer vers l’espace de travail 1</p></td>
	<td><p><keyseq><key>Super</key><key>Origine</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Basculer vers l’espace de travail 2</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Basculer vers l’espace de travail 3</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Basculer vers l’espace de travail 4</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
        <td><p>Basculer entre les fenêtres</p></td>
        <td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Basculer entre les fenêtres directement</p></td>
	<td><p><keyseq><key>Alt</key><key>Échap</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Basculer entre les fenêtres d’une application directement</p></td>
	<td><p><keyseq><key>Alt</key><key>F6</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Basculer entre les fenêtres d’une application</p></td>
	<td><p>Désactivé</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Captures d’écran</title>
  <tr>
	<td><p>Copier une capture d’écran d’une fenêtre dans le presse-papiers</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Impr. écran</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Copier une capture d’une partie de l’écran dans le presse-papiers</p></td>
	<td><p><keyseq><key>Maj</key><key>Ctrl</key><key>Impr. écran</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Copier une capture d’écran dans le presse-papiers</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Impr. écran</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Enregistrer une courte vidéo d’écran</p></td>
        <td><p><keyseq><key>Maj</key><key>Ctrl</key><key>Alt</key><key>R</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Enregistrer une capture d’écran d’une fenêtre dans le dossier Images</p></td>
	<td><p><keyseq><key>Alt</key><key>Impr. écran</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Enregistrer une capture d’une partie de l’écran dans le dossier Images</p></td>
	<td><p><keyseq><key>Maj</key><key>Impr. écran</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Enregistrer une capture d’écran dans le dossier Images</p></td>
	<td><p><key>Impr. écran</key></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Son et média</title>
  <tr>
	<td><p>Éjecter</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-eject.svg"> <key>Eject</key> key symbol</media> (Éjecter)</p></td>
  </tr>
  <tr>
	<td><p>Lancer le lecteur multimédia</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-media.svg"> <key>Media</key> key symbol</media> (Lecteur multimédia)</p></td>
  </tr>
  <tr>
	<td><p>Microphone mute/unmute</p></td>
	<td/>
  </tr>
  <tr>
	<td><p>Morceau suivant</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-next.svg"> <key>Next</key> key symbol</media> (Audio suivant)</p></td>
  </tr>
  <tr>
	<td><p>Mettre en pause la lecture</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-pause.svg"> <key>Pause</key> key symbol</media> (Pause audio)</p></td>
  </tr>
  <tr>
	<td><p>Lire (ou lecture/pause)</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-play.svg"> <key>Play</key> key symbol</media> (Lecture audio)</p></td>
  </tr>
  <tr>
	<td><p>Morceau précédent</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-previous.svg"> <key>Previous</key> key symbol</media> (Audio précédent)</p></td>
  </tr>
  <tr>
	<td><p>Stopper la lecture</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-stop.svg"> <key>Stop</key> key symbol</media> (Arrêt audio)</p></td>
  </tr>
  <tr>
	<td><p>Baisser le volume</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-voldown.svg"> <key>Volume Down</key> key symbol</media> (Diminuer le volume sonore)</p></td>
  </tr>
  <tr>
	<td><p>Couper le volume</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mute.svg"> <key>Mute</key> key symbol</media> (Sourdine)</p></td>
  </tr>
  <tr>
	<td><p>Augmenter le volume</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-volup.svg"> <key>Volume Up</key> key symbol</media> (Augmenter le volume sonore)</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Système</title>
  <tr>
        <td><p>Focaliser sur la notification active</p></td>
        <td><p><keyseq><key>Super</key><key>N</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Verrouiller l’écran</p></td>
	<td><p><keyseq><key>Super</key><key>L</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Show the Power Off dialog</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Suppr</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Ouvrir le menu de l’application</p></td>
        <td><p><keyseq><key>Super</key><key>F10</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Restaurer les raccourcis clavier</p></td>
        <td><p><keyseq><key>Super</key><key>Échap</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Afficher toutes les applications</p></td>
        <td><p><keyseq><key>Super</key><key>A</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Afficher la vue d’ensemble des activités</p></td>
	<td><p><keyseq><key>Alt</key><key>F1</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Afficher la liste des notifications</p></td>
	<td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Afficher la vue d’ensemble</p></td>
	<td><p><keyseq><key>Super</key><key>S</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Afficher la fenêtre de saisie de commande</p></td>
	<td><p><keyseq><key>Alt</key><key>F2</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Saisie</title>
  <tr>
  <td><p>Passer à la source de saisie suivante</p></td>
  <td><p><keyseq><key>Super</key><key>Barre d’espace</key></keyseq></p></td>
  </tr>

  <tr>
  <td><p>Passer à la source de saisie précédente</p></td>
  <td><p><keyseq><key>Maj</key><key>Super</key><key>Barre d’espace</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Fenêtres</title>
  <tr>
	<td><p>Activer le menu de la fenêtre</p></td>
	<td><p><keyseq><key>Alt</key><key>Barre d’espace</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Fermer la fenêtre</p></td>
	<td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Masquer la fenêtre</p></td>
        <td><p><keyseq><key>Super</key><key>H</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Placer la fenêtre au-dessous des autres</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Maximiser la fenêtre</p></td>
	<td><p><keyseq><key>Super</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Maximiser la fenêtre horizontalement</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Maximiser la fenêtre verticalement</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Déplacer la fenêtre</p></td>
	<td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Placer la fenêtre au-dessus des autres</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Placer la fenêtre au-dessus des autres si elle est recouverte, sinon au-dessous</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Redimensionner la fenêtre</p></td>
	<td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Restaurer la fenêtre</p></td>
        <td><p><keyseq><key>Super</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Activer ou désactiver le mode plein écran</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Activer ou désactiver l’état maximisé</p></td>
	<td><p><keyseq><key>Alt</key><key>F10</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Afficher la fenêtre sur tous les espaces de travail ou sur un seul</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
        <td><p>Séparer l’affichage sur la gauche</p></td>
        <td><p><keyseq><key>Super</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Séparer l’affichage sur la droite</p></td>
        <td><p><keyseq><key>Super</key><key>→</key></keyseq></p></td>
  </tr>
</table>

</section>

<section id="custom">
<title>Raccourcis personnalisés</title>

  <p>To create your own application keyboard shortcut in the
  <gui>Keyboard</gui> settings:</p>

  <steps>
    <item>
      <p>Select <gui>Custom Shortcuts</gui>.</p>
    </item>
    <item>
      <p>Click the <gui style="button">Add Shortcut</gui> button if no custom
      shortcut is set yet. Otherwise click the <gui style="button">+</gui>
      button. The <gui>Add Custom Shortcut</gui> window will appear.</p>
    </item>
    <item>
      <p>Saisissez un <gui>Nom</gui> pour identifier le raccourci, et une <gui>Commande</gui> pour lancer une application. Par exemple, si vous voulez un raccourci pour ouvrir <app>Rhythmbox</app>, vous pouvez le nommer <input>Musique</input> et utiliser la commande <input>rhythmbox</input>.</p>
    </item>
    <item>
      <p>Click the <gui style="button">Add Shortcut…</gui> button. In the
      <gui>Add Custom Shortcut</gui> window, hold down the desired
      shortcut key combination.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Ajouter</gui>.</p>
    </item>
  </steps>

  <p>Le nom de la commande que vous saisissez doit être une commande système valide. Vous pouvez vérifier que la commande fonctionne en ouvrant un Terminal et en la saisissant dedans. Il est possible que la commande ouvrant une application n’aie pas exactement le même nom que l’application elle-même.</p>

  <p>If you want to change the command that is associated with a custom
  keyboard shortcut, click the row of the shortcut. The
  <gui>Set Custom Shortcut</gui> window will appear, and you can edit the
  command.</p>

</section>

</page>
