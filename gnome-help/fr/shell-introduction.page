<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" version="1.0 if/1.0" id="shell-introduction" xml:lang="fr">

  <info>
    <link type="guide" xref="shell-overview" group="#first"/>
    <link type="guide" xref="index" group="intro"/>

    <revision pkgversion="3.6.0" date="2012-10-13" status="review"/>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-28" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="review"/>
    <revision pkgversion="3.35.91" date="2020-07-19" status="review"/>
    <revision version="gnome:40" date="2021-06-16" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Un aperçu visuel du bureau, de la barre supérieure et de la vue d’ensemble des <gui>Activités</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hydroxyp</mal:name>
      <mal:email>hydroxyp@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Charles Monzat</mal:name>
      <mal:email>charles.monzat@free.fr</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Aperçu visuel de GNOME</title>

  <p>GNOME features a user interface designed to stay out of your way, minimize
  distractions, and help you get things done. When you first log in, you will
  see the <gui>Activities</gui> overview and the top bar.</p>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-top-bar.png" width="600" if:test="!target:mobile">
      <p>La barre supérieure de Shell de GNOME</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-top-bar-classic.png" width="500" if:test="!target:mobile">
      <p>La barre supérieure de Shell de GNOME</p>
    </media>
  </if:when>
</if:choose>

  <p>La barre supérieure vous donne accès à vos fenêtres et applications, votre agenda et rendez-vous et aux <link xref="status-icons">propriétés du système</link> comme le son, la mise en réseau et l’alimentation. Dans le menu système de la barre supérieure, vous pouvez modifier le volume ou la luminosité de l’écran, modifier votre connexion <gui>Wi-Fi</gui>, vérifier la charge de votre batterie, vous déconnecter ou changer d’utilisateur et éteindre votre ordinateur.</p>

<links type="section"/>

<!-- TODO: Replace "Activities overview" title for classic mode with something
like "Application windows" by using if:when and if:else ? -->
<section id="activities">
  <title>La vue d’ensemble des <gui>Activités</gui></title>

  <p if:test="!platform:gnome-classic">When you start GNOME, you automatically
  enter the <gui>Activities</gui> overview. The overview allows you to access
  your windows and applications. In the overview, you can also just start
  typing to search your applications, files, folders, and the web.</p>

  <p if:test="!platform:gnome-classic">To access the overview at any time,
  click the <gui>Activities</gui> button, or just move your mouse pointer to
  the top-left hot corner. You can also press the
  <key xref="keyboard-key-super">Super</key> key on your keyboard.</p>

  <p if:test="platform:gnome-classic">Pour accéder à vos fenêtres et applications, cliquez sur le bouton en bas à gauche de l’écran dans la liste des fenêtres. Vous pouvez aussi appuyer sur la touche <key xref="keyboard-key-super">Super</key> de votre clavier pour afficher une vue d’ensemble avec un aperçu dynamique de toutes les fenêtres de l’espace de travail actuel.</p>

  <media type="image" its:translate="no" src="figures/shell-activities-dash.png" height="65" style="floatend floatright" if:test="!target:mobile, !platform:gnome-classic">
    <p>Activities button and Dash</p>
  </media>
  <p if:test="!platform:gnome-classic">At the bottom of the overview, you will find the <em>dash</em>. The dash
  shows you your favorite and running applications. Click any icon in the
  dash to open that application; if the application is already running, it will
  have a small dot below its icon. Clicking its icon will bring up the most
  recently used window. You can also drag the icon onto a workspace.</p>

  <p if:test="!platform:gnome-classic">Un clic droit sur l’icône affiche un menu qui permet de choisir n’importe quelle fenêtre d’une application en cours d’exécution, ou d’ouvrir une nouvelle fenêtre. Vous pouvez aussi cliquer sur l’icône en maintenant appuyée la touche <key>Ctrl</key> pour ouvrir une nouvelle fenêtre.</p>

  <p if:test="!platform:gnome-classic">Lorsque vous arrivez dans la vue d’ensemble, vous êtes au départ dans la vue d’ensemble des fenêtres. Un aperçu dynamique de toutes les fenêtres de l’espace de travail actuel est affiché.</p>

  <p if:test="!platform:gnome-classic">Click the grid button (which has nine dots) in the dash to display the
  applications overview. This shows you all the applications installed on your
  computer. Click any application to run it, or drag an application to the
  onto a workspace shown above the installed applications. You can also drag an application onto
  the dash to make it a favorite. Your favorite applications stay in the dash
  even when they’re not running, so you can access them quickly.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-apps-open">Approfondissez vos connaissances sur le lancement d’applications.</link></p>
    </item>
    <item>
      <p><link xref="shell-windows">Approfondissez vos connaissances sur les fenêtres et les espaces de travail.</link></p>
    </item>
  </list>

</section>

<section id="appmenu">
  <title>Menu de l’application</title>
  <if:choose>
    <if:when test="!platform:gnome-classic">
      <media type="image" src="figures/shell-appmenu-shell.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>Menu de <app>Terminal</app></p>
      </media>
      <p>Le menu d’une application, situé à côté du bouton <gui>Activités</gui>, affiche le nom de l’application active avec son icône et fournit un accès rapide aux fenêtres et aux détails de l’application, ainsi qu’un élément de fermeture.</p>
    </if:when>
    <!-- TODO: check how the app menu removal affects classic mode -->
    <if:when test="platform:gnome-classic">
      <media type="image" src="figures/shell-appmenu-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>Menu de <app>Terminal</app></p>
      </media>
      <p>Le menu d’une application, situé à côté des menus <gui>Applications</gui> et <gui>Emplacements</gui>, affiche le nom de l’application en cours avec son icône et permet un accès rapide aux préférences ou à la rubrique d’aide du programme. Les éléments accessibles depuis ce menu varient en fonction de l’application.</p>
    </if:when>
  </if:choose>

</section>

<section id="clock">
  <title>Horloge, agenda et rendez-vous</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-appts.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Horloge, agenda, rendez-vous et notifications</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-appts-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Horloge, agenda et rendez-vous</p>
    </media>
  </if:when>
</if:choose>

  <p>Cliquez sur l’horloge au centre de la barre supérieure pour voir la date actuelle, un calendrier mensuel, une liste de vos rendez-vous à venir et les nouvelles notifications. Vous pouvez également ouvrir l’agenda en appuyant sur <keyseq><key>Super</key><key>M</key></keyseq>. Pour accéder aux réglages de la date, de l’heure et accéder à l’application Agenda, passez directement par le menu.</p>

  <list style="compact">
    <item>
      <p><link xref="clock-calendar">Approfondissez vos connaissances sur l’agenda et les rendez-vous.</link></p>
    </item>
    <item>
      <p><link xref="shell-notifications">Approfondissez vos connaissances sur les notifications et la liste des notifications.</link></p>
    </item>
  </list>

</section>


<section id="systemmenu">
  <title>Menu système</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-exit.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Menu utilisateur</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-exit-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Menu utilisateur</p>
    </media>
  </if:when>
</if:choose>

  <p>Cliquez sur le menu système dans le coin supérieur droit pour gérer votre profil et votre ordinateur.</p>

<!-- TODO: Update for 3.36 UI option "Do Not Disturb" in calendar dropdown:

<p>If you set yourself to Unavailable, you won’t be bothered by message popups
at the bottom of your screen. Messages will still be available in the message
tray when you move your mouse to the bottom-right corner. But only urgent
messages will be presented, such as when your battery is critically low.</p>
-->

  <p>Lorsque vous quittez votre ordinateur, vous pouvez verrouiller votre écran pour empêcher d’autres personnes de l’utiliser. Vous pouvez changer rapidement d’utilisateur sans vous déconnecter complètement pour offrir un accès à votre ordinateur à quelqu’un. Vous pouvez aussi mettre en veille ou éteindre l’ordinateur depuis le menu. Si votre écran prend en charge la rotation verticale ou horizontale, vous pouvez rapidement faire pivoter l’écran à partir du menu système. Si votre écran ne prend pas en charge la rotation, le bouton n’est pas visible.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-exit">Approfondissez vos connaissances sur le changement d’utilisateur, la déconnexion et l’extinction de votre ordinateur.</link></p>
    </item>
  </list>

</section>

<section id="lockscreen">
  <title>Verrouillage de l’écran</title>

  <p>Lorsque vous verrouillez l’écran, ou qu’il se verrouille automatiquement, l’écran de verrouillage apparaît. En plus de la protection de votre bureau pendant que vous êtes absent, il affiche également des informations concernant la date et l’heure, l’état de la batterie et celui du réseau.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-lockscreen">En savoir plus sur le verrouillage de l’écran.</link></p>
    </item>
  </list>

</section>

<section id="window-list">
  <title>Liste des fenêtres</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <p>GNOME a choisi une approche différente des autres environnements de bureau pour gérer les fenêtres. Au lieu d’en proposer une liste fixe, il vous permet de naviguer entre les fenêtres ouvertes afin de ne vous concentrer que sur votre travail.</p>
    <list style="compact">
      <item>
        <p><link xref="shell-windows-switching">Approfondissez vos connaissances sur le basculement entre les fenêtres.</link></p>
      </item>
    </list>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-window-list-classic.png" width="800" if:test="!target:mobile">
      <p>Liste des fenêtres</p>
    </media>
    <p>La liste des fenêtres en bas de l’écran donne accès à toutes les fenêtres et applications ouvertes et permet de les réduire et de les restaurer rapidement.</p>
    <p>Sur le côté droit de la liste des fenêtres, GNOME affiche les quatre espaces de travail. Pour basculer vers un autre espace de travail, sélectionnez celui que vous souhaitez utiliser.</p>
  </if:when>
</if:choose>

</section>

</page>
