<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="sound-volume" xml:lang="fr">

  <info>
    <link type="guide" xref="media#sound"/>

    <revision version="gnome:40" date="2021-02-26" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Régler le volume du son pour l’ordinateur et contrôler l’intensité sonore de chaque application.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hydroxyp</mal:name>
      <mal:email>hydroxyp@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Charles Monzat</mal:name>
      <mal:email>charles.monzat@free.fr</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

<title>Modification du volume sonore</title>

  <p>Pour modifier le volume sonore, ouvrez le <gui xref="shell-introduction#systemmenu">menu système</gui> situé à droite dans la barre supérieure et déplacez le curseur de volume vers la gauche ou la droite. Vous pouvez couper le son en déplaçant le curseur complètement à gauche.</p>

  <p>Certains claviers disposent de touches qui permettent un réglage du volume sonore. Elles représentent habituellement des haut-parleurs stylisés avec des ondes émanant d’eux. On les trouve souvent près des touches « F » en haut du clavier. Sur les claviers de portables, elles se situent sur ces touches de fonction ; maintenez conjointement l’appui sur la touche <key>Fn</key> pour les utiliser.</p>

  <p>Si vous possédez des haut-parleurs externes, vous pouvez aussi modifier le volume sonore en utilisant le bouton de réglage du volume sur les haut-parleurs. Certains écouteurs possèdent aussi un tel réglage.</p>

<section id="apps">
 <title>Modification du volume sonore d’applications individuelles</title>

  <p>Il est possible de modifier le volume d’une application particulière tout en conservant le volume des autres identique. C’est une chose utile pour, par exemple, écouter de la musique tout en navigant sur le Web. Vous pouvez alors couper le son du navigateur Web pour ne pas gêner l’audition de la musique.</p>

  <p>Certaines applications possèdent des réglages de volume dans leurs fenêtres principales. Si tel est le cas dans votre application, utilisez ces réglages pour changer le volume. Autrement :</p>

    <steps>
    <item>
      <p>Ouvrez la vue d’ensemble des <gui xref="shell-introduction#activities">Activités</gui> et commencez à saisir <gui>Son</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Son</gui> pour ouvrir le panneau.</p>
    </item>
    <item>
      <p>Under <gui>Volume Levels</gui>, change the volume of the applications
      listed there. The button at the end of the volume slider toggles
      <gui>Mute</gui> on and off.</p>

  <note style="tip">
    <p>Seules les applications en train d’émettre des sons figurent dans cette liste. Si une application en train d’émettre ne figure pas dans la liste, c’est qu’elle ne prend pas en charge un réglage de cette façon. Dans ce cas, vous ne pouvez pas modifier son volume sonore.</p>
  </note>
    </item>

  </steps>

</section>

</page>
