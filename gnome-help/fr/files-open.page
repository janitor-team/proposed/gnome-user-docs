<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-open" xml:lang="fr">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ouvrir des fichiers avec une autre application que celle définie par défaut pour ce type de fichier et comment modifier la valeur par défaut.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hydroxyp</mal:name>
      <mal:email>hydroxyp@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Charles Monzat</mal:name>
      <mal:email>charles.monzat@free.fr</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

<title>Ouverture de fichiers avec une autre application</title>

  <p>Quand vous faites un double-clic (ou clic milieu) sur un fichier dans le gestionnaire de fichiers, il s’ouvre avec l’application par défaut pour ce type de fichier. Vous pouvez l’ouvrir avec une autre application, chercher une application en ligne ou modifier l’application par défaut pour ce type de fichier.</p>

  <p>Pour ouvrir un fichier avec une application différente de celle par défaut, faites un clic droit sur le fichier et choisissez celle que vous voulez depuis le haut du menu. Si vous ne trouvez pas l’application voulue, cliquez sur <gui>Ouvrir avec une autre application</gui>. Par défaut, le gestionnaire de fichiers ne propose que les applications dont il sait qu’elles peuvent prendre en charge le fichier. Pour rechercher parmi toutes les applications installées sur votre ordinateur, cliquez sur <gui>Afficher toutes les applications</gui>.</p>

<p>Si néanmoins vous ne trouvez toujours pas l’application voulue, cliquez sur <gui>Chercher de nouvelles applications</gui>. Le gestionnaire de fichiers va chercher en ligne des paquets contenant des applications connues pour prendre en charge les fichiers de ce type.</p>

<section id="default">
  <title>Changement d’application par défaut</title>
  <p>Vous pouvez modifier l’application par défaut qui est utilisée pour ouvrir un certain type de fichier. Cela vous permet d’ouvrir votre application préférée quand vous faites un double-clic sur un fichier. Par exemple, vous pouvez lancer votre lecteur de musique préféré quand vous faites un double-clic sur un fichier MP3.</p>

  <steps>
    <item><p>Sélectionnez un type de fichier dont vous souhaitez changer l’application par défaut. Par exemple, pour définir l’application à lancer lors de l’ouverture d’un fichier MP3, sélectionnez un fichier <file>.mp3</file>.</p></item>
    <item><p>Faites un clic droit sur le fichier et choisissez <gui>Propriétés</gui>.</p></item>
    <item><p>Cliquez sur l’onglet <gui>Ouvrir avec</gui>.</p></item>
    <item><p>Choisissez l’application que vous voulez et cliquez sur <gui>Définir par défaut</gui>.</p>
    <p>Si <gui>Autres applications</gui> contient une application que vous voulez utiliser parfois, mais sans qu’elle soit par défaut, choisissez l’application et cliquez sur <gui>Ajouter</gui>. Cela l’ajoute aux <gui>Applications recommandées</gui>. Il vous sera désormais possible d’utiliser l’application par un clic droit sur le fichier et en la choisissant dans la liste.</p></item>
  </steps>

  <p>Cela change l’application par défaut pas uniquement pour le fichier sélectionné, mais pour tous les fichiers du même type.</p>

<!-- TODO: mention resetting the open with list with the "Reset" button -->

</section>

</page>
