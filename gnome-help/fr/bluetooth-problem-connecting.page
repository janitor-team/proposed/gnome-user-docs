<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="bluetooth-problem-connecting" xml:lang="fr">

  <info>
    <link type="guide" xref="bluetooth#problems"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>L’adaptateur peut être débranché ou ne pas avoir de pilote (driver), ou bien le Bluetooth peut être désactivé ou bloqué.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hydroxyp</mal:name>
      <mal:email>hydroxyp@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Charles Monzat</mal:name>
      <mal:email>charles.monzat@free.fr</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Impossible de connecter mon périphérique Bluetooth</title>

  <p>Les raisons qui empêchent de connecter un périphérique Bluetooth, comme un téléphone ou un micro-casque, sont nombreuses.</p>

  <terms>
    <item>
      <title>Connexion bloquée ou manquant de fiabilité</title>
      <p>Certains périphériques Bluetooth bloquent les connexions par défaut, ou nécessitent un changement de paramètres pour autoriser les connexions. Assurez-vous que votre périphérique est configuré pour autoriser les connexions.</p>
    </item>
    <item>
      <title>Le matériel Bluetooth n’est pas reconnu</title>
      <p>Si votre adaptateur ou dongle Bluetooth n’est pas reconnu par l’ordinateur, cela peut être dû au fait que les <link xref="hardware-driver">pilotes</link> pour cet adaptateur ne sont pas installés. Certains adaptateurs Bluetooth ne sont pas pris en charge par Linux et vous ne pouvez pas trouver de pilote qui convienne. Dans ce cas, il faudra vous procurer un autre adaptateur Bluetooth.</p>
    </item>
    <item>
      <title>L’adaptateur est éteint</title>
        <p>Assurez-vous que votre adaptateur est bien allumé. Ouvrez le panneau Bluetooth et vérifiez qu’il n’est pas <link xref="bluetooth-turn-on-off">Désactivé</link>.</p>
    </item>
    <item>
      <title>La connexion du périphérique Bluetooth est coupée</title>
      <p>Vérifiez que le Bluetooth est activé sur le périphérique que vous essayez de connecter et qu’il est bien <link xref="bluetooth-visibility">détectable ou visible</link>. Par exemple, si vous êtes en train de connecter un téléphone, assurez-vous qu’il n’est pas en mode avion.</p>
    </item>
    <item>
      <title>Il n’y a pas d’adaptateur Bluetooth sur votre ordinateur</title>
      <p>Beaucoup d’ordinateurs n’ont pas d’adaptateur Bluetooth. Achetez un adaptateur si vous voulez utiliser le Bluetooth.</p>
    </item>
  </terms>

</page>
