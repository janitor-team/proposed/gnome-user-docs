<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-repair" xml:lang="fr">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>Le projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <revision pkgversion="3.25.90" date="2017-08-17" status="review"/>

    <desc>Vérifier si un système de fichiers est endommagé et le remettre en état afin de pouvoir l’utiliser.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hydroxyp</mal:name>
      <mal:email>hydroxyp@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Charles Monzat</mal:name>
      <mal:email>charles.monzat@free.fr</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

<title>Réparation d’un système de fichiers endommagé</title>

  <p>Les systèmes de fichiers peuvent être corrompus en raison d’une panne de courant, de plantages système et d’un démontage non sécurisé du disque. Après un tel incident, il est recommandé de <em>réparer</em> ou au moins de <em>vérifier</em> le système de fichiers pour éviter toute perte de données ultérieure.</p>
  <p>Parfois, une réparation est nécessaire pour monter ou modifier un système de fichiers. Même si une <em>vérification</em> ne signale aucun dommage, le système de fichiers peut toujours être marqué comme « affecté » en interne et nécessiter une réparation.</p>

<steps>
  <title>Vérification d’un système de fichiers</title>
  <item>
    <p>Ouvrez <app>Disques</app> à partir de la vue d’ensemble des <gui>Activités</gui>.</p>
  </item>
  <item>
    <p>Choisissez le disque contenant le système de fichiers concerné dans la liste des périphériques de stockage située sur la gauche. S’il y a plus d’un volume sur le disque, sélectionnez le volume qui contient le système de fichiers.</p>
  </item>
  <item>
    <p>Dans la barre d’outils en dessous de la section <gui>Volumes</gui>, cliquez sur le bouton de menu, puis sur <gui>Vérifier le système de fichiers</gui>.</p>
  </item>
  <item>
    <p>La durée de la vérification dépend du nombre de données stockées dans le système de fichiers. Dans la boîte de dialogue qui apparaît, validez pour démarrer l’opération.</p>
   <p>L’action ne modifie pas le système de fichiers mais le démonte si nécessaire. Veuillez patienter pendant la vérification du système de fichiers.</p>
  </item>
  <item>
    <p>Une fois terminé, vous serez informé si le système de fichiers est endommagé. Notez que dans certains cas, même si le système de fichiers n’est pas endommagé, il peut encore être nécessaire de le réparer pour réinitialiser un marqueur interne « affecté ».</p>
  </item>
</steps>

<note style="warning">
 <title>Perte de données possible lors de la réparation</title>
  <p>Si la structure du système de fichiers est endommagée, cela peut affecter les fichiers qui y sont stockés. Dans certains cas, ces fichiers ne peuvent pas être remis en état et sont supprimés ou déplacés vers un répertoire spécial. Il s’agit en temps normal du dossier <em>lost+found</em> dans le répertoire de premier niveau du système de fichiers où les fichiers remis en état sont stockés.</p>
  <p>Si les données sont trop importantes pour être perdues au cours de ce processus, il est conseillé de les sauvegarder en enregistrant une image du volume avant de réparer.</p>
  <p>Cette image peut ensuite être traitée avec des outils d’analyse de laboratoire comme <app>sleuthkit</app>, afin de récupérer davantage de fichiers et de parties de données qui n’ont pas été restaurés pendant la réparation, ainsi que les fichiers précédemment supprimés.</p>
</note>

<steps>
  <title>Réparation d’un système de fichiers</title>
  <item>
    <p>Ouvrez <app>Disques</app> à partir de la vue d’ensemble des <gui>Activités</gui>.</p>
  </item>
  <item>
    <p>Choisissez le disque contenant le système de fichiers concerné dans la liste des périphériques de stockage située sur la gauche. S’il y a plus d’un volume sur le disque, sélectionnez le volume qui contient le système de fichiers.</p>
  </item>
  <item>
    <p>Dans la barre d’outils en dessous de la section <gui>Volumes</gui>, cliquez sur le bouton de menu, puis sur <gui>Réparer le système de fichiers</gui>.</p>
  </item>
  <item>
    <p>La durée de la réparation dépend de la quantité de données stockées dans le système de fichiers. Dans la boîte de dialogue qui apparaît, validez pour démarrer l’opération.</p>
   <p>L’opération peut démonter le système de fichiers si nécessaire. L’opération de réparation tente de ramener le système de fichiers à un état cohérent et déplace les fichiers qui ont été endommagés dans un dossier spécial. Veuillez patienter pendant la réparation du système de fichiers.</p>
  </item>
  <item>
    <p>Une fois terminé, vous serez informé si le système de fichiers a pu être réparé avec succès. En cas de réussite, il peut être réutilisé normalement.</p>
    <p>Si le système de fichiers n’a pas pu être réparé, sauvegardez-le en enregistrant une image du volume pour pouvoir récupérer les fichiers importants ultérieurement. Cela peut être fait en montant l’image en lecture seule ou en utilisant des outils d’analyse de laboratoire comme <app>sleuthkit</app>.</p>
    <p>Pour utiliser le volume à nouveau, il doit être <link xref="disk-format">formaté</link> avec un nouveau système de fichiers. Toutes les données seront supprimées.</p>
  </item>
</steps>

</page>
