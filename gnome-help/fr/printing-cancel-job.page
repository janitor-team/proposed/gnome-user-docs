<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-cancel-job" xml:lang="fr">

  <info>
    <link type="guide" xref="printing#problems"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Annuler une impression en attente et la retirer de la file.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hydroxyp</mal:name>
      <mal:email>hydroxyp@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Charles Monzat</mal:name>
      <mal:email>charles.monzat@free.fr</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Annulation, pause ou déblocage d’une impression</title>

  <p>Vous pouvez annuler une impression en attente et la retirer de la file dans les paramètres de l’imprimante.</p>

  <section id="cancel-print-job">
    <title>Annulation d’une impression</title>

  <p>Si vous lancez involontairement une impression, vous pouvez l’annuler rapidement pour ne pas gaspiller du papier et de l’encre.</p>

  <steps>
    <title>Pour annuler une impression :</title>
    <item>
      <p>Ouvrez la vue d’ensemble des <gui xref="shell-introduction#activities">Activités</gui> et commencez à saisir <gui>Imprimantes</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Imprimantes</gui> pour ouvrir le panneau.</p>
    </item>
    <item>
      <p>Cliquez sur le bouton <gui>Afficher les tâches</gui> à droite de la boîte de dialogue <gui>Imprimantes</gui>.</p>
    </item>
    <item>
      <p>Annulez la tâche d’impression en cliquant sur le bouton stop.</p>
    </item>
  </steps>

  <p>Si cette action n’arrête pas l’impression, maintenez enfoncé le bouton <em>annuler</em> de votre imprimante.</p>

  <p>En dernier ressort, et surtout si vous avez lancé l’impression d’un gros document avec beaucoup de pages et qui ne veut pas s’annuler, retirez le papier vierge du bac de l’imprimante. Celle-ci va s’arrêter faute de papier et vous pouvez ensuite essayer d’annuler à nouveau l’impression, ou d’éteindre et rallumer l’imprimante.</p>

  <note style="warning">
    <p>Attention cependant à ne pas endommager l’imprimante en retirant le papier : s’il faut tirer fort pour sortir le papier, alors il serait plus prudent de le laisser là où il est.</p>
  </note>

  </section>

  <section id="pause-release-print-job">
    <title>Mise en pause et relance d’une impression</title>

  <p>Si vous désirez mettre en pause ou reprendre une impression, vous pouvez le faire en ouvrant la boîte des tâches et en cliquant sur le bouton adéquat.</p>

  <steps>
    <item>
      <p>Ouvrez la vue d’ensemble des <gui xref="shell-introduction#activities">Activités</gui> et commencez à saisir <gui>Imprimantes</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Imprimantes</gui> pour ouvrir le panneau.</p>
    </item>
    <item>
      <p>Cliquez sur le bouton <gui>Afficher les tâches</gui> dans la partie gauche de la boîte de dialogue <gui>Imprimantes</gui> et suspendez ou relancez la tâche selon vos besoins.</p>
    </item>
  </steps>

  </section>

</page>
