<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="user-goodpassword" xml:lang="sl">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Dokumentacijski projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Uporabite daljša, bolj zapletena gesla.</desc>
  </info>

  <title>Izbor varnega gesla</title>

  <note style="important">
    <p>Naredite gesla dovolj enostavna, da si jih lahko zapomnite, vendar zelo težka za ugibanje (tudi za računalniške programe).</p>
  </note>

  <p>Izbira dobrega gesla vam bo pomagala obdržati vaš računalnik varen. V primeru da je vaše geslo enostavno uganiti, ga lahko nekdo ugotovi in pridobi dostop do vaših osebnih podatkov.</p>

  <p>Ljudje lahko za sistematično ugibanje gesel uporabijo računalniške programe. Zato je lahko geslo, ki bi ga človek težko uganil, za računalnik izjemno enostavno. Tukaj je nekaj namigov za izbiro dobrega gesla:</p>
  
  <list>
    <item>
      <p>V geslu uporabite mešanico velikih in malih črk, števil, simbolov in presledkov. To naredi ugibanje težavnejše. Na voljo za izbiranje je več simbolov, zato mora napadalec pri ugibanju vašega gesla preveriti več možnih gesel.</p>
      <note>
        <p>A good method for choosing a password is to take the first letter of
        each word in a phrase that you can remember. The phrase could be the
        name of a movie, a book, a song or an album. For example, “Flatland: A
        Romance of Many Dimensions” would become F:ARoMD or faromd or f:
        aromd.</p>
      </note>
    </item>
    <item>
      <p>Naredite svoja gesla kolikor je mogoče dolga. Več črk kot vsebujejo, dlje časa bo oseba ali računalnik porabila za njihovo ugibanje.</p>
    </item>
    <item>
      <p>Do not use any words that appear in a standard dictionary in any
      language. Password crackers will try these first. The most common
      password is “password” — people can guess passwords like this very
      quickly!</p>
    </item>
    <item>
      <p>Do not use any personal information such as a date, license plate
      number, or any family member’s name.</p>
    </item>
    <item>
      <p>Ne uporabljajte samostalnikov.</p>
    </item>
    <item>
      <p>Izberite geslo, ki ga je mogoče hitro vtipkati in s tem zmanjšajte možnost, da lahko nekdo, ki stoji za vami in vas gleda, ugotovi kaj ste vtipkali.</p>
      <note style="tip">
        <p>Nikoli si ne zapišite gesel. Enostavno jih je mogoče najti!</p>
      </note>
    </item>
    <item>
      <p>Uporabite različna gesla za različne stvari.</p>
    </item>
    <item>
      <p>Uporabite različna gesla za različne račune.</p>
      <p>V primeru da za vse račune uporabljate isto geslo, bo tisti, ki ga ugane, lahko nemudoma dostopal do vseh vaših računov.</p>
      <p>It can be difficult to remember lots of passwords, however. Though not
      as secure as using a different passwords for everything, it may be easier
      to use the same one for things that do not matter (like websites), and
      different ones for important things (like your online banking account and
      your email).</p>
   </item>
   <item>
     <p>Redno spreminjajte svoje geslo.</p>
   </item>
  </list>

</page>
