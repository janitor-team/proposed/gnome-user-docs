<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting-initial-check" xml:lang="sl">

  <info>
    <link type="next" xref="net-wireless-troubleshooting-hardware-info"/>
    <link type="guide" xref="net-wireless-troubleshooting"/>

    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <credit type="author">
      <name>Sodelavci wiki dokumentacije Ubuntu</name>
    </credit>
    <credit type="author">
      <name>Dokumentacijski projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Prepričajte se, da so nastavitve omrežja pravilne in se pripravite za naslednjih nekaj korakov odpravljanja težav.</desc>
  </info>

  <title>Odpravljanje težav z brezžičnimi omrežji</title>
  <subtitle>Preverite svojo internetno povezavo.</subtitle>

  <p>In this step you will check some basic information about your wireless
  network connection. This is to make sure that your networking problem isn’t
  caused by a relatively simple issue, like the wireless connection being
  turned off, and to prepare for the next few troubleshooting steps.</p>

  <steps>
    <item>
      <p>Prepričajte se, da vaš prenosnik ni povezan z <em>žično</em> internetno povezavo.</p>
    </item>
    <item>
      <p>V primeru da imate zunanjo brezžično kartico (kot je brezžična kartica USB ali kartica PCMICA), se prepričajte, da je vstavljena v ustrezen prostor na računalniku.</p>
    </item>
    <item>
      <p>V primeru da je brezžična kartica <em>znotraj</em> računalnika, se prepričajte, da je stikalo brezžičnega prilagodilnika vklopljeno.</p>
    </item>
    <item>
      <p>Open the
      <gui xref="shell-introduction#systemmenu">system menu</gui> from the right
      side of the top bar and select the Wi-Fi network, then select <gui>Wi-Fi
      Settings</gui>. Make sure that the <gui>Wi-Fi</gui> switch is set to on.
      You should also check that <link xref="net-wireless-airplane">Airplane
      Mode</link> is <em>not</em> switched on.</p>
    </item>
    <item>
      <p>Open the Terminal, type <cmd>nmcli device</cmd> and press
      <key>Enter</key>.</p>
      <p>This will display information about your network interfaces and
      connection status. Look down the list of information and see if there is
      an item related to the wireless network adapter. If the state is
      <code>connected</code>, it means that the adapter is working and connected
      to your wireless router.</p>
    </item>
  </steps>

  <p>V primeru da ste povezani s svojim brezžičnim usmerjevalnikom in še vedno ne morete dostopati do interneta, morda vaš usmerjevalnik ni nastavljen pravilno ali pa ima vaš ponudnik internetnih storitev tehnične težave. Preglejte svoj usmerjevalnik in vodiče za nastavitev svojega ponudnika internetnih storitev ali stopite v stik s podporo svojega ponudnika internetnih storitev.</p>

  <p>If the information from <cmd>nmcli device</cmd> did not indicate that you were
  connected to the network, click <gui>Next</gui> to proceed to the next
  portion of the troubleshooting guide.</p>

</page>
