<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="keyboard-nav" xml:lang="sl">
  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision pkgversion="3.7.5" version="0.2" date="2013-02-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.20" date="2016-08-13" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
       <name>Julita Inca</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
       <name>Ekaterina Gerasimova</name>
       <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Uporaba programov in namizja brez miške.</desc>
  </info>

  <title>Krmarjenje s tipkami</title>

  <p>Ta stran opisuje krmarjenje s tipkovnico za ljudi, ki ne morejo uporabiti miške ali druge kazalne naprave ali za tiste, ki želijo tipkovnico uporabljati kolikor je le mogoče. Za tipkovne bližnjice, ki so uporabne za vse uporabnike, si oglejte <link xref="shell-keyboard-shortcuts"/>.</p>

  <note style="tip">
    <p>V primeru da imate težave z uporabo miške ali druge kazalne naprave, lahko miškin kazalec nadzirate s številsko  tipkovnico. Za podrobnosti si oglejte <link xref="mouse-mousekeys"/>.</p>
  </note>

<table frame="top bottom" rules="rows">
  <title>Krmarjenje po uporabniškem vmesniku</title>
  <tr>
    <td><p><key>Tab</key> and</p>
    <p><keyseq><key>Ctrl</key><key>Tab</key></keyseq></p>
    </td>
    <td>
      <p>Premika žarišče tipkovnice med različnimi nadzorniki. <keyseq><key>Ctrl</key> <key>Tab</key></keyseq> se premika med skupinami nadzornikov kot iz stranske vrstice v glavno vsebino. S <keyseq><key>Ctrl</key><key>Tab</key></keyseq> se lahko premaknete tudi izven področij, ki uporabljajo <key>Tab</key> kot je področje besedila.</p>
      <p>Držite tipko<key>Shift</key> za premik žarišča v obratnem vrstnem redu.</p>
    </td>
  </tr>
  <tr>
    <td><p>Smerne tipke</p></td>
    <td>
      <p>Premika izbiro med predmeti v enem nadzorniku ali med zbirko pvoezanih nadzornikov. Uporabite smerne tipke za osredotočenje na gumbe v orodni vrstici, izbiro predmetov v seznamskem ali ikonskem pogledu ali izbiro izbrinega gumba iz skupine.</p>
    </td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key>Smerne tipke</keyseq></p></td>
    <td><p>V seznamskem ali ikonskem pogledu premaknite žarišče tipkovnice na drug predmet ne da bi spremenili kateri predmet je izbran.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key>Smerne tipke</keyseq></p></td>
    <td><p>V seznamskem ali ikonskem pogledu izberite vse predmete iz trenutno izbranega predmeta na predmet, ki je na novo v žarišču.</p>
    <p>In a tree view, items that have children can be expanded or collapsed,
    to show or hide their children: expand by pressing
    <keyseq><key>Shift</key><key>→</key></keyseq>, and collapse by
    pressing <keyseq><key>Shift</key><key>←</key></keyseq>.</p></td>
  </tr>
  <tr>
    <td><p><key>Preslednica</key></p></td>
    <td><p>Omogoči predmet v žarišču kot je gumb, izbiro polje ali predmet seznama.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>Preslednica</key></keyseq></p></td>
    <td><p>V seznamskem ali ikonskem pogledu lahko izberite ali odstranite izbiro predmeta v žarišču ne da bi odstranili izbiro drugih predmetov.</p></td>
  </tr>
  <tr>
    <td><p><key>Alt</key></p></td>
    <td><p>Držite tipko<key>Alt</key> za prikaz<em>pospeševalnikov</em> : podčrtanih črk na predmetih menija, gumbih in drugih nadzornikih. Pritisnite <key>Alt</key> in podčrtano črko za omogočitev nadzornika kot če bi nanj kliknili.</p></td>
  </tr>
  <tr>
    <td><p><key>Esc</key></p></td>
    <td><p>Konča meni, pojavno okno, preklopnik ali pogovorno okno.</p></td>
  </tr>
  <tr>
    <td><p><key>F10</key></p></td>
    <td><p>Odpre prvi meni v menijski vrstici okna. Uporabite smerne tipke za krmarjenje po menijih.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key xref="keyboard-key-super">Super</key>
    <key>F10</key></keyseq></p></td>
    <td><p>Odprite meni programi v vrhnji vrstici.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key><key>F10</key></keyseq> or</p>
    <p><key xref="keyboard-key-menu">Menu</key></p></td>
    <td>
      <p>Prikaže vsebinski meni za trenutno izbiro kot če bi desno kliknili.</p>
    </td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>F10</key></keyseq></p></td>
    <td><p>V upravljalniku datotek odpre vsebinski meni za trenutno mapo kot če bi desno kliknili na ozadje in ne na enega od predmetov.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>PageUp</key></keyseq></p>
    <p>and</p>
    <p><keyseq><key>Ctrl</key><key>PageDown</key></keyseq></p></td>
    <td><p>V vmesniku z zavihki preklopi na zavihek na levi ali desni.</p></td>
  </tr>
</table>

<table frame="top bottom" rules="rows">
  <title>Krmarjenje po namizju</title>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="alt-f1"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-tab"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-tick"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="ctrl-alt-tab"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-updown"/>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F6</key></keyseq></p></td>
    <td><p>Kroži med okni istega programa. Držite tipko <key>Alt</key> in pritisnite <key>F6</key> dokler ni želeno okno poudarjeno. Nato izpustite <key>Alt</key>. To je podobno zmožnosti <keyseq><key>Alt</key><key>`</key></keyseq>.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
    <td><p>Kroži med vsemi odprtimi okni na delovni površini.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
    <td><p><link xref="shell-notifications#notificationlist">Open the
    notification list.</link> Press <key>Esc</key> to close.</p></td>
  </tr>
</table>

<table frame="top bottom" rules="rows">
  <title>Krmarjenje po oknih</title>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
    <td><p>Zapre trenutno okno.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F5</key></keyseq> or
    <keyseq><key>Super</key><key>↓</key></keyseq></p></td>
    <td><p>Restore a maximized window to its original size. Use
    <keyseq><key>Alt</key><key>F10</key></keyseq> to maximize.
    <keyseq><key>Alt</key><key>F10</key></keyseq> both maximizes and
    restores.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
    <td><p>Premakne trenutno okno. Pritisnite <keyseq><key>Alt</key><key>F7</key></keyseq> in nato uporabite smerne tipke za premik okna. Pritisnite <key>Enter</key> za končanje premikanja okna ali <key>Esc</key> za njegovo vrnitev na njegovo izvirno mesto.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
    <td><p>Spremeni velikost trenutnega okna. Pritisnite<keyseq><key>Alt</key><key>F8</key></keyseq> in nato uporabite smerne tipke za spremembo velikosti okna. Pritisnite <key>Enter</key> za končanje spreminjanja velikosti okna ali <key>Esc</key> za njegovo vrnitev na izvirno velikost.</p></td>
  </tr>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-updown"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-left"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-right"/>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F10</key></keyseq> or
    <keyseq><key>Super</key><key>↑</key></keyseq></p>
    </td>
    <td><p><link xref="shell-windows-maximize">Maximize</link> a window. Press
    <keyseq><key>Alt</key><key>F10</key></keyseq> or
    <keyseq><key>Super</key><key>↓</key></keyseq> to
    restore a maximized window to its original size.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>H</key></keyseq></p></td>
    <td><p>Skrči okno.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>←</key></keyseq></p></td>
    <td><p>Maximize a window vertically along the left side of the screen.
    Press again to restore the window to its previous size. Press
    <keyseq><key>Super</key><key>→</key></keyseq> to switch
    sides.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>→</key></keyseq></p></td>
    <td><p>Maximize a window vertically along the right side of the screen.
    Press again to restore the window to its previous size. Press
    <keyseq><key>Super</key><key>←</key></keyseq> to
    switch sides.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Preslednica</key></keyseq></p></td>
    <td><p>Prikaže meni okna kot če bi desno kliknili v nazivni vrstici.</p></td>
  </tr>
</table>

</page>
