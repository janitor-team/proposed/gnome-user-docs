<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="backup-check" xml:lang="sl">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Dokumentacijski projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>davidk@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Preverite, da je bila vaša varnostna kopija uspešna.</desc>
  </info>

  <title>Preverjanje varnostnih kopij</title>

  <p>After you have backed up your files, you should make sure that the
 backup was successful. If it didn’t work properly, you could lose important
 data since some files could be missing from the backup.</p>

   <p>When you use <app>Files</app> to copy or move files, the computer checks
   to make sure that all of the data transferred correctly. However, if you are
   transferring data that is very important to you, you may want to perform
   additional checks to confirm that your data has been transferred
   properly.</p>

  <p>Dodatno preverjanje lahko izvedete z ogledom kopiranih datotek in map na ciljnem mediju. S preverjanjem, da so prenesene datoteke in mape dejansko v varnostni kopiji, imate lahko dodatno zaupanje, da je opravilo uspelo.</p>

  <note style="tip"><p>V primeru da delate redne varnostne kopije velikih količin podatkov, boste morda ugotovili, da je lažje uporabiti namenski program za varnostne kopije kot je <app>Déjà Dup</app>. Takšen program je bolj zmogljiv in zanesljivejši kot kopiranje in lepljenje datotek.</p></note>

</page>
