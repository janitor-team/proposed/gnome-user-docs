<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-where" xml:lang="sl">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Dokumentacijski projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Nasvet kje shraniti varnostne kopije in katero vrsto naprave shrambe uporabiti.</desc>
  </info>

<title>Kam shraniti varnostne kopije</title>

  <p>You should store backup copies of your files somewhere separate from your
 computer — on an external hard disk, for example. That way, if the computer
 breaks, or is lost or is stolen, the backup will still be intact. For maximum
 security, you shouldn’t keep the backup in the same building as your computer.
 If there is a fire or theft, both copies of the data could be lost if they are
 kept together.</p>

  <p>Pomembno je tudi, da izberete primeren <em>medij varnostne kopije</em>. Varnostne kopije morate shraniti na napravo, ki ima dovolj prostora na disku za varnostno kopijo vseh datotek.</p>

   <list style="compact">
    <title>Možnosti krajevne in oddaljene shrambe</title>
    <item>
      <p>Pomnilniški ključ USB (nizka velikost)</p>
    </item>
    <item>
      <p>Notranji diskovni pogon (visoka velikost)</p>
    </item>
    <item>
      <p>Zunanji trdi disk (običajno visoka velikost)</p>
    </item>
    <item>
      <p>Pogon priključen na omrežje (visoka velikost)</p>
    </item>
    <item>
      <p>Datotečni strežnik/Strežnik za varnostne kopije (visoka velikost)</p>
    </item>
    <item>
     <p>Zapisljivi CD-ji ali DVD-ji (nizka/srednja velikost)</p>
    </item>
    <item>
     <p>Online backup service
     (<link href="http://aws.amazon.com/s3/">Amazon S3</link>, for example;
     capacity depends on price)</p>
    </item>
   </list>

  <p>Nekatere izmed teh možnosti imajo dovoljšnjo velikost, da lahko shranite vsako datoteko vašega sistema. To je poznano tudi kot <em>popolna varnostna kopija sistema</em></p>
</page>
