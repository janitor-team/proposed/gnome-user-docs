<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-delete" xml:lang="sl">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-recover"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.16" date="2015-02-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Odstranite datoteke ali mape, ki jih ne potrebujete več.</desc>
  </info>

<title>Izbris datotek in map</title>

  <p>If you do not want a file or folder any more, you can delete it. When you
  delete an item it is moved to the <gui>Trash</gui>, where it is stored
  until you empty the trash. You can <link xref="files-recover">restore
  items</link> in the <gui>Trash</gui> to their original location if you
  decide you need them, or if they were accidentally deleted.</p>

  <steps>
    <title>Za pošiljanje datoteke v smeti:</title>
    <item><p>Izberite predmet, ki ga želite izbrisati, tako da nanj enkrat kliknete.</p></item>
    <item><p>Press <key>Delete</key> on your keyboard. Alternatively, drag the
    item to the <gui>Trash</gui> in the sidebar.</p></item>
  </steps>

  <p>The file will be moved to the trash, and you’ll be presented with an
  option to <gui>Undo</gui> the deletion. The <gui>Undo</gui> button will appear
  for a few seconds. If you select <gui>Undo</gui>, the file will be restored
  to its original location.</p>

  <p>Za trajen izbris datotek in sprostitev prostora na računalniku morate izprazniti smeti.  Za izpraznitev smeti desno kliknite na <gui>Smeti</gui> v stranski vrstici in izberite <gui>Izprazni smeti</gui>.</p>

  <section id="permanent">
    <title>Trajno izbriši datoteko</title>
    <p>Datoteko lahko takoj trajno izbrišete brez pošiljanja v smeti.</p>

  <steps>
    <title>Za trajen izbris datoteke:</title>
    <item><p>Izberite predmet, ki ga želite izbrisati.</p></item>
    <item><p>Pritisnite in držite tipko <key>Shift</key> in nato pritisnite tipko <key>Delete</key>.</p></item>
    <item><p>Ker tega ne morete razveljaviti boste morali potrditi, da želite datoteko ali mapo izbrisati.</p></item>
  </steps>

  <note><p>Izbrisane datoteke na <link xref="files#removable">odstranljivih napravah</link> morda na drugih operacijskih sistemih kot sta Windows ali Mac OS ne bodo vidne. Datoteke so še vedno tam in bodo na voljo, ko napravo priklopite nazaj v svoj računalnik.</p></note>

  </section>

</page>
