<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:itst="http://itstool.org/extensions/" type="topic" style="task" id="nautilus-file-properties-permissions" xml:lang="sl">

  <info>
    <its:rules xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="files#faq"/>
    <link type="seealso" xref="nautilus-file-properties-basic"/>

    <desc>Nadzira kdo si lahko ogleda in uredi vaše datoteke in mape.</desc>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>
  <title>Nastavitev dovoljenja datotek</title>

  <p>Za nadzor kdo lahko vidi in ureja datoteke katerih lastnik ste lahko uporabite dovoljenja datotek. Za ogled in nastavitev dovoljenj datoteke, desno kliknite nanjo in izberite <gui>Lastnosti</gui> in nato izberite zavihek <gui>Dovoljenja</gui>.</p>

  <p>Oglejte si <link xref="#files"/> and <link xref="#folders"/> spodaj za podrobnosti o vrstah dovoljenj, ki jih lahko nastavite.</p>

  <section id="files">
    <title>Datoteke</title>

    <p>You can set the permissions for the file owner, the group owner,
    and all other users of the system. For your files, you are the owner,
    and you can give yourself read-only or read-and-write permission.
    Set a file to read-only if you don’t want to accidentally change it.</p>

    <p>Every user on your computer belongs to a group. On home computers,
    it is common for each user to have their own group, and group permissions
    are not often used. In corporate environments, groups are sometimes used
    for departments or projects. As well as having an owner, each file belongs
    to a group. You can set the file’s group and control the permissions for
    all users in that group. You can only set the file’s group to a group you
    belong to.</p>

    <p>You can also set the permissions for users other than the owner and
    those in the file’s group.</p>

    <p>If the file is a program, such as a script, you must select <gui>Allow
    executing file as program</gui> to run it. Even with this option selected,
    the file manager will still open the file in an application. See
    <link xref="nautilus-behavior#executable"/> for more information.</p>
  </section>

  <section id="folders">
    <title>Mape</title>
    <p>Dovoljenja map lahko nastavite za lastnika, skupino in druge uporabnike. Oglejte si podrobnosti dovoljenj datotek zgoraj za razlago o skrbnikih, skupinah in drugih uporabnikih.</p>
    <p>Dovoljenja, ki jih nastavite za mapo, so drugačna od dovoljenj za datoteko.</p>
    <terms>
      <item>
        <title><gui itst:context="permission">None</gui></title>
        <p>Uporabnik ne bo mogel niti videti katere datoteke so v mapi.</p>
      </item>
      <item>
        <title><gui>Le izpiši datoteke</gui></title>
        <p>Uporabnik bo lahko videl katere datoteke so v mapi, vendar datotek ne bo mogel odpreti, ustvariti ali izbrisati.</p>
      </item>
      <item>
        <title><gui>Dostop do datotek</gui></title>
        <p>Uporabnik bo lahko odprl datoteke v mapi (pod pogojem, da ima za to dovoljenje do določene datoteke), vendar ne bodo mogli brisati datotek ali ustvariti novih datotek.</p>
      </item>
      <item>
        <title><gui>Ustvarjanje in brisanje datotek</gui></title>
        <p>Uporabnik bo imel poln dostop do mape ključno z odpiranjem, ustvarjanjem in brisanjem datotek.</p>
      </item>
    </terms>

    <p>You can also quickly set the file permissions for all the files
    in the folder by clicking <gui>Change Permissions for Enclosed Files</gui>.
    Use the drop-down lists to adjust the permissions of contained files or
    folders, and click <gui>Change</gui>. Permissions are applied to files and
    folders in subfolders as well, to any depth.</p>
  </section>

</page>
