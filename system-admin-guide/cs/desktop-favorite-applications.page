<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="desktop-favorite-applications" xml:lang="cs">

  <info>
    <link type="guide" xref="appearance"/>
    <revision pkgversion="3.30" date="2019-02-22" status="review"/>

    <credit type="author">
      <name>Aruna Sankaranarayanan</name>
      <email>aruna.evam@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak si přizpůsobit výchozí aplikace v přehledu Činností.</desc>

  </info>

  <title>Nastavení výchozích oblíbených aplikací</title>

  <p>Oblíbené aplikace jsou ty, které vidíte v <link href="help:gnome-help/shell-introduction#activities">postranním panelu</link> prostředí GNOME Shell. Můžete použít <sys its:translate="no">dconf</sys> k nastavení oblíbených aplikací pro jednoho uživatele nebo sady stejných oblíbených aplikací pro všechny uživatele. V obou případech musíte nejdříve upravit profil <sys its:translate="no">dconf</sys> v <file its:translate="no">/etc/dconf/profile</file>.</p>

<section id="per-user">
  <title>Nastavení různých oblíbených aplikací pro různé uživatele</title>

  <p>Můžete nastavit výchozí oblíbené aplikace jednotlivým uživatelům tak, že upravíte jejich soubory s uživatelskými databázemi v <file its:translate="no">~/.config/dconf/user</file>. Úryvek v následujícím příkladu používá <sys its:translate="no">dconf</sys> k nastavení aplikací <app>gedit</app>, <app>Terminál</app> a <app>Soubory</app> (<app>Nautilus</app>) jako výchozích oblíbených pro uživatele. Kód příkladu umožňuje uživateli si je později v případě potřeby upravit.</p>

  <listing>
    <title>Obsah souboru <file its:translate="no">/etc/dconf/profile</file>:</title>
<code its:translate="no">
# <span its:translate="yes">Tento řádek umožňuje uživateli výchozí oblíbené dodatečně změnit</span>
user-db:user
</code>
  </listing>

  <listing>
    <title>Obsah souboru <file its:translate="no">~/.config/dconf/user</file>:</title>
<code its:translate="no">
# <span its:translate="yes">Nastaví gedit, terminal a nautilus jako výchozí oblíbené aplikace</span>
[org/gnome/shell]
favorite-apps = [<var>'gedit.desktop'</var>, <var>'gnome-terminal.desktop'</var>, <var>'nautilus.desktop'</var>]
</code>
  </listing>

  <note style="tip">
    <p>Výše uvedená nastavení můžete také <link xref="dconf-lockdown">uzamknout</link>, aby je uživatelé nemohli měnit.</p>
  </note>

</section>

<section id="all-users">
  <title>Nastavení stejných oblíbených aplikací pro všechny uživatele</title>

  <p>Abyste měli stejné oblíbené pro všechny uživatele, musíte pomocí <link xref="dconf-keyfiles">souborů s klíči dconf</link> upravit systémovou databázi. Následující kroky upraví profil <sys its:translate="no">dconf</sys> a následně vytvoří soubor s klíči pro nastavení výchozích oblíbených aplikací pro všechny uživatelele v databázi nastavení <code>local</code>.</p>

  <steps>
    <title>Nastavení výchozích oblíbených aplikací</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p>Vytvořte soubor s klíči <file>/etc/dconf/db/local.d/00-favorite-apps</file>, který bude poskytovat informace pro databázi <sys>local</sys>.</p>
      <listing>
      <title>Obsah souboru <file its:translate="no">/etc/dconf/db/local.d/00-favorite-apps</file>:</title>
<code>
# Úryvek nastavuje aplikace gedit, terminal a nautilus jako výchozí oblíbené pro všechny uživatele
[org/gnome/shell]
favorite-apps = [<var>'gedit.desktop'</var>, <var>'gnome-terminal.desktop'</var>, <var>'nautilus.desktop'</var>]
</code>
      </listing>
    </item>
    <item>
      <p>Aby uživatel nemohl tato nastavení přepsat, vytvořte soubor <file>/etc/dconf/db/local.d/locks/favorite-apps</file> s následujícím obsahem:</p>
      <listing>
      <title><file>/etc/dconf/db/local.db/locks/favorite-apps</file></title>
<code>
# Zamyká výchozí oblíbené aplikace
/org/gnome/shell/favorite-apps
</code>
      </listing>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

</section>

</page>
