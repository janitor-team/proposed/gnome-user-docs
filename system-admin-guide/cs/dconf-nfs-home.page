<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="dconf-nfs-home" xml:lang="cs">

  <info>
    <link type="guide" xref="setup"/>
    <link type="seealso" xref="dconf-profiles"/>
    <link type="seealso" xref="dconf-custom-defaults"/>
    <revision version="0.1" date="2013-03-19" status="draft"/>
    <revision pkgversion="3.8" date="2013-05-09" status="review"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak změnit nastavení systému pro <sys>dconf</sys>, aby ukládal nastavení v domovské složce na <sys>NFS</sys>.</desc>
  </info>

  <title>Ukládání nastavení přes <sys>NFS</sys></title>

  <p>Aby <sys its:translate="no">dconf</sys> fungoval správně při použití domovské složky na souborovém systému <sys>NFS</sys>, musí <sys its:translate="no">dconf</sys> používat <em>serverovou část pro soubory s klíči</em>.</p>

  <steps>
    <item>
      <p>Na každém klientovi vytvořte nebo upravte soubor <file its:translate="no">/etc/dconf/profile/user</file>.</p>
    </item>
    <item>
      <p>Na úplný začátek tohoto souboru přidejte následující řádek:</p>
      <code><input its:translate="no">service-db:keyfile/user</input></code>
    </item>
  </steps>
  <p><em>Serverová část souborů s klíči</em> <sys its:translate="no">dconf</sys> se projeví jen při příštím přihlášení uživatele. Dotazuje se souborů s klíči, aby zjistila, jestli nedošlo ke změně, takže se nastavení nemusí aktualizovat okamžitě.</p>
  <p>Pokud se <em>serverová část pro soubory s klíči</em> nepoužije, nepůjdou uživatelská nastavení správné získávat a aktualizovat.</p>

</page>
