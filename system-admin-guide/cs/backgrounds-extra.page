<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="backgrounds-extra" xml:lang="cs">

  <info>
    <link type="guide" xref="appearance"/>
    <revision pkgversion="3.30" date="2019-02-08" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak uživatelům zpřístupnit další dodatečná pozadí.</desc>
  </info>

  <title>Přidání dalších pozadí</title>

  <p>Uživatelům systému může zpřístupnit dodatečná pozadí pomocí následujících kroků.</p>

  <steps>
  <title>Nastavení dalších pozadí</title>
  <item>
  <p>Vytvořte soubor XML, například <file><var>nazevsouboru</var>.xml</file>. V tomto souboru použijte klíče ze schématu GSettings <sys>org.gnome.desktop.background</sys> k určení dalších pozadí a jejich vzhledu.</p>

  <p>Níže je seznam nejčastěji používaných klíčů:</p>

  <table frame="top bottom" rules="all" shade="rows">
    <title>Klíč schématu GSettings org.gnome.desktop.background</title>
    <tbody>
    <tr>
      <td><p>Název klíče</p></td>
      <td><p>Možné hodnoty</p></td>
      <td><p>Popis</p></td>
    </tr>
    <tr>
      <td><p>picture-options</p></td>
      <td><p>"none" (ni), "wallpaper" (tapeta), "centered" (na střed), "scaled" (škálovat se zachováním poměru stran), "stretched" (roztáhnout přes celou plochu), "zoom" (zvětšit podle celé plochy), "spanned" (roztáhnout přes všechny monitory)</p></td>
      <td><p>Určuje, jak se má vykreslit obrázek nastavený pomocí <var>wallpaper_filename</var>.</p></td>
    </tr>
    <tr>
      <td><p>color-shading-type</p></td>
      <td><p>"horizontal" (vodorovně), "vertical" svisle a "solid" (jednolitě)</p></td>
      <td><p>Jak vytvořit barevný přechod pozadí.</p></td>
    </tr>
    <tr>
      <td><p>primary-color</p></td>
      <td><p>výchozí: #023c88</p></td>
      <td><p>Barva vlevo nebo vpravo při vykreslování přechodu, případně barva pro jednolité pozadí.</p></td>
    </tr>
    <tr>
      <td><p>secondary-color</p></td>
      <td><p>výchozí: #5789ca</p></td>
      <td><p>Barva vpravo nebo dole při vykreslování přechodu, pro jednolité pozadí se nevyužije.</p></td>
    </tr>
  </tbody>
  </table>

  <p>Úplný seznam klíčů <sys>org.gnome.desktop.background</sys> si můžete zobrazit pomocí aplikace <app>dconf-editor</app> nebo nástrojem příkazové řádky <cmd>gsettings</cmd>. Více informací viz <link xref="gsettings-browse"/>.</p>
  <p>Níže se můžete podívat na ukázkový soubor <file><var>nazevsouboru</var>.xml</file>:</p>

<code mime="application/xml">
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;!DOCTYPE wallpapers SYSTEM "gnome-wp-list.dtd"&gt;
&lt;wallpapers&gt;
  &lt;wallpaper deleted="false"&gt;
    &lt;name&gt;Company Background&lt;/name&gt;
    &lt;name xml:lang="cs"&gt;Firemní pozadí&lt;/name&gt;
    &lt;name xml:lang="de"&gt;Firmenhintergrund&lt;/name&gt;
    &lt;filename&gt;/usr/local/share/backgrounds/company-wallpaper.jpg&lt;/filename&gt;
    &lt;options&gt;zoom&lt;/options&gt;
    &lt;shade_type&gt;solid&lt;/shade_type&gt;
    &lt;pcolor&gt;#ffffff&lt;/pcolor&gt;
    &lt;scolor&gt;#000000&lt;/scolor&gt;
  &lt;/wallpaper&gt;
&lt;/wallpapers&gt;

</code>

  </item>
  <item>
  <p>Umístěte soubor <file><var>nazevsouboru</var>.xml</file> do složky <file>/usr/share/gnome-background-properties/</file>.</p>
  <p>Uživatel bude mít k dalším pozadím přístup v nastavení přes <guiseq><gui>Nastavení</gui> <gui>Pozadí</gui></guiseq>.</p>
  </item>
  </steps>


  <section id="backgrounds-extra-two-wallpapers">
  <title>Zadání více pozadí</title>
  <p>V jednom souboru s nastavením můžete zadat i více prvků <code>&lt;wallpaper&gt;</code>, čímž přidáte více pozadí.</p>
  <p>Podívejte se na následující příklad přidávající dvě různá pozadí pomocí dvou prvků <code>&lt;wallpaper&gt;</code>.</p>

<code mime="application/xml">
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;!DOCTYPE wallpapers SYSTEM "gnome-wp-list.dtd"&gt;
&lt;wallpapers&gt;
  &lt;wallpaper deleted="false"&gt;
    &lt;name&gt;Company Background 1&lt;/name&gt;
    &lt;name xml:lang="cs"&gt;Firemní pozadí 1&lt;/name&gt;
    &lt;name xml:lang="de"&gt;Firmenhintergrund 1&lt;/name&gt;
    &lt;filename&gt;/usr/local/share/backgrounds/company-wallpaper-1.jpg&lt;/filename&gt;
    &lt;options&gt;zoom&lt;/options&gt;
    &lt;shade_type&gt;solid&lt;/shade_type&gt;
    &lt;pcolor&gt;#ffffff&lt;/pcolor&gt;
    &lt;scolor&gt;#000000&lt;/scolor&gt;
  &lt;/wallpaper&gt;
  &lt;wallpaper deleted="false"&gt;
    &lt;name&gt;Company Background 2&lt;/name&gt;
    &lt;name xml:lang="cs"&gt;Firemní pozadí 2&lt;/name&gt;
    &lt;name xml:lang="de"&gt;Firmenhintergrund 2&lt;/name&gt;
    &lt;filename&gt;/usr/local/share/backgrounds/company-wallpaper-2.jpg&lt;/filename&gt;
    &lt;options&gt;zoom&lt;/options&gt;
    &lt;shade_type&gt;solid&lt;/shade_type&gt;
    &lt;pcolor&gt;#ff0000&lt;/pcolor&gt;
    &lt;scolor&gt;#00ffff&lt;/scolor&gt;
  &lt;/wallpaper&gt;
&lt;/wallpapers&gt;
</code>

</section>
</page>
