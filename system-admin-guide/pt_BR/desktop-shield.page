<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="desktop-shield" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="appearance"/>
    <revision pkgversion="3.22" date="2017-01-09" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2017</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Altere o plano de fundo da proteção da tela de bloqueio.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  </info>

  <title>Alterando a proteção da tela de bloqueio</title>

  <p>A <em>proteção da tela de bloqueio</em> é a tela que rapidamente desliza para baixo quando o sistema é bloqueado. O plano de fundo da proteção da tela de bloqueio é controlado pela chave GSettings <sys>org.gnome.desktop.screensaver.picture-uri</sys>. Já que <sys>GDM</sys> usa seu próprio perfil <sys>dconf</sys>, você pode definir o plano de fundo padrão alterando as configurações naquele perfil.</p>

<steps>
  <title>Definindo a chave org.gnome.desktop.screensaver.picture-uri</title>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-gdm'])"/>
  <item><p>Crie um banco de dados <sys>gdm</sys> para configurações de todo sistema em <file>/etc/dconf/db/gdm.d/<var>01-screensaver</var></file>:</p>
  <code>[org/gnome/desktop/screensaver]
picture-uri='file://<var>/opt/corp/plano-de-fundo.jpg</var>'</code>
  <p>Substitua <var>/opt/corp/plano-de-fundo.jpg</var> com o caminho do arquivo de imagem que você deseja usar como plano de fundo da tela de bloqueio.</p>
  <p>Formatos aceitos são PNG, JPG, JPEG e TGA. A imagem será dimensionada, se necessário, para ajustar à tela.</p>
  </item>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  <item><p>Usuários devem encerrar a sessão e inciá-la novamente antes das configurações do sistema terem efeito.</p>
  </item>
</steps>

<p>Na próxima vez que se entrar na tela de bloqueio, a nova proteção de tela de bloqueio mostrará o plano de fundo. Em primeiro plano, horário, data e o dia atual da semana serão exibidos.</p>

<section id="troubleshooting-background">
  <title>E se o plano de fundo não for atualizado?</title>

  <p>Certifique-se de que você tenha executado o comando <cmd its:translate="no">dconf update</cmd> para atualizar os bancos de dados de sistema.</p>

  <p>No caso do plano de fundo não ser atualizado, tente reiniciar o <sys>GDM</sys>.</p>
</section>

</page>
