<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mime-types-application" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="seealso" xref="mime-types-application-user"/>
    <link type="seealso" xref="mime-types-custom-user"/>
    <link type="seealso" xref="mime-types-custom"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Personalize qual aplicativo deve abrir um tipo MIME específico.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  </info>

    <title>Sobrepondo o aplicativo registrado padrão para todos os usuários</title>
    <p>Os arquivos <file>/usr/share/applications/mimeapps.list</file> e <file>/usr/share/applications/gnome-mimeapps.list</file> especifica qual aplicativo é registrado para abrir tipos MIME específicos por padrão. Esses arquivos são fornecidos pela distribuição.</p>

      <p>Para sobrepondo os padrões do sistema para todos os usuários no sistema, você precisa criar um arquivo <file>/etc/xdg/mimeapps.list</file> ou <file>/etc/xdg/gnome-mimeapps.list</file> com uma lista de tipos MIME para os quais você deseja sobrepor o aplicativo padrão registrado.</p>

      <note>
      <p>Arquivos localizados em <file>/etc/xdg/</file> tem precedência sobre arquivos encontrados em <file>/usr/share/applications/</file>. Adicionalmente, <file>/etc/xdg/gnome-mimeapps.list</file> tem precedência sobre <file>/etc/xdg/mimeapps.list</file>, mas pode ser sobrescrito pela configuração do usuário em <file>~/.config/mimeapps.list</file>.</p>
      </note>

    <steps>
      <title>Sobrepondo o aplicativo registrado padrão para todos os usuários</title>
      <item>
        <p>Consulte o arquivo <file>/usr/share/applications/mimeapps.list</file> para determinar os tipos MIME para os quais você deseja alterar o aplicativo registrado padrão. Por exemplo, o exemplo a seguir de arquivo <file>mimeapps.list</file> especifica o aplicativo padrão registrado para os tipos MIME <code>text/html</code> e <code>application/xhtml+xml</code>:</p>
        <code>[Default Applications]
text/html=epiphany.desktop
application/xhtml+xml=epiphany.desktop</code>
        <p>O aplicativo padrão (<app>Epiphany</app>) é definido especificando seu arquivo <file>.desktop</file> correspondente (<file>epiphany.desktop</file>). A localização padrão para arquivos <file>.desktop</file> files is <file>/usr/share/applications/</file>.</p>
      </item>
      <item>
        <p>Crie o arquivo <file>/etc/xdg/mimeapps.list</file>. No arquivo, especifique os tipos MIME e seus aplicativos padrão registrados correspondentes:</p>
        <code>[Default Applications]
text/html=<var>meuaplicativo1.desktop</var>
application/xhtml+xml=<var>meuaplicativo2.desktop</var>

[Added Associations]
text/html=<var>meuaplicativo1.desktop</var>;
application/xhtml+xml=<var>meuaplicativo2.desktop</var>;</code>
      <p>Isso define o aplicativo registrado padrão para o tipo MIME <code>text/html</code> para <code>meuaplicativo1.desktop</code> e o aplicativo registrado padrão para o tipo MIME <code>application/xhtml+xml</code> para <code>meuaplicativo2.desktop</code>.</p>
        <p>Para essas configurações funcionarem corretamente, assegure-se de que ambos <file>meuaplicativo1.desktop</file> e <file>meuaplicativo2.desktop</file> sejam colocados no diretório <file>/usr/share/applications/</file>.</p>
      </item>
      <item>
        <p>Você pode usar o comando <cmd>gio mime</cmd> para verificar se o aplicativo registrado padrão foi definido corretamente:</p>
        <screen><output>$ </output><input>gio mime text/html</input>
Aplicativo padrão para “text/html”: meuaplicativo1.desktop
Aplicativos registrados:
	meuaplicativo1.desktop
	epiphany.desktop
Aplicativos recomendados:
	meuaplicativo1.desktop
	epiphany.desktop</screen>
      </item>
    </steps>
</page>
