<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="dconf-profiles" xml:lang="ko">

  <info>
    <link type="guide" xref="setup"/>
    <link type="seealso" xref="dconf-custom-defaults"/>
    <link type="seealso" xref="dconf"/>
    <revision pkgversion="3.30" date="2019-02-22" status="incomplete"/>

    <credit type="author copyright">
      <name>Ryan Lortie</name>
      <email>desrt@desrt.ca</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Švárová</name>
      <email>jana.svarova@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>프로파일 상세 정보와 프로파일 선택.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조재은</mal:name>
      <mal:email>ckr971028@gmail.com</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2019, 2021.</mal:years>
    </mal:credit>
  </info>

  <title>프로파일</title>

  <p><em>프로파일</em>은 설정 데이터베이스 목록입니다. 프로파일의 첫번째 데이터베이스는 기록 데이터베이스이며 나머지는 읽기전용입니다. 각 시스템 데이터베이스는 키 파일 디렉터리에 만듭니다. 각 키 파일 디렉터리에는 하나 이상의 키 파일을 담고 있습니다. 각 키 파일에는 최소한 하나 이상의 dconf 경로와 하나 이상의 키와 해당 값이 들어있습니다.</p>

  <p><sys>dconf</sys> <em>프로파일</em>에 설정한 키 쌍은 여러분이 설정한 값에 문제가 없는 한 기본 설정 값으로 우선 적용합니다.</p>

  <p><em>user 데이터베이스</em>와 최소한 하나의 시스템 데이터베이스를 구성하는 여러분만의 <sys>dconf</sys> 프로파일을 갖추고 싶을 때가 있습니다. 이 프로파일에는 반드시 줄 하나당 데이터베이스 하나가 들어가야합니다.</p>

  <p>프로파일의 첫번째 줄은 바꿀 설정을 기록할 데이터베이스입니다. 보통 <code>user-db:<input>user</input></code> 입니다. <input>user</input>는 <file>~/.config/dconf</file>에서 보통 찾을 수 있는 사용자 데이터베이스 이름입니다.</p>

  <p><code>system-db</code> 부분은 시스템 데이터베이스를 정의합니다. 이 데이터베이스는 <file>/etc/dconf/db/</file> 위치에 있습니다.</p>

  <example>
    <listing>
      <title>프로파일 예제</title>
<code its:translate="no">
user-db:user
system-db:<var>local</var>
system-db:<var>site</var>
</code>
    </listing>
  </example>

  <!-- TODO: explain the profile syntax (maybe new page) -->
  <!--TODO: explain local and site -->

  <p>단일 사용자 및 다중 시스템 데이터베이스를 설정하면 기본 설정을 계층별로 분할할 수 있습니다. <code>user</code> 데이터베이스 설정은 <code>site</code> 데이터베이스 설정에 우선하는 <code>local</code> 데이터베이스 파일의 설정에 우선합니다.</p>

  <p>그러나, <link xref="dconf-lockdown">잠금</link> 에 대한 우선순위는 역순입니다. <code>site</code> 데이터베이스 파일 또는 <code>local</code> 데이터베이스 파일에 도입한 잠금은 <code>user</code> 데이터베이스 파일에 나타나는 해당 설정보다 우선합니다.</p>

  <note style="important">
  <p><sys>dconf</sys> 세션 프로파일은 로그인할 때 결정하므로 사용자는 로그아웃한 후 새 <sys>dconf</sys> 사용자 프로파일을 자신의 세션에 적용해야합니다.</p>
  </note>

  <p>자세한 정보는 <link its:translate="no" href="man:dconf(7)">
  <cmd>dconf</cmd>(7)</link> 맨 페이지를 참고하십시오.</p>

  <section id="dconf-profiles">

  <title>프로파일 선택</title>

  <p>시작시 <sys>dconf</sys>에서는 <sys>DCONF_PROFILE</sys> 환경 변수를 찾아봅니다. 이 변수에 <file>/etc/dconf/profile/</file> 경로에 대한 파일의 상대 경로 또는 절대 경로를 지정할 수 있습니다. 설정 값의 예를 들자면, 사용자 기본 디렉터리가 있습니다.</p>

  <p>환경 변수를 설정했다면, <sys>dconf</sys>에서 이름이 붙은 프로파일 열기를 시도하고 동작에 실패하면 중단합니다. 변수를 설정하지 않으면, <sys>dconf</sys> 는 “user” 프로파일 열기를 시도합니다. 이에 실패하면, 내부적으로 강연결 상태의 설정으로 돌아갑니다.</p>

  <p>자세한 정보는 <link its:translate="no" href="man:dconf(7)">
  <cmd>dconf</cmd>(7)</link> 맨 페이지를 참고하십시오.</p>

  </section>

</page>
