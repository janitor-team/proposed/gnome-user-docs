<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mime-types-custom-user" xml:lang="ko">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="seealso" xref="mime-types"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>사용자별 MIME 명세를 만들고 기본 프로그램을 등록합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조재은</mal:name>
      <mal:email>ckr971028@gmail.com</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2019, 2021.</mal:years>
    </mal:credit>
  </info>

    <title>개별 사용자에게 개별 MIME 설정 추가하기</title>
    <p>개별 MIME 형식을 개별 사용자에게 추가하고 해당 MIME 형식에 대해 기본 프로그램을 등록하려면, 새 MIME 형식 정의 파일을 <file>~/.local/share/mime/packages/</file> 디렉터리에 만들고 <file>~/.local/share/applications/</file> 디렉터리에 <file>.desktop</file> 파일을 만들어야합니다.</p>
    <steps>
      <title>개별 사용자에게 <code>application/x-newtype</code> 개별 MIME 형식 추가하기</title>
      <item>
        <p> <file>~/.local/share/mime/packages/application-x-newtype.xml</file> 파일을 만드십시오:</p>
        <code mime="application/xml">&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;mime-info xmlns="http://www.freedesktop.org/standards/shared-mime-info"&gt;
  &lt;mime-type type="application/x-newtype"&gt;
    &lt;comment&gt;new mime type&lt;/comment&gt;
    &lt;glob pattern="*.xyz"/&gt;
  &lt;/mime-type&gt;
&lt;/mime-info&gt;</code>
      <p>위 예제 <file>application-x-newtype.xml</file> 파일에서는 새 <sys>application/x-newtype</sys> MIME 형식을 정의하고 해당 MIME 형식에 <file>.xyz</file> 파일 이름 확장자를 할당했습니다.</p>
      </item>
      <item>
        <p><file>myapplication1.desktop</file> 파일과 같이 이름을 붙인 새 <file>.desktop</file> 파일을 만들어 <file>~/.local/share/applications/</file> 디렉터리에 넣으십시오:</p>
        <code>[Desktop Entry]
Type=Application
MimeType=application/x-newtype
Name=<var>My Application 1</var>
Exec=<var>myapplication1</var></code>
      <p>위 <file>myapplication1.desktop</file> 예제 파일은 <code>application/x-newtype</code> MIME 형식을 <cmd>myapplication1</cmd> 명령을 실행하는 <app>My Application 1</app> 프로그램에 연결합니다.</p>
      </item>
      <item>
        <p>바뀐 설정대로 동작하게 하려면 MIME 데이터베이스를 업데이트하십시오:</p>
        <screen><output>$ </output><input>update-mime-database ~/.local/share/mime</input>
        </screen>
      </item>
      <item>
        <p>프로그램 데이터베이스를 업데이트하십시오:</p>
        <screen><output>$ </output><input>update-desktop-database ~/.local/share/applications</input>
        </screen>
      </item>
      <item>
        <p><file>*.xyz</file> 파일과 <sys>application/x-newtype</sys> 형식 연계를 제대로 했는지 확인하려면 , 우선 <file>test.xyz</file>와 같은 빈 파일을 만드십시오:</p>
        <screen><output>$ </output><input>touch test.xyz</input></screen>
        <p>그런 다음 <cmd>gio info</cmd> 명령을 실행하십시오:</p>
        <screen><output>$ </output><input>gio info test.xyz | grep "standard::content-type"</input>
  standard::content-type: application/x-newtype</screen>
        </item>
        <item>
          <p><file>myapplication1.desktop</file> 파일을 <sys>application/x-newtype</sys> MIME 형식에 대해 기본 등록 프로그램으로 제대로 설정했는지 확인하려면 <cmd>gio mime</cmd> 명령을 실행하십시오:</p>
        <screen><output>$ </output><input>gio mime application/x-newtype</input>
Default application for “application/x-newtype”: myapplication1.desktop
Registered applications:
	myapplication1.desktop
Recommended applications:
	myapplication1.desktop</screen>
      </item>
    </steps>
</page>
