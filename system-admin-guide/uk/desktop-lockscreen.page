<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="desktop-lockscreen" xml:lang="uk">

  <info>
    <link type="guide" xref="appearance"/>
    <link type="seealso" xref="dconf-profiles"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <desc>Автоматичне блокування екрана, таке, щоб користувач мав ввести пароль, якщо комп'ютер був бездіяльним певний час.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Блокування екрана, коли користувач не виконує дій</title>

  <p>Ви можете наказати системі автоматично блокувати екран, якщо користувач не виконує з комп'ютером ніяких дій протягом певного періоду часу. Це блокування є корисним, якщо комп'ютер лишають без нагляду у громадських або доступних для сторонніх користувачів місцях.</p>

  <steps>
    <title>Вмикання автоматичного блокування екрана</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p>Створіть файл ключів <file>/etc/dconf/db/local.d/00-screensaver</file> для надання відомостей для бази даних <sys>local</sys>.</p>
      <listing>
      <title><file>/etc/dconf/db/local.d/00-screensaver</file></title>
<code>
# Визначаємо шлях dconf
[org/gnome/desktop/session]

# Кількість секунд бездіяльності до очищення екрана
# Встановіть значення 0 секунд, якщо хочете вимкнути зберігач екрана.
idle-delay=uint32 180

# Визначаємо шлях dconf
[org/gnome/desktop/screensaver]

# Кількість секунд після очищення екрана до блокування екрана
lock-delay=uint32 0
</code>
      </listing>
      <p>Вам слід додавати <code>uint32</code> до цілочисельних ключів, як це показано вище.</p>
    </item>
    <item>
      <p>Щоб запобігти перевизначенню цих параметрів користувачем, створіть файл <file>/etc/dconf/db/local.d/locks/screensaver</file> із таким вмістом:</p>
      <listing>
      <title><file>/etc/dconf/db/local.db/locks/screensaver</file></title>
<code>
# Блокуємо параметри зберігача екрана стільниці
/org/gnome/desktop/session/idle-delay
/org/gnome/desktop/screensaver/lock-delay
</code>
      </listing>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

</page>
