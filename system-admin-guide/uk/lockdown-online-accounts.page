<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="lockdown-online-accounts" xml:lang="uk">

  <info>
    <link type="guide" xref="user-settings#lockdown"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2015</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

   <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Вмикання або вимикання деяких або усіх облікових записів інтернету.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>
  <title>Дозвіл та заборона користування обліковими записами інтернету</title>

  <p>Програму <app>Облікові записи інтернету GNOME</app> (GOA) використовують для інтеграції особистих облікових записів інтернету зі стільничним середовищем GNOME і програмами. Користувачі можуть додавати облікові записи інтернету, зокрема Google, Facebook, Flickr, ownCloud та інші, за допомогою програми <app>Облікові записи</app>.</p>

  <p>Як адміністратор системи, ви можете:</p>
  <list>
    <item><p>вмикати усі облікові записи інтернету;</p></item>
    <item><p>вибірково вмикати деякі облікові записи інтернету;</p></item>
    <item><p>вимикати усі облікові записи інтернету.</p></item>
  </list>

<steps>
  <title>Налаштовування облікових записів інтернету</title>
  <item><p>Переконайтеся, що у вашій системі встановлено пакунок <sys>gnome-online-accounts</sys>.</p>
  </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
  <item>
  <p>Створіть файл ключів <file>/etc/dconf/db/local.d/00-goa</file> для надання відомостей для бази даних <sys>local</sys>, що міститиме наведені нижче налаштування.</p>
   <list>
   <item><p>Щоб дозволити певних надавачів даних:</p>
<code>
[org/gnome/online-accounts]
whitelisted-providers= ['google', 'facebook']
</code>
  </item>
   <item><p>Щоб вимкнути усіх надавачів даних:</p>
<code>
[org/gnome/online-accounts]
whitelisted-providers= ['']
</code>
  </item>
  <item><p>Щоб дозволити усіх доступних надавачів даних:</p>
<code>
[org/gnome/online-accounts]
whitelisted-providers= ['all']
</code>
  <p>Це типовий варіант.</p></item>
   </list>
  </item>
  <item>
    <p>Щоб запобігти перевизначенню цих параметрів користувачем, створіть файл <file>/etc/dconf/db/local.d/locks/goa</file> із таким вмістом:</p>
    <listing>
    <title><file>/etc/dconf/db/local.db/locks/goa</file></title>
<code>
# Блокуємо список надавачів даних, які дозволено завантажувати
/org/gnome/online-accounts/whitelisted-providers
</code>
    </listing>
  </item>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
</steps>

</page>
