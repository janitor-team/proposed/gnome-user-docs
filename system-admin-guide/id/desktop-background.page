<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="desktop-background" xml:lang="id">

  <info>
    <link type="guide" xref="appearance"/>
    <link type="seealso" xref="backgrounds-extra"/>
    <revision pkgversion="3.11" date="2014-01-29" status="draft"/>
    <revision pkgversion="3.14" date="2014-06-17" status="incomplete">
      <desc>All prose and instructions are up to par. Extra info needs
      to be provided on picture options, per comment below. --shaunm</desc>
    </revision>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>davidk@gnome.org</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>

    <desc>Mengubah latar belakang desktop baku untuk semua pengguna.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Menata latar belakang ubahan</title>

  <p>You can change the default desktop background to one that you want to use.
  For example, you may want to use a background with your company or university
  logo instead of the default GNOME background.</p>

  <steps>
    <title>Menata latar belakang baku</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p>Create the key file
      <file>/etc/dconf/db/local.d/00-background</file> to provide
      information for the <sys>local</sys> database.</p>
      <listing>
        <title><berkas>/etc/dconf/db/local.d/00-background</berkas></title>
<code>
# Tentukan path dconf
[org/gnome/desktop/background]

# Tentukan path ke desktop berkas gambar latar belakang
picture-uri='file:///usr/local/share/backgrounds/wallpaper.jpg'

# Tentukan salah satu opsi render untuk gambar latar belakang:
picture-options='scaled'

# Tentukan warna kiri atau atas saat menggambar gradien, atau warna solid
primary-color='000000'

# Tentukan warna kanan atau bawah saat menggambar gradien
secondary-color='FFFFFF'
</code>
      </listing>
    </item>
    <item>
      <p>To prevent the user from overriding these settings, create the file
      <file>/etc/dconf/db/local.d/locks/background</file> with the following
      content:</p>
      <listing>
        <title><berkas>/etc/dconf/db/local.d/locks/background</berkas></title>
<code>
# Lock desktop background settings
/org/gnome/desktop/background/picture-uri
/org/gnome/desktop/background/picture-options
/org/gnome/desktop/background/primary-color
/org/gnome/desktop/background/secondary-color
</code>
      </listing>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

</page>
