<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="lockdown-repartitioning" xml:lang="hu">

  <info>
    <link type="guide" xref="user-settings#lockdown"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.14" date="2014-12-10" status="review"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>A felhasználó megakadályozása a lemez partícióinak megváltoztatásában.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur.balazs at fsf dot hu</mal:email>
      <mal:years>2018, 2019, 2020, 2021.</mal:years>
    </mal:credit>
  </info>

  <title>Újraparticionálás letiltása</title>

  <p>A <sys>polkit</sys> lehetővé teszi, hogy jogosultságokat állítson be az egyes műveletekhez. A <sys>udisks2</sys>, a lemezkezelési szolgáltatások segédprogramja esetén a beállítások az <file>/usr/share/polkit-1/actions/org.freedesktop.udisks2.policy</file> fájlban találhatók. Ez a fájl olyan műveletek és alapértelmezett értékek halmazát tartalmazza, amelyeket a rendszergazda felülbírálhat.</p>

  <note style="tip">
    <p>Az <file>/etc</file> mappában lévő <sys>polkit</sys> beállítás felülbírálja azt, amelyik az <file>/usr/share</file> fájlban érkezik a csomagokkal.</p>
  </note>

  <steps>
    <title>Újraparticionálás letiltása</title>
    <item>
      <p>Hozzon létre egy fájlt az <file>/usr/share/polkit-1/actions/org.freedesktop.udisks2.policy</file> fájllal megegyező tartalommal: <cmd>cp /usr/share/polkit-1/actions/org.freedesktop.udisks2.policy /etc/share/polkit-1/actions/org.freedesktop.udisks2.policy</cmd></p>
      <note style="important">
        <p>Ne változtassa meg az <file>/usr/share/polkit-1/actions/org.freedesktop.udisks2.policy</file> fájlt, mert a változtatásait a következő csomagfrissítés felül fogja írni.</p>
      </note>
    </item>
    <item>
      <p>Töröljön minden olyan műveletet a <code>policyconfig</code> elemen belül, amelyre nincs szüksége, és adja hozzá a következő sorokat az <file>/etc/polkit-1/actions/org.freedesktop.udisks2.policy</file> fájlhoz:</p>
      <listing>
<code>
  &lt;action id="org.freedesktop.udisks2.modify-device"&gt;
     &lt;description&gt;Meghajtó beállításainak módosítása&lt;/description&gt;
     &lt;message&gt;Hitelesítés szükséges a meghajtó beállításainak módosításához&lt;/message&gt;
    &lt;defaults&gt;
      &lt;allow_any&gt;no&lt;/allow_any&gt;
      &lt;allow_inactive&gt;no&lt;/allow_inactive&gt;
      &lt;allow_active&gt;yes&lt;/allow_active&gt;
    &lt;/defaults&gt;
&lt;/action&gt;
</code>
      </listing>
      <p>Cserélje ki a <code>no</code> értéket <code>auth_admin</code> értékre, ha biztosítani szeretné, hogy csak a rendszergazda felhasználó legyen képes végrehajtani a műveletet.</p>
    </item>
    <item>
      <p>Mentse el a változtatásokat.</p>
    </item>
  </steps>

  <p>Amikor a felhasználó megpróbálja megváltoztatni a lemez beállításait, akkor a következő üzenet jelenik meg: <gui>Hitelesítés szükséges a meghajtó beállításainak módosításához</gui>.</p>

</page>
