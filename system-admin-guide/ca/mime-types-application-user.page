<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mime-types-application-user" xml:lang="ca">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="seealso" xref="mime-types-custom-user"/>
    <link type="seealso" xref="mime-types-custom"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Personalitzeu per l'usuari quina aplicació obre un tipus MIME específic.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

    <title>Substituïu l'aplicació registrada per defecte per als usuaris individuals</title>

    <p>Els fitxers <file>/usr/share/applications/mimeapps.list</file> i <file>/usr/share/applications/gnome-mimeapps.list</file> especifiquen quina aplicació està registrada per a obrir els tipus MIME per defecte. Aquests fitxers els proporciona la distribució.</p>
    <p>Per sobreescriure els valor per defecte del sistema per als usuaris individuals, us cal crear un fitxer <file>~/.config/mimeapps.list</file> amb una llista de tipus MIME per als quals voleu sobreescriure l'aplicació registrada per defecte.</p>
    <steps>
      <title>Substituïu l'aplicació registrada per defecte per als usuaris individuals</title>
      <item>
        <p>Consulteu el fitxer <file>/usr/share/applications/mimeapps.list</file> per a determinar els tipus MIME per als quals voleu canviar l'aplicació registrada per defecte. Per exemple, la següent mostra d'exemple al fitxer <file>mimeapps.list</file> especifica l'aplicació registrada per defecte per a <code>text/html</code> i els tipus MIME <code>application/xhtml+xml</code>:</p>
        <code>[Aplicacions per defecte]
text/html=epiphany.desktop
application/xhtml+xml=epiphany.desktop</code>
        <p>L'aplicació predeterminada (<app>Epiphany</app>) es defineix especificant el seu fitxer corresponent <file>.desktop</file> a (<file>epiphany.desktop</file>). La ubicació predeterminada del sistema dels fitxers per altres aplicacions <file>.desktop</file> es troba a <file>/usr/share/applications/</file>. Per als usuaris individuals els fitxers <file>.desktop</file> es poden emmagatzemar a <file>~/.local/share/applications/</file>.</p>
      </item>
      <item>
        <p>Creeu el fitxer <file>~/.config/mimeapps.list</file>. Al fitxer , especifiqueu els tipus MIME i les seves aplicacions corresponents predeterminades:</p>
        <code>[Aplicació predeterminada]
text/html=<var>myapplication1.desktop</var>
application/xhtml+xml=<var>myapplication2.desktop</var>

[Associacions afegides]
text/html=<var>myapplication1.desktop</var>;
application/xhtml+xml=<var>myapplication2.desktop</var>;</code>
      <p>Estableix l'aplicació registrada per defecte per al tipus MIME <code>text/html</code> a <code>myapplication1.desktop</code>, i la predeterminada pel tipus MIME <code>application/xhtml+xml</code> a <code>myapplication2.desktop</code>.</p>
        <p>Per tal que aquesta configuració funcioni correctament, assegureu-vos que tant els fitxers <file>myapplication1.desktop</file> i <file>myapplication2.desktop</file> es col·loquen al directori <file>/usr/share/applications/</file>. Els fitxers <file>.desktop</file> dels usuaris individuals es poden emmagatzemar a <file>~/.local/share/applications/</file>.</p>
      </item>
      <item>
        <p>Podeu utilitzar la comanda <cmd>gio mime</cmd> per verificar que l'aplicació registrada per defecte s’ha establert correctament:</p>
        <screen><output>$ </output><input>gio mime text/html</input>
L'aplicació predeterminada per “text/html”: myapplication1.desktop
Aplicacions registrades:
	myapplication1.desktop
	epiphany.desktop
Applications recomanades:
	myapplication1.desktop
	epiphany.desktop</screen>
      </item>
    </steps>
</page>
