<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="desktop-shield" xml:lang="ca">

  <info>
    <link type="guide" xref="appearance"/>
    <revision pkgversion="3.22" date="2017-01-09" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2017</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Canvieu el fons del protector de pantalla de bloqueig.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>Canviar la pantalla de bloqueig</title>

  <p>La <em>pantalla de bloqueig</em> és la pantalla que es desplaça ràpidament avall quan el sistema es bloqueja. el fons de la pantalla el controla la clau <sys>org.gnome.desktop.screensaver.picture-uri</sys> GSettings. Com que <sys>GDM</sys> utilitza el seu propi perfil <sys>dconf</sys>, podeu establir el fons per defecte canviant-ne la configuració.</p>

<steps>
  <title>Establiu la clau org.gnome.desktop.screensaver.picture-uri</title>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-gdm'])"/>
  <item><p>Crear una base de dades <sys>gdm</sys> per a la configuració de l'equip a <file>/etc/dconf/db/gdm.d/<var>01-screensaver</var></file>:</p>
  <code>[org/gnome/desktop/screensaver]
picture-uri='file://<var>/opt/corp/background.jpg</var>'</code>
  <p>Reemplaceu <var>/opt/corp/background.jpg</var> amb el camí d'accés al fitxer d'imatge que voleu utilitzar com a fons de la pantalla de bloqueig.</p>
  <p>Els formats compatibles són PNG, JPG, JPEG i TGA. La imatge s’escala si cal per a ajustar-la a la pantalla.</p>
  </item>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  <item><p>Heu de tancar la sessió abans que els paràmetres del sistema tinguin efecte.</p>
  </item>
</steps>

<p>El proper cop que bloquegeu la pantalla, al fons es mostrarà la nova pantalla de protecció . Al primer pla es mostrarà, l'hora, la data, i el dia de la setmana.</p>

<section id="troubleshooting-background">
  <title>Què passa si el fons no s'actualitza?</title>

  <p>Assegureu-vos d'haver executat la <cmd its:translate="no">dconf update</cmd> comanda per a actualitzar les bases de dades de sistema.</p>

  <p>En cas que el fons no s'actualitzi, reinicieu <sys>GDM</sys>.</p>
</section>

</page>
