<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-custom" xml:lang="ca">

  <info>
    <link type="guide" xref="sundry#session"/>
    <link type="guide" xref="login#management"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author copyright">
      <name>minnie_eg</name>
      <email>amany.elguindy@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Creeu una sessió personalitzada instal·lant un fitxer de sessió a l'escriptori.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>Crear una sessió personalitzada</title>

 <p>Per a crear la vostra pròpia sessió amb configuració personalitzada, seguiu aquests passos:</p>

<steps>
  <item><p>Creeu un fitxer <file>.desktop</file> a <file>/etc/X11/sessions/<var>new-session</var>.desktop</file>. Assegureu-vos que el fitxer conté les següents entrades:</p>
  <code>[Desktop Entry]
Encoding=UTF-8
Type=Application
Name=<input>Sessió personalitzada</input>
Comment=<input>Aquesta és la nostra sessió personalitzada</input>
Exec=<input>gnome-session --session=new-session</input></code>
   <p>L'entrada <code>Exec</code> especifica la comanda, possiblement amb arguments, a executar. Podeu executar la sessió personalitzada amb la comanda <cmd>gnome-session --session=<var>new-session</var></cmd>.</p>
   <p>Per a més informació sobre els paràmetres que podeu utilitzar amb <cmd>gnome-session</cmd>, consulteu la pàgina «man» <link its:translate="no" href="man:gnome-session">gnome-session</link>.</p>
  </item>
  <item><p>Creeu un fitxer de sessió personalitzada a <file>/usr/share/gnome-session/sessions/<var>new-session</var>.session</file> on podeu especificar el nom i els components necessaris per a la sessió:</p>
   <code>[GNOME Session]
Name=<input>Sessió personalitzada</input>
RequiredComponents=<input>gnome-shell-classic;gnome-settings-daemon;</input></code>
  <p>Fixeu-vos que cada element que especifiqueu a <code>RequiredComponents</code> necessita el seu corresponent fitxer <file>.desktop</file> a <file>/usr/share/applications/</file>.</p>
  </item>
</steps>

  <p>Després de configurar els fitxers d'una sessió personalitzada, la nova sessió estarà disponible a la llista de sessions a la pantalla d'inici de sessió de GDM.</p>

  <section id="custom-session-issues">
  <title>Incidències conegudes</title>
    <p>A Debian o sistemes basats en Debian, es pot produir el següent error:</p>

    <screen>Xsession: impossible de llançar la sessió""
    Xsession --- "" no s'ha trobat; tornant al valor predeterminat
    .</screen>

    <p>Si us passa, seguiu els següents passos per a canviar el fitxer <file>/etc/X11/Xsession.d/20x11-common_process-args</file>:</p>
    <steps>
    <item><p>Canvieu <code>STARTUP_FULL_PATH=$(/usr/bin/which <input>"$1"</input>|| true)</code> a <code>STARTUP_FULL_PATH=$(/usr/bin/which <input>$1</input> || true)</code></p></item>
    <item><p>Canvia <code>STARTUP=<input>"$1"</input></code> a <code>STARTUP=<input>$1</input></code></p></item>
    </steps>
  </section>

</page>
