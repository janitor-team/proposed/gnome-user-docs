<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="network-server-list" xml:lang="sv">

  <info>
    <link type="guide" xref="network"/>
    <revision pkgversion="3.8" date="2013-06-03" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
   <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
   <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@gvolny.cz</email>
      <years>2013</years>
    </credit>

    <desc>Hur gör jag det enkelt för olika användare att komma åt olika fildelningar?</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Konfigurera en standardserverlista</title>

  <p><app>Nautilus</app> (programmet <app>Filer</app>) lagrar en lista över fildelningsservrar i filen <file>~/.config/gtk-3.0/servers</file> i XBEL-formatet. Lägg till listan över fildelningsservrar till den filen för att göra delade filer lätt åtkomliga för din användare.</p>
  
  <note style="tip">
    <p><em>XBEL</em> (<em>XML Bookmark Exchange Language</em>) är en XML-standard som låter dig dela URI:er (Uniform Resource Identifiers). I GNOME används XBEL för att dela skrivbordsbokmärken i program som <app>Nautilus</app>.</p>
  </note>

  <example>
    <p>Ett exempel på filen <file>~/.config/gtk-3.0/servers</file>:</p>
    <code>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;xbel version="1.0"
      xmlns:bookmark="http://www.freedesktop.org/standards/desktop-bookmarks"
      xmlns:mime="http://www.freedesktop.org/standards/shared-mime-info"&gt;
   &lt;bookmark href="<input>ftp://ftp.gnome.org/</input>"&gt;
      &lt;title&gt;<input>GNOME FTP</input>&lt;/title&gt;
   &lt;/bookmark&gt;
&lt;/xbel&gt;
</code>
    <p>I exemplet ovan skapar <app>Nautilus</app> ett bokmärke med namnet <em>GNOME FTP</em> vars URI är <code>ftp://ftp.gnome.org/</code>.</p>
  </example>

</page>
