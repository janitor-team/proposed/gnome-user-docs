<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="fonts" xml:lang="sv">

  <info>
    <link type="guide" xref="appearance"/>
    <link type="seealso" xref="fonts-user"/>
    <revision pkgversion="3.11" date="2014-01-29" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <desc>Lägg till extra typsnitt för alla användare.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Lägga till ett extra typsnitt för alla användare</title>

  <p>Du kan installera ett extra typsnitt som kommer vara tillgängligt för användare i program som använder <sys>fontconfig</sys> för typsnittshantering.</p>

  <steps>
    <title>Installera ett extra typsnitt</title>
    <item>
      <p>Kopiera typsnittet till katalogen <file>/usr/local/share/fonts/</file> för att installera det.</p>
    </item>
    <item>
      <p>Du kan behöva köra följande kommando för att uppdatera typsnittscachen:</p>
      <screen><output>$ </output><input>fc-cache /usr/local/share/fonts/</input></screen>
    </item>
  </steps>

  <p>Du kan behöva starta om körande program för att se ändringarna. Användarsessioner behöver inte startas om.</p>
  
  <p>Alternativt kan du också installera typsnitt i en annan systemkatalog än <file>/usr/local/share/fonts/</file> om den katalogen finns listad i filen <file>/etc/fonts/fonts.conf</file>. Om den inte är det kommer du behöva skapa din egna maskinomfattande konfigurationsfil i <file>/etc/fonts/local.conf</file> som innehåller katalogen som du vill använda. Se manualsidan <cmd>fonts-conf</cmd>(5) för mer information.</p>
  <p>Om du använder en alternativ katalog, kom ihåg att ange katalognamnet då du uppdaterar typsnittscachen med kommandot <cmd>fc-cache</cmd>:</p>
  <screen><output>$ </output><input>fc-cache <var>directory_name</var></input></screen>

</page>
