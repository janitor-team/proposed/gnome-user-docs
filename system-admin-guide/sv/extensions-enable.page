<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="extensions-enable" xml:lang="sv">

  <info>
    <link type="guide" xref="software#extension"/>
    <link type="seealso" xref="extensions-lockdown"/>
    <link type="seealso" xref="extensions"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>
    <credit type="author">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
   </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Aktivera GNOME Shell-tillägg för alla användare.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Aktivera maskinomfattande tillägg</title>
  
  <p>För att göra tillägg tillgängliga för alla användare på systemet, installera dem i katalogen <file>/usr/share/gnome-shell/extensions</file>. Observera att nyligen installerade maskinomfattande tillägg är inaktiverade som standard.</p>

  <p>Du behöver ställa in nyckeln <code>org.gnome.shell.enabled-extensions</code> för att ställa in tilläggen som är aktiverade som standard. Det finns dock för närvarande inget sätt att aktivera ytterligare tillägg för användare som redan har loggat in. Detta gäller inte för befintliga användare som har installerat och aktiverat sina egna GNOME-tillägg.</p>

  <steps>
    <title>Ställa in nyckeln org.gnome.shell.enabled-extensions</title>
    <item>
      <p>Skapa en profil <code>user</code> i <file>/etc/dconf/profile/user</file>:</p>
      <listing>
        <code>
user-db:user
system-db:local
</code>
      </listing>
    </item>
    <item>
      <p>Skapa en databas <code>local</code> för maskinomfattande inställningar i <file>/etc/dconf/db/local.d/00-extensions</file>:</p>
      <listing>
        <code>
[org/gnome/shell]
# Lista alla tillägg som du vill ha aktiverade för alla användare
enabled-extensions=['<input>mittillagg1@mittnamn.example.com</input>', '<input>mittillagg2@mittnamn.example.com</input>']
</code>
      </listing>
      <p>Nyckeln <code>enabled-extensions</code> anger de aktiverade tilläggen med tilläggens uuid (<code>mittillagg1@mittnamn.example.com</code> och <code>mittillagg2@mittnamn.example.com</code>).</p>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  </steps>

</page>
