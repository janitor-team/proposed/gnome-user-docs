<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="login-automatic" xml:lang="de">

  <info>
    <link type="guide" xref="login#management"/>
    <revision pkgversion="3.4.2" date="2012-12-01" status="review"/>
    <revision pkgversion="3.8" date="2013-04-04" status="review"/>
    <revision pkgversion="3.12" date="2014-06-18" status="review"/>
    
    <credit type="author copyright">
      <name>minnie_eg</name>
      <email>amany.elguindy@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2012, 2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Einen Benutzer beim Systemstart automatisch anmelden.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2017, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>Automatische Anmeldung einrichten</title>

  <p its:locNote="TRANSLATORS: 'Administrator' and 'Automatic Login' are   strings from the Users dialog in the Settings panel user interface.">Ein Benutzer mit den Rechten eines <em>Systemverwalters</em> kann <link href="help:gnome-help/user-autologin">die <em>automatische Anmeldung</em> in den <app>Einstellungen</app></link> aktivieren. Sie können die automatische Anmeldung auch in der benutzerdefinierten Konfigurationsdatei <sys its:translate="no">GDM</sys> manuell einrichten, wie folgt.</p>

  <p>Bearbeiten Sie die Datei <file its:translate="no">/etc/gdm/custom.conf</file> und stellen Sie sicher, dass der <code>[daemon]</code>-Abschnitt in der Datei Folgendes enthält:</p>

  <listing its:translate="no">
    <title><file>custom.conf</file></title>
<code>
[daemon]
AutomaticLoginEnable=<input>True</input>
AutomaticLogin=<input>username</input>
</code>
  </listing>

  <p>Ersetzen Sie <input its:translate="no">username</input> durch den Benutzer, der automatisch angemeldet werden soll.</p>

  <note>
    <p>Die Datei <file its:translate="no">custom.conf</file> finden Sie üblicherweise in <file its:translate="no">/etc/gdm/</file>, aber der Ort kann abhängig von Ihrer Distribution variieren.</p>
  </note>

</page>
