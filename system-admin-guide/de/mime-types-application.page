<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mime-types-application" xml:lang="de">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="seealso" xref="mime-types-application-user"/>
    <link type="seealso" xref="mime-types-custom-user"/>
    <link type="seealso" xref="mime-types-custom"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ermitteln, welche Anwendung einen spezifische MIME-Typ öffnen soll.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2017, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

    <title>Die voreingestellte registrierte Anwendung für alle Benutzer überschreiben</title>
    <p>Die Dateien <file>/usr/share/applications/mimeapps.list</file> und <file>/usr/share/applications/gnome-mimeapps.list</file> geben an, welche Anwendung per Vorgabe zum Öffnen spezifischer MIME-Typen registriert ist. Diese Dateien werden von der Distribution bereitgestellt.</p>

      <p>Um die Systemstandards für alle Benutzer des Systems außer Kraft zu setzen, müssen Sie die Datei <file>/etc/xdg/mimeapps.list</file> oder <file>/etc/xdg/gnome-mimeapps.list</file> anlegen und dort eine Liste der MIME-Typen speichern, für welche die in der Voreinstellung registrierte Anwendung nicht gelten soll.</p>

      <note>
      <p>Dateien in <file>/etc/xdg/</file> haben gegenüber denen in <file>/usr/share/applications/</file> Vorrang. Außerdem hat <file>/etc/xdg/gnome-mimeapps.list</file> Vorrang gegenüber <file>/etc/xdg/mimeapps.list</file>, was aber durch die Benutzerkonfiguration in <file>~/.config/mimeapps.list</file> außer Kraft gesetzt werden kann.</p>
      </note>

    <steps>
      <title>Die voreingestellte registrierte Anwendung für alle Benutzer überschreiben</title>
      <item>
        <p>Anhand der Datei <file>/usr/share/applications/defaults.list</file> können Sie die MIME-Typen ermitteln, für die Sie die in der Voreinstellung registrierte Anwendung ändern wollen. Beispielsweise zeigt der folgende Ausschnitt aus der Datei <file>mimeapps.list</file> die in der Voreinstellung registrierte Anwendung für die MIME-Typen <code>text/html</code> und <code>application/xhtml+xml</code>:</p>
        <code>[Default Applications]
text/html=epiphany.desktop
application/xhtml+xml=epiphany.desktop</code>
        <p>Die Standardanwendung (<app>Epiphany</app>) wird durch die Angabe der entsprechenden <file>.desktop</file>-Datei definiert (<file>epiphany.desktop</file>). Der Standardspeicherort für die <file>.desktop</file>-Dateien anderer Anwendungen ist <file>/usr/share/applications/</file>.</p>
      </item>
      <item>
        <p>Legen Sie die Datei <file>/etc/xdg/mimeapps.list</file> an. Geben Sie in dieser Datei die MIME-Typen und deren korrespondierende, als Voreinstellung registrierte Anwendungen an:</p>
        <code>[Default Applications]
text/html=<var>myapplication1.desktop</var>
application/xhtml+xml=<var>myapplication2.desktop</var>

[Added Associations]
text/html=<var>myapplication1.desktop</var>;
application/xhtml+xml=<var>myapplication2.desktop</var>;</code>
      <p>Dies setzt die voreingestellte registrierte Anwendung für den MIME-Typ <code>text/html</code> auf <code>myapplication1.desktop</code> und für den MIME-Typ <code>application/xhtml+xml</code> auf <code>myapplication2.desktop</code>.</p>
        <p>Damit dies reibungslos funktioniert, müssen Sie sicherstellen, dass sich sowohl <file>myapplication1.desktop</file> als auch <file>myapplication2.desktop</file> im gleichen Ordner befinden.</p>
      </item>
      <item>
        <p>Mit dem Befehl <cmd>gio mime</cmd> können Sie überprüfen, ob die registrierte Standardanwendung korrekt eingerichtet wurde:</p>
        <screen><output>$ </output><input>gio mime text/html</input>
Default application for “text/html”: myapplication1.desktop
Registered applications:
	myapplication1.desktop
	epiphany.desktop
Recommended applications:
	myapplication1.desktop
	epiphany.desktop</screen>
      </item>
    </steps>
</page>
