<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="extensions" xml:lang="es">

  <info>
    <link type="guide" xref="software#extension"/>
    <link type="seealso" xref="extensions-lockdown"/>
    <link type="seealso" xref="extensions-enable"/>
    <revision pkgversion="3.9" date="2013-08-07" status="review"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Las extensiones de GNOME Shell permiten personalizar el interfaz predeterminado de GNOME Shell.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2017 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oliver Gutiérrez</mal:name>
      <mal:email>ogutsua@gmail.com</mal:email>
      <mal:years>2018 - 2020</mal:years>
    </mal:credit>
  </info>

  <title>¿Qué son las extensiones de GNOME Shell?</title>
  
  <p>Las extensiones de GNOME Shell permiten personalizar el interfaz predeterminado de GNOME Shell y sus componentes, como la administración de ventanas y ejecución de aplicaciones.</p>
  
  <p>Cada extensión de GNOME Shell está identificada por un identificador único, el UUID. El UUID se utiliza también como nombre de la carpeta donde se instala la extensión. Puede instalar la extensión por usuario en <file>~/.local/share/gnome-shell/extensions/&lt;uuid&gt;</file> o para todo el sistema en <file>/usr/share/gnome-shell/extensions/&lt;uuid&gt;</file>.</p>
  
  <p>Para ver las extensiones instaladas, puede utilizar <app>Looking Glass</app>, la herramienta integrada de depuración e inspección de GNOME Shell.</p>
  
  <steps>
    <title>Ver las extensiones instaladas</title>
    <item>
      <p>Pulse <keyseq type="combo"><key>Alt</key><key>F2</key></keyseq>, escriba <em>lg</em> y pulse <key>Intro</key> para abrir <app>Looking Glass</app>.</p>
    </item>
    <item>
      <p>En la barra superior de <app>Looking Glass</app>, haga click en <gui>Extensiones</gui> para abrir la lista de extensiones instaladas.</p>
    </item>
  </steps>

</page>
