<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="extensions-enable" xml:lang="es">

  <info>
    <link type="guide" xref="software#extension"/>
    <link type="seealso" xref="extensions-lockdown"/>
    <link type="seealso" xref="extensions"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>
    <credit type="author">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
   </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Activar las extensiones de GNOME Shell para todos los usuarios.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2017 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oliver Gutiérrez</mal:name>
      <mal:email>ogutsua@gmail.com</mal:email>
      <mal:years>2018 - 2020</mal:years>
    </mal:credit>
  </info>

  <title>Activar las extensiones para todo el equipo</title>
  
  <p>Para hacer que las extensiones estén disponibles para todos los usuarios del sistema, instálelas en la carpeta <file>/usr/share/gnome-shell/extensions</file>. Tenga en cuenta que las extensiones instaladas recientemente están desactivadas de manera predeterminada.</p>

  <p>Necesita establecer la clave <code>org.gnome.shell.enabled-extensions</code> para configurar las extensiones activas predeterminadas. Sin embargo, actualmente no hay manera de activar extensiones adicionales para los usuarios que ya hayan iniciado sesión. Esto no se aplica a usuarios existentes que hayan instalado y activado sus propias extensiones de GNOME.</p>

  <steps>
    <title>Configurar la clave org.gnome.shell.enabled-extensions</title>
    <item>
      <p>Cree un perfil <code>user</code> en <file>/etc/dconf/profile/user</file>:</p>
      <listing>
        <code>
user-db:user
system-db:local
</code>
      </listing>
    </item>
    <item>
      <p>Cree una base de datos <code>local</code> para opciones a nivel del equipo en <file>/etc/dconf/db/local.d/00-extensions</file>:</p>
      <listing>
        <code>
[org/gnome/shell]
# List all extensions that you want to have enabled for all users
enabled-extensions=['<input>myextension1@myname.example.com</input>', '<input>myextension2@myname.example.com</input>']
</code>
      </listing>
      <p>La clave <code>enabled-extensions</code> especifica las extensiones activas usando el UUID de las extensiones (<code>myextension1@myname.example.com</code> y <code>myextension2@myname.example.com</code>).</p>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  </steps>

</page>
