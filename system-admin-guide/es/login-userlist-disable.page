<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="login-userlist-disable" xml:lang="es">

  <info>
    <link type="guide" xref="login#appearance"/>
    <revision pkgversion="3.11" date="2014-01-29" status="draft"/>
    <revision pkgversion="3.14" date="2014-06-17" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Hacer que los usuarios escriban su nombre de usuario en la pantalla de inicio de sesión.</desc>
   
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2017 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oliver Gutiérrez</mal:name>
      <mal:email>ogutsua@gmail.com</mal:email>
      <mal:years>2018 - 2020</mal:years>
    </mal:credit>
  </info>

  <title>Desactivar la lista de usuarios</title>

  <p>Puede desactivar la lista de usuarios mostrados en la pantalla de inicio de sesión modificando la clave <sys>org.gnome.login-screen.disable-user-list</sys>.</p>
  <p>Cuando la lista de usuarios está desactivada, los usuarios deberán escribir su nombre de usuario y contraseña en la pantalla para iniciar sesión</p>
  <steps>
    <title>Establecer la clave org.gnome.login-screen.disable-user-list</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-gdm'])"/>
    <item>
      <p>Cree una base de datos <sys>gdm</sys> para opciones que apliquen a todo el sistema en <file>/etc/dconf/db/gdm.d/00-login-screen</file>:</p>
        <code>[org/gnome/login-screen]
# Do not show the user list
disable-user-list=true
</code>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  </steps>

</page>
